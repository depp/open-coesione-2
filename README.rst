Open Coesione rel. 2
====================

Open Coesione is the official government portal that shows how all EU funds are
allocated and spent in Italy.

See `docs/` for sphinx-generated documentation, where install, deploy, tests and
operations are described.

License and Authors
-------------------
See LICENSE.txt for the license this software is released under.
See authors in CONTRIBUTORS.txt
