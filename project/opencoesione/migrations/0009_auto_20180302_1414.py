# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-03-02 13:14
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('opencoesione', '0008_auto_20180301_1543'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='faq',
            name='slug_en',
        ),
        migrations.RemoveField(
            model_name='faq',
            name='slug_it',
        ),
        migrations.RemoveField(
            model_name='news',
            name='slug_en',
        ),
        migrations.RemoveField(
            model_name='news',
            name='slug_it',
        ),
        migrations.RemoveField(
            model_name='pillola',
            name='slug_en',
        ),
        migrations.RemoveField(
            model_name='pillola',
            name='slug_it',
        ),
    ]
