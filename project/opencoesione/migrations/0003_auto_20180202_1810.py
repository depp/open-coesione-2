# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-02-02 17:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('opencoesione', '0002_auto_20180202_1743'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pillola',
            name='description',
            field=models.TextField(blank=True, null=True, verbose_name='descrizione'),
        ),
        migrations.AlterField(
            model_name='pillola',
            name='description_en',
            field=models.TextField(blank=True, null=True, verbose_name='descrizione'),
        ),
        migrations.AlterField(
            model_name='pillola',
            name='description_it',
            field=models.TextField(blank=True, null=True, verbose_name='descrizione'),
        ),
    ]
