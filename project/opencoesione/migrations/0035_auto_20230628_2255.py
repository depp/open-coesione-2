# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2023-06-28 20:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('opencoesione', '0034_auto_20230627_1832'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectstory',
            name='milestones_image',
        ),
        migrations.RemoveField(
            model_name='projectstory',
            name='milestones_image_en',
        ),
        migrations.RemoveField(
            model_name='projectstory',
            name='milestones_image_it',
        ),
        migrations.AddField(
            model_name='projectstory',
            name='progetto_url_data',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='codice locale del progetto collegato o query string di ricerca (per più progetti)'),
        ),
        migrations.AddField(
            model_name='projectstorytext',
            name='description_minrows',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='numero di righe visibili della descrizione'),
        ),
    ]
