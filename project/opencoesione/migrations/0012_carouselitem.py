# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-03-16 17:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('opencoesione', '0011_auto_20180316_1156'),
    ]

    operations = [
        migrations.CreateModel(
            name='CarouselItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='testo')),
                ('text_it', models.TextField(null=True, verbose_name='testo')),
                ('text_en', models.TextField(null=True, verbose_name='testo')),
                ('image', models.ImageField(max_length=200, upload_to='immagini/', verbose_name='immagine')),
                ('image_it', models.ImageField(max_length=200, null=True, upload_to='immagini/', verbose_name='immagine')),
                ('image_en', models.ImageField(max_length=200, null=True, upload_to='immagini/', verbose_name='immagine')),
                ('priority', models.PositiveSmallIntegerField(default=0, verbose_name='priorità')),
                ('visible', models.BooleanField(default=True, verbose_name='visibile')),
            ],
            options={
                'verbose_name': 'elemento carousel',
                'ordering': ['priority'],
                'verbose_name_plural': 'elementi carousel',
            },
        ),
    ]
