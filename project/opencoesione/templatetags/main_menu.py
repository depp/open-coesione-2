# -*- coding: utf-8 -*-
from django import template
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _, pgettext_lazy

from ..monitoraggio.gruppo_programmi import Config
from ..monitoraggio.models import (
    ClassificazioneAzione, Ruolo, TemaSintetico, Territorio, TemaSinteticoCoesione, TerritorioCoesione, Focus,
    StrategiaS3, Area,
)

register = template.Library()


@register.inclusion_tag('templatetags/main_menu.html', takes_context=True)
def main_menu(context, current_section=None, focus_version=False):
    # territori_tipi = (Territorio.TIPO.R, Territorio.TIPO.M, Territorio.TIPO.N)
    # territori = [{'name': str(o).title(), 'url': o.get_absolute_url()} for o in Territorio.objects.filter(tipo__in=territori_tipi).order_by(Case(*[When(tipo=tipo, then=Value(i)) for i, tipo in enumerate(territori_tipi)], default=None).asc(), 'denominazione')] + [{'name': _('Ambito Estero'), 'url': reverse('territorioestero_list')}],

    programmi = Config().get_lista_programmi
    for idx, programmi_pac_0713_x in enumerate(programmi['pac_0713_x']):
        programmi['pac_0713_x'][idx] = [{'descrizione': '{} {}'.format(_('Progetti PAC nel'), p.descrizione), 'get_absolute_url': '{}?q=&{}&selected_facets=fonte:PAC0713'.format(reverse('progetto_search'), p.get_search_filter_querystring())} for p in programmi_pac_0713_x]

    focus = Focus.objects.focus()

    menus = context['MENUS']

    main_menu = {
        'coesione': menus['mainmenu1'][:3] + [
            {
                'name': _('I territori della coesione'),
                'item_list': [{'name': str(o).title(), 'url': o.get_absolute_url()} for o in TerritorioCoesione.objects.all()],
            },
            {
                'name': _('I temi della coesione'),
                'item_list': [{'name': o.descrizione_breve, 'url': o.get_absolute_url()} for o in TemaSinteticoCoesione.objects.all()],
            },
            {
                'name': _('I cicli della programmazione'),
                'item_list': [
                    {
                        'name': '2021-2027',
                        'item_list': menus['mainmenu1d'],
                    },
                    {
                        'name': '2014-2020',
                        'item_list': menus['mainmenu1c'],
                    },
                    {
                        'name': '2007-2013',
                        'item_list': menus['mainmenu1b'],
                    },
                ] + menus['mainmenu1a'],
            },
        ] + menus['mainmenu1'][3:],
        'approfondimento': menus['mainmenu2'],
        'dati': [
            {
                'name': pgettext_lazy('main menu', 'Naviga i dati'),
                'url': reverse('globale'),
            },
            {
                'name': _('Progetti'),
                'item_list': [
                    {
                        'name': _('Natura'),
                        'item_list': [{'name': o.descrizione_breve, 'url': o.get_absolute_url()} for o in ClassificazioneAzione.objects.principali()],
                    },
                    {
                        'name': _('Temi'),
                        'item_list': [{'name': o.descrizione_breve, 'url': o.get_absolute_url()} for o in TemaSintetico.objects.all()],
                    },
                    {
                        'name': _('Territori'),
                        'item_list': [{'name': str(o).title(), 'url': o.get_absolute_url()} for o in Territorio.objects.filter(tipo__in=(Territorio.TIPO.M, Territorio.TIPO.R)).order_by('tipo', 'denominazione')] + [{'name': _('Ambito Estero'), 'url': reverse('territorioestero_list')}],
                    },
                ],
            },
            {
                'name': _('Programmi'),
                'programmi': programmi,
            },
            {
                'name': _('Strategie'),
                'item_list': [{'name': o.descrizione, 'url': o.get_absolute_url()} for o in list(Area.objects.tipi_aree()) + list(StrategiaS3.objects.filter(tipo=StrategiaS3.TIPO.strategia))],
            },
            {
                'name': _('Soggetti'),
                'item_list': [{'name': {'1': _('Programmatori'), '2': _('Attuatori'), '3': _('Beneficiari'), '4': _('Realizzatori')}[x[0]], 'url': '{}?q=&selected_facets=ruolo:{}'.format(reverse('soggetto_search'), x[0])} for x in Ruolo.RUOLO],
            },
            {
                'name': pgettext_lazy('main menu', 'Focus'),
                'item_list': [x for x in menus['mainmenu3'] if '/focus/' in x['url']] + [
                    {
                        'name': _('Focus Strategie'),
                        'item_list': [{'name': str(o), 'url': o.get_absolute_url()} for o in focus if
                                      o.tipo_focus == Focus.TIPO_FOCUS.S],
                    },
                    {
                        'name': _('Focus Policy'),
                        'item_list': [{'name': str(o), 'url': o.get_absolute_url()} for o in focus if
                                      o.tipo_focus == Focus.TIPO_FOCUS.P],
                    },
                ],
            },
            {
                'name': _('Scarica gli opendata'),
                'item_list': [x for x in menus['mainmenu3'] if '/focus/' not in x['url']],
            },
        ],
        'monitoraggio': menus['mainmenu4'],
        'riuso': [
            {
                'name': _('Riuso'),
                'item_list': menus['mainmenu5a'],
            },
        ] + menus['mainmenu5'],
    }

    if not current_section:
        request = context['request']

        if request.resolver_match and  request.resolver_match.view_name == 'home':
            current_section = 'homepage'
        else:
            request_path = request.path.strip('/')
            if request_path in (x['url'].strip('/') for x in menus['mainmenu1'] + menus['mainmenu1a'] + menus['mainmenu1b'] + menus['mainmenu1c'] + menus['mainmenu1d']):
                current_section = 'coesione'
            elif request_path in (x['url'].strip('/') for x in menus['mainmenu2']):
                current_section = 'approfondimento'
            elif request_path in (x['url'].strip('/') for x in menus['mainmenu3']):
                current_section = 'dati'
            elif request_path in (x['url'].strip('/') for x in menus['mainmenu4']):
                current_section = 'monitoraggio'
            elif request_path in (x['url'].strip('/') for x in menus['mainmenu5'] + menus['mainmenu5a']):
                current_section = 'riuso'

    return {
        'main_menu': main_menu,
        'current_section': current_section,
        'focus_version': focus_version,
        'query': context.get('query'),
    }
