# -*- coding: utf-8 -*-
from django import template

register = template.Library()


@register.filter
def getkey(dictionary, key):
    try:
        return dictionary.get(key)
    except:
        return None
