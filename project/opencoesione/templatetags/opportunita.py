# -*- coding: utf-8 -*-
import csv
import datetime
import decimal
from collections import OrderedDict
from django import template
from django.conf import settings
from ..opendata.models import Dataset, MetadataFile

register = template.Library()


@register.inclusion_tag('templatetags/opportunita.html', takes_context=True)
def opportunita(context):
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')

    today = datetime.date.today().strftime('%Y%m%d')

    data_file = Dataset.objects.get(slug='opportunita')

    reader = csv.DictReader(open(data_file.file.path, 'r', encoding='utf-8-sig'), delimiter=';')

    opportunita = OrderedDict([(False, []), (True, [])])

    for row in sorted(reader, key=lambda x: (x['DATA_SCADENZA'] or '@', x['DATA_PUBBLICAZIONE'])):
        is_expired = bool(row['DATA_SCADENZA'] and (row['DATA_SCADENZA'] < today))

        for c in ('DATA_SCADENZA', 'DATA_PUBBLICAZIONE'):
            row[c] = datetime.datetime.strptime(row[c], '%Y%m%d') if row[c] else ''
        row['IMPORTO'] = decimal.Decimal(row['IMPORTO'].replace('.', '').replace(',', '.')) if row['IMPORTO'] else ''

        opportunita[is_expired].append(row)

    return {
        'opportunita': opportunita,
        'data_file': data_file,
        'metadata_file': MetadataFile.objects.get(slug='opportunita'),
        sezikai_ctx_var: context[sezikai_ctx_var],
    }
