# -*- coding: utf-8 -*-
from django.utils.formats import date_format
import xlrd
from django import template
from django.conf import settings
from django.utils.translation import get_language
from ..models import DataFile

register = template.Library()


@register.inclusion_tag('templatetags/indicatori_accesso.html', takes_context=True)
def indicatori_accesso(context):
    def value_format(cell):
        ctype = cell.ctype
        value = cell.value
        if ctype == xlrd.XL_CELL_DATE:
            return date_format(xlrd.xldate_as_datetime(value, workbook.datemode), 'd/m/Y')
        else:
            return value

    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')

    extra_contents = [x.strip() for x in context['flatpage'].extra_content.split('<hr />')]

    workbook = xlrd.open_workbook(DataFile.objects.get(slug='indicatori_accesso_data').file.path)

    header_row = 1 if get_language() == 'it' else 0

    return {
        'indicators': [{
            'data': [[value_format(sheet.cell(i, j)) for j in range(sheet.ncols)] for i in [header_row] + list(range(2, sheet.nrows))],
            'text': extra_contents[idx],
        } for idx, sheet in enumerate(workbook.sheets())],
        sezikai_ctx_var: context[sezikai_ctx_var],
    }
