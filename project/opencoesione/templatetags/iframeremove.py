# -*- coding: utf-8 -*-
import re
from bs4 import BeautifulSoup
from django import template

register = template.Library()


@register.filter
def iframeremove(html):
    if html is None:
        return ''

    services = {
        'www.youtube.com/embed/': 'youtube',
        'www.slideshare.net/slideshow/embed_code/key/': 'slideshare',
        'e.infogram.com/': 'infogram',
        'www.facebook.com/plugins/video.php': 'facebook',
    }

    pattern = '(https?:)?//({})'.format('|'.join(services.keys()))

    soup = BeautifulSoup(html, 'html.parser')

    for iframe_tag in soup.find_all('iframe'):
        m = re.match(pattern, iframe_tag['src'])
        if m:
            params = iframe_tag['src'].replace(m[0], '').split('?', 1)

            div_tag = soup.new_tag('div')
            div_tag['data-service'] = services[m[2]]
            div_tag['data-id'] = params[0]
            if len(params) > 1:
                div_tag['data-params'] = params[1]

            styles = []
            for attr in ('width', 'height'):
                if attr in iframe_tag.attrs:
                    styles.append('{}: {}px;'.format(attr, iframe_tag[attr]))
                    div_tag['data-frame-{}'.format(attr)] = iframe_tag[attr]

            if styles:
                div_tag['style'] = ' '.join(styles)

            iframe_tag.replace_with(div_tag)

    return soup.decode()
