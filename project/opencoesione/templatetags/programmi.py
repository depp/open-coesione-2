# -*- coding: utf-8 -*-
import csv
import decimal
from django import template
from django.conf import settings
from django.utils import translation

from ..models import DataFile

register = template.Library()


def parse_programmidatafile(ciclo, extra=''):
    try:
        data_file = DataFile.objects.get(slug='programmi{}{}_data'.format(ciclo, extra or ''))
    except:
        from django.http import Http404
        raise Http404

    reader = csv.DictReader(open(data_file.file.path, 'r', encoding='utf-8-sig'), delimiter=';')

    lng = translation.get_language().upper()

    # tabs = {
    #     'IT': ['FESR', 'FSE', 'FSE+', 'FTG', 'IOG', 'CTE', 'FEASR', 'FEAMP', 'FSC', 'POC', 'SNAI-Servizi', 'PAC'],
    #     'EN': ['ERDF', 'ESF', 'ESF+', 'JTF', 'YEI', 'ETC', 'EAFRD', 'MFF', 'DCF', 'COP', 'IANS', 'CAP'],
    # }[lng]

    # programmi = OrderedDict([(x, []) for x in tabs])
    programmi = {}

    for row in reader:
        row['OC_CODICE_PROGRAMMA'] = row['OC_CODICE_PROGRAMMA'].strip()

        for lbl in ('RISORSE', 'RISORSE_UE'):
            row[lbl] = decimal.Decimal(row[lbl].replace(',', '')) if row[lbl] and row[lbl] != '0' else ''

        for lbl in ('PROGRAMMA', 'AMBITO', 'TIPO', 'DECISIONE', 'DOC', 'SITO'):
            row['LABEL_{}'.format(lbl)] = row['LABEL_{}_{}'.format(lbl, lng)]
            del row['LABEL_{}_IT'.format(lbl)]
            del row['LABEL_{}_EN'.format(lbl)]

        for lbl in ('DECISIONE', 'DOC'):
            row[lbl] = zip(row['LINK_{}'.format(lbl)].split(':::'), row['LABEL_{}'.format(lbl)].split(':::'))
            del row['LABEL_{}'.format(lbl)]
            del row['LINK_{}'.format(lbl)]

        if row['LABEL_AMBITO'] not in programmi:
            programmi[row['LABEL_AMBITO']] = []
        programmi[row['LABEL_AMBITO']].append(row)

    # return OrderedDict([(k, v) for k, v in programmi.items() if len(v)])
    return programmi


@register.inclusion_tag('templatetags/programmi.html', takes_context=True)
def programmi(context, ciclo):
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')

    return {
        'programmi_per_ambito': parse_programmidatafile(ciclo),
        sezikai_ctx_var: context[sezikai_ctx_var],
    }


@register.inclusion_tag('templatetags/programmi_ambito.html', takes_context=True)
def programmi_ambito(context, ciclo, ambito, extra):
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')

    return {
        'programmi': parse_programmidatafile(ciclo, extra).get(ambito, []),
        sezikai_ctx_var: context[sezikai_ctx_var],
    }
