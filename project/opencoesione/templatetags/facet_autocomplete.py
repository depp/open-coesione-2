# -*- coding: utf-8 -*-
import json

from django import template

register = template.Library()


@register.inclusion_tag('templatetags/facet_autocomplete.html')
def facet_autocomplete(items):
    return {
        'items': json.dumps([{'name': x['short_label'], 'query': x['urls']['add_filter']} for x in items if x['urls']['add_filter']])
    }
