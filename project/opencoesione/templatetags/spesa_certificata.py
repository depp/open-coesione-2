# -*- coding: utf-8 -*-
import csv
import datetime
import decimal
from collections import defaultdict, OrderedDict
from django import template
from django.conf import settings
from django.utils.formats import date_format
from django.utils.translation import ugettext_lazy as _
from ..models import DataFile

register = template.Library()


def parse_float(num):
    try:
        return round(decimal.Decimal(num.replace('.', '').replace(',', '.')), 2)
    except:
        return None


@register.inclusion_tag('templatetags/spesa_certificata.html', takes_context=True)
def spesa_certificata(context):
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')

    reader = csv.DictReader(open(DataFile.objects.get(slug='spesacertificata_data').file.path, 'r'), delimiter=';')

    return {
        'spesacertificata_data': [{
            'mese': datetime.date(int(row['Anno']), int(row['Mese_NUM']), 1),
            'spesa': parse_float(row['Spesa certificata su dotazione']),
            'pagamenti': round(100 * parse_float(row['pag ammessi']) / parse_float(row['dotazione']), 2) if row['pag ammessi'] else None,
            'obiettivo': parse_float(row['Obiettivo comunitario di spesa certificata']),
        } for row in reader if row['Anno']],
        sezikai_ctx_var: context[sezikai_ctx_var],
    }


@register.inclusion_tag('templatetags/spesa_certificata_grafici.html', takes_context=True)
def spesa_certificata_grafici(context):
    def name_format(value):
        name_map = {
            'PON GOVERNANCE E AZIONI DI SISTEMA': 'Pon GAS',
            'PON COMPETENZE PER LO SVILUPPO': 'Pon Istruzione',
            'POIN ATTRATTORI CULTURALI, NATURALI E TURISMO': 'Poin Attrattori',
            'POIN ENERGIE RINNOVABILI E RISPARMIO ENERGETICO': 'Poin Energie',
            'PON GOVERNANCE E ASSISTENZA TECNICA': 'Pon GAT',
            "PON ISTRUZIONE - AMBIENTI PER L'APPRENDIMENTO": 'Pon Istruzione',
            'PON RETI E MOBILITÀ': 'Pon Reti',
            'PON RICERCA E COMPETITIVITÀ': 'Pon Ricerca',
            'PON AZIONI DI SISTEMA': 'Pon AS',
        }
        value = ' '.join(x for x in value.split() if x not in ['CRO', 'CONV', 'FSE', 'FESR'])
        try:
            value = name_map[value]
        except:
            value = value.title()
            value = value.replace(' Pa ', ' PA ')
            value = value.replace("D'", "d'")
        return value

    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')

    dates = ['{}{:02d}31'.format(y, m) for y in range(2010, 2016) for m in [5, 10, 12]] + ['20160731', '20161231', '20170331']

    csv2data_key_map = (
        ('TARGET {}', 'target'),
        ('TARGET NAZIONALE {}', 'target'),
        ('TARGET UE {}', 'target'),
        ('STIMA TARGET {}', 'target'),
        ('quota spesa certificata {}', 'risultato_spesa'),
        ('quota pagamenti ammessi {}', 'risultato_pagamenti'),
    )

    reader = csv.DictReader(open(DataFile.objects.get(slug='pagamenti_ammessi_data').file.path, 'r'), delimiter=';')

    data = defaultdict(list)
    for row in reader:
        if row['OC_CODICE_PROGRAMMA']:
            program_name = name_format(row['OC_DESCRIZIONE_PROGRAMMA'])
            group_key = '{}_{}'.format(row['QSN_AREA_OBIETTIVO_UE'], row['QSN_FONDO_COMUNITARIO '])

            dates_data = []
            for date in dates:
                date_data = {}

                for csv_key, data_key in csv2data_key_map:
                    csv_key = csv_key.format(date)
                    if csv_key in row:
                        date_data[data_key] = row[csv_key]

                if date_data:
                    dates_data.append((date_format(datetime.datetime.strptime(date, '%Y%m%d'), 'd/m/Y'), date_data))

            for type_name, type_key in [(_('Obiettivo di spesa certificata'), 'target'), (_('Spesa certificata su dotazione'), 'risultato_spesa'), (_('Pagamenti su dotazione'), 'risultato_pagamenti')]:
                data[group_key].append(OrderedDict([(_('Programma operativo'), program_name), (_('Tipo dato'), type_name)] + [(date, parse_float(date_data.get(type_key))) for date, date_data in dates_data]))

    return {
        'grouped_data': [({'CRO': _('Competitività'), 'CONV': _('Convergenza')}[type], fund, data['{}_{}'.format(type, fund)]) for type in ('CRO', 'CONV') for fund in ('FESR', 'FSE')],
        sezikai_ctx_var: context[sezikai_ctx_var],
    }
