# -*- coding: utf-8 -*-
from django import template
from django.conf import settings
from ..models import DataFile
from ..monitoraggio.models import MonitoraggioASOC

register = template.Library()


@register.inclusion_tag('templatetags/monitoraggi_asoc.html', takes_context=True)
def monitoraggi_asoc(context):
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')

    return {
        # 'monitoraggi_asoc': sorted(MonitoraggioASOC.objects.select_related('progetto', 'istituto_comune').order_by('-edizione_asoc'), key=lambda x: (x.istituto_regione.denominazione, x.istituto_provincia.denominazione, x.istituto_comune.denominazione, x.istituto_nome)),
        'monitoraggi_asoc': MonitoraggioASOC.objects.select_related('progetto', 'istituto_comune').order_by('-edizione_asoc'),
        'data_file': DataFile.objects.get(slug='monitoraggi_asoc_data'),
        'metadata_file': DataFile.objects.get(slug='monitoraggi_asoc_metadata'),
        sezikai_ctx_var: context[sezikai_ctx_var],
    }
