# -*- coding: utf-8 -*-
from django import template
from django.conf import settings
from ..models import DataFile

register = template.Library()


@register.inclusion_tag('templatetags/risorse0713.html', takes_context=True)
def risorse0713(context):
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')

    return {
        'data_file': DataFile.objects.get(slug='risorse0713_data'),
        sezikai_ctx_var: context[sezikai_ctx_var],
    }


@register.inclusion_tag('templatetags/risorse1420.html', takes_context=True)
def risorse1420(context):
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')

    return {
        'data_file': DataFile.objects.get(slug='risorse1420_data'),
        sezikai_ctx_var: context[sezikai_ctx_var],
    }
