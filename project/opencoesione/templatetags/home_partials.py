# -*- coding: utf-8 -*-
from django import template

register = template.Library()


@register.inclusion_tag('templatetags/home_partials/highlights.html')
def highlights():
    from ..models import CarouselItem
    return {
        'objects': CarouselItem.objects.filter(visible=True),
    }


@register.inclusion_tag('templatetags/home_partials/dati_coesione.html')
def dati_coesione(request):
    from ..monitoraggio.views.coesione import get_aggregatedata_from_api, get_totals_from_data
    return get_totals_from_data(get_aggregatedata_from_api('api-aggregati-globale', request=request))


@register.inclusion_tag('templatetags/home_partials/temi_sintetici.html')
def temi_sintetici():
    from ..monitoraggio.models import TemaSinteticoCoesione
    return {
        'objects': TemaSinteticoCoesione.objects.all(),
    }


@register.inclusion_tag('templatetags/home_partials/faq.html')
def faq():
    from ..models import FAQ
    return {
        'objects': FAQ.objects.highlights(),
    }


@register.inclusion_tag('templatetags/home_partials/territori.html', takes_context=True)
def territori(context):
    from django.conf import settings
    from ..monitoraggio.models import TerritorioCoesione
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')
    return {
        'objects_by_type': {
            'macroaree': TerritorioCoesione.objects.macroaree(),
            'regioni': TerritorioCoesione.objects.regioni(),
        },
        sezikai_ctx_var: context[sezikai_ctx_var],
    }


@register.inclusion_tag('templatetags/home_partials/storie_territorio.html')
def storie_territorio(tema=None, territorio=None):
    from django.utils.timezone import now
    from django.utils.translation import ugettext_lazy as _
    from ..models import StoriaTerritorio, StoriaTerritorioTemaSinteticoCoesione, StoriaTerritorioTerritorioCoesione

    if tema:
        queryset = StoriaTerritorioTemaSinteticoCoesione.objects.filter(tema_sintetico_coesione=tema).prefetch_related('storia_territorio__content_object')
    elif territorio:
        queryset = StoriaTerritorioTerritorioCoesione.objects.filter(territorio_coesione=territorio).prefetch_related('storia_territorio__content_object')
    else:
        queryset = StoriaTerritorio.objects.prefetch_related('content_object')

    objects = []
    for obj in queryset:
        if isinstance(obj, StoriaTerritorio):
            object = obj.content_object
        else:
            object = obj.storia_territorio.content_object

        if object.published_at <= now().date():
            object.is_pinned = obj.is_pinned
            objects.append(object)

    objects = sorted(objects, key=lambda x: (x.is_pinned, x.published_at), reverse=True)[:3]

    type_id2name = {'asocstory': _('ASOC stories'), 'pillola': _('Pillole / Data card'), 'projectstory': _('Storie di progetto'), 'videostory': _('Video racconti')}
    for object in objects:
        object.type_id = object._meta.model_name
        object.type_name = type_id2name.get(object.type_id, object.type_id.title())

    return {
        'objects': objects,
    }


@register.inclusion_tag('templatetags/home_partials/video_tutorial.html')
def video_tutorial():
    from ..models import VideoTutorial
    return {
        'objects': VideoTutorial.objects.all()[:3],
    }


@register.inclusion_tag('templatetags/home_partials/opportunita_finanziamento.html')
def opportunita_finanziamento():
    return {}


@register.inclusion_tag('templatetags/home_partials/asoc.html', takes_context=True)
def asoc(context):
    from django.conf import settings
    from django.utils import translation
    from ..models import ASOCStory
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')
    return {
        'objects': ASOCStory.objects.with_video(),
        'LANGUAGE_CODE': translation.get_language(),
        sezikai_ctx_var: context[sezikai_ctx_var],
    }
