# -*- coding: utf-8 -*-
from copy import deepcopy
from django.db import models
from django.utils.translation import get_language
from haystack import signals, query

from .search_indexes import SEARCH_MODELS as CONTENTS_SEARCH_MODELS
from .opendata.search_indexes import SEARCH_MODELS as OPENDATA_SEARCH_MODELS

SEARCH_MODELS = CONTENTS_SEARCH_MODELS + OPENDATA_SEARCH_MODELS


class MyRealtimeSignalProcessor(signals.BaseSignalProcessor):
    def handle_save(self, sender, instance, **kwargs):
        new_instance = deepcopy(instance)
        if sender._meta.proxy:
            sender = sender._meta.concrete_model
            new_instance.__class__ = sender
        super().handle_save(sender, new_instance, **kwargs)

    def handle_delete(self, sender, instance, **kwargs):
        new_instance = deepcopy(instance)
        if sender._meta.proxy:
            sender = sender._meta.concrete_model
            new_instance.__class__ = sender
        super().handle_delete(sender, new_instance, **kwargs)

    def setup(self):
        for model in SEARCH_MODELS:
            models.signals.post_save.connect(self.handle_save, sender=model)
            models.signals.post_delete.connect(self.handle_delete, sender=model)

    def teardown(self):
        for model in SEARCH_MODELS:
            models.signals.post_save.disconnect(self.handle_save, sender=model)
            models.signals.post_delete.disconnect(self.handle_delete, sender=model)


class ModeltranslationSearchQuerySet(query.SearchQuerySet):
    def filter(self, *args, **kwargs):
        if 'content' in kwargs:
            kwargs['text_{}'.format(get_language())] = kwargs.pop('content')

        return super().filter(*args, **kwargs)
