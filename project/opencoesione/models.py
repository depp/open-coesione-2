# -*- coding: utf-8 -*-
import os
import re
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.flatpages.models import FlatPage
from django.urls import reverse
from django.db import models
from django.dispatch import receiver
from django_extensions.db.fields import CreationDateTimeField, ModificationDateTimeField
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from embed_video.fields import EmbedVideoField

from ..tagging import models as tagging_models


FlatPage.add_to_class('extra_content', models.TextField('contenuto aggiuntivo', blank=True))


class ArticoloQuerySet(models.QuerySet):
    def visibili(self):
        return self.filter(published_at__lte=now())


class FAQQuerySet(models.QuerySet):
    def highlights(self):
        return self.filter(highlight=True)


class ASOCStoryQuerySet(ArticoloQuerySet):
    def with_video(self):
        return self.exclude(models.Q(video='') | models.Q(video__isnull=True))


class CommunicationContentQuerySet(models.QuerySet):
    def visibili(self):
        return self.filter(status='1')


class StoriaTerritorioTemaSinteticoCoesione(models.Model):
    storia_territorio = models.ForeignKey('StoriaTerritorio', verbose_name='storia del territorio', on_delete=models.CASCADE)
    tema_sintetico_coesione = models.ForeignKey('monitoraggio.TemaSinteticoCoesione', verbose_name='tema della coesione', on_delete=models.CASCADE)

    is_pinned = models.BooleanField(default=False, verbose_name='bloccato su questo tema')

    def __str__(self):
        return str(self.tema_sintetico_coesione)

    class Meta:
        verbose_name = 'tema della coesione per storia del territorio'
        verbose_name_plural = 'temi della coesione per storia del territorio'
        unique_together = ('storia_territorio', 'tema_sintetico_coesione')


class StoriaTerritorioTerritorioCoesione(models.Model):
    storia_territorio = models.ForeignKey('StoriaTerritorio', verbose_name='storia del territorio', on_delete=models.CASCADE)
    territorio_coesione = models.ForeignKey('monitoraggio.TerritorioCoesione', verbose_name='territorio della coesione', on_delete=models.CASCADE)

    is_pinned = models.BooleanField(default=False, verbose_name='bloccato su questo territorio')

    def __str__(self):
        return str(self.territorio_coesione)

    class Meta:
        verbose_name = 'territorio della coesione per storia del territorio'
        verbose_name_plural = 'territori della coesione per storia del territorio'
        unique_together = ('storia_territorio', 'territorio_coesione')


class StoriaTerritorio(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.CharField(max_length=255)
    content_object = GenericForeignKey(for_concrete_model=False)

    temi = models.ManyToManyField('monitoraggio.TemaSinteticoCoesione', through=StoriaTerritorioTemaSinteticoCoesione, related_name='storie_territorio', blank=True)
    territori = models.ManyToManyField('monitoraggio.TerritorioCoesione', through=StoriaTerritorioTerritorioCoesione, related_name='storie_territorio', blank=True)
    is_pinned = models.BooleanField(default=False, verbose_name='bloccato in homepage')

    def __str__(self):
        return str(self.content_object)

    class Meta:
        verbose_name = 'storia del territorio'
        verbose_name_plural = 'storie del territorio'
        unique_together = ('content_type', 'object_id')


class StoriaTerritorioMixin(models.Model):
    storie_territorio = GenericRelation(StoriaTerritorio, for_concrete_model=False)

    # def save(self, *args, **kwargs):
    #     is_new = self.pk is None
    #
    #     super().save(*args, **kwargs)
    #
    #     if is_new and not self.storie_territorio:
    #         StoriaTerritorio.objects.create(
    #             content_type=ContentType.objects.get_for_model(self.__class__, for_concrete_model=False),
    #             object_id=self.pk,
    #         )

    @property
    def storia_territorio(self):
        return self.storie_territorio.first()

    class Meta:
        abstract = True


class BaseResource(models.Model):
    CATEGORY_CHOICES = (
        ('1', _('Documenti di programmazione')),
        ('2', _('Altro')),
    )

    content_type = models.ForeignKey(ContentType)
    object_id = models.CharField(max_length=255)
    content_object = GenericForeignKey()

    description = models.CharField(max_length=255, verbose_name='descrizione')
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=1, verbose_name='categoria', blank=True, null=True)
    priority = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')

    def __str__(self):
        return self.description

    class Meta:
        abstract = True
        ordering = ['category', 'priority', 'description']


class File(BaseResource):
    def upload_to(self, filename):
        return 'files/{}/{}'.format(slugify('{} {}'.format(self.content_type, self.object_id)), filename)

    file = models.FileField(max_length=255, upload_to=upload_to)

    @property
    def url(self):
        return self.file.url


class Link(BaseResource):
    url = models.URLField(max_length=255, verbose_name='URL')


class MenuItem(models.Model):
    MENU_CHOICES = (
        ('mainmenu1', 'Main menu / La coesione'),
        ('mainmenu1a', 'Main menu / La coesione / I cicli di programmazione / 2000-2006'),
        ('mainmenu1b', 'Main menu / La coesione / I cicli di programmazione / 2007-2013'),
        ('mainmenu1c', 'Main menu / La coesione / I cicli di programmazione / 2014-2020'),
        ('mainmenu1d', 'Main menu / La coesione / I cicli di programmazione / 2021-2027'),
        ('mainmenu2', 'Main menu / Le storie'),
        ('mainmenu3', 'Main menu / I dati'),
        ('mainmenu4', 'Main menu / Il monitoraggio civico'),
        ('mainmenu5', 'Main menu / Riuso e pubblicazioni'),
        ('mainmenu5a', 'Main menu / Riuso e pubblicazioni / Riuso'),
        ('header', 'Header'),
        ('footer1', 'Footer / OpenCoesione'),
        ('footer2', 'Footer / Materiali'),
    )

    description = models.CharField(max_length=50, verbose_name='descrizione')
    menu = models.CharField(choices=MENU_CHOICES, max_length=20, verbose_name='nome menu')
    priority = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')
    page = models.ForeignKey(FlatPage, verbose_name='pagina', on_delete=models.CASCADE)

    @property
    def url(self):
        return reverse('flatpage', kwargs={'url': self.page.url.lstrip('/')})

    @property
    def identifier(self):
        return self.page.url.strip('/')

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = 'voce menu'
        verbose_name_plural = 'voci menu'
        ordering = ['menu', 'priority']


class CarouselItem(models.Model):
    overtitle = models.CharField(max_length=200, verbose_name='occhiello', blank=True, null=True)
    title = models.CharField(max_length=200, verbose_name='titolo')
    text = models.TextField(verbose_name='testo', blank=True, null=True)
    link = models.CharField(max_length=200, verbose_name='link', blank=True, null=True)
    image = models.ImageField(max_length=200, upload_to='immagini/carousel/', verbose_name='immagine', blank=True, null=True)
    priority = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')
    visible = models.BooleanField(default=True, verbose_name='visibile')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'elemento carousel'
        verbose_name_plural = 'elementi carousel'
        ordering = ['priority', '-pk']


class Pillola(tagging_models.TagMixin, StoriaTerritorioMixin, models.Model):
    title = models.CharField(max_length=200, verbose_name='titolo')
    slug = models.SlugField(max_length=255, unique=True)
    abstract = models.TextField(max_length=1024, verbose_name='descrizione breve', blank=True, null=True)
    description = models.TextField(verbose_name='descrizione', blank=True, null=True)
    image = models.ImageField(max_length=200, upload_to='immagini/pillole/', verbose_name='immagine', blank=True, null=True)
    published_at = models.DateField(default=now, verbose_name='data di pubblicazione')
    documents = GenericRelation(File, verbose_name='documenti')

    objects = ArticoloQuerySet.as_manager()

    def get_absolute_url(self):
        return reverse('pillola_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('pillola')
        verbose_name_plural = _('pillole')
        ordering = ['-published_at', '-id']


class BaseVideo(models.Model):
    title = models.CharField(max_length=200, verbose_name='titolo')
    slug = models.SlugField(max_length=255, unique=True)
    abstract = models.TextField(max_length=1024, verbose_name='descrizione breve', blank=True, null=True)
    description = models.TextField(verbose_name='descrizione', blank=True, null=True)
    video = EmbedVideoField()
    published_at = models.DateField(default=now, verbose_name='data di pubblicazione')

    def __str__(self):
        return self.title

    class Meta:
        abstract = True
        ordering = ['-published_at', '-id']


class ASOCStory(StoriaTerritorioMixin, models.Model):
    title = models.CharField(max_length=200, verbose_name='titolo')
    slug = models.SlugField(max_length=255, unique=True)
    abstract = models.TextField(max_length=1024, verbose_name='descrizione breve', blank=True, null=True)
    description = models.TextField(verbose_name='descrizione', blank=True, null=True)
    image = models.ImageField(max_length=200, upload_to='immagini/asoc/', verbose_name='immagine', blank=True, null=True)
    video = EmbedVideoField(blank=True, null=True)
    progetto_codice_locale = models.CharField(max_length=100, verbose_name='codice locale del progetto collegato', blank=True, null=True)
    asoc_url = models.URLField(max_length=255, verbose_name='pagina ASOC del progetto', blank=True, null=True)
    published_at = models.DateField(default=now, verbose_name='data di pubblicazione')

    objects = ASOCStoryQuerySet.as_manager()

    @cached_property
    def progetto(self):
        if self.progetto_codice_locale:
            from .monitoraggio.models import Progetto
            try:
                return Progetto.objects.get(codice_locale=self.progetto_codice_locale)
            except Progetto.DoesNotExist:
                pass
        return None

    def clean_fields(self, exclude=None):
        from django.core.exceptions import ValidationError

        super().clean_fields(exclude=exclude)

        if self.progetto_codice_locale and not self.progetto:
            raise ValidationError({'progetto_codice_locale': 'Il codice locale inserito non corrisponde a nessun progetto'})

    def get_absolute_url(self):
        return reverse('asocstory_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('ASOC story')
        verbose_name_plural = _('ASOC stories')
        ordering = ['-published_at', '-id']


class ProjectStory(StoriaTerritorioMixin, models.Model):
    title = models.CharField(max_length=200, verbose_name='titolo')
    slug = models.SlugField(max_length=255, unique=True)
    abstract = models.TextField(max_length=1024, verbose_name='descrizione breve', blank=True, null=True)
    description = models.TextField(verbose_name='descrizione', blank=True, null=True)
    image = models.ImageField(max_length=200, upload_to='immagini/storie_progetto/', verbose_name='immagine', blank=True, null=True)
    video = EmbedVideoField(blank=True, null=True)
    progetto_url_data = models.CharField(max_length=200, verbose_name='codice locale del progetto collegato o query string di ricerca (per più progetti)', blank=True, null=True)
    published_at = models.DateField(default=now, verbose_name='data di pubblicazione')

    objects = ArticoloQuerySet.as_manager()

    @cached_property
    def progetto_url(self):
        if self.progetto_url_data:
            if self.progetto_url_data[0] == '?':
                from urllib.parse import parse_qs
                if parse_qs(self.progetto_url_data[1:]):
                    return '{}{}'.format(reverse('progetto_search'), self.progetto_url_data)
            else:
                from .monitoraggio.models import Progetto
                try:
                    return Progetto.objects.get(codice_locale=self.progetto_url_data).get_absolute_url()
                except Progetto.DoesNotExist:
                    pass
        return None

    def clean_fields(self, exclude=None):
        from django.core.exceptions import ValidationError

        super().clean_fields(exclude=exclude)

        if self.progetto_url_data and not self.progetto_url:
            if self.progetto_url_data[0] == '?':
                msg = 'La query string inserita non è corretta'
            else:
                msg = 'Il codice locale inserito non corrisponde a nessun progetto'
            raise ValidationError({'progetto_url_data': msg})

    def get_absolute_url(self):
        return reverse('projectstory_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('storia di progetto')
        verbose_name_plural = _('storie di progetto')
        ordering = ['-published_at', '-id']


class ProjectStoryText(models.Model):
    project_story = models.ForeignKey(ProjectStory, related_name='texts', on_delete=models.CASCADE)

    title = models.CharField(max_length=200, verbose_name='titolo')
    description = models.TextField(verbose_name='descrizione', blank=True, null=True)
    description_minrows = models.PositiveSmallIntegerField(verbose_name='numero di righe visibili della descrizione', blank=True, null=True)
    priority = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'testo storia di progetto'
        verbose_name_plural = 'testi storia di progetto'
        ordering = ('priority', 'id')


class VideoStory(StoriaTerritorioMixin, BaseVideo):
    project_story = models.ForeignKey(ProjectStory, verbose_name='storia di progetto collegata', related_name='video_stories', null=True, blank=True, on_delete=models.SET_NULL)

    objects = ArticoloQuerySet.as_manager()

    def get_absolute_url(self):
        return reverse('videostory_detail', kwargs={'slug': self.slug})

    class Meta(BaseVideo.Meta):
        verbose_name = _('video racconto')
        verbose_name_plural = _('video racconti')


class VideoTutorial(BaseVideo):
    progetto_codice_locale = models.CharField(max_length=100, verbose_name='codice locale del progetto collegato', blank=True, null=True)

    @cached_property
    def progetto(self):
        if self.progetto_codice_locale:
            from .monitoraggio.models import Progetto
            try:
                return Progetto.objects.get(codice_locale=self.progetto_codice_locale)
            except Progetto.DoesNotExist:
                pass
        return None

    def clean_fields(self, exclude=None):
        from django.core.exceptions import ValidationError

        super().clean_fields(exclude=exclude)

        if self.progetto_codice_locale and not self.progetto:
            raise ValidationError({'progetto_codice_locale': 'Il codice locale inserito non corrisponde a nessun progetto'})

    def get_absolute_url(self):
        return reverse('videotutorial_detail', kwargs={'slug': self.slug})

    class Meta(BaseVideo.Meta):
        verbose_name = _('video tutorial')
        verbose_name_plural = _('video tutorial')


class News(tagging_models.TagMixin, models.Model):
    title = models.CharField(max_length=255, verbose_name='titolo')
    slug = models.SlugField(max_length=255, unique=True)
    body = models.TextField(verbose_name='testo')
    published_at = models.DateField(default=now, verbose_name='data di pubblicazione')

    objects = ArticoloQuerySet.as_manager()

    def get_absolute_url(self):
        return reverse('news_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('news')
        verbose_name_plural = _('news')
        ordering = ['-published_at', '-id']


class PressReview(models.Model):
    title = models.CharField(max_length=200, verbose_name='titolo')
    source = models.CharField(max_length=200, verbose_name='fonte')
    author = models.CharField(max_length=200, verbose_name='autore')
    file = models.FileField(max_length=150, upload_to='files/pressreview', blank=True, null=True)
    url = models.URLField(blank=True, null=True)
    published_at = models.DateField(default=now, verbose_name='data di pubblicazione')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'rassegna stampa'
        verbose_name_plural = 'rassegna stampa'
        ordering = ['-published_at', '-id']


class BaseItem(models.Model):
    title = models.CharField(max_length=255, verbose_name='titolo')
    body = models.TextField(verbose_name='testo')
    start_date = models.DateField(default=now, verbose_name='data di inizio')
    end_date = models.DateField(verbose_name='data di fine', blank=True, null=True)
    location = models.CharField(max_length=255, verbose_name='luogo', blank=True, null=True)
    priority = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')

    documents = GenericRelation(File, verbose_name='documenti')

    def __str__(self):
        return self.title

    class Meta:
        abstract = True
        ordering = ['-start_date', 'priority', '-pk']


class Document(BaseItem):
    class Meta(BaseItem.Meta):
        verbose_name = 'documento'
        verbose_name_plural = 'documenti'


class Event(BaseItem):
    class Meta(BaseItem.Meta):
        verbose_name = 'evento/riunione'
        verbose_name_plural = 'eventi/riunioni'


class EventVideo(BaseItem):
    class Meta(BaseItem.Meta):
        verbose_name = 'video evento/riunione'
        verbose_name_plural = 'video eventi/riunioni'


class Reuse(BaseItem):
    class Meta(BaseItem.Meta):
        verbose_name = 'esperienza di riuso'
        verbose_name_plural = 'esperienze di riuso'


class Workshop(BaseItem):
    class Meta(BaseItem.Meta):
        verbose_name = 'esperienza di laboratorio'
        verbose_name_plural = 'esperienze di laboratorio'


class FAQ(tagging_models.TagMixin, models.Model):
    domanda = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True)
    risposta = models.TextField(blank=True, null=True)
    priorita = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')
    highlight = models.BooleanField(default=False, verbose_name='in evidenza')

    objects = FAQQuerySet.as_manager()

    def get_absolute_url(self):
        return '{}#{}'.format(reverse('faq_list'), self.slug)

    def __str__(self):
        return self.domanda

    class Meta:
        verbose_name = _('domanda frequente')
        verbose_name_plural = _('domande frequenti')
        ordering = ['-priorita', 'id']


class DataFile(models.Model):
    slug = models.SlugField(unique=True, verbose_name='identificativo')
    name = models.CharField(max_length=255, verbose_name='nome')
    file = models.FileField(upload_to='opendata')
    updated_at = models.DateField(default=now, verbose_name='data di aggiornamento')
    # in_opendata = models.BooleanField(default=False)

    def get_absolute_url(self):
        return self.file.url

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'file di dati'
        verbose_name_plural = 'file di dati'
        ordering = ['slug']


class OpportunitaProgramma(models.Model):
    TIPO_CHOICES = (
        ('pr-reg', _('Programmi Regionali')),
        ('pr-naz', _('Programmi Nazionali')),
        ('cte-tit', _('Programmi CTE a titolarità italiana')),
        ('cte-par', _('Programmi CTE a partecipazione italiana')),
    )

    def upload_to(self, filename):
        return 'immagini/opportunita_finanziamento/{}/{}'.format(self.tipo.replace('-', '_'), filename)

    tipo = models.CharField(choices=TIPO_CHOICES, max_length=10)
    nome = models.CharField(max_length=100)
    immagine = models.ImageField(max_length=200, upload_to=upload_to)
    priorita = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'programma opportunità di finanziamento'
        verbose_name_plural = 'programmi opportunità di finanziamento'
        ordering = ['-tipo', 'priorita', 'pk']


class OpportunitaProgrammaGruppo(models.Model):
    opportunita_programma = models.ForeignKey(OpportunitaProgramma, verbose_name='programma', related_name='gruppi', on_delete=models.CASCADE)
    nome = models.CharField(max_length=40)
    url1 = models.URLField(max_length=255, verbose_name='URL avvisi', blank=True, null=True)
    url2 = models.URLField(max_length=255, verbose_name='URL preavvisi', blank=True, null=True)
    priorita = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'gruppo programma opportunità di finanziamento'
        verbose_name_plural = 'gruppi programmi opportunità di finanziamento'
        ordering = ['priorita', 'pk']


class CommunicationContent(models.Model):
    STATUS_CHOICES = (
        ('0', 'Da valutare'),
        ('1', 'Pubblicato'),
        ('2', 'Respinto'),
    )
    TYPE_CHOICES = (
        ('V', _('Video')),
        ('A', _('Audio')),
        ('P', _('PDF')),
        ('I', _('Immagine')),
        ('W', _('Sito web')),
        ('M', _('Multi-contenuto')),
        ('O', 'OpenCoesione'),
    )

    status = models.CharField(choices=STATUS_CHOICES, default=STATUS_CHOICES[0][0], max_length=1, verbose_name='stato', db_index=True)
    type = models.CharField(choices=TYPE_CHOICES, max_length=1, verbose_name='tipo', blank=True, null=True)
    title = models.CharField(max_length=80, verbose_name='titolo')
    description = models.TextField(max_length=200, verbose_name='descrizione', blank=True, null=True)
    url = models.URLField(max_length=255, verbose_name='URL')
    # region = models.ForeignKey('monitoraggio.Territorio', limit_choices_to={'tipo': 'R'}, verbose_name='regione', null=True, blank=True, on_delete=models.SET_NULL)
    regions = models.ManyToManyField('monitoraggio.Territorio', limit_choices_to={'tipo': 'R'}, verbose_name='regioni', blank=True)
    administration = models.CharField(max_length=100, verbose_name='amministrazione', blank=True, null=True)
    user_name = models.CharField(max_length=100, verbose_name='nome del segnalatore', blank=True, null=True)
    user_email = models.EmailField(verbose_name='email del segnalatore', blank=True, null=True)
    priority = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')
    created_at = CreationDateTimeField(verbose_name='data/ora creazione')
    updated_at = ModificationDateTimeField(verbose_name='data/ora modifica')

    objects = CommunicationContentQuerySet.as_manager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'contenuto di comunicazione'
        verbose_name_plural = 'contenuti di comunicazione'
        ordering = ['priority', '-updated_at']


class CommunicationContentReference(models.Model):
    TYPE_CHOICES = (
        ('1', 'Programma'),
        ('2', 'Progetto'),
    )

    communication_content = models.ForeignKey(CommunicationContent, verbose_name='contenuto', related_name='references', on_delete=models.CASCADE)
    type = models.CharField(choices=TYPE_CHOICES, max_length=1, verbose_name='tipo')
    code = models.CharField(max_length=100, verbose_name='codice')

    @property
    def code_is_cup(self):
        return bool(self.type == '2' and re.search('^\w\d\d\w\d{11}$', self.code))

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)

        if self.type and self.code:
            from django.core.exceptions import ObjectDoesNotExist, ValidationError
            try:
                if self.type == '1':
                    from .monitoraggio.models import Programma
                    Programma.objects.get(codice=self.code)
                elif self.type == '2' and not self.code_is_cup:
                    from .monitoraggio.models import Progetto
                    Progetto.objects.get(codice_locale=self.code)
            except ObjectDoesNotExist:
                raise ValidationError({'code': 'Il codice inserito non corrisponde a nessun {}'.format(self.get_type_display().lower())})

    def __str__(self):
        return '{} {}'.format(self.get_type_display(), self.code)

    class Meta:
        verbose_name = 'riferimento contenuto di comunicazione'
        verbose_name_plural = 'riferimenti contenuti di comunicazione'
        unique_together = ('communication_content', 'type', 'code')
        ordering = ['pk']


class ContactMessage(models.Model):
    REASON_CHOICES = (
        ('1', _('domanda sui dati')),
        ('2', _('domanda sul sito')),
        ('3', _('richiesta accesso API')),
        ('4', _('esempio di riuso: applicazioni')),
        ('5', _('esempio di riuso: visualizzazioni')),
        ('6', _('esempio di riuso: analisi')),
        ('7', _('segnalazione errore nei dati')),
        ('8', _('segnalazione relativa a un progetto')),
        ('9', _('suggerimenti e consigli')),
    )

    sender = models.CharField(max_length=50, verbose_name='autore')
    email = models.EmailField()
    organization = models.CharField(max_length=100, verbose_name='organizzazione')
    location = models.CharField(max_length=300, verbose_name='luogo')
    reason = models.CharField(choices=REASON_CHOICES, max_length=1, verbose_name='motivo del contatto')
    body = models.TextField(verbose_name='messaggio')
    sent_at = models.DateTimeField(auto_now_add=True, verbose_name='data di invio')

    class Meta:
        verbose_name = 'messaggio di contatto'
        verbose_name_plural = 'messaggi di contatto'


from .monitoraggio.models import TemaSintetico, TemaSinteticoCoesione, TemaSinteticoIndicatore, TerritorioCoesione, Valutazione
from .opendata.models import MetadataFile, Dataset


@receiver(models.signals.post_delete, sender=File)
@receiver(models.signals.post_delete, sender=DataFile)
@receiver(models.signals.post_delete, sender=PressReview)
@receiver(models.signals.post_delete, sender=CarouselItem)
@receiver(models.signals.post_delete, sender=Pillola)
@receiver(models.signals.post_delete, sender=ASOCStory)
@receiver(models.signals.post_delete, sender=ProjectStory)
@receiver(models.signals.post_delete, sender=OpportunitaProgramma)
@receiver(models.signals.post_delete, sender=TemaSintetico)
@receiver(models.signals.post_delete, sender=TemaSinteticoCoesione)
@receiver(models.signals.post_delete, sender=TemaSinteticoIndicatore)
@receiver(models.signals.post_delete, sender=TerritorioCoesione)
@receiver(models.signals.post_delete, sender=Valutazione)
@receiver(models.signals.post_delete, sender=MetadataFile)
@receiver(models.signals.post_delete, sender=Dataset)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem when corresponding sender object is deleted.
    """
    for attr in (f.name for f in sender._meta.fields if isinstance(f, models.FileField)):
        file = getattr(instance, attr, None)
        if file and os.path.isfile(file.path):
            os.remove(file.path)


@receiver(models.signals.pre_save, sender=File)
@receiver(models.signals.pre_save, sender=DataFile)
@receiver(models.signals.pre_save, sender=PressReview)
@receiver(models.signals.pre_save, sender=CarouselItem)
@receiver(models.signals.pre_save, sender=Pillola)
@receiver(models.signals.pre_save, sender=ASOCStory)
@receiver(models.signals.pre_save, sender=ProjectStory)
@receiver(models.signals.pre_save, sender=OpportunitaProgramma)
@receiver(models.signals.pre_save, sender=TemaSintetico)
@receiver(models.signals.pre_save, sender=TemaSinteticoCoesione)
@receiver(models.signals.pre_save, sender=TemaSinteticoIndicatore)
@receiver(models.signals.pre_save, sender=TerritorioCoesione)
@receiver(models.signals.pre_save, sender=Valutazione)
@receiver(models.signals.pre_save, sender=MetadataFile)
@receiver(models.signals.pre_save, sender=Dataset)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes file from filesystem when corresponding sender object is changed.
    """
    if not instance.pk:
        return False

    for attr in (f.name for f in sender._meta.fields if isinstance(f, models.FileField)):
        try:
            old_file = getattr(sender.objects.get(pk=instance.pk), attr, None)
        except sender.DoesNotExist:
            continue

        try:
            if not hasattr(old_file, 'file'):
                continue
        except (FileNotFoundError, ValueError):
            continue

        new_file = getattr(instance, attr, None)
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)
