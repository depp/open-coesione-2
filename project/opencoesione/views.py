# -*- coding: utf-8 -*-
import datetime
import json
from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.http import HttpResponseRedirect, BadHeaderError, HttpResponse
from django.utils.decorators import method_decorator
from django.utils.formats import date_format
from django.utils.translation import ugettext_lazy as _, get_language
from django.views.generic import DetailView, ListView, TemplateView, RedirectView, CreateView, FormView
from django.views.generic.base import TemplateResponseMixin
from honeypot.decorators import check_honeypot

from .forms import ContactMessageForm, SetProgrammiWidgetForm, CommunicationContentForm
from .models import (
    Pillola, News, PressReview, FAQ, DataFile, OpportunitaProgramma, ASOCStory, VideoStory, VideoTutorial,
    ProjectStory, CommunicationContent
)
from .monitoraggio.models import Programma, Progetto, Ambito
from .search_indexes import SEARCH_MODELS
from .search_utils import ModeltranslationSearchQuerySet
from ..enhancedsearch.views import MultiSelectWithRangeFacetedSearchView
from ..tagging.views import TagFilterMixin


def cached_context(get_context_data):
    """
    This decorator is used to cache the ``get_context_data()`` method
    called by a ``get()`` or ``post()`` in the views.
    It generates a unique key for the request,
    checks if the key is in the cache:
    if it is, then it returns it,
    else it will generate and save the key, before returning it.
    """

    def decorator(self, **kwargs):
        if getattr(self, 'cache_enabled', True):
        # if False:
            key = 'context{}'.format(self.request.get_full_path())
            context = cache.get(key)
            if context is None:
                context = get_context_data(self, **kwargs)
                serializable_context = context.copy()
                serializable_context.pop('view', None)
                cache.set(key, serializable_context)
            return context
        else:
            return get_context_data(self, **kwargs)
    return decorator


class FlatPageMixin(object):
    flatpage_url = None

    def get_flatpage(self, **kwargs):
        from django.contrib.flatpages.models import FlatPage

        try:
            return FlatPage.objects.get(url=self.flatpage_url)
        except FlatPage.DoesNotExist:
            return None

    def get_context_data(self, **kwargs):
        return super().get_context_data(flatpage=self.get_flatpage(), **kwargs)


class JSONResponseMixin(object):
    response_class = HttpResponse

    def render_to_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        response_kwargs['content_type'] = 'application/json'
        serializable_context = context.copy()
        serializable_context.pop('view', None)
        return self.response_class(
            json.dumps(serializable_context),
            **response_kwargs
        )


class XRobotsTagTemplateResponseMixin(TemplateResponseMixin):
    x_robots_tag = False

    def get_x_robots_tag(self):
        return self.x_robots_tag

    def render_to_response(self, context, **response_kwargs):
        response = super().render_to_response(context, **response_kwargs)
        if self.get_x_robots_tag():
            response['X-Robots-Tag'] = self.get_x_robots_tag()
        return response


class DateFilterMixin(object):
    def _get_date_filter_value(self):
        return self.request.GET.get('date', None)

    def _apply_date_filter(self, qs):
        end_date = datetime.datetime.now()

        filter_value = self._get_date_filter_value()

        if filter_value is None:
            qs = qs.filter(published_at__lte=end_date)
        else:
            start_date = datetime.datetime.combine(end_date.date(), datetime.time.min)
            if filter_value == 'w':
                start_date = start_date - datetime.timedelta(days=7)
            elif filter_value == 'm':
                start_date = start_date.replace(day=1)
            elif filter_value == 'y':
                start_date = start_date.replace(month=1, day=1)
            elif filter_value != 't':
                try:
                    start_date = datetime.datetime.strptime(filter_value, '%Y')
                except:
                    pass
                else:
                    end_date = datetime.datetime.combine(start_date.date().replace(month=12, day=31), datetime.time.max)
            qs = qs.filter(published_at__range=(start_date, end_date))

        return qs

    def _get_date_choices(self):
        filter_value = self._get_date_filter_value()

        choices = [
            {
                'name': _('Oggi'),
                'param': 't',
            },
            {
                'name': _('Ultimi 7 giorni'),
                'param': 'w',
            },
            {
                'name': _('Questo mese'),
                'param': 'm',
            },
            {
                'name': _("Quest'anno"),
                'param': 'y',
            }
        ] + [
            {
                'name': str(x),
                'param': str(x),
            } for x in range(datetime.datetime.now().year - 1, 2011, -1)
        ]

        for choice in choices:
            choice['selected'] = choice['param'] == filter_value
        return choices


class WidgetEmbedCodeMixin(object):
    widget_embed_code_template_name = 'widget_embed_code.html'

    def get_widget_embed_code(self, source_url):
        from django.core.exceptions import DisallowedHost
        try:
            source_url = self.request.build_absolute_uri(source_url)
        except DisallowedHost:
            return False
        else:
            from django.template.loader import render_to_string
            if 'localhost' not in source_url:
                source_url = source_url.replace('http://', 'https://')
            return render_to_string(self.widget_embed_code_template_name, {'source_url': source_url}).strip()


class ContentSearchView(MultiSelectWithRangeFacetedSearchView):
    template_name = 'opencoesione/content_search.html'

    queryset = ModeltranslationSearchQuerySet()

    FACETS = ('type',)

    def get_queryset(self):
        import datetime

        queryset = super().get_queryset()

        queryset = queryset.models(*SEARCH_MODELS)
        queryset = queryset.exclude(published_at__gt=datetime.date.today())
        # queryset = queryset.order_by('-published_at')

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['filters'] = []
        if context['query']:
            context['filters'].append({'name': _('Testo contenuto'), 'value': context['query'], 'remove_url': self._get_params('q')})

        type_display_map = {**{m._meta.model_name: m._meta.verbose_name for m in SEARCH_MODELS}, **{'flatpage': _('altre pagine')}}

        for result in context['object_list']:
            result.object.type_display = type_display_map[result.type]

        facet_info = self._build_facet_field_info('type', _('Tipologia'), {k: (v, v) for k, v in type_display_map.items()})
        facet_info['values'] = [x for x in facet_info['values'] if x['count']]
        context['my_facets'] = {
            'type': facet_info,
        }

        return context


class FlatPageTemplateView(FlatPageMixin, TemplateView):
    pass


class FlatPageListView(FlatPageMixin, ListView):
    pass


class BaseFilterListView(FlatPageListView, TagFilterMixin, DateFilterMixin):
    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = self._apply_date_filter(queryset)
        queryset = self._apply_tag_filter(queryset)

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['date_choices'] = self._get_date_choices()
        context['tag_choices'] = self._get_tag_choices()

        return context


class PillolaListView(BaseFilterListView):
    model = Pillola
    paginate_by = 10
    flatpage_url = '/pillole/'

    def get_queryset(self):
        return super().get_queryset().visibili()


class PillolaDetailView(DetailView):
    model = Pillola

    def get_context_data(self, **kwargs):
        return super().get_context_data(section='approfondimento', **kwargs)


class ASOCStoryListView(FlatPageListView):
    model = ASOCStory
    flatpage_url = '/ASOCStories/'

    def get_queryset(self):
        return super().get_queryset().visibili()


class ASOCStoryDetailView(DetailView):
    model = ASOCStory

    def get_context_data(self, **kwargs):
        context = super().get_context_data(section='approfondimento', **kwargs)

        context['object_list'] = self.model.objects.all()[:3]

        return context


class ProjectStoryListView(FlatPageListView):
    model = ProjectStory
    flatpage_url = '/storie_territorio/'

    def get_queryset(self):
        return super().get_queryset().visibili()


class ProjectStoryDetailView(DetailView):
    model = ProjectStory

    def get_context_data(self, **kwargs):
        return super().get_context_data(section='approfondimento', **kwargs)


class VideoStoryListView(FlatPageListView):
    model = VideoStory
    flatpage_url = '/video_racconti/'

    def get_queryset(self):
        return super().get_queryset().visibili()


class VideoStoryDetailView(DetailView):
    model = VideoStory

    def get_context_data(self, **kwargs):
        return super().get_context_data(section='approfondimento', **kwargs)


class VideoTutorialListView(FlatPageListView):
    model = VideoTutorial
    flatpage_url = '/scopri/video/'


class VideoTutorialDetailView(DetailView):
    model = VideoTutorial

    def get_context_data(self, **kwargs):
        return super().get_context_data(section='riuso', **kwargs)


class NewsListView(BaseFilterListView):
    model = News
    paginate_by = 10
    flatpage_url = '/news/'

    def get_queryset(self):
        return super().get_queryset().visibili()


class LatestNewsFeed(Feed):
    title = 'OpenCoesione > News'
    description = 'OpenCoesione > News'

    def link(self):
        return reverse('news_list')

    def items(self):
        return News.objects.visibili().order_by('-published_at')[:20]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.body

    def item_pubdate(self, item):
        return datetime.datetime.combine(item.published_at, datetime.datetime.min.time())


class NewsDetailView(DetailView):
    model = News

    def get_context_data(self, **kwargs):
        return super().get_context_data(section='approfondimento', **kwargs)


class PressReviewListView(FlatPageListView):
    model = PressReview
    paginate_by = 20
    flatpage_url = '/rassegna-stampa/'


class ItemListView(FlatPageListView):
    template_name = 'opencoesione/item_list.html'
    paginate_by = 20


class FAQListView(FlatPageListView, TagFilterMixin):
    model = FAQ
    flatpage_url = '/faq/'

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = self._apply_tag_filter(queryset)

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['tag_choices'] = self._get_tag_choices()

        return context


class OpportunitaProgrammaListView(FlatPageListView):
    model = OpportunitaProgramma
    flatpage_url = '/opportunita_2021_2027/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        gruppi_programmi = {x[0]: {'id': x[0], 'nome': x[1], 'programmi': []} for x in self.model.TIPO_CHOICES}
        for obj in self.object_list:
            gruppi_programmi[obj.tipo]['programmi'].append(obj)

        context['gruppi_programmi'] = gruppi_programmi.values()

        return context


class CommunicationContentCreateView(FlatPageMixin, CreateView):
    model = CommunicationContent
    form_class = CommunicationContentForm
    flatpage_url = '/contenuti_comunicazione_segnalazione/'

    def form_valid(self, form):
        from django.conf import settings
        from django.core.mail import send_mail

        ret = super().form_valid(form)

        obj = form.instance
        meta = obj._meta

        subject = 'Nuova segnalazione di contenuto di comunicazione per opencoesione.gov.it'

        message = 'È stata inviata una nuova segnalazione di un contenuto di comunicazione\n\n'
        message += 'BACKEND:\n{}'.format(
            self.request.build_absolute_uri(reverse('admin:{}_{}_change'.format(meta.app_label,  meta.model_name), args=(obj.pk,))),
        )

        send_mail(subject, message, 'oc@depp.it', settings.CONTACTS_EMAIL)

        return ret

    def get_success_url(self):
        return '{}?completed=true'.format(reverse('communicationcontent_form'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['form_submitted'] = self.request.GET.get('completed', '') == 'true'

        return context

    # def get_form_kwargs(self):
    #     kwargs = super().get_form_kwargs()
    #     kwargs['request'] = self.request
    #     return kwargs


class CommunicationContentListView(FlatPageListView):
    model = CommunicationContent
    # paginate_by = 20
    flatpage_url = '/contenuti_comunicazione/'

    def get_queryset(self):
        queryset = super().get_queryset().visibili()

        self.search_term = self.request.GET.get('q')
        if self.search_term:
            import operator
            from django.db.models import Q
            from functools import reduce
            search_fields = ('title', 'description', 'administration', 'regions__denominazione')
            orm_lookups = ['{}__icontains'.format(search_field) for search_field in search_fields]
            for bit in self.search_term.split():
                or_queries = [Q(**{orm_lookup: bit}) for orm_lookup in orm_lookups]
                queryset = queryset.filter(reduce(operator.or_, or_queries))

        return queryset.prefetch_related('regions', 'references')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(cc_query=self.search_term, **kwargs)

        prg_codes = []
        prj_codes = []
        for obj in context['object_list']:
            for ref in obj.references.all():
                if ref.type == '1':
                    prg_codes.append(ref.code)
                elif ref.type == '2' and not ref.code_is_cup:
                    prj_codes.append(ref.code)

        prg_code2obj = {o.codice: o for o in Programma.objects.programmi().filter(codice__in=prg_codes)} if prg_codes else {}
        prj_code2obj = {o.codice_locale: o for o in Progetto.objects.filter(codice_locale__in=prj_codes)} if prj_codes else {}

        for prg in prg_code2obj.values():
            prg.ciclo_programmazione_display = Ambito.inv_cicli_programmazione_dict()[prg.ciclo_programmazione].replace('_', '-')

        for obj in context['object_list']:
            obj.programs = [prg_code2obj[ref.code] for ref in obj.references.all() if ref.type == '1' and ref.code in prg_code2obj]
            obj.projects = [{'cup': prj_code2obj[ref.code].cup, 'url': prj_code2obj[ref.code].get_absolute_url()} for ref in obj.references.all() if ref.type == '2' and ref.code in prj_code2obj]
            obj.projects += [{'cup': ref.code, 'url': '{}?cup={}'.format(reverse('progetto_search'), ref.code)} for ref in obj.references.all() if ref.type == '2' and ref.code_is_cup]

        return context


@method_decorator(check_honeypot, name='post')
class ContactMessageView(FlatPageTemplateView):
    flatpage_url = '/contatti/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['form'] = ContactMessageForm(self.request.POST or None)
        context['form_submitted'] = self.request.GET.get('completed', '') == 'true'

        return context

    def post(self, request, *args, **kwargs):
        form = ContactMessageForm(self.request.POST)

        if form.is_valid():
            try:
                form.execute()
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            else:
                return HttpResponseRedirect('{}?completed=true'.format(reverse('contatto_form')))

        return self.get(request, *args, **kwargs)


class IndicatoriAccessoCSVView(TemplateView):
    def get(self, request, *args, **kwargs):
        import csv
        import xlrd
        from django.utils.formats import number_format
        from django.utils.text import slugify

        def value_format(cell):
            ctype = cell.ctype
            value = cell.value
            if ctype == xlrd.XL_CELL_DATE:
                return date_format(xlrd.xldate_as_datetime(value, workbook.datemode), 'd/m/Y')
            elif ctype != xlrd.XL_CELL_NUMBER:
                return value
            elif int(value) == value:
                return int(value)
            else:
                return number_format(value)

        n = int(self.kwargs['n'])

        workbook = xlrd.open_workbook(DataFile.objects.get(slug='indicatori_accesso_data').file.path)
        sheet = workbook.sheet_by_index(n - 1)

        header_row = 1 if get_language() == 'it' else 0

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(slugify(sheet.name))

        writer = csv.writer(response, dialect='excel-semicolon')

        writer.writerows([value_format(sheet.cell(i, j)) for j in range(sheet.ncols)] for i in [header_row] + list(range(2, sheet.nrows)))

        return response


class DocumentsRedirectView(RedirectView):
    def get_redirect_url(self, **kwargs):
        return '/media/uploads/documenti/{}'.format(kwargs['path'])


class RisorseTreemapView(TemplateView):
    def get_context_data(self, **kwargs):
        import csv
        from django.contrib.humanize.templatetags.humanize import intcomma
        from django.utils import translation

        def convert_number(str):
            return float(str.replace('.', '').replace(',', '.'))

        context = super().get_context_data(**kwargs)

        lng = translation.get_language().upper()

        data_file = DataFile.objects.get(slug='risorse{}{}_data'.format(self.kwargs.get('ciclo'), self.kwargs.get('tipo')))

        reader = csv.DictReader(open(data_file.file.path, 'r', encoding='utf-8-sig'), delimiter=';')

        rows = [row for row in reader if row['AREA'] != '0']

        context['data'] = json.dumps([{
            'nest_columns': [row['AMBITO_{}'.format(lng)], row['LABEL_{}'.format(lng)]],
            'size_columns': [convert_number(row['AREA'])],
            'popup_metadata': [row['INFO_{}'.format(lng)], intcomma(round(convert_number(row['RISORSE']), 1))],
        } for row in rows])

        context['thematisms'] = {row['LABEL_{}'.format(lng)]: row['TEMATISMO'] for row in rows}

        context['total'] = intcomma(round(sum(convert_number(row['RISORSE']) for row in rows), 1))

        context['updated_at'] = data_file.updated_at

        return context


class SetProgrammiWidgetFormView(XRobotsTagTemplateResponseMixin, WidgetEmbedCodeMixin, FormView):
    x_robots_tag = 'noindex'
    form_class = SetProgrammiWidgetForm

    def get_form_kwargs(self):
        kwargs = {}
        if self.request.method == 'GET':
            kwargs.update({
                'data': self.request.GET or None,
            })
        return kwargs

    def form_valid(self, form):
        return self.render_to_response(self.get_context_data(form=form, cleaned_data=form.cleaned_data))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if 'cleaned_data' in context:
            context['widget_embed_code'] = self.get_widget_embed_code(reverse('programmi_widget', kwargs=context['cleaned_data']))
        else:
            context['widget_embed_code'] = False

        return context

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)


class BackwardCompatibilityRedirectView(RedirectView):
    permanent = True
    query_string = True

    def get_redirect_url(self, **kwargs):
        path = kwargs['path'].lstrip('/')
        slug = kwargs['slug'].lstrip('/')

        if path == 'tipologie':
            path =  'nature'
            if slug.startswith('lavori-pubblici'):
                slug = slug.replace('lavori-pubblici', 'infrastrutture')
        elif path == 'gruppo-programmi':
            path = 'gruppi-programmi'
        elif path in ('regioni', 'province', 'comuni'):
            path = 'territori'
        elif path == 'pillola':
            path = 'pillole'

        return '/it/{path}/{slug}'.format(path=path, slug=slug)


class BackwardCompatibility2RedirectView(RedirectView):
    permanent = True
    query_string = True

    def get_redirect_url(self, **kwargs):
        lang = kwargs['lang'].lstrip('/')
        path = kwargs['path'].lstrip('/')
        slug = kwargs['slug'].lstrip('/')

        return '/{lang}/dati/{path}/{slug}'.format(lang=lang, path=path, slug=slug)
