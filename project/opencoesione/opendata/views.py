# -*- coding: utf-8 -*-
from django.http import Http404
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, RedirectView

from .models import OpenDataSection, Dataset
from .utils import get_latest_localfile
from ..monitoraggio.models import Programma, TemaSintetico, Territorio
from ..search_utils import ModeltranslationSearchQuerySet
from ..views import FlatPageMixin
from ...enhancedsearch.views import MultiSelectWithRangeFacetedSearchView


class DatasetSearchView(FlatPageMixin, MultiSelectWithRangeFacetedSearchView):
    template_name = 'opendata/dataset_search.html'
    flatpage_url = '/opendata/'

    queryset = ModeltranslationSearchQuerySet()

    model = Dataset

    FACETS = ('category', 'period', 'program', 'theme', 'region', 'format')

    @staticmethod
    def _get_objects_by_pk(pks):
        return {str(key): value for key, value in Dataset.objects.select_related('section', 'program', 'theme', 'region').in_bulk(pks).items()}

    def get_queryset(self):
        return  super().get_queryset().order_by('priority')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['filters'] = []
        if context['query']:
            context['filters'].append({'name': _('Testo contenuto'), 'value': context['query'], 'remove_url': self._get_params('q')})

        facets = {}

        facets['category'] = self._build_facet_field_info('category', _('Categoria'), {k: (v, v) for k, v in OpenDataSection.CATEGORY_CHOICES})
        facets['period'] = self._build_facet_field_info('period', _('Ciclo di programmazione'), {k: (v, v) for k, v in Dataset.PERIOD_CHOICES})
        facets['program'] = self._build_facet_field_info('program', _('Programma'), {o.codice: (o.descrizione, o.descrizione) for o in Programma.objects.programmi()})
        facets['theme'] = self._build_facet_field_info('theme', _('Tema'), {o.codice: (o.descrizione, o.descrizione_breve) for o in TemaSintetico.objects.all()})
        facets['region'] = self._build_facet_field_info('region', _('Regione'), {str(o.cod_reg): (str(o).title(), str(o).title()) for o in Territorio.objects.regioni()})
        facets['format'] = self._build_facet_field_info('format', _('Formato dati'), {x[0]: (x[0], x[0]) for x in self.queryset.facet_counts().get('fields', {}).get('format', [])})

        try:
            facets['period']['values'] = sorted(facets['period']['values'], key=lambda x: x['label'])
        except KeyError:
            pass

        try:
            facets['region']['values'] = sorted(facets['region']['values'], key=lambda x: x['label'])
        except KeyError:
            pass

        context['my_facets'] = facets

        context['no_filter'] = context['paginator'].count == self.model.objects.count()

        return context


class DatasetDetailView(DetailView):
    model = Dataset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(section='dati', **kwargs)

        filter = {'section': self.object.section}
        if self.object.theme:
            filter.update({'theme': self.object.theme})
        if self.object.region:
            filter.update({'region': self.object.region})
        context['related_objects'] = self.model.objects.exclude(pk=self.object.pk).filter(**filter)[:4]

        return context


class LocalDatasetRedirectView(RedirectView):
    def get_redirect_url(self, **kwargs):
        try:
            return '/media/open_data/{}'.format(get_latest_localfile(kwargs['path'], as_urlpath=True))
        except:
            raise Http404('File not found.')
