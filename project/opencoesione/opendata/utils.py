# -*- coding: utf-8 -*-
import glob
import os
import ssl
from django.conf import settings
from django.core.urlresolvers import reverse
from urllib.request import Request, urlopen

context = ssl._create_unverified_context()


def get_localfile_info(file_name):
    try:
        file_path = get_latest_localfile(file_name)
        file_size = os.stat(file_path).st_size
    except Exception:
        file_size = None

    return {
        'file_name': reverse('localdataset_redirect', kwargs={'path': file_name}),
        'file_size': file_size,
        'file_ext': _get_file_ext(file_name),
    }


def get_remotefile_info(file_name):
    try:
        headers = urlopen(Request(file_name, method='HEAD'), context=context, timeout=1).info()
        file_size = headers['Content-Length']
    except Exception:
        file_size = None

    return {
        'file_name': file_name,
        'file_size': file_size,
        'file_ext': _get_file_ext(file_name),
    }


def get_uploadedfile_info(file):
    return {
        'file_name': file.url,
        'file_size': file.size,
        'file_ext': _get_file_ext(file.name),
    }


def get_latest_localfile(file_name, as_urlpath=False):
    opendata_path = os.path.join(settings.MEDIA_ROOT, 'open_data')

    def add_wildcard(file_name):
        wildcard = '202?????'
        file_name, file_ext = os.path.splitext(file_name)
        return file_name + '_' + wildcard + file_ext

    file_path = os.path.join(opendata_path, *file_name.split('/'))
    if not os.path.isfile(file_path):
        files = sorted(glob.glob(add_wildcard(file_path)))
        if files:
            file_path = files[-1]
        else:
            raise IOError

    if as_urlpath:
        file_path = '/'.join(os.path.split(os.path.relpath(file_path, opendata_path)))

    return file_path


def _get_file_ext(file_name):
    _, file_ext = os.path.splitext(file_name)
    file_ext = file_ext[1:]
    if file_ext == 'zip':
        file_ext += '/csv'

    return file_ext
