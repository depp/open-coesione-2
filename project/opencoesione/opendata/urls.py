# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import DatasetSearchView, DatasetDetailView, LocalDatasetRedirectView


urlpatterns = [
    url(r'^$', DatasetSearchView.as_view(), name='dataset_search'),
    url(r'^dataset/(?P<slug>[\w-]+)/$', DatasetDetailView.as_view(), name='dataset_detail'),
    url(r'^(?P<path>.+)$', LocalDatasetRedirectView.as_view(), name='localdataset_redirect'),
]
