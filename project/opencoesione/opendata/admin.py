# -*- coding: utf-8 -*-
from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib import admin
from django.forms import HiddenInput
from modeltranslation.admin import TabbedTranslationAdmin
from .models import OpenDataSection, MetadataFile, LocalDataset, RemoteDataset, UploadedDataset


class OpenDataSectionAdmin(TabbedTranslationAdmin):
    list_display = ('slug', 'title', 'category', 'priority', 'highlight')
    list_editable = ('priority',)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'body':
            kwargs['widget'] = CKEditorUploadingWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if not request.user.is_superuser:
            return self.readonly_fields + ('slug',)
        return super().get_readonly_fields(request, obj)


class MetadataFileAdmin(admin.ModelAdmin):
    list_display = ('slug', 'name', 'file', 'num_dataset')

    def num_dataset(self, obj):
        return obj.num_dataset
    num_dataset.short_description = 'numero di dataset collegati'

    def get_queryset(self, request):
        from django.db.models import Count
        return super().get_queryset(request).annotate(num_dataset=Count('dataset'))

    def get_readonly_fields(self, request, obj=None):
        if not request.user.is_superuser:
            return self.readonly_fields + ('slug',)
        return super().get_readonly_fields(request, obj)


class DatasetAdmin(TabbedTranslationAdmin):
    list_display = ('slug', 'name', 'updated_at', 'section', 'priority', 'in_mainpage')
    list_editable = ('priority',)
    list_filter = ('in_mainpage', ('section', admin.RelatedOnlyFieldListFilter),)
    date_hierarchy = 'updated_at'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.exclude = [x for x in ('local_file', 'remote_file', 'uploaded_file') if x != '{}_file'.format(dict(self.model.TYPE_CHOICES).get(self.model.TYPE))]

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'type':
            kwargs['widget'] = HiddenInput()
            kwargs['initial'] = self.model.TYPE
        elif db_field.name == 'description':
            kwargs['widget'] = CKEditorWidget()
        elif db_field.name == '{}_file'.format(dict(self.model.TYPE_CHOICES).get(self.model.TYPE)):
            kwargs['required'] = True
        return super().formfield_for_dbfield(db_field, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if not request.user.is_superuser:
            return self.readonly_fields + ('slug',)
        return super().get_readonly_fields(request, obj)


class LocalDatasetAdmin(DatasetAdmin):
    pass


class RemoteDatasetAdmin(DatasetAdmin):
    pass


class UploadedDatasetAdmin(DatasetAdmin):
    pass


admin.site.register(OpenDataSection, OpenDataSectionAdmin)
admin.site.register(MetadataFile, MetadataFileAdmin)
admin.site.register(LocalDataset, LocalDatasetAdmin)
admin.site.register(RemoteDataset, RemoteDatasetAdmin)
admin.site.register(UploadedDataset, UploadedDatasetAdmin)
