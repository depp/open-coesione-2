# -*- coding: utf-8 -*-
from modeltranslation.translator import register, TranslationOptions
from .models import OpenDataSection, Dataset, LocalDataset, RemoteDataset, UploadedDataset


@register(OpenDataSection)
class OpenDataSectionTranslationOptions(TranslationOptions):
    fields = ('title', 'body')


@register((Dataset, LocalDataset, RemoteDataset, UploadedDataset))
class DatasetTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'source', 'update_frequency', 'license')
