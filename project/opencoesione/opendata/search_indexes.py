# -*- coding: utf-8 -*-
from haystack import indexes

from .models import Dataset, LocalDataset, RemoteDataset, UploadedDataset
from ..monitoraggio.models import Programma, TemaSintetico, Territorio
from ..search_indexes import ModeltranslationIndex

SEARCH_MODELS = (
    Dataset, LocalDataset, RemoteDataset, UploadedDataset,
)


class DatasetIndex(ModeltranslationIndex, indexes.Indexable):
    priority = indexes.IntegerField(indexed=False, stored=False)

    category = indexes.FacetCharField(stored=False)
    period = indexes.FacetMultiValueField(stored=False)
    program = indexes.FacetMultiValueField(stored=False)
    theme = indexes.FacetMultiValueField(stored=False)
    region = indexes.FacetMultiValueField(stored=False)
    format = indexes.FacetMultiValueField(stored=False)

    def get_model(self):
        return Dataset

    def index_queryset(self, using=None):
        return self.get_model().objects.select_related('section', 'program', 'theme', 'region')

    def prepare_priority(self, obj):
        return 10000 * obj.section.priority + obj.priority

    def prepare_category(self, obj):
        return obj.section.category

    def prepare_period(self, obj):
        if obj.period:
            return [obj.period]
        else:
            # return [x[0] for x in obj.PERIOD_CHOICES]
            return []

    def prepare_program(self, obj):
        if obj.program:
            return [obj.program.codice]
        else:
            # return list(Programma.objects.programmi().values_list('codice', flat=True))
            return []

    def prepare_theme(self, obj):
        if obj.theme:
            return [obj.theme.codice]
        else:
            # return list(TemaSintetico.objects.values_list('codice', flat=True))
            return []

    def prepare_region(self, obj):
        if obj.region:
            return [obj.region.cod_reg]
        else:
            # return list(Territorio.objects.regioni().values_list('cod_reg', flat=True))
            return []

    def prepare_format(self, obj):
        return [x['file_ext'] for x in obj.get_files_info()]
