# -*- coding: utf-8 -*-
import csv
import datetime
import io
import os
import re
import shutil
import tempfile
import zipfile

from django.conf import settings
from django.core.management.base import BaseCommand
from django.urls import reverse
from django.utils.text import slugify

from ...models import Dataset, MetadataFile, OpenDataSection
from ....monitoraggio.models import Programma, Progetto
from ....opendata.utils import get_latest_localfile


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write('Inizio procedura.')

        start_time = datetime.datetime.now()

        zipped_file = get_latest_localfile('progetti_esteso.zip')

        self.stdout.write('- Svuotamento directory di destinazione...')

        data_path = os.path.join(os.path.dirname(zipped_file), 'programmi')
        if os.path.isdir(data_path):
            shutil.rmtree(data_path)
        os.makedirs(data_path, exist_ok=True)

        self.stdout.write('- Fatto.')

        self.stdout.write('- Suddivisione CSV...')

        target_files = {}
        with zipfile.ZipFile(zipped_file) as zfile:
            with io.TextIOWrapper(zfile.open(zfile.namelist().pop(0)), 'UTF-8-sig') as source_file:
                for n, line in enumerate(source_file):
                    csv_line = next(csv.reader([line], delimiter=';'))
                    if n == 0:
                        header = line
                        idx = csv_line.index('OC_CODICE_PROGRAMMA')
                    else:
                        codici = csv_line[idx].split(':::')
                        for codice in codici:
                            if codice not in target_files:
                                target_files[codice] = tempfile.NamedTemporaryFile(mode='w', suffix='.csv', encoding='UTF-8-sig')
                                target_files[codice].write(header)
                            target_files[codice].write(line)

        self.stdout.write('- Fatto.')

        self.stdout.write('- Salvataggio file ZIP e creazione Dataset...')

        source_it = 'Sistema Nazionale di Monitoraggio delle Politiche di Coesione (MEF-RGS-IGRUE)'
        source_en = 'National Monitoring System for Cohesion Policies (MEF-RGS-IGRUE)'
        update_frequency_it = 'bimestrale'
        update_frequency_en = 'bimonthly'
        license = 'CC BY 4.0'

        progetti_programma_section = OpenDataSection.objects.get(slug='progetti_programma_section')
        metadata_file = MetadataFile.objects.get(slug='progetti')

        programs_map = {o.pk: o for o in Programma.objects.programmi().filter(**Progetto.computabili_filter_dict('classificazione_set__classificazione_set__ambiti__progetto__')).distinct()}

        new_datasets = []

        filename_fmt = 'progetti_esteso_{}'

        match = re.search('_((202\d)(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01]))\.', zipped_file)
        date = match.group(1) if match else None

        for codice, target_file in target_files.items():
            target_file.flush()

            if codice in programs_map:
                slug = filename = filename_fmt.format(slugify(codice).replace('-', '_'))

                if date:
                    filename += '_' + date
                with zipfile.ZipFile(os.path.join(data_path, '{}.zip'.format(filename)), 'w', zipfile.ZIP_DEFLATED) as zfile:
                    zfile.write(target_file.name, arcname='{}.csv'.format(filename))

                program = programs_map[codice]
                dataset, created = Dataset.objects.get_or_create(
                    slug=slug,
                    defaults={
                        'type': 'L',
                        'in_mainpage': False,
                        'name': 'Progetti con tracciato esteso {}'.format(program.descrizione),
                        'name_it': 'Progetti con tracciato esteso {}'.format(program.descrizione),
                        'name_en': '{} projects with extended record'.format(program.descrizione),
                        'source': source_it,
                        'source_it': source_it,
                        'source_en': source_en,
                        'section': progetti_programma_section,
                        'period': program.ciclo_programmazione,
                        'program': program,
                        'update_frequency': update_frequency_it,
                        'update_frequency_it': update_frequency_it,
                        'update_frequency_en': update_frequency_en,
                        'license': license,
                        'license_it': license,
                        'license_en': license,
                        'updated_at': settings.DATA_AGGIORNAMENTO,
                        'local_file': os.path.join('programmi', '{}.zip'.format(slug)),
                        'metadata_file': metadata_file,
                    },
                )
                if created:
                    new_datasets.append(dataset)

            target_file.close()

        self.stdout.write('- Fatto.')

        if new_datasets:
            self.stdout.write('- Sono stati creati i seguenti dataset:')
            for dataset in new_datasets:
                self.stdout.write('-- {} ({})'.format(dataset, reverse('admin:opendata_localdataset_change', args=(dataset.pk,))))

        empty_datasets = []
        for dataset in progetti_programma_section.dataset_set.filter(type='L'):
            try:
                get_latest_localfile(dataset.local_file)
            except IOError:
                empty_datasets.append(dataset)

        if empty_datasets:
            self.stdout.write('- Sono stati individuati i seguenti dataset vuoti:')
            for dataset in empty_datasets:
                self.stdout.write('-- {} ({})'.format(dataset, reverse('admin:opendata_localdataset_change', args=(dataset.pk,))))

        duration = datetime.datetime.now() - start_time
        seconds = round(duration.total_seconds())

        self.stdout.write('Fine. Tempo di esecuzione: {:02d}:{:02d}:{:02d}.'.format(int(seconds // 3600), int((seconds % 3600) // 60), int(seconds % 60)))
