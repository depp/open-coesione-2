# -*- coding: utf-8 -*-
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from .utils import get_localfile_info, get_remotefile_info, get_uploadedfile_info
from ..monitoraggio.models import Programma, TemaSintetico, Territorio


class DatasetManager(models.Manager):
    def get_queryset(self):
        queryset = super().get_queryset()
        if self.model.TYPE:
            queryset = queryset.filter(type=self.model.TYPE)
        return queryset

    def create(self, **kwargs):
        kwargs.update({'type': self.model.TYPE})
        return super().create(**kwargs)


class OpenDataSection(models.Model):
    CATEGORY_CHOICES = (
        ('progetti', _('Attuazione delle politiche di coesione: progetti')),
        ('programmazione', _('Programmazione delle politiche di coesione: risorse finanziarie')),
        ('focus', _('Politiche di coesione: informazioni e dati aggregati')),
        ('valutazioni', _('Osservatorio dei processi valutativi')),
        ('contesto', _('Contesto socio-economico')),
    )

    slug = models.SlugField(unique=True, verbose_name='identificativo')
    title = models.CharField(max_length=255, verbose_name='titolo')
    body = models.TextField(verbose_name='testo', blank=True, null=True)
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=20, verbose_name='categoria')
    priority = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')
    highlight = models.BooleanField(default=False, verbose_name='in evidenza')

    @property
    def metadata(self):
        metadata = set()
        for obj in self.dataset_set.all():
            if obj.metadata_file:
                metadata.add(obj.metadata_file)
            if obj.metadata_url:
                metadata.add(obj.metadata_url)
        return metadata

    @property
    def updated_at(self):
        try:
            return max(o.updated_at for o in self.dataset_set.all() if o.updated_at)
        except Exception:
            return None

    @property
    def all_datasets_in_portal(self):
        return all(o.in_portal for o in self.dataset_set.all())

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'sezione open data'
        verbose_name_plural = 'sezioni open data'
        ordering = ['priority', 'pk']


class MetadataFile(models.Model):
    slug = models.SlugField(unique=True, verbose_name='identificativo')
    name = models.CharField(max_length=255, verbose_name='nome')
    file = models.FileField(upload_to='opendata')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'file di metadati'
        verbose_name_plural = 'file di metadati'
        ordering = ['slug']


class Dataset(models.Model):
    TYPE = None
    PERIOD_CHOICES = (
        # ('9', '2000-2006'),
        ('1', '2007-2013'),
        ('2', '2014-2020'),
        ('3', '2021-2027'),
    )
    TYPE_CHOICES = (
        ('L', 'local'),
        ('R', 'remote'),
        ('U', 'uploaded'),
    )

    type = models.CharField(choices=TYPE_CHOICES, max_length=1, verbose_name='tipo')
    slug = models.SlugField(max_length=60, unique=True, verbose_name='identificativo')

    in_mainpage = models.BooleanField(default=True, verbose_name='visibile nella pagina principale')

    name = models.CharField(max_length=255, verbose_name='nome')
    description = models.TextField(verbose_name='descrizione', blank=True, null=True)
    source = models.CharField(max_length=255, verbose_name='fonte', blank=True, null=True)

    section = models.ForeignKey(OpenDataSection, verbose_name='sezione', on_delete=models.CASCADE)
    period = models.CharField(choices=PERIOD_CHOICES, max_length=1, verbose_name='ciclo di programmazione', blank=True, null=True)
    program = models.ForeignKey(Programma, limit_choices_to={'tipo': Programma.TIPO.programma}, verbose_name='programma', null=True, blank=True, on_delete=models.CASCADE)
    theme = models.ForeignKey(TemaSintetico, verbose_name='tema', null=True, blank=True, on_delete=models.CASCADE)
    region = models.ForeignKey(Territorio, limit_choices_to={'tipo': Territorio.TIPO.R}, verbose_name='regione', null=True, blank=True, on_delete=models.CASCADE)

    update_frequency = models.CharField(max_length=255, verbose_name='frequenza di aggiornamento prevista', blank=True, null=True)
    license = models.CharField(max_length=255, verbose_name='licenza di utilizzo', blank=True, null=True)

    updated_at = models.DateField(verbose_name='data di aggiornamento', blank=True, null=True)

    local_file = models.CharField(max_length=100, verbose_name='file', blank=True, null=True)
    remote_file = models.URLField(verbose_name='indirizzo file remoto', blank=True, null=True)
    uploaded_file = models.FileField(upload_to='opendata', verbose_name='file', blank=True, null=True)

    metadata_file = models.ForeignKey(MetadataFile, verbose_name='file di metadati caricato', blank=True, null=True, on_delete=models.CASCADE)
    metadata_url = models.URLField(verbose_name='indirizzo file di metadati remoto', blank=True, null=True)

    in_portal = models.BooleanField(default=True, verbose_name='dati navigabili sul portale')

    priority = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')

    objects = DatasetManager()

    @property
    def file(self):
        return getattr(self, '{}_file'.format(self.get_type_display()))

    def get_files_info(self):
        files_info = []
        if self.type == 'L':
            files_info.append(get_localfile_info(self.file))
            parquet_info = get_localfile_info(self.file.replace('.zip', '.parquet'))
            if parquet_info['file_size']:
                files_info.append(parquet_info)
        elif self.type == 'R':
            files_info.append(get_remotefile_info(self.file))
        elif self.type == 'U':
            files_info.append(get_uploadedfile_info(self.file))
        return files_info

    def get_metadatafile_info(self):
        if self.metadata_file:
            return get_uploadedfile_info(self.metadata_file.file)
        elif self.metadata_url:
            return get_remotefile_info(self.metadata_url)
        else:
            return None

    def get_absolute_url(self):
        return reverse('dataset_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'dataset'
        verbose_name_plural = 'dataset'
        ordering = ['section__priority', 'priority', 'name']


class LocalDataset(Dataset):
    TYPE = 'L'

    class Meta:
        proxy = True
        verbose_name = 'dataset locale'
        verbose_name_plural = 'dataset locali'


class RemoteDataset(Dataset):
    TYPE = 'R'

    class Meta:
        proxy = True
        verbose_name = 'dataset remoto'
        verbose_name_plural = 'dataset remoti'


class UploadedDataset(Dataset):
    TYPE = 'U'

    class Meta:
        proxy = True
        verbose_name = 'dataset caricato'
        verbose_name_plural = 'dataset caricati'
