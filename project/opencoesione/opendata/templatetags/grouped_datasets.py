# -*- coding: utf-8 -*-
import datetime
from django import template
from django.conf import settings
from django.db.models import Prefetch
from django.utils.translation import ugettext_lazy as _

from ..models import OpenDataSection, Dataset
from ..utils import get_remotefile_info, get_uploadedfile_info

register = template.Library()


def _set_subitems(data, name, is_subitem, parent_identifier):
    subitem_identifiers = [k for k in data if is_subitem(k)]

    for subitem_identifier in subitem_identifiers:
        identifier = parent_identifier(subitem_identifier)
        if identifier in data:
            item = data.pop(subitem_identifier)
            if any(x['file_size'] for x in item['files']):
                if 'subitems' not in data[identifier]:
                    data[identifier]['subitems'] = {
                        'name': name,
                        'items': [],
                    }
                data[identifier]['subitems']['items'].append(item)


@register.inclusion_tag('opendata/templatetags/grouped_datasets.html', takes_context=True)
def grouped_datasets(context):
    opendata_sections = {o.slug: o for o in OpenDataSection.objects.prefetch_related(Prefetch('dataset_set', queryset=Dataset.objects.filter(in_mainpage=True).select_related('metadata_file')))}

    for section in opendata_sections.values():
        section.data = {
            o.slug: {
                'identifier': o.slug,
                'name': o.name + ('' if o.in_portal else ' *'),
                'url': o.get_absolute_url(),
                'files': o.get_files_info(),
            } for o in section.dataset_set.all()
        }

        _set_subitems(
            section.data,
            _('Programmi'),
            lambda x: x.startswith('progetti_esteso_psr_feasr'),
            lambda x: 'progetti_esteso_feasr_2014_2020'
        )

        _set_subitems(
            section.data,
            _('Cicli'),
            lambda x: x.endswith(('2007-2013', '2014-2020', '2021-2027')),
            lambda x: x[:-10]
        )

        section.data = list(section.data.values())

        if section.slug == 'progetti_section' or not section.slug.startswith('progetti_'):
            section.data += [
                {
                    'name': _('Metadati'),
                    'files': [get_remotefile_info(m) if isinstance(m, str) else get_uploadedfile_info(m.file)],
                } for m in section.metadata
            ]

    for sfx in ('', '2'):
        subsection_keys = [key for key in opendata_sections if key.startswith('approfondimenti{}_'.format(sfx)) and key.endswith('_subsection')]
        opendata_sections['approfondimenti{}_section'.format(sfx)].subsections = [
            opendata_sections.pop(key) for key in subsection_keys
        ]

    section_categories = {x[0]: {'identifier': x[0], 'name': x[1], 'sections': []} for x in OpenDataSection.CATEGORY_CHOICES}
    for section in opendata_sections.values():
        if section.data or hasattr(section, 'subsections'):
            section_categories[section.category]['sections'].append(section)

    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')

    return {
        'data_aggiornamento': datetime.datetime.strptime(settings.DATA_AGGIORNAMENTO, '%Y-%m-%d'),
        'data_pubblicazione_aggiornamento': datetime.datetime.strptime(settings.DATA_PUBBLICAZIONE_AGGIORNAMENTO, '%Y-%m-%d'),
        'section_categories': section_categories.values(),
        sezikai_ctx_var: context[sezikai_ctx_var],
    }
