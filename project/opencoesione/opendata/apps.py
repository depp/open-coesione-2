from django.apps import AppConfig


class OpendataConfig(AppConfig):
    name = 'project.opencoesione.opendata'
