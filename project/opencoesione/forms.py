# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from django.utils.translation import get_language, ugettext_lazy as _
from modeltranslation.forms import TranslationModelForm
# from nocaptcha_recaptcha import NoReCaptchaField
from .models import CommunicationContent, CommunicationContentReference, ContactMessage


class CommunicationContentForm(TranslationModelForm):
    references = forms.CharField(widget=forms.Textarea, label=_('Programmi e/o progetti correlati'), help_text=_("Inserire le URL assolute di programmi e/o progetti correlati, una per riga. Ad esempio nel caso del progetto OpenCoesione, l'URL assoluta sarebbe: https://opencoesione.gov.it/it/dati/progetti/1agcoe150/, mentre l'URL del programma di appartenenza sarebbe: https://opencoesione.gov.it/it/dati/programmi/2014IT05M2OP002/"))
    url_eng = forms.URLField(max_length=255, label=_('Link al contenuto (versione inglese)'), required=False)

    def __init__(self, *args, **kwargs):
        # self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)
        self.fields['type'].choices = [x for x in self.fields['type'].choices if x[0] != 'O']
        for field_name in ('administration', 'user_name', 'user_email'):
            self.fields[field_name].required = True
        if get_language() == 'en':
            del self.fields['url_eng']

    def save(self, commit=True):
        from django.db import transaction

        if self.cleaned_data.get('url_eng'):
            self.instance.url_en = self.cleaned_data.get('url_eng')

        with transaction.atomic():
            super().save(commit=commit)

            CommunicationContentReference.objects.bulk_create(
                CommunicationContentReference(communication_content=self.instance, **reference) for reference in self.cleaned_data['references']
            )

    def clean_references(self):
        from django.core.urlresolvers import resolve
        from django.http.request import validate_host
        from django.urls import Resolver404
        from urllib.parse import urlparse
        from .monitoraggio.models import Progetto, Programma

        data = self.cleaned_data['references']

        # base_uri = self.request.build_absolute_uri('/')

        references = []
        errors = []
        for n, line in enumerate(data.splitlines()[:10], 1):
            reference = None
            parsedline = urlparse(line)
            if parsedline.hostname and validate_host(parsedline.hostname, settings.ALLOWED_HOSTS):
                try:
                    path = parsedline.path
                    if path.startswith(tuple('/{}/'.format(lng[0]) for lng in settings.LANGUAGES if lng[0] != get_language())):
                        path = '/{}/{}'.format(get_language(), path[4:])
                    match = resolve(path)
                except Resolver404:
                    pass
                else:
                    if match.url_name == 'programma_detail':
                        try:
                            obj = Programma.objects.get(codice=match.kwargs['pk'])
                        except Programma.DoesNotExist:
                            pass
                        else:
                            reference = {'type': '1', 'code': obj.codice}
                    elif match.url_name == 'progetto_detail':
                        try:
                            obj = Progetto.objects.get(slug=match.kwargs['slug'])
                        except Progetto.DoesNotExist:
                            pass
                        else:
                            reference = {'type': '2', 'code': obj.codice_locale}

            if not reference:
                errors.append(n)
            elif reference not in references:
                references.append(reference)

        if errors:
            from django.core.exceptions import ValidationError
            raise ValidationError(_('Sono stati inseriti valori non validi nelle righe') + ': {}'.format(', '.join(str(x) for x in errors)))

        return references

    class Meta:
        model = CommunicationContent
        fields = ['title', 'description', 'type', 'url', 'url_eng', 'user_name', 'user_email', 'administration', 'regions', 'references']
        labels = {
            'title': _('Titolo'),
            'description': _('Breve descrizione'),
            'type': _('Tipo di contenuto'),
            'url': _('Link al contenuto'),
            'user_name': _('Nome e cognome'),
            'user_email': _('Email'),
            'administration': _('Amministrazione di appartenenza'),
            'regions': _('Regioni'),
        }


class ContactMessageForm(forms.Form):
    REASON_CHOICES = (('', '--------------'),) + ContactMessage.REASON_CHOICES

    name = forms.CharField(max_length=50, label=_('Nome'))
    surname = forms.CharField(max_length=100, label=_('Cognome'))
    email = forms.EmailField()
    organization = forms.CharField(max_length=100, label=_('Istituzione/Società/Ente'))
    location = forms.CharField(max_length=300, label=_('Luogo'))
    reason = forms.TypedChoiceField(choices=REASON_CHOICES, label=_('Motivo del contatto'))
    body = forms.CharField(widget=forms.Textarea, label=_('Messaggio'))
    # captcha = NoReCaptchaField(label=_('Controllo anti-spam'))

    def execute(self):
        from django.core.mail import EmailMultiAlternatives
        from django.utils.html import strip_tags
        from django.template.loader import render_to_string

        values_dict = {
            'sender': '{} {}'.format(self.cleaned_data.get('name', ''), self.cleaned_data.get('surname', '')),
            'email': self.cleaned_data.get('email', ''),
            'organization': self.cleaned_data.get('organization', ''),
            'location': self.cleaned_data.get('location', ''),
            'reason': self.cleaned_data.get('reason'),
            'body': self.cleaned_data.get('body', ''),
        }

        # create new ContactMessage
        omsg = ContactMessage.objects.create(**values_dict)

        # convert selected choice to text
        values_dict['reason'] = omsg.get_reason_display()

        html_content = render_to_string('mail/contatti.html', values_dict)

        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(
            'Richiesta informazioni da opencoesione.gov.it',
            strip_tags(html_content),
            'oc@depp.it',
            settings.CONTACTS_EMAIL,
            reply_to=[values_dict['email']]
        )
        msg.attach_alternative(html_content, 'text/html')
        msg.send()


class SetProgrammiWidgetForm(forms.Form):
    ciclo = forms.ChoiceField(choices=[('0713', '2007-2013'), ('1420', '2014-2020')], label=_('Ciclo'))
    ambito = forms.ChoiceField(choices=(), label=_('Ambito'))

    def __init__(self, *args, **kwargs):
        from django.utils import translation

        lng = translation.get_language().upper()

        ambiti = {
            'IT': ['FESR', 'FSE', 'IOG', 'CTE', 'FEASR', 'FEAMP', 'FSC', 'POC', 'SNAI-Servizi', 'PAC'],
            'EN': ['ERDF', 'ESF', 'YEI', 'ETC', 'EAFRD', 'MFF', 'DCF', 'COP', 'IANS', 'CAP'],
        }[lng]

        super().__init__(*args, **kwargs)

        self.fields['ambito'].choices = zip(ambiti, ambiti)
