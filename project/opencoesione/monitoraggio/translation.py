# -*- coding: utf-8 -*-
from modeltranslation.translator import register, TranslationOptions
from .models import (
    Programma, ClassificazioneOggetto, ClassificazioneQSN, ClassificazioneAzione, TemaPrioritario, TemaSintetico,
    TemaSinteticoCoesione, TemaSinteticoIndicatore, TerritorioCoesione, TerritorioTemaSinteticoCoesione, Fonte, Focus,
    FaseProcedurale, IndicatoreRealizzazione, IndicatoreRealizzazioneUnitaMisura, Valutazione, Area, StrategiaS3,
    TerritorioS3,
)


@register(Area)
@register(Programma)
@register(StrategiaS3)
@register(TerritorioS3)
class ProgrammaTranslationOptions(TranslationOptions):
    fields = ('descrizione', 'descrizione_estesa')


@register(ClassificazioneOggetto)
@register(ClassificazioneQSN)
@register(TemaPrioritario)
@register(FaseProcedurale)
@register(IndicatoreRealizzazione)
@register(IndicatoreRealizzazioneUnitaMisura)
class ClassificazioneTranslationOptions(TranslationOptions):
    fields = ('descrizione',)


@register(ClassificazioneAzione)
class ClassificazioneAzioneTranslationOptions(TranslationOptions):
    fields = ('descrizione', 'descrizione_breve', 'descrizione_estesa')


@register(TemaSinteticoCoesione)
@register(TemaSintetico)
class TemaSinteticoTranslationOptions(TranslationOptions):
    fields = ('descrizione', 'descrizione_breve', 'descrizione_estesa', 'testo_dati')


@register(TemaSinteticoIndicatore)
class TemaSinteticoIndicatoreTranslationOptions(TranslationOptions):
    fields = ('titolo', 'testo', 'link', 'titolo_dati', 'immagine_dati')


@register(TerritorioCoesione)
class TerritorioCoesioneTranslationOptions(TranslationOptions):
    fields = ('descrizione', 'testo_dati')


@register(TerritorioTemaSinteticoCoesione)
class TerritorioTemaSinteticoCoesioneTranslationOptions(TranslationOptions):
    fields = ('descrizione', 'indicatore_valore', 'indicatore_unitamisura')


@register(Fonte)
class FonteTranslationOptions(TranslationOptions):
    fields = ('descrizione', 'descrizione_breve')


@register(Focus)
class FocusTranslationOptions(TranslationOptions):
    fields = ('nome',)


@register(Valutazione)
class ValutazioneTranslationOptions(TranslationOptions):
    fields = ('testo', 'testo_dati', 'testo_grafico')
