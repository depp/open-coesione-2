# -*- coding: utf-8 -*-
from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib import admin
from django.core.validators import FileExtensionValidator
from django.forms import forms
from modeltranslation.admin import TabbedTranslationAdmin, TranslationStackedInline, TranslationTabularInline, \
    TranslationGenericStackedInline
from .models import *
from ..admin import FileInline, LinkInline


class LoadCSVForm(forms.Form):
    file = forms.FileField(
        validators=[FileExtensionValidator(allowed_extensions=['csv', 'zip'])],
        label='File CSV o ZIP',
        help_text='Carica un file CSV. Puoi caricare anche un file CSV zippato.'
    )

    def save(self):
        import csv
        import zipfile
        from io import StringIO
        from pathlib import Path

        file = self.cleaned_data['file']

        try:
            if Path(file.name).suffix[1:].lower() == 'zip':
                with zipfile.ZipFile(file) as zfile:
                    content = zfile.read(zfile.namelist().pop(0))
            else:
                content = file.read()

            reader = csv.DictReader(StringIO(content.decode('utf-8-sig')), delimiter=';')
        except zipfile.BadZipFile:
            raise Exception('File ZIP non valido.')
        except Exception:
            raise Exception('File CSV non valido.')
        else:
            headers_diff = set(self.CSV_REQUIRED_COLUMNS).difference(set(reader.fieldnames))
            if headers_diff:
                raise Exception('Formato colonne non valido. Le colonne mancanti sono: {}.'.format(list(headers_diff)))
            else:
                return self.process(list(reader))

    def process(self, data):
        raise NotImplementedError('subclasses of LoadCSVForm must provide a process() method')


class ImportFocusForm(LoadCSVForm):
    CSV_REQUIRED_COLUMNS = ('COD_LOCALE_PROGETTO', 'OC_FOCUS')

    def process(self, data):
        return data


class ImportProgrammiForm(LoadCSVForm):
    CSV_REQUIRED_COLUMNS = ('OC_CODICE_PROGRAMMA', 'OC_DESCRIZIONE_PROGRAMMA', 'OC_COD_CICLO')

    def process(self, data):
        programmi_esistenti_codici = list(Programma.objects.programmi().filter(codice__in=(r['OC_CODICE_PROGRAMMA'] for r in data)).values_list('codice', flat=True))

        objs = []
        for row in data:
            if row['OC_CODICE_PROGRAMMA'] not in programmi_esistenti_codici:
                objs.append(Programma(
                    tipo=Programma.TIPO.programma,
                    codice=row['OC_CODICE_PROGRAMMA'],
                    descrizione=row['OC_DESCRIZIONE_PROGRAMMA'],
                    ciclo_programmazione=row['OC_COD_CICLO'],
                ))

        objs = Programma.objects.bulk_create(objs)

        return {'processed': len(data), 'created': len(objs), 'existing': len(programmi_esistenti_codici)}


class ProgrammaFileInline(FileInline):
    exclude = ()


class ProgrammaLinkInline(LinkInline):
    exclude = ()


class FocusInline(TranslationTabularInline):
    model = Focus
    verbose_name = 'classe'
    verbose_name_plural = 'classi'
    fields = ('nome', 'num_progetti')
    readonly_fields = ('num_progetti',)
    extra = 0

    def num_progetti(self, obj):
        return obj.num_progetti
    num_progetti.short_description = 'numero di progetti'

    def get_queryset(self, request):
        from django.db.models import Count
        return super().get_queryset(request).annotate(num_progetti=Count('progetti'))


class ValutazioneInline(TranslationGenericStackedInline):
    model = Valutazione
    verbose_name_plural = 'valutazione'
    exclude = ('slug',)
    extra = 0
    max_num = 1

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in ('testo', 'testo_dati', 'testo_grafico'):
            kwargs['widget'] = CKEditorUploadingWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class TerritorioTemaSinteticoCoesioneInline(TranslationTabularInline):
    model = TerritorioTemaSinteticoCoesione
    verbose_name = 'tema sintetico'
    verbose_name_plural = 'temi sintetici'
    exclude = ('tema_sintetico_coesione',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'descrizione':
            kwargs['widget'] = CKEditorWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class TemaSinteticoIndicatoreInline(TranslationStackedInline):
    model = TemaSinteticoIndicatore
    verbose_name = 'indicatore'
    verbose_name_plural = 'indicatori'
    extra = 0

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'testo':
            kwargs['widget'] = CKEditorWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class SoggettoAdmin(admin.ModelAdmin):
    search_fields = ('denominazione',)


class TerritorioAdmin(admin.ModelAdmin):
    list_display = ('denominazione', 'tipo', 'cod_mac', 'cod_reg', 'cod_prov', 'cod_com')
    list_filter = ('tipo',)
    search_fields = ('denominazione',)


class TerritorioCoesioneAdmin(TabbedTranslationAdmin):
    list_display = ('denominazione', 'tipo', 'num_in_evidenza')
    exclude = ('capoluogo_nome', 'capoluogo_codice', 'superficie_hmq')
    readonly_fields = ('territorio',)
    inlines = (TerritorioTemaSinteticoCoesioneInline, ValutazioneInline)
    actions = ['reset_in_evidenza']

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def reset_in_evidenza(self, request, queryset):
        from django.contrib import messages
        from django.utils.translation import ngettext

        updated = 0
        for obj in queryset:
            updated += obj.territori_temi_sintetici_coesione_per_territorio.filter(in_evidenza=True).update(in_evidenza=False)

        self.message_user(
            request,
            ngettext(
                '"in evidenza" è stato resettato per %d tema sintetico.',
                '"in evidenza" è stato resettato per %d temi sintetici.',
                updated,
            )
            % updated,
            messages.SUCCESS,
        )

    reset_in_evidenza.short_description = 'Resetta "in evidenza" nei temi sintetici dei %(verbose_name_plural)s selezionati'

    def num_in_evidenza(self, obj):
        return len([o for o in obj.territori_temi_sintetici_coesione_per_territorio.all() if o.in_evidenza])
    num_in_evidenza.short_description = 'numero di temi sintetici in evidenza'

    def denominazione(self, obj):
        return obj.territorio.denominazione
    denominazione.admin_order_field = 'territorio__denominazione'

    def tipo(self, obj):
        return obj.territorio.get_tipo_display()
    tipo.admin_order_field = 'territorio__tipo'

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('territori_temi_sintetici_coesione_per_territorio')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in ('descrizione', 'testo_dati'):
            kwargs['widget'] = CKEditorWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class ClassificazioneAdmin(TabbedTranslationAdmin):
    list_display = ('descrizione', 'codice')

    # def get_queryset(self, request):
    #     qs = super().get_queryset(request)
    #     if not request.user.is_superuser:
    #         qs = qs.filter(classificazione_superiore__isnull=True)
    #     return qs

    # def get_list_filter(self, request):
    #     if not request.user.is_superuser:
    #         return ()
    #     return super().get_list_filter(request)

    # def get_fieldsets(self, request, obj=None):
    #     if not request.user.is_superuser and not self.fieldsets:
    #         self.fieldsets = ((None, {'fields': ('codice', 'descrizione')}),)
    #     return super().get_fieldsets(request, obj)

    def get_readonly_fields(self, request, obj=None):
        if not request.user.is_superuser:
            return self.readonly_fields + ('codice',)
        return super().get_readonly_fields(request, obj)


class ClassificazioneEstesaAdmin(ClassificazioneAdmin):
    list_display = ('descrizione', 'codice', 'priorita')
    list_editable = ('priorita',)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'descrizione_estesa':
            kwargs['widget'] = CKEditorWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)

    def get_prepopulated_fields(self, request, obj=None):
        if request.user.is_superuser:
            return {'slug': ('descrizione_breve_it',)}
        return super().get_prepopulated_fields(request, obj)

    def get_form(self, request, obj=None, **kwargs):
        if request.user.is_superuser:
            self.exclude = None
            self.prepopulated_fields = {'slug': ('descrizione_breve_it',)}
        else:
            self.exclude = ['slug']
            self.prepopulated_fields = {}
        return super().get_form(request, obj, **kwargs)

    # def get_fieldsets(self, request, obj=None):
    #     if not request.user.is_superuser and not self.fieldsets:
    #         self.fieldsets = ((None, {'fields': ('codice', 'descrizione', 'descrizione_breve', 'descrizione_estesa', 'priorita')}),)
    #     return super().get_fieldsets(request, obj)


class ClassificazioneAzioneAdmin(ClassificazioneEstesaAdmin):
    list_filter = ('tipo',)


class TemaSinteticoCoesioneAdmin(ClassificazioneEstesaAdmin):
    inlines = (TemaSinteticoIndicatoreInline, ValutazioneInline)
    actions = ['reset_in_evidenza']

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def reset_in_evidenza(self, request, queryset):
        from django.contrib import messages
        from django.utils.translation import ngettext

        updated = 0
        for obj in queryset:
            updated += obj.indicatori_sintesi.filter(in_evidenza=True).update(in_evidenza=False)

        self.message_user(
            request,
            ngettext(
                '"in evidenza" è stato resettato per %d indicatore.',
                '"in evidenza" è stato resettato per %d indicatori.',
                updated,
            )
            % updated,
            messages.SUCCESS,
        )

    reset_in_evidenza.short_description = 'Resetta "in evidenza" negli indicatori dei %(verbose_name_plural)s selezionati'

    def num_in_evidenza(self, obj):
        return len([o for o in obj.indicatori_sintesi.all() if o.in_evidenza])
    num_in_evidenza.short_description = 'numero di indicatori in evidenza'

    def get_list_display(self, request):
        return super().get_list_display(request) + ['num_in_evidenza']

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('indicatori_sintesi')

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'testo_dati':
            kwargs['widget'] = CKEditorWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class ClassificazioneOggettoAdmin(ClassificazioneAdmin):
    list_filter = ('tipo',)


class ClassificazioneQSNAdmin(ClassificazioneAdmin):
    list_filter = ('tipo',)


class FocusAdmin(TabbedTranslationAdmin):
    list_display = ('nome', 'tipo_focus', 'slug', 'data_aggiornamento', 'num_progetti', 'priorita')
    list_editable = ('priorita',)
    list_filter = ('tipo_focus',)
    exclude = ('classificazione_superiore', 'tipo')
    inlines = (FocusInline,)
    readonly_fields = ('num_progetti',)

    def num_progetti(self, obj):
        return obj.num_progetti
    num_progetti.short_description = 'numero di progetti'
    num_progetti.admin_order_field = 'num_progetti'

    def get_queryset(self, request):
        from django.db.models import Count
        return super().get_queryset(request).focus().annotate(num_progetti=Count('progetti') + Count('classificazione_set__progetti')).select_related('classificazione_superiore')

    def get_readonly_fields(self, request, obj=None):
        if not request.user.is_superuser:
            return self.readonly_fields + ('slug',)
        return super().get_readonly_fields(request, obj)

    def get_urls(self):
        from django.conf.urls import url
        urls = super().get_urls()
        my_urls = [
            url(r'^import/$', self.admin_site.admin_view(self.import_focus), name='{}_{}_import'.format(self.model._meta.app_label, self.model._meta.model_name)),
            url(r'^import/report/$', self.admin_site.admin_view(self.import_focus_report), name='{}_{}_importreport'.format(self.model._meta.app_label, self.model._meta.model_name)),
            url(r'^clearcache/$', self.admin_site.admin_view(self.clear_cache), name='{}_{}_clearcache'.format(self.model._meta.app_label, self.model._meta.model_name)),
        ]
        return my_urls + urls

    def import_focus(self, request):
        from django.shortcuts import redirect, render

        opts = self.model._meta

        form = ImportFocusForm(request.POST or None, request.FILES or None)
        if request.method == 'POST':
            if form.is_valid():
                try:
                    data = form.save()
                except Exception as e:
                    from django.contrib import messages
                    self.message_user(request, 'Import non riuscito. {}'.format(e), level=messages.ERROR)
                    return redirect('..')
                else:
                    import csv
                    import tempfile
                    from taskmanager.models import AppCommand, Task

                    task, _ = Task.objects.get_or_create(
                        command=AppCommand.objects.get_or_create(name='csvfocusimport', app_name='project.opencoesione.monitoraggio')[0],
                        defaults={
                            'name': 'CSV Focus Import',
                        }
                    )

                    if task.status == Task.STATUS_STARTED:
                        from django.contrib import messages
                        self.message_user(request, 'Import già in esecuzione. È necessario attendere la fine della procedura prima di eseguirla nuovamente.', level=messages.ERROR)
                    else:
                        if task.status != Task.STATUS_IDLE:
                            task.stop()

                        with tempfile.NamedTemporaryFile(mode='w', delete=False) as ftemp:
                            writer = csv.DictWriter(ftemp, fieldnames=data[0].keys())
                            writer.writeheader()
                            writer.writerows(data)
                            name = ftemp.name

                        task.arguments = '--filename {}'.format(name)

                        task.launch()

                    return redirect('admin:{}_{}_importreport'.format(opts.app_label, opts.model_name))

        context = {
            **self.admin_site.each_context(request),
            'title': 'Import {}'.format(opts.verbose_name_plural),
            'opts': opts,
            'form': form,
        }

        return render(request, 'admin/{}/{}/import_form.html'.format(opts.app_label, opts.model_name), context)

    def import_focus_report(self, request):
        from django.shortcuts import render
        from taskmanager.models import AppCommand, Task

        opts = self.model._meta

        task = Task.objects.get(command=AppCommand.objects.get(name='csvfocusimport'))

        context = {
            **self.admin_site.each_context(request),
            'title': 'Import {} - Report'.format(opts.verbose_name_plural),
            'opts': opts,
            'task': task,
        }

        return render(request, 'admin/{}/{}/import_report.html'.format(opts.app_label, opts.model_name), context)

    def clear_cache(self, request):
        from django.conf import settings
        from django.core.cache import cache
        from django.shortcuts import redirect
        from django.utils import translation

        objs = self.model.objects.all()

        for language in settings.LANGUAGES:
            with translation.override(language[0]):
                for obj in objs:
                    for ciclo_programmazione in [''] + [x[0] for x in settings.CICLO_PROGRAMMAZIONE]:
                        url = obj.get_absolute_url() + ('?ciclo_programmazione={}'.format(ciclo_programmazione) if ciclo_programmazione else '')
                        cache.delete('context{}'.format(url))

        self.message_user(request, 'Cache cancellata con successo.')

        return redirect('..')


class FonteAdmin(TabbedTranslationAdmin):
    list_display = ('descrizione_breve', 'codice')

    def get_readonly_fields(self, request, obj=None):
        if not request.user.is_superuser:
            return self.readonly_fields + ('tipo', 'codice')
        return super().get_readonly_fields(request, obj)


class ProgrammaAdmin(ClassificazioneAdmin):
    list_filter = ('tipo',)
    search_fields = ('descrizione', 'codice')
    inlines = (ProgrammaFileInline, ProgrammaLinkInline)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'descrizione_estesa':
            kwargs['widget'] = CKEditorWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)

    # def get_queryset(self, request):
    #     qs = super().get_queryset(request)
    #     if not request.user.is_superuser:
    #         qs = qs.programmi()
    #     return qs

    # def get_list_filter(self, request):
    #     if not request.user.is_superuser:
    #         return ()
    #     return super().get_list_filter(request)

    def get_fieldsets(self, request, obj=None):
        if not request.user.is_superuser and not self.fieldsets:
            self.fieldsets = ((None, {'fields': ('codice', 'descrizione', 'descrizione_estesa')}),)
        return super().get_fieldsets(request, obj)

    def get_urls(self):
        from django.conf.urls import url
        urls = super().get_urls()
        my_urls = [
            url(r'^import/$', self.admin_site.admin_view(self.import_programmi), name='{}_{}_import'.format(self.model._meta.app_label, self.model._meta.model_name)),
        ]
        return my_urls + urls

    def import_programmi(self, request):
        from django.shortcuts import redirect, render

        form = ImportProgrammiForm(request.POST or None, request.FILES or None)
        if request.method == 'POST':
            if form.is_valid():
                try:
                    counts = form.save()
                except Exception as e:
                    from django.contrib import messages
                    self.message_user(request, 'Import non riuscito. {}'.format(e), level=messages.ERROR)
                else:
                    self.message_user(request, 'Import completato con successo. Sono stati processati {processed} elementi ({created} creati, {existing} già esistenti)'.format(**counts))

                return redirect('..')

        opts = self.model._meta

        context = {
            **self.admin_site.each_context(request),
            'title': 'Import {}'.format(opts.verbose_name_plural),
            'opts': opts,
            'form': form,
        }

        return render(request, 'admin/{}/{}/import_form.html'.format(opts.app_label, opts.model_name), context)


class TerritorioS3Admin(ClassificazioneAdmin):
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'descrizione_estesa':
            kwargs['widget'] = CKEditorWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class StrategiaS3Admin(ClassificazioneAdmin):
    list_filter = ('tipo',)
    search_fields = ('descrizione', 'codice')

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'descrizione_estesa':
            kwargs['widget'] = CKEditorWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)

    def get_fieldsets(self, request, obj=None):
        if not request.user.is_superuser and not self.fieldsets:
            self.fieldsets = ((None, {'fields': ('descrizione', 'descrizione_estesa')}),)
        return super().get_fieldsets(request, obj)


class AreaAdmin(StrategiaS3Admin):
    exclude = ('localizzazioni',)


class IndicatoreRealizzazioneAdmin(TabbedTranslationAdmin):
    list_display = ('codice', 'descrizione', 'tipo', 'unita_misura')
    list_filter = ('tipo', 'unita_misura')
    search_fields = ('descrizione', 'codice')
    readonly_fields = ('codice', 'tipo', 'unita_misura')

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class IndicatoreRealizzazioneUnitaMisuraAdmin(TabbedTranslationAdmin):
    list_display = ('codice', 'descrizione')
    readonly_fields = ('codice',)

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Soggetto, SoggettoAdmin)
admin.site.register(Territorio, TerritorioAdmin)
admin.site.register(TerritorioCoesione, TerritorioCoesioneAdmin)
admin.site.register(ClassificazioneAzione, ClassificazioneAzioneAdmin)
admin.site.register(ClassificazioneOggetto, ClassificazioneOggettoAdmin)
admin.site.register(ClassificazioneQSN, ClassificazioneQSNAdmin)
admin.site.register(FaseProcedurale, ClassificazioneAdmin)
admin.site.register(Fonte, FonteAdmin)
admin.site.register(Focus, FocusAdmin)
admin.site.register(Programma, ProgrammaAdmin)
admin.site.register(Area, AreaAdmin)
admin.site.register(StrategiaS3, StrategiaS3Admin)
admin.site.register(TerritorioS3, TerritorioS3Admin)
admin.site.register(TemaPrioritario, ClassificazioneAdmin)
admin.site.register(TemaSinteticoCoesione, TemaSinteticoCoesioneAdmin)
admin.site.register(IndicatoreRealizzazione, IndicatoreRealizzazioneAdmin)
admin.site.register(IndicatoreRealizzazioneUnitaMisura, IndicatoreRealizzazioneUnitaMisuraAdmin)
