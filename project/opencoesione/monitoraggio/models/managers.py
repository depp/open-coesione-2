# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Subquery, Sum, Count, F
from django.db.models.functions import ExtractYear
from django.utils.functional import cached_property
from modeltranslation.manager import MultilingualQuerySet


class ProgettoQuerySet(models.QuerySet):
    def computabili(self, ciclo_programmazione=None, flag_visualizzazione=None):
        return self.filter(**self.model.computabili_filter_dict('', ciclo_programmazione, flag_visualizzazione))

    def no_privacy(self):
        return self.filter(is_privacy=False)

    def del_soggetto(self, soggetto):
        return self.filter(soggetti__pk=soggetto.pk)  #.distinct()

    def nei_territori(self, territori):
        from itertools import groupby
        from operator import itemgetter

        conditions = models.Q()
        for key, group in groupby(sorted([(k, v) for territorio in territori for k, v in territorio.get_filter_dict('territori__').items()], key=itemgetter(0)), key=itemgetter(0)):
            vals = list(map(itemgetter(1), group))
            conditions.add(models.Q(**{key: vals[0]} if len(vals) == 1 else {'{}__in'.format(key): vals}), models.Q.OR)
        return self.filter(conditions).distinct()

    def con_tema_sintetico(self, tema_sintetico):
        return self.filter(tema_sintetico=tema_sintetico)

    def con_classificazione_azione(self, classificazione_azione):
        if classificazione_azione.is_root:
            return self.filter(classificazione_azione__classificazione_superiore=classificazione_azione)
        else:
            return self.filter(classificazione_azione=classificazione_azione)

    def nel_focus(self, focus, localizzabili=False):
        extra_conditions = {'progetti_focus__regione__isnull': False} if localizzabili else {}
        if focus.has_children:
            return self.filter(focus__classificazione_superiore=focus, **extra_conditions)
        else:
            return self.filter(focus=focus, **extra_conditions)

    def in_area(self, area, regione=False):
        if area.is_root:
            from ..models import Territorio
            conditions = {'aree__classificazione_superiore': area}
            need_distinct = False
            if isinstance(regione, Territorio):
                conditions.update(regione.get_filter_dict('aree__classificazione_set__comuni__'))
                need_distinct = True
            queryset = self.filter(**conditions)
            if need_distinct:
                queryset = queryset.distinct()
            return queryset
        else:
            return self.filter(aree=area)

    def in_strategia_s3(self, strategia_s3, territorio_s3=False, territorio=False):
        from ..models import Territorio, TerritorioS3
        conditions = strategia_s3.get_filter_dict('strategie_s3__')
        if isinstance(territorio_s3, TerritorioS3):
            conditions.update({'strategie_s3__territorio': territorio_s3})
        if isinstance(territorio, Territorio):
            conditions.update(territorio.get_filter_dict('territori__'))
        queryset = self.filter(**conditions)
        if not strategia_s3.is_traiettoria:
            queryset = queryset.distinct()
        return queryset

    def con_programmi(self, programmi):
        return self.filter(ambiti__programma__classificazione_superiore__classificazione_superiore__in=programmi).distinct()

    def totali(self):
        return self._totali()[0]

    def totali_group_by(self, group_by):
        return self._totali(group_by)

    def _totali(self, group_by=None):
        from django.db import connection

        def dictfetchall(cursor):
            col_names = [x.name for x in cursor.description]
            for row in cursor.fetchall():
                yield dict(zip(col_names, row))

        queryset = self.annotate(costo_coesione=Sum('ambiti__costo_coesione'), pagamenti_coesione=Sum('ambiti__pagamenti_coesione')).values('codice_locale', 'finanz_totale_pubblico_netto', 'pagamento', 'costo_coesione', 'pagamenti_coesione')

        if group_by:
            alias = 'xxx'
            queryset = queryset.annotate(**{alias: F(group_by)})
            select = 'SELECT sq.{0} AS "id", {{}} GROUP BY sq.{0}'.format(alias)
        else:
            select = 'SELECT {}'

        sql, params = queryset.query.sql_with_params()

        sql = select.format(
            'COUNT(*) AS "totale_progetti", SUM(sq.finanz_totale_pubblico_netto) AS "totale_finanziamenti", SUM(sq.pagamento) AS "totale_pagamenti", SUM(sq.costo_coesione) AS "finanziamenti_coesione", SUM(sq.pagamenti_coesione) AS "pagamenti_coesione" FROM ({}) AS sq'.format(sql)
        )

        with connection.cursor() as cursor:
            cursor.execute(sql, params)
            totali = list(dictfetchall(cursor))

        # if group_by:
        #     totali = Subquery(queryset).queryset.values(id=F(group_by)).annotate(totale_finanziamenti=Sum('finanz_totale_pubblico_netto'), totale_pagamenti=Sum('pagamento'), totale_progetti=Count('codice_locale'))
        # else:
        #     totali = [Subquery(queryset).queryset.aggregate(totale_finanziamenti=Sum('finanz_totale_pubblico_netto'), totale_pagamenti=Sum('pagamento'), totale_progetti=Count('codice_locale'))]

        for item in totali:
            for key in item:
                if key not in ('id', 'totale_progetti'):
                    item[key] = round(float(item[key] or 0.0))

        return totali


class ClassificazioneAzioneQuerySet(MultilingualQuerySet):
    def principali(self):
        return self.filter(classificazione_superiore__isnull=True).filter(priorita__gt=0)


class FocusQuerySet(models.QuerySet):
    def focus(self):
        return self.filter(classificazione_superiore__isnull=True)


class RuoloQuerySet(models.QuerySet):
    def con_ruolo(self, ruolo):
        return self.filter(**{'progressivo_{}__gt'.format(ruolo): 0})


class PagamentoQuerySet(models.QuerySet):
    def per_anno(self):
        return self.annotate(anno=ExtractYear('data')).values('anno').annotate(ammontare=Sum('ammontare')).order_by('anno')


class ProgrammaQuerySet(models.QuerySet):
    def programmi(self):
        return self.filter(tipo=self.model.TIPO.programma)

    def programmi_con_progetti(self):
        return self.programmi().annotate(n=Count('classificazione_set__classificazione_set__ambiti')).filter(n__gt=0)


class AreaQuerySet(models.QuerySet):
    def tipi_aree(self):
        return self.filter(tipo=self.model.TIPO.tipo_area)

    def aree(self):
        return self.filter(tipo=self.model.TIPO.area)

    def partizioni(self):
        return self.filter(tipo=self.model.TIPO.partizione)

    def non_partizioni(self):
        return self.exclude(tipo=self.model.TIPO.partizione)


class TerritorioManager(models.Manager):
    @cached_property
    def macroaree_by_cod(self):
        return {o.cod_mac: o for o in self.macroaree()}

    @cached_property
    def regioni_by_cod(self):
        return {o.cod_reg: o for o in self.regioni()}

    @cached_property
    def province_by_cod(self):
        return {o.cod_prov: o for o in self.province()}


class TerritorioQuerySet(models.QuerySet):
    def nazionale(self):
        return self.get(tipo=self.model.TIPO.N)

    def macroaree(self):
        return self.filter(tipo=self.model.TIPO.M)

    def regioni(self, with_nations=False):
        codes = [self.model.TIPO.R]
        if with_nations:
            codes.append(self.model.TIPO.N)
            codes.append(self.model.TIPO.E)
        return self.filter(tipo__in=codes)

    def province(self):
        return self.filter(tipo=self.model.TIPO.P)

    def comuni(self):
        return self.filter(tipo=self.model.TIPO.C)

    def esteri(self):
        return self.filter(tipo=self.model.TIPO.E)

    def get_from_istat_code(self, istat_code):
        if istat_code is None or len(istat_code) != 9:
            return None
        else:
            return self.exclude(tipo=self.model.TIPO.M).get(cod_reg=int(istat_code[0:3]), cod_prov=int(istat_code[3:6]), cod_com=int(istat_code[3:9]))


class TerritorioCoesioneManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('territorio')


class TerritorioCoesioneQuerySet(models.QuerySet):
    # def filter(self, *args, **kwargs):
    #     if 'slug' in kwargs:
    #         kwargs['territorio__slug'] = kwargs.pop('slug')
    #     return super().filter(*args, **kwargs)

    def macroaree(self):
        return self.filter(territorio__tipo='M')

    def regioni(self):
        return self.filter(territorio__tipo='R')
