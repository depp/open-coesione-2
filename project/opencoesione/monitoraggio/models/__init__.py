# -*- coding: utf-8 -*-
import collections
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _, ugettext
from django_extensions.db.fields import AutoSlugField
from jsonfield import JSONField
from model_utils import Choices
from .classificazioni import (
    Area, ClassificazioneAzione, ClassificazioneOggetto, ClassificazioneQSN, Focus, Fonte, Programma,
    StrategiaS3, TerritorioS3, TemaSintetico, TemaPrioritario,
)
from .coesione import (
    TemaSinteticoCoesione, TemaSinteticoIndicatore, TerritorioCoesione, TerritorioTemaSinteticoCoesione, Valutazione
)
from .managers import ProgettoQuerySet, RuoloQuerySet, TerritorioQuerySet, TerritorioManager, PagamentoQuerySet


class Territorio(models.Model):
    TIPO = Choices(
        ('C', 'Comune'),
        ('P', 'Provincia'),
        ('R', 'Regione'),
        ('M', 'Macroarea'),
        ('N', 'Nazionale'),
        ('E', 'Estero'),
    )

    cod_mac = models.IntegerField(default=0, null=True, blank=True, db_index=True)
    cod_reg = models.IntegerField(default=0, null=True, blank=True, db_index=True)
    cod_prov = models.IntegerField(default=0, null=True, blank=True, db_index=True)
    cod_com = models.IntegerField(default=0, null=True, blank=True, db_index=True)

    denominazione = models.CharField(max_length=128, db_index=True)
    denominazione_ted = models.CharField(max_length=128, null=True, blank=True, db_index=True)

    slug = AutoSlugField(populate_from='nome_per_slug', max_length=256, unique=True)

    tipo = models.CharField(max_length=1, choices=TIPO, db_index=True)

    popolazione_totale = models.IntegerField(null=True, blank=True)
    popolazione_maschile = models.IntegerField(null=True, blank=True)
    popolazione_femminile = models.IntegerField(null=True, blank=True)

    centroid_lat = models.FloatField(null=True, blank=True)
    centroid_lon = models.FloatField(null=True, blank=True)

    objects = TerritorioManager.from_queryset(TerritorioQuerySet)()

    @property
    def nome(self):
        if self.is_nazionale:
            return ugettext('Ambito Nazionale')
        elif self.is_macroarea:
            return ugettext(self.denominazione)
        else:
            return ' - '.join(str(x) for x in (self.denominazione, self.denominazione_ted) if x)

    @property
    def nome_completo(self):
        if self.is_comune or self.is_provincia:
            return '{} di {}'.format(self.get_tipo_display(), self.nome)
        elif self.is_regione:
            return '{} {}'.format(self.get_tipo_display(), self.nome)
        else:
            return self.nome

    @property
    def nome_con_provincia(self):
        if self.is_provincia:
            return '{} ({})'.format(self.nome, _('provincia'))
        else:
            return self.nome

    @property
    def nome_per_slug(self):
        return '{} {}'.format(self.denominazione, self.get_tipo_display())

    @cached_property
    def macroarea(self):
        if self.is_comune or self.is_provincia or self.is_regione:
            return self.__class__.objects.macroaree_by_cod[self.cod_mac]
        else:
            return None

    @cached_property
    def regione(self):
        if self.is_comune or self.is_provincia:
            return self.__class__.objects.regioni_by_cod[self.cod_reg]
        else:
            return None

    @cached_property
    def provincia(self):
        if self.is_comune:
            return self.__class__.objects.province_by_cod[self.cod_prov]
        else:
            return None

    def get_filter_dict(self, prefix=''):
        """
        Returns: a dict with {prefix}cod_{type} key initialized with correct value
        """
        if self.is_comune:
            return {'{}cod_com'.format(prefix): self.cod_com}
        elif self.is_provincia:
            return {'{}cod_prov'.format(prefix): self.cod_prov}
        elif self.is_macroarea:
            return {'{}cod_mac'.format(prefix): self.cod_mac}
        else:
            return {'{}cod_reg'.format(prefix): self.cod_reg}

    def get_search_filter_querystring(self):
        import urllib.parse
        return urllib.parse.urlencode({k.replace('cod', 'territorio'): v for k, v in self.get_filter_dict().items()})

    def get_absolute_url(self):
        if self.is_estero:
            return reverse('territorioestero_list')
        else:
            return reverse('territorio_detail', kwargs={'slug': self.slug})

    def __getattr__(self, item):
        import re
        match = re.search('^is_({})$'.format('|'.join(x[1] for x in self.TIPO).lower()), item)
        if match:
            return self.get_tipo_display().lower() == match.group(1)
        else:
            raise AttributeError('{0!r} object has no attribute {1!r}'.format(self.__class__.__name__, item))

    def __str__(self):
        return '{}'.format(self.nome)

    class Meta:
        verbose_name = 'territorio'
        verbose_name_plural = 'territori'
        ordering = ['denominazione']


class Soggetto(models.Model):
    codice_fiscale = models.CharField(max_length=16)
    denominazione = models.CharField(max_length=512)
    slug = AutoSlugField(populate_from='denominazione_univoca', max_length=300, unique=True)
    territorio = models.ForeignKey(Territorio, null=True, on_delete=models.CASCADE)
    indirizzo = models.CharField(max_length=300, null=True, blank=True)
    cap = models.CharField(max_length=5, null=True, blank=True)
    is_privacy = models.BooleanField(default=False)

    @property
    def denominazione_univoca(self):
        return ('{} {}'.format(self.denominazione, '' if self.codice_fiscale == '*CODICE FISCALE*' else self.codice_fiscale)).strip()

    def get_search_filter_querystring(self):
        import urllib.parse
        return urllib.parse.urlencode({'soggetto': self.slug})

    def get_absolute_url(self):
        return reverse('soggetto_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.denominazione or self.codice_fiscale

    class Meta:
        verbose_name = 'soggetto'
        verbose_name_plural = 'soggetti'
        unique_together = ('denominazione', 'codice_fiscale')
        index_together = [
            ['denominazione', 'codice_fiscale']
        ]


class Progetto(models.Model):
    STATO = Choices(
        ('0', 'non_determinabile', _('Non determinabile')),
        ('1', 'non_avviato', _('Non avviato')),
        ('2', 'in_corso', _('In corso')),
        ('3', 'liquidato', _('Liquidato')),
        ('4', 'concluso', _('Concluso')),
    )
    STATO_PROCEDURALE = Choices(
        ('1', 'non_avviato', _('Non avviato')),
        ('2', 'in_avvio_progettazione', _('In avvio di progettazione')),
        ('3', 'in_corso_progettazione', _('In corso di progettazione')),
        ('4', 'in_affidamento', _('In affidamento')),
        ('5', 'in_esecuzione', _('In esecuzione')),
        ('6', 'eseguito', _('Eseguito')),
    )
    MACRO_AREA = Choices(
        ('C', 'centronord', _('Centro-Nord')),
        ('M', 'mezzogiorno', _('Mezzogiorno')),
        ('E', 'estero', _('Estero')),
        ('N', 'nazionale', _('Ambito Nazionale')),
        ('X', 'altro', _('Altro')),
    )

    codice_locale = models.CharField(max_length=100, unique=True)

    slug = AutoSlugField(populate_from='codice_locale', max_length=100, unique=True)

    is_active = models.BooleanField(default=True, db_index=True)
    is_privacy = models.BooleanField(default=False)

    flag_visualizzazione = models.CharField(max_length=1, db_index=True)

    cup = models.CharField(max_length=15, null=True, blank=True)

    titolo = models.TextField()

    sintesi = models.TextField(null=True, blank=True)
    sintesi_fonte = models.TextField(null=True, blank=True)

    stato = models.CharField(max_length=1, choices=STATO, null=True, blank=True, db_index=True)
    stato_procedurale = models.CharField(max_length=1, choices=STATO_PROCEDURALE, null=True, blank=True, db_index=True)

    macro_area = models.CharField(max_length=1, choices=MACRO_AREA, null=True, blank=True, db_index=True)

    finanz_totale_pubblico = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True, db_index=True)
    finanz_totale_pubblico_netto = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True, db_index=True)

    economie_totali = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True, db_index=True)
    economie_totali_pubbliche = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True, db_index=True)

    finanz_ue = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_stato_pac = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_stato_fondo_di_rotazione = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_stato_fsc = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_stato_altri_provvedimenti = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_regione = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_provincia = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_comune = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_risorse_liberate = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_altro_pubblico = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_stato_estero = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_privato = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    finanz_da_reperire = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)

    pagamento = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)

    data_inizio = models.DateField(null=True, blank=True)
    data_fine_prevista = models.DateField(null=True, blank=True)
    data_fine_effettiva = models.DateField(null=True, blank=True)

    data_aggiornamento = models.DateField(null=True, blank=True)

    classificazione_azione = models.ForeignKey(ClassificazioneAzione, related_name='progetti', on_delete=models.CASCADE)
    classificazione_oggetto = models.ForeignKey(ClassificazioneOggetto, related_name='progetti', null=True, blank=True, on_delete=models.CASCADE)
    tema_sintetico = models.ForeignKey(TemaSintetico, related_name='progetti', on_delete=models.CASCADE)

    soggetti = models.ManyToManyField(Soggetto, through='Ruolo', related_name='progetti')
    territori = models.ManyToManyField(Territorio, through='Localizzazione', related_name='progetti')
    aree = models.ManyToManyField(Area, related_name='progetti')
    strategie_s3 = models.ManyToManyField(StrategiaS3, related_name='progetti')

    focus = models.ManyToManyField(Focus, through='ProgettoFocus', related_name='progetti')

    progetti_attuati = models.ManyToManyField('self', symmetrical=False, related_name='progetti_attuatori')

    geo_data = JSONField(null=True, blank=True)

    csv_data = JSONField(load_kwargs={'object_pairs_hook': collections.OrderedDict})

    objects = ProgettoQuerySet.as_manager()

    @classmethod
    def computabili_filter_dict(cls, prefix='', ciclo_programmazione=None, flag_visualizzazione=None):
        if flag_visualizzazione is None:
            flag_visualizzazione = '0'
        if not isinstance(flag_visualizzazione, (list, tuple)):
            flag_visualizzazione = (flag_visualizzazione,)
        filter_dict = {'{}is_active'.format(prefix): True, '{}flag_visualizzazione__in'.format(prefix): flag_visualizzazione}
        if ciclo_programmazione and ciclo_programmazione in settings.CICLO_PROGRAMMAZIONE:
            filter_dict.update({'{}ambiti__ciclo_programmazione'.format(prefix): ciclo_programmazione})
        return filter_dict

    @classmethod
    def inv_stati_dict(cls):
        # build an inverse dictionary for STATO, code => identifier
        return {v: k for k, v in cls.STATO._identifier_map.items()}

    @property
    def costo_coesione(self):
        return sum(o.costo_coesione for o in self.ambiti.all())

    @property
    def pagamenti_coesione(self):
        return sum(o.pagamenti_coesione for o in self.ambiti.all())

    @property
    def fase_procedurale_esecutiva(self):
        try:
            return next(o for o in self.fasi_procedurali.all() if o.is_esecutiva)
        except StopIteration:
            return None

    @property
    def has_risorse_ue(self):
        from ..gruppo_programmi import GruppoProgrammi
        codici_programmi_ue = [o.codice for o in GruppoProgrammi('ue-0713').programmi + GruppoProgrammi('ue-1420').programmi]
        for programma in self.programmi:
            if programma.codice.split('/')[0] in codici_programmi_ue:
                return True
        return False

    @property
    def classificazioni_qsn(self):
        return set(o.classificazione_qsn for o in self.ambiti.all() if o.classificazione_qsn)

    @property
    def temi_prioritari(self):
        return set(o.tema_prioritario for o in self.ambiti.all() if o.tema_prioritario)

    @property
    def fonti(self):
        return set(o.fonte for o in self.ambiti.all() if o.fonte)

    @property
    def programmi(self):
        return [o.programma for o in self.ambiti.all()]

    @property
    def fonte_fs(self):
        try:
            return next(o for o in self.fonti if o.tipo == 'FS')
        except StopIteration:
            return None

    def _soggetti_per_tipo(self, tipo):
        return [o.soggetto for o in self.ruoli.all() if getattr(o, 'progressivo_{}'.format(tipo), 0) > 0]

    @property
    def programmatori(self):
        return self._soggetti_per_tipo('programmatore')

    @property
    def attuatori(self):
        return self._soggetti_per_tipo('attuatore')

    @property
    def beneficiari(self):
        return self._soggetti_per_tipo('beneficiario')

    @property
    def realizzatori(self):
        return self._soggetti_per_tipo('realizzatore')

    @property
    def ultimo_aggiornamento(self):
        # la data_aggiornamento potrebbe essere obsoleta rispetto ai pagamenti
        pagamenti = self.pagamenti.all()
        if not pagamenti and self.data_aggiornamento:
            return self.data_aggiornamento
        return max(self.data_aggiornamento, *[p.data for p in pagamenti])

    @property
    def percentuale_pagamenti(self):
        return self.percentuale_su_finanziamentopubblico(self.pagamento)

    def percentuale_su_finanziamentopubblico(self, pagamento):
        return int((pagamento or 0) / self.finanz_totale_pubblico_netto * 100) if self.finanz_totale_pubblico_netto else 0

    def get_absolute_url(self):
        return reverse('progetto_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.codice_locale

    class Meta:
        verbose_name = 'progetto'
        verbose_name_plural = 'progetti'
        indexes = [
            models.Index(
                fields=['-data_fine_effettiva', '-finanz_totale_pubblico_netto'],
                name="data_fine_finanz_pubb_netto"
            ),
        ]

class Ambito(models.Model):
    FLAG_PAC = Choices(
        ('0', 'Il progetto non appartiene al PAC'),
        ('1', 'Il progetto appartiene al PAC ed è finanziato con risorse dedicate, al di fuori dei Programmi Operativi'),
        ('2', "Il progetto appartiene al PAC ed è finanziato nell'ambito dei Programmi Operativi"),
    )
    FONDO_COMUNITARIO = Choices(
        ('FESR', _('Fondo Europeo di Sviluppo Regionale (FESR)')),
        ('FSE', _('Fondo Sociale Europeo (FSE)')),
        ('FEASR', _('Fondo Europeo Agricolo per lo Sviluppo Rurale (FEASR)')),
        ('FSC', _('Fondo per lo Sviluppo e la Coesione (FSC)')),
        ('PAC', _("Piano d'Azione Coesione (PAC)")),
        ('IOG', _('Iniziativa Occupazione Giovani (IOG)')),
        ('FEAMP', _('Fondo Europeo per gli Affari Marittimi e la Pesca (FEAMP)')),
    )

    progetto = models.ForeignKey(Progetto, related_name='ambiti', on_delete=models.CASCADE)

    ciclo_programmazione = models.CharField(max_length=1, choices=settings.CICLO_PROGRAMMAZIONE, db_index=True)

    flag_pac = models.CharField(max_length=1, choices=FLAG_PAC)
    fondo_comunitario = models.CharField(max_length=5, choices=FONDO_COMUNITARIO, null=True, blank=True)

    classificazione_qsn = models.ForeignKey(ClassificazioneQSN, related_name='ambiti', null=True, blank=True, on_delete=models.CASCADE)
    tema_prioritario = models.ForeignKey(TemaPrioritario, related_name='ambiti', null=True, blank=True, on_delete=models.CASCADE)
    fonte = models.ForeignKey(Fonte, related_name='ambiti', null=True, blank=True, on_delete=models.CASCADE)
    programma = models.ForeignKey(Programma, related_name='ambiti', on_delete=models.CASCADE)

    costo_coesione = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    pagamenti_coesione = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)

    csv_data = JSONField(load_kwargs={'object_pairs_hook': collections.OrderedDict})

    @classmethod
    def inv_cicli_programmazione_dict(cls):
        # build an inverse dictionary for settings.CICLO_PROGRAMMAZIONE, code => identifier
        return {v: k for k, v in settings.CICLO_PROGRAMMAZIONE._identifier_map.items()}

    def __str__(self):
        return '{} - {}'.format(self.progetto, self.programma_id)

    class Meta:
        verbose_name = 'ambito'
        verbose_name_plural = 'ambiti'
        unique_together = ('progetto', 'programma')


class Localizzazione(models.Model):
    progetto = models.ForeignKey(Progetto, on_delete=models.CASCADE)
    territorio = models.ForeignKey(Territorio, on_delete=models.CASCADE)

    indirizzo = models.CharField(max_length=550, null=True, blank=True)
    cap = models.CharField(max_length=5, null=True, blank=True)

    def __str__(self):
        return '{} - {}'.format(self.progetto, self.territorio)

    class Meta:
        verbose_name = 'localizzazione'
        verbose_name_plural = 'localizzazioni'


class Ruolo(models.Model):
    RUOLO = Choices(
        ('1', 'programmatore', _('Programmatore')),
        ('2', 'attuatore', _('Attuatore')),
        ('3', 'beneficiario', _('Beneficiario')),
        ('4', 'realizzatore', _('Realizzatore')),
    )

    progetto = models.ForeignKey(Progetto, related_name='ruoli', on_delete=models.CASCADE)
    soggetto = models.ForeignKey(Soggetto, related_name='ruoli', on_delete=models.CASCADE)

    progressivo_programmatore = models.PositiveIntegerField(default=0)
    progressivo_attuatore = models.PositiveIntegerField(default=0)
    progressivo_beneficiario = models.PositiveIntegerField(default=0)
    progressivo_realizzatore = models.PositiveIntegerField(default=0)

    objects = RuoloQuerySet.as_manager()

    @classmethod
    def inv_ruoli_dict(cls):
        # build an inverse dictionary for the ruoli, code => identifier
        return {v: k for k, v in cls.RUOLO._identifier_map.items()}

    def __str__(self):
        return '{} - {}'.format(self.progetto, self.soggetto)

    class Meta:
        verbose_name = 'ruolo'
        verbose_name_plural = 'ruoli'
        unique_together = ('progetto', 'soggetto')


class ProgettoFocus(models.Model):
    AMBITO = Choices(
        ('CTE', _('CTE')),
        ('FEAMP', _('FEAMP')),
        ('FEASR', _('FEASR')),
        ('FESR', _('FESR')),
        ('FSC', _('FSC')),
        ('FSE', _('FSE')),
        ('IOG', _('IOG')),
        ('PAC', _('PAC')),
        ('POC', _('POC')),
        ('SNAI', _('SNAI')),
    )

    progetto = models.ForeignKey(Progetto, related_name='progetti_focus', on_delete=models.CASCADE)
    focus = models.ForeignKey(Focus, related_name='progetti_focus', on_delete=models.CASCADE)

    ciclo_programmazione = models.CharField(max_length=1, choices=settings.CICLO_PROGRAMMAZIONE, db_index=True)
    ambito = models.CharField(max_length=5, choices=AMBITO, null=True, blank=True)
    regione = models.ForeignKey(Territorio, limit_choices_to={'tipo': 'R'}, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('progetto', 'focus')


class Pagamento(models.Model):
    progetto = models.ForeignKey(Progetto, related_name='pagamenti', on_delete=models.CASCADE)
    data = models.DateField()
    ammontare = models.DecimalField(max_digits=14, decimal_places=2)
    ammontare_fsc = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    ammontare_pac = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    ammontare_rendicontabile_ue = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True)
    codice_programma = models.CharField(max_length=32, null=True, blank=True)

    objects = PagamentoQuerySet.as_manager()

    @property
    def percentuale(self):
        return self.progetto.percentuale_su_finanziamentopubblico(self.ammontare)

    def __str__(self):
        return 'Pagamento del progetto {} del {} di {}'.format(self.progetto, self.data, self.ammontare)

    class Meta:
        verbose_name = 'pagamento'
        verbose_name_plural = 'pagamenti'
        ordering = ['data']


class Impegno(models.Model):
    progetto = models.ForeignKey(Progetto, related_name='impegni', on_delete=models.CASCADE)
    data = models.DateField()
    ammontare = models.DecimalField(max_digits=14, decimal_places=2)

    objects = PagamentoQuerySet.as_manager()

    def __str__(self):
        return 'Impegno del progetto {} del {} di {}'.format(self.progetto, self.data, self.ammontare)

    class Meta:
        verbose_name = 'impegno'
        verbose_name_plural = 'impegni'
        ordering = ['data']


class FaseProcedurale(models.Model):
    codice = models.CharField(max_length=4, primary_key=True)
    descrizione = models.CharField(max_length=50)
    is_esecutiva = models.BooleanField(default=False, db_index=True)

    def __str__(self):
        return '{} - {}'.format(self.codice, self.descrizione)

    class Meta:
        verbose_name = 'fase procedurale'
        verbose_name_plural = 'fasi procedurali'
        ordering = ['codice']


class ProgettoFaseProcedurale(models.Model):
    progetto = models.ForeignKey(Progetto, related_name='fasi_procedurali', on_delete=models.CASCADE)
    fase_procedurale = models.ForeignKey(FaseProcedurale, related_name='+', on_delete=models.CASCADE)

    data_inizio_prevista = models.DateField(null=True, blank=True)
    data_inizio_effettiva = models.DateField(null=True, blank=True)
    data_fine_prevista = models.DateField(null=True, blank=True)
    data_fine_effettiva = models.DateField(null=True, blank=True)

    @property
    def codice(self):
        return self.fase_procedurale_id

    @property
    def descrizione(self):
        return self.fase_procedurale.descrizione

    @property
    def is_esecutiva(self):
        return self.fase_procedurale.is_esecutiva

    def __str__(self):
        return 'Fase procedurale {} del progetto {}'.format(self.codice, self.progetto)

    class Meta:
        ordering = ['fase_procedurale']
        unique_together = ('progetto', 'fase_procedurale')


class IndicatoreRealizzazioneUnitaMisura(models.Model):
    codice = models.CharField(max_length=10, primary_key=True)
    descrizione = models.CharField(max_length=30)

    def __str__(self):
        return self.descrizione or self.codice

    class Meta:
        verbose_name = 'indicatore di realizzazione - unità di misura'
        verbose_name_plural = 'indicatori di realizzazione - unità di misura'
        ordering = ['codice']


class IndicatoreRealizzazione(models.Model):
    TIPO = Choices(
        ('COM', 'COM'),
        ('DPR', 'DPR'),
    )

    codice = models.CharField(max_length=50, primary_key=True)
    descrizione = models.CharField(max_length=300)
    tipo = models.CharField(max_length=3, choices=TIPO)
    unita_misura = models.ForeignKey(IndicatoreRealizzazioneUnitaMisura, related_name='indicatori_realizzazione', on_delete=models.CASCADE)

    def __str__(self):
        return '{} - {}'.format(self.codice, self.descrizione)

    class Meta:
        verbose_name = 'indicatore di realizzazione'
        verbose_name_plural = 'indicatori di realizzazione'
        ordering = ['codice']


class ProgettoIndicatoreRealizzazione(models.Model):
    progetto = models.ForeignKey(Progetto, related_name='indicatori_realizzazione', on_delete=models.CASCADE)
    indicatore_realizzazione = models.ForeignKey(IndicatoreRealizzazione, related_name='+', on_delete=models.CASCADE)

    valore_programmato = models.DecimalField(max_digits=16, decimal_places=2)
    valore_realizzato = models.DecimalField(max_digits=16, decimal_places=2, null=True, blank=True)

    flag_visualizzazione = models.CharField(max_length=1, db_index=True)

    @property
    def codice(self):
        return self.indicatore_realizzazione_id

    @property
    def descrizione(self):
        return self.indicatore_realizzazione.descrizione

    @property
    def tipo(self):
        return self.indicatore_realizzazione.tipo

    @property
    def unita_misura(self):
        return self.indicatore_realizzazione.unita_misura

    def __str__(self):
        return 'Indicatore di realizzazione {} del progetto {}'.format(self.codice, self.progetto)

    class Meta:
        ordering = ['indicatore_realizzazione']
        unique_together = ('progetto', 'indicatore_realizzazione')


class MonitoraggioASOC(models.Model):
    progetto = models.ForeignKey(Progetto, related_name='monitoraggi_asoc', on_delete=models.CASCADE)
    titolo_progetto = models.TextField()
    edizione_asoc = models.CharField(max_length=20)
    istituto_nome = models.CharField(max_length=255)
    istituto_comune = models.ForeignKey(Territorio, on_delete=models.CASCADE)
    team = models.CharField(max_length=255, null=True, blank=True)
    blog_url = models.URLField(null=True, blank=True)
    monithon_url = models.URLField(null=True, blank=True)
    elaborato_url = models.URLField(max_length=255, null=True, blank=True)
    asoc_stories_url = models.URLField(null=True, blank=True)
    experience_url = models.URLField(null=True, blank=True)

    @property
    def edizione(self):
        return '20{}-{}'.format(self.edizione_asoc[4:6], self.edizione_asoc[6:8])

    @property
    def istituto_regione(self):
        return self.istituto_comune.regione

    @property
    def istituto_provincia(self):
        return self.istituto_comune.provincia
