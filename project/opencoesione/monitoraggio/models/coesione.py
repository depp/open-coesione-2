# -*- coding: utf-8 -*-
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType, ContentTypeManager
from django.db import models
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

from . import TemaSintetico
from .managers import TerritorioCoesioneManager, TerritorioCoesioneQuerySet


def _get_opts(self, model, for_concrete_model):
    return model._meta


ContentTypeManager._get_opts = _get_opts


class Valutazione(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.CharField(max_length=255)
    content_object = GenericForeignKey(for_concrete_model=False)

    slug = models.SlugField(max_length=256, unique=True, null=True)
    testo = models.TextField(blank=True, null=True)
    testo_dati = models.TextField(blank=True, null=True)
    testo_grafico = models.TextField(blank=True, null=True)
    url = models.URLField(max_length=255, verbose_name='URL per approfondimento', blank=True, null=True)

    def save(self, *args, **kwargs):
        self.slug = getattr(self.content_object, 'slug', None)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('valutazione_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return str(self.content_object)

    class Meta:
        verbose_name = 'valutazione'
        verbose_name_plural = 'valutazioni'
        unique_together = ('content_type', 'object_id')


class ValutazioneMixin(models.Model):
    valutazioni = GenericRelation(Valutazione, for_concrete_model=False)

    @cached_property
    def valutazione(self):
        # return self.valutazioni.first()
        return list(self.valutazioni.all())[0]

    class Meta:
        abstract = True


class TemaSinteticoCoesione(ValutazioneMixin, TemaSintetico):
    def get_absolute_url(self):
        return reverse('temasinteticocoesione_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.descrizione_breve

    class Meta(TemaSintetico.Meta):
        proxy = True
        verbose_name = _('tema della coesione')
        verbose_name_plural = _('temi della coesione')


class TemaSinteticoIndicatore(models.Model):
    def upload_to(self, filename):
        return 'immagini/{}/indicatori/{}'.format(self.tema_sintetico.slug, filename)

    tema_sintetico = models.ForeignKey(TemaSinteticoCoesione, related_name='indicatori_sintesi', on_delete=models.CASCADE)

    titolo = models.CharField(max_length=200)
    testo = models.TextField()
    link = models.CharField(max_length=200, blank=True, null=True)

    titolo_dati = models.CharField(max_length=200)
    immagine_dati = models.ImageField(max_length=200, upload_to=upload_to)

    priorita = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')

    in_evidenza = models.BooleanField(default=False)

    def __str__(self):
        return self.titolo

    class Meta:
        verbose_name = 'indicatore tema sintetico'
        verbose_name_plural = 'indicatori tema sintetico'
        ordering = ['priorita', 'pk']


class TerritorioCoesione(ValutazioneMixin, models.Model):
    def upload_to(self, filename):
        return 'immagini/{}/{}'.format(self.slug, filename)

    territorio = models.OneToOneField('Territorio', limit_choices_to={'tipo__in': ('M', 'R')}, on_delete=models.CASCADE)

    descrizione = models.TextField()

    immagine = models.ImageField(max_length=200, upload_to=upload_to, blank=True, null=True)
    capoluogo_nome = models.CharField(max_length=20, blank=True, null=True)
    capoluogo_codice = models.CharField(max_length=9, blank=True, null=True)
    pil_procapite = models.PositiveIntegerField(blank=True, null=True)
    superficie_hmq = models.PositiveIntegerField()
    sito_web = models.URLField(blank=True, null=True)

    testo_dati = models.TextField(blank=True, null=True)

    objects = TerritorioCoesioneManager.from_queryset(TerritorioCoesioneQuerySet)()

    @property
    def slug(self):
        return self.territorio.slug

    @property
    def codice(self):
        return list(self.territorio.get_filter_dict().values())[0]

    @property
    def popolazione(self):
        return self.territorio.popolazione_totale

    @property
    def superficie_kmq(self):
        return self.superficie_hmq / 100

    @cached_property
    def num_regioni(self):
        if self.territorio.is_regione:
            return None
        return self.territorio.__class__.objects.regioni().filter(**self.territorio.get_filter_dict()).count()

    @cached_property
    def num_comuni(self):
        return self.territorio.__class__.objects.comuni().filter(**self.territorio.get_filter_dict()).count()

    def get_absolute_url(self):
        return reverse('territoriocoesione_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return str(self.territorio)

    class Meta:
        verbose_name = _('territorio della coesione')
        verbose_name_plural = _('territori della coesione')
        ordering = ['territorio__tipo', 'territorio__denominazione']


class TerritorioTemaSinteticoCoesione(models.Model):
    territorio_coesione = models.ForeignKey(TerritorioCoesione, related_name='territori_temi_sintetici_coesione_per_territorio', on_delete=models.CASCADE)
    tema_sintetico_coesione = models.ForeignKey(TemaSinteticoCoesione, related_name='territori_temi_sintetici_coesione_per_tema_sintetico', on_delete=models.CASCADE)

    descrizione = models.TextField()

    indicatore_valore = models.CharField(max_length=50, blank=True, null=True)
    indicatore_unitamisura = models.CharField(max_length=200, blank=True, null=True)

    in_evidenza = models.BooleanField(default=False)

    def __str__(self):
        return str(self.tema_sintetico_coesione)

    class Meta:
        verbose_name = 'tema sintetico per territorio'
        verbose_name_plural = 'temi sintetici per territorio'
        ordering = ['territorio_coesione__territorio__denominazione', 'tema_sintetico_coesione__priorita']
