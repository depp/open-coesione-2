# -*- coding: utf-8 -*-
import urllib.parse
from django.contrib.contenttypes.fields import GenericRelation
from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils.text import format_lazy
from django.utils.translation import ugettext_lazy as _
from model_utils import Choices

from .managers import AreaQuerySet, ClassificazioneAzioneQuerySet, FocusQuerySet, ProgrammaQuerySet
from ...models import File, Link


class ClassificazioneBase(models.Model):
    codice = models.CharField(max_length=32, primary_key=True)
    descrizione = models.TextField()

    def __str__(self):
        return '{} - {}'.format(self.codice, self.descrizione)

    class Meta:
        abstract = True
        ordering = ['codice']


class ClassificazioneEstesaBase(ClassificazioneBase):
    descrizione_breve = models.CharField(max_length=64, null=True, blank=True)
    descrizione_estesa = models.TextField(null=True, blank=True)
    slug = models.SlugField(max_length=64, unique=True, null=True, blank=True)
    priorita = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')

    class Meta:
        abstract = True
        ordering = ['priorita', 'codice']


class ClassificazioneGerarchicaBase(models.Model):
    TIPO = Choices()

    classificazione_superiore = models.ForeignKey('self', default=None, related_name='classificazione_set', null=True, blank=True, on_delete=models.CASCADE)
    tipo = models.CharField(max_length=25, choices=TIPO)

    @property
    def is_root(self):
        return self.classificazione_superiore_id is None

    @property
    def has_children(self):
        return self.classificazione_set.exists()

    @property
    def get_tipo_display(self):
        return self.TIPO[self.tipo]

    def __getattr__(self, item):
        import re
        match = re.search('^is_({})$'.format('|'.join(x[0] for x in self.TIPO).lower()), item)
        if match:
            return self.tipo == match.group(1)
        else:
            raise AttributeError('{0!r} object has no attribute {1!r}'.format(self.__class__.__name__, item))

    class Meta:
        abstract = True


class Programma(ClassificazioneBase, ClassificazioneGerarchicaBase):
    TIPO = Choices(
        ('programma', _('Programma')),
        ('asse', _('Asse')),
        ('obiettivo_operativo', _('Obiettivo operativo')),
        ('obiettivo_specifico', _('Obiettivo specifico')),
        ('linea', _('Linea')),
        ('azione', _('Azione')),
        ('priorita', _('Priorità')),
        ('tipo_intervento', _('Tipo intervento')),
        ('settore_strategico_fsc', _('Settore strategico FSC')),
        ('asse_tematico_fsc', _('Asse tematico FSC')),
        ('asse_tematico_pac', _('Asse tematico PAC')),
        ('linea_azione_pac', _('Linea azione PAC')),
        ('misura', _('Misura')),
        ('area_snai', _('Area Interna')),
        ('ambito_snai', _('Ambito di Intervento')),
        ('area_tematica_psc', _('Area tematica')),
        ('sett_interv_psc', _('Settore di intervento')),
    )
    # CICLO_PROGRAMMAZIONE = Choices(
    #     ('9', _('FSC da ciclo 2000-2006')),
    #     ('1', format_lazy(_('Ciclo di programmazione {}'), '2007-2013')),
    #     ('2', format_lazy(_('Ciclo di programmazione {}'), '2014-2020')),
    #     ('3', format_lazy(_('Ciclo di programmazione {}'), '2021-2027')),
    # )

    ciclo_programmazione = models.CharField(max_length=1, choices=settings.CICLO_PROGRAMMAZIONE, db_index=True)
    descrizione_estesa = models.TextField(null=True, blank=True)

    documenti = GenericRelation(File)
    collegamenti = GenericRelation(Link)

    objects = ProgrammaQuerySet.as_manager()

    def get_search_filter_querystring(self):
        return urllib.parse.urlencode({'programma': self.codice})

    def get_absolute_url(self):
        return reverse('programma_detail', kwargs={'pk': self.codice})

    class Meta(ClassificazioneBase.Meta):
        verbose_name = 'programma'
        verbose_name_plural = 'programmi'


class Area(ClassificazioneBase, ClassificazioneGerarchicaBase):
    TIPO = Choices(
        ('tipo_area', _('Tipo area')),
        ('area', _('Area')),
        ('partizione', _('Partizione')),
    )

    descrizione_estesa = models.TextField(null=True, blank=True)

    comuni = models.ManyToManyField('Territorio', limit_choices_to={'tipo': 'C'}, related_name='aree', blank=True)
    localizzazioni = models.ManyToManyField('Localizzazione', limit_choices_to={'territorio__tipo': 'C'}, related_name='aree', blank=True)

    objects = AreaQuerySet.as_manager()

    @property
    def lista_comuni(self):
        if self.tipo == self.TIPO.partizione:
            return self.comuni.all()
        else:
            return set(comune for child in self.classificazione_set.all() for comune in child.lista_comuni)

    @property
    def lista_regioni(self):
        return sorted(set(comune.regione for comune in self.lista_comuni), key=lambda o: o.nome)

    @property
    def popolazione_totale(self):
        return sum(comune.popolazione_totale for comune in self.lista_comuni)

    @property
    def get_ciclo_programmazione_display(self):
        return format_lazy(_('Ciclo di programmazione {}'), '2014-2020')

    def get_search_filter_querystring(self):
        if self.is_root:
            return urllib.parse.urlencode({'selected_facets': 'tipo_area:{}'.format(self.codice)}, safe=':')
        else:
            return urllib.parse.urlencode({'area': self.codice})

    def get_absolute_url(self):
        return reverse('area_detail', kwargs={'pk': self.codice})

    def __str__(self):
        return self.descrizione

    class Meta(ClassificazioneBase.Meta):
        verbose_name = 'area'
        verbose_name_plural = 'aree'


class TerritorioS3(ClassificazioneBase):
    descrizione_estesa = models.TextField(null=True, blank=True)

    def get_absolute_url(self):
        return reverse('strategias3root_territorios3_detail', kwargs={'pk': 'S3', 'territorio_s3_pk': self.codice})

    class Meta(ClassificazioneBase.Meta):
        verbose_name = 'strategia di sviluppo intelligente - territorio'
        verbose_name_plural = 'strategie di sviluppo intelligente - territori'


class StrategiaS3(ClassificazioneBase, ClassificazioneGerarchicaBase):
    TIPO = Choices(
        ('strategia', _('Strategia')),
        ('tipo_strategia', _('Tipo strategia')),
        ('specializzazione', _('Area di specializzazione')),
        ('traiettoria', _('Traiettoria di sviluppo')),
    )

    descrizione_estesa = models.TextField(null=True, blank=True)
    territorio = models.ForeignKey(TerritorioS3, null=True, blank=True, on_delete=models.CASCADE)

    def get_filter_dict(self, prefix=''):
        if self.is_strategia:
            lookup = '{}classificazione_superiore__classificazione_superiore__classificazione_superiore_id'
        elif self.is_tipo_strategia:
            lookup = '{}classificazione_superiore__classificazione_superiore_id'
        elif self.is_specializzazione:
            lookup = '{}classificazione_superiore_id'
        else:
            lookup = '{}pk'
        return {lookup.format(prefix): self.pk}

    def get_search_filter_querystring(self):
        if self.is_root:
            return urllib.parse.urlencode({'selected_facets': 'tipo_area:{}'.format(self.codice)}, safe=':')
        else:
            return urllib.parse.urlencode({'s3': self.codice})

    def get_absolute_url(self):
        if self.is_root:
            viewname = 'strategias3root_detail'
        else:
            viewname = 'strategias3_detail'
        return reverse(viewname, kwargs={'pk': self.codice})

    def __str__(self):
        return self.descrizione

    class Meta(ClassificazioneBase.Meta):
        verbose_name = 'strategia di sviluppo intelligente'
        verbose_name_plural = 'strategie di sviluppo intelligente'


class ClassificazioneOggetto(ClassificazioneBase, ClassificazioneGerarchicaBase):
    TIPO = Choices(
        ('settore', _('Settore')),
        ('sottosettore', _('Sotto settore')),
        ('categoria', _('Categoria')),
    )

    class Meta(ClassificazioneBase.Meta):
        verbose_name = 'classificazione oggetto'
        verbose_name_plural = 'classificazioni oggetto'


class ClassificazioneQSN(ClassificazioneBase, ClassificazioneGerarchicaBase):
    TIPO = Choices(
        ('priorita', _('Priorità')),
        ('obiettivo_generale', _('Obiettivo generale')),
        ('obiettivo_specifico', _('Obiettivo specifico'))
    )

    class Meta(ClassificazioneBase.Meta):
        verbose_name = 'classificazione QSN'
        verbose_name_plural = 'classificazioni QSN'


class ClassificazioneAzione(ClassificazioneEstesaBase, ClassificazioneGerarchicaBase):
    TIPO = Choices(
        ('natura', _('Natura')),
        ('tipologia', _('Tipologia')),
    )

    objects = ClassificazioneAzioneQuerySet.as_manager()

    def get_search_filter_querystring(self):
        return urllib.parse.urlencode({'selected_facets': 'natura:{}'.format(self.codice)}, safe=':')

    def get_absolute_url(self):
        return reverse('classificazioneazione_detail', kwargs={'slug': self.slug})

    class Meta(ClassificazioneEstesaBase.Meta):
        verbose_name = 'classificazione azione'
        verbose_name_plural = 'classificazioni azione'


class TemaSintetico(ClassificazioneEstesaBase):
    def upload_to(self, filename):
        return 'immagini/{}/{}'.format(self.slug, filename)

    immagine = models.ImageField(max_length=200, upload_to=upload_to, blank=True, null=True)

    testo_dati = models.TextField(blank=True, null=True)
    immagine_dati = models.ImageField(max_length=200, upload_to=upload_to, blank=True, null=True)

    def get_search_filter_querystring(self):
        return urllib.parse.urlencode({'selected_facets': 'tema:{}'.format(self.codice)}, safe=':')

    def get_absolute_url(self):
        return reverse('temasintetico_detail', kwargs={'slug': self.slug})

    class Meta(ClassificazioneEstesaBase.Meta):
        verbose_name = 'tema sintetico'
        verbose_name_plural = 'temi sintetici'


class TemaPrioritario(ClassificazioneBase):
    class Meta(ClassificazioneBase.Meta):
        verbose_name = 'tema prioritario'
        verbose_name_plural = 'temi prioritari'


class Fonte(ClassificazioneBase):
    descrizione_breve = models.CharField(max_length=64, null=True, blank=True)

    def get_search_filter_querystring(self):
        return urllib.parse.urlencode({'selected_facets': 'fonte:{}'.format(self.codice)}, safe=':')

    @property
    def tipo(self):
        import re
        return re.sub(r'\d', '', self.codice)

    class Meta(ClassificazioneBase.Meta):
        verbose_name = 'fonte'
        verbose_name_plural = 'fonti'


class Focus(ClassificazioneGerarchicaBase):
    TIPO = Choices(
        ('focus', _('Focus')),
        ('classe', _('Classe')),
    )

    TIPO_FOCUS = Choices(
        ('P', _('Policy')),
        ('S', _('Strategia')),
    )

    nome = models.CharField(max_length=40)
    slug = models.SlugField(max_length=40, unique=True)
    tipo_focus = models.CharField(max_length=1, choices=TIPO_FOCUS, default=TIPO_FOCUS.P)
    data_aggiornamento = models.DateField(null=True, blank=True)
    priorita = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')

    file_dati = models.OneToOneField('opendata.Dataset', limit_choices_to={'slug__startswith': 'focus_'}, related_name='+', null=True, blank=True, on_delete=models.SET_NULL)
    file_metadati = models.OneToOneField('opendata.MetadataFile', limit_choices_to={'slug__startswith': 'focus_'}, related_name='+', null=True, blank=True, on_delete=models.SET_NULL)

    objects = FocusQuerySet.as_manager()

    def get_search_filter_querystring(self):
        return urllib.parse.urlencode({'selected_facets': 'focus:{}'.format(self.slug)}, safe=':')

    def get_absolute_url(self):
        return reverse('focus_detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        self.tipo = self.TIPO.classe if self.classificazione_superiore_id else self.TIPO.focus
        super().save(*args, **kwargs)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'focus'
        verbose_name_plural = 'focus'
        ordering = ['priorita', 'nome']
