# -*- coding: utf-8 -*-
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *
from django.conf.urls import url, include


router = routers.SimpleRouter()
router.register(r'progetti', ProgettoViewSet, basename='api-progetto')
router.register(r'soggetti', SoggettoViewSet, basename='api-soggetto')
router.register(r'nature', NaturaViewSet, basename='api-classificazioneazione')
router.register(r'temi', TemaViewSet, basename='api-temasintetico')
router.register(r'programmi', ProgrammaViewSet, basename='api-programma')
router.register(r'territori', TerritorioViewSet, basename='api-territorio')

urlpatterns = [
    url(r'^$', ApiRootView.as_view(), name='api-root'),

    url(r'^aggregati/$', AggregatoGlobaleView.as_view(), name='api-aggregati-globale'),
    url(r'^aggregati/temi/(?P<slug>[\w-]+)$', AggregatoTemaDetailView.as_view(), name='api-aggregati-temasintetico-detail'),
    url(r'^aggregati/nature/(?P<slug>[\w-]+)$', AggregatoNaturaDetailView.as_view(), name='api-aggregati-classificazioneazione-detail'),
    url(r'^aggregati/territori/(?P<slug>[\w-]+)$', AggregatoTerritorioDetailView.as_view(), name='api-aggregati-territorio-detail'),
    url(r'^aggregati/programmi/(?P<pk>[\w-]+)$', AggregatoProgrammaDetailView.as_view(), name='api-aggregati-programma-detail'),

    url(r'^', include(router.urls)),

    url(r'^api-auth/', include('rest_framework.urls')),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])
