# -*- coding: utf-8 -*-
from collections import OrderedDict

from django.conf import settings
from django.db.models import Prefetch
from django.test import RequestFactory
from django.urls import resolve
from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from ..models import (
    ClassificazioneAzione, TemaSintetico, Programma, Territorio, Progetto, Ruolo, Soggetto, ProgettoFaseProcedurale,
    Focus, Fonte, ProgettoIndicatoreRealizzazione, Ambito,
)
from ..views import ProgettoSearchView, SoggettoSearchView
from .serializers import (
    ClassificazioneAzioneDetailModelSerializer, ClassificazioneAzioneListModelSerializer,
    TemaSinteticoDetailModelSerializer, TemaSinteticoListModelSerializer,
    ProgrammaDetailModelSerializer, ProgrammaListModelSerializer,
    ProgettoDetailModelSerializer, ProgettoSearchResultSerializer,
    SoggettoDetailModelSerializer, SoggettoSearchResultSerializer,
    TerritorioModelSerializer,
)


class ExtendedPageNumberPagination(PageNumberPagination):
    page_size_query_param = 'page_size'
    max_page_size = 500

    def get_paginated_response(self, data):
        response = super().get_paginated_response(data)

        response.data['page_size'] = self.get_page_size(self.request)
        response.data['current_page'] = self.page.number
        response.data['last_page'] = self.page.paginator.num_pages

        response.data.move_to_end('results')

        return response


class BaseViewSet(viewsets.ReadOnlyModelViewSet):
    pagination_class = ExtendedPageNumberPagination

    def get_serializer_class(self):
        serializer_class = super().get_serializer_class()
        if self.detail:
            serializer_class = globals()[serializer_class.__name__.replace('ListModel', 'DetailModel')]
        return serializer_class

    def get_view_description(self, html=False):
        if self.detail:
            return ''
        return super().get_view_description(html)


class BaseSearchViewSet(BaseViewSet):
    @staticmethod
    def _get_objects_by_pk(pks):
        return {}

    def _hydrate(self, object_list):
        objects_by_pk = self._get_objects_by_pk(object.pk for object in object_list)

        for object in object_list:
            try:
                object.object = objects_by_pk[object.pk]
            except KeyError:
                pass

        return object_list

    def _get_facet_counts(self):
        facet_counts = self.get_searchqueryset().facet_counts()

        facets_cod2slug = [
            ('ciclo_programmazione', Ambito.inv_cicli_programmazione_dict()),
            ('natura', {x['codice']: x['slug'] for x in ClassificazioneAzione.objects.principali().values('codice', 'slug')}),
            ('tema', {x['codice']: x['slug'] for x in TemaSintetico.objects.all().values('codice', 'slug')}),
            ('fonte', {x['codice']: x['codice'] for x in Fonte.objects.values('codice')}),
            ('stato', Progetto.inv_stati_dict()),
            ('focus', {x['slug']: x['slug'] for x in Focus.objects.values('slug')}),
            ('ruolo', Ruolo.inv_ruoli_dict()),
        ]

        return OrderedDict([
            (facet_name, [
                '{} ({})'.format(facet_cod2slug[x[0]], x[1]) for x in facet_counts['fields'][facet_name] if x[1] and x[0] in facet_cod2slug
            ]) for facet_name, facet_cod2slug in facets_cod2slug if facet_name in facet_counts['fields']
        ])

    def get_searchqueryset(self):
        raise NotImplementedError('{}.get_searchqueryset() must be implemented'.format(self.__class__.__name__))

    def get_serializer_class(self):
        serializer_class = super().get_serializer_class()
        if self.detail:
            serializer_class = globals()[serializer_class.__name__.replace('SearchResult', 'DetailModel')]
        return serializer_class

    def list(self, request, *args, **kwargs):
        queryset = self.get_searchqueryset()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(self._hydrate(page), many=True)
            response = self.get_paginated_response(serializer.data)
            response.data['facet_counts'] = self._get_facet_counts()
            response.data.move_to_end('results')
            return response

        serializer = self.get_serializer(self._hydrate(queryset), many=True)
        return Response(OrderedDict([
            ('facet_counts', self._get_facet_counts()),
            ('results', serializer.data),
        ]))


class AggregatoMixin(object):
    def get_aggregate_page_url(self):
        raise NotImplementedError('{}.get_aggregate_page_url() must be implemented'.format(self.__class__.__name__))

    def get_extra_context(self, object):
        return None

    def _get_aggregate_page_context(self, ciclo_programmazione, *args, **kwargs):
        url = self.get_aggregate_page_url()

        resolver = resolve(url)

        view = resolver.func.view_class()

        if ciclo_programmazione:
            url += '?ciclo_programmazione={}'.format(getattr(settings.CICLO_PROGRAMMAZIONE, ciclo_programmazione))

        view.request = RequestFactory().get(url)
        view.args = resolver.args
        view.kwargs = resolver.kwargs

        if hasattr(view, 'get_object'):
            view.object = view.get_object()
        elif hasattr(view, 'get_queryset'):
            view.object_list = view.get_queryset()

        return view.get_context_data(*args, **kwargs)

    def _get_totali(self, context):
        for key in ('totale_finanziamenti', 'finanziamenti_coesione', 'totale_pagamenti', 'pagamenti_coesione'):
            context[key] = str(context.get(key, 0))
            if '.' in context[key]:
                context[key] = context[key].replace('.', ',')
            else:
                context[key] += ',00'

        return OrderedDict([
            ('costo_pubblico', context['totale_finanziamenti']),
            ('costo_pubblico_coesione', context['finanziamenti_coesione']),
            ('pagamenti', context['totale_pagamenti']),
            ('pagamenti_coesione', context['pagamenti_coesione']),
            ('progetti', str(context['totale_progetti'])),
        ])

    def get_aggregate_data(self, request, format=None, *args, **kwargs):
        ciclo_programmazione = request.query_params.get('ciclo_programmazione', '')
        if not hasattr(settings.CICLO_PROGRAMMAZIONE, ciclo_programmazione):
            ciclo_programmazione = None

        context = self._get_aggregate_page_context(ciclo_programmazione, *args, **kwargs)

        return OrderedDict([
            ('contesto', self.get_extra_context(context.get('object'))),
            ('data_aggiornamento', settings.DATA_AGGIORNAMENTO.replace('-', '')),
            ('aggregati', OrderedDict([
                ('totali', self._get_totali(context)),
                ('stati_progetti', OrderedDict([
                    (Progetto.inv_stati_dict()[k], {
                        'label': Progetto.STATO[k],
                        'totali': self._get_totali(v),
                    }) for k, v in context.get('stati_progetto', {}).items()
                ])),
                ('temi', OrderedDict([
                    (x['slug'], {
                        'label': x['nome'],
                        'link': reverse('api-aggregati-temasintetico-detail', request=request, format=format, kwargs={'slug': x['slug']}) + ('?ciclo_programmazione={}'.format(ciclo_programmazione) if ciclo_programmazione else ''),
                        'totali': self._get_totali(x),
                    }) for x in context.get('temi_principali', [])
                ])),
                ('nature', OrderedDict([
                    (x['slug'], {
                        'label': x['nome'],
                        'link': reverse('api-aggregati-classificazioneazione-detail', request=request, format=format, kwargs={'slug': x['slug']}) + ('?ciclo_programmazione={}'.format(ciclo_programmazione) if ciclo_programmazione else ''),
                        'totali': self._get_totali(x),
                    }) for x in context.get('classificazioniazione_principali', [])
                ])),
                ('territori', OrderedDict([
                    (c, OrderedDict([
                        (x['slug'], {
                            'label': x['nome'],
                            'link': reverse('api-aggregati-territorio-detail', request=request, format=format, kwargs={'slug': x['slug']}) + ('?ciclo_programmazione={}'.format(ciclo_programmazione) if ciclo_programmazione else '') if x['slug'] and not x['slug'].endswith('-estero') else '',
                            'totali': self._get_totali(x),
                        }) for x in context[c]
                    ])) for c in ('regioni', 'province', 'comuni', 'territori') if c in context
                ])),
                ('impegni_e_pagamenti_per_anno', context['impegnipagamenti_per_anno']),
            ])),
        ])


class BaseAggregatoView(AggregatoMixin, APIView):
    def get(self, request, format=None, *args, **kwargs):
        return Response(self.get_aggregate_data(request, format, *args, **kwargs))


class ApiRootView(APIView):
    """
    This is the root entry-point of the OpenCoesione APIs.

    The APIs are read-only, freely accessible to all through HTTP requests at ``http://www.opencoesione.gov.it/api``.

    Responses are emitted in both **browseable-HTML** and **JSON** formats (``http://www.opencoesione.gov.it/api/.json``)

    To serve all requests and avoid slow responses or downtimes due to misuse, we limit the requests rate.
    When accessing the API **anonymously**, your client is limited to **12 requests per minute** from the same IP.
    You can contact us to become an **authenticated API user** (it's still free),
    then the rate-limit would be lifted to **1 request per second**.

    Authentication is done through HTTP Basic Authentication.

    You can request a username/password to authenticate,
    by writing an email to the following address: info@opencoesione.gov.it.

    If for some reasons, you need to scrape all the OpenCoesione data, please consider a bulk **CSV download**.
    See the <http://www.opencoesione.gov.it/opendata/> page in the web site.
    """
    def get(self, request, format=None):
        return Response(OrderedDict([
            ('progetti', reverse('api-progetto-list', request=request, format=format)),
            ('soggetti', reverse('api-soggetto-list', request=request, format=format)),
            ('aggregati', reverse('api-aggregati-globale', request=request, format=format)),
            ('temi', reverse('api-temasintetico-list', request=request, format=format)),
            ('nature', reverse('api-classificazioneazione-list', request=request, format=format)),
            ('territori', reverse('api-territorio-list', request=request, format=format)),
            ('programmi', reverse('api-programma-list', request=request, format=format)),
            ('data_aggiornamento', settings.DATA_AGGIORNAMENTO.replace('-', '')),
        ]))


class NaturaViewSet(BaseViewSet):
    """
    Elenco di tutte le **nature**.

    Le pagine possono essere navigate tramite i link "next" e "previous".

    La scheda di dettaglio può essere raggiunta tramite il link "url" di ogni elemento della lista.
    """
    serializer_class = ClassificazioneAzioneListModelSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        return ClassificazioneAzione.objects.principali()


class TemaViewSet(BaseViewSet):
    """
    Elenco di tutti i **temi**.

    Le pagine possono essere navigate tramite i link "next" e "previous".

    La scheda di dettaglio può essere raggiunta tramite il link "url" di ogni elemento della lista.
    """
    serializer_class = TemaSinteticoListModelSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        return TemaSintetico.objects.all()


class ProgrammaViewSet(BaseViewSet):
    """
    Elenco di tutti i **programmi operativi**.

    Le pagine possono essere navigate tramite i link "next" e "previous".

    La scheda di dettaglio può essere raggiunta tramite il link "url" di ogni elemento della lista.

    Can be filtered by ``descrizione`` GET parameter, to extract all items containing a given substring, case-insensitive.

    Examples
    ========

    To filter ``FSE`` and ``FESR`` groups::

        GET api/programmi?descrizione=FSE
        GET api/programmi?descrizione=FESR

    To get all programmi for a given region

        GET api/programmi?descrizione=Lazio

    To filter on codice::

        GET api/programmmi?codice=2007IT052PO012

    """
    serializer_class = ProgrammaListModelSerializer

    def get_queryset(self):
        queryset = Programma.objects.programmi().order_by('descrizione')

        descrizione = self.request.GET.get('descrizione')
        if descrizione:
            queryset = queryset.filter(descrizione__icontains=descrizione)

        codice = self.request.GET.get('codice')
        if codice:
            if codice.startswith('^'):
                queryset = queryset.filter(codice__startswith=codice[1:])
            elif ',' in codice:
                queryset = queryset.filter(codice__in=codice.split(','))
            else:
                queryset = queryset.filter(codice=codice)

        return queryset


class TerritorioViewSet(BaseViewSet):
    """
    Elenco di tutti i **territori**.

    Le pagine possono essere navigate tramite i link "next" e "previous".

    Filtrabili per

     * ``tipo`` (C, R, P, N, E)
     * ``cod_com`` (codice ISTAT del comune: Roma-58091)
     * ``cod_prov`` (codice ISTAT della provincia: Roma-58)
     * ``cod_reg`` (codice ISTAT della regione: Lazio-12)
     * ``denominazione`` (estrae tutti i territori che iniziano per ...)
    """
    serializer_class = TerritorioModelSerializer

    def get_queryset(self):
        queryset = Territorio.objects.all()

        for key in ('tipo', 'cod_reg', 'cod_prov', 'cod_com', 'denominazione'):
            value = self.request.GET.get(key)
            if value:
                queryset = queryset.filter(**{'denominazione__istartswith' if key == 'denominazione' else key: value})

        return queryset


class ProgettoViewSet(BaseSearchViewSet):
    """
    Elenco di tutti i **progetti**.

    Le pagine possono essere navigate tramite i link "next" e "previous".

    La scheda di dettaglio può essere raggiunta tramite il link "url" di ogni elemento della lista.

    Progetti can be filtered through ``tema``, ``natura``, ``programma``, ``soggetto`` and ``territorio`` filters in the GET query-string parameters.
    Filters use slugs, and multiple filters can be built.

    Slugs values to be used in the filters are shown in the progetto list, and the complete lists can be extracted at:

    * ``/api/temi``
    * ``/api/nature``
    * ``/api/programmi``
    * ``/api/soggetti``
    * ``/api/territori``

    Progetti can also be filtered through ``cup`` filter in the GET query-string parameters.

    Examples
    ========

    * ``/api/progetti?natura=incentivi-alle-imprese``
    * ``/api/progetti?tema=istruzione``
    * ``/api/progetti?natura=incentivi-alle-imprese&tema=istruzione``
    * ``/api/progetti?territorio=roma-comune``
    * ``/api/progetti?natura=incentivi-alle-imprese&tema=istruzione&territorio=roma-comune``
    * ``/api/progetti?soggetto=miur``
    * ``/api/progetti?cup=J77I04000000009``

    The results are paginated by default to 25 items per page.
    The number of items per page can be changed through the ``page_size`` GET parameter.
    500 is the maximum value allowed for the page_size parameter.

    Results are sorted by default by descending costs (``-costo``).
    You can change the sorting order, using the ``order_by`` GET parameter.
    These are the possible values:

    * ``-costo`` (default), ``costo``
    * ``-data_inizio``, ``data_inizio``

    a minus (-) in front of the field name indicates a *descending* order criterion.

    """
    queryset = Progetto.objects.prefetch_related(
        Prefetch('ruoli', queryset=Ruolo.objects.select_related('soggetto')),
        Prefetch('fasi_procedurali', queryset=ProgettoFaseProcedurale.objects.select_related('fase_procedurale')),
        Prefetch('indicatori_realizzazione', queryset=ProgettoIndicatoreRealizzazione.objects.select_related('indicatore_realizzazione__unita_misura'))
    )
    serializer_class = ProgettoSearchResultSerializer
    lookup_field = 'slug'

    @staticmethod
    def _get_objects_by_pk(pks):
        return {str(key): value for key, value in Progetto.objects.prefetch_related('territori', Prefetch('ruoli', queryset=Ruolo.objects.select_related('soggetto'))).in_bulk(pks).items()}

    def get_searchqueryset(self):
        ret_sqs = ProgettoSearchView().get_queryset()

        ret_sqs = ret_sqs.filter(is_active=True, is_pubblicato=1)

        ciclo_programmazione_slug = self.request.query_params.get('ciclo_programmazione')
        if ciclo_programmazione_slug:
            ciclo_programmazione_code = getattr(settings.CICLO_PROGRAMMAZIONE, ciclo_programmazione_slug, None)
            ret_sqs = ret_sqs.filter(ciclo_programmazione=ciclo_programmazione_code)

        natura_slug = self.request.query_params.get('natura')
        if natura_slug:
            natura = ClassificazioneAzione.objects.get(slug=natura_slug)
            ret_sqs = ret_sqs.filter(natura=natura.codice)

        tema_slug = self.request.query_params.get('tema')
        if tema_slug:
            tema = TemaSintetico.objects.get(slug=tema_slug)
            ret_sqs = ret_sqs.filter(tema=tema.codice)

        fonte_codice = self.request.query_params.get('fonte')
        if fonte_codice:
            ret_sqs = ret_sqs.filter(fonte=fonte_codice)

        stato_slug = self.request.query_params.get('stato')
        if stato_slug:
            stato_code = getattr(Progetto.STATO, stato_slug, None)
            ret_sqs = ret_sqs.filter(stato=stato_code)

        focus_slug = self.request.query_params.get('focus')
        if focus_slug:
            ret_sqs = ret_sqs.filter(focus=focus_slug)

        programma_codice = self.request.query_params.get('programma')
        if programma_codice:
            ret_sqs = ret_sqs.filter(programma=programma_codice)

        soggetto_slug = self.request.query_params.get('soggetto')
        if soggetto_slug:
            ret_sqs = ret_sqs.filter(soggetto__in=['{}|{}'.format(soggetto_slug, r[0]) for r in Ruolo.RUOLO])

        territorio_slug = self.request.query_params.get('territorio')
        if territorio_slug:
            territorio = Territorio.objects.get(slug=territorio_slug)
            ret_sqs = ret_sqs.filter(**{k.replace('cod', 'territorio'): v for k, v in territorio.get_filter_dict().items()})

        cup = self.request.query_params.get('cup')
        if cup:
            ret_sqs = ret_sqs.filter(cup_s=cup)

        sort_field = self.request.query_params.get('order_by')
        if sort_field and sort_field.lstrip('-') in ('costo', 'data_inizio'):
            ret_sqs.query.order_by = []  # reset default order_by parameter set in definition
            ret_sqs = ret_sqs.order_by(sort_field)

        return ret_sqs


class SoggettoViewSet(AggregatoMixin, BaseSearchViewSet):
    """
    Elenco di tutti i **soggetti**.

    Le pagine possono essere navigate tramite i link "next" e "previous".

    La scheda di dettaglio può essere raggiunta tramite il link "url" di ogni elemento della lista.

    Soggetti can be filtered through ``tema`` and ``ruolo`` filters in the GET query-string parameters.
    Filters use slugs, and multiple filters can be built.

    Slugs values to be used in the filters are shown in the soggetto list.

    Examples
    ========

    * ``/api/soggetti?tema=competitivita-imprese``
    * ``/api/soggetti?ruolo=attuatore``
    * ``/api/soggetti?tema=istruzione&ruolo=attuatore``


    The results are paginated by default to 25 items per page.
    The number of items per page can be changed through the ``page_size`` GET parameter.
    500 is the maximum value allowed for the page_size parameter.

    Results are sorted by default by descending total projects' costs (``-costo``).
    You can change the sorting order, using the ``order_by`` GET parameter.
    These are the possible values:

    * ``-costo`` (default), ``costo``
    * ``-n_progetti``, ``n_progetti``

    a minus (-) in front of the field name indicates a *descending* order criterion.
    """
    queryset = Soggetto.objects.all()
    serializer_class = SoggettoSearchResultSerializer
    lookup_field = 'slug'

    @staticmethod
    def _get_objects_by_pk(pks):
        return {str(key): value for key, value in Soggetto.objects.in_bulk(pks).items()}

    def get_searchqueryset(self):
        ret_sqs = SoggettoSearchView().get_queryset()

        tema_slug = self.request.query_params.get('tema')
        if tema_slug:
            tema = TemaSintetico.objects.get(slug=tema_slug)
            ret_sqs = ret_sqs.filter(tema=tema.codice)

        ruolo_slug = self.request.query_params.get('ruolo')
        if ruolo_slug:
            ruolo_code = getattr(Ruolo.RUOLO, ruolo_slug, None)
            ret_sqs = ret_sqs.filter(ruolo=ruolo_code)

        territorio_slug = self.request.query_params.get('territorio')
        if territorio_slug:
            territorio = Territorio.objects.get(slug=territorio_slug)
            ret_sqs = ret_sqs.filter(**{k.replace('cod', 'territorio'): v for k, v in territorio.get_filter_dict().items()})

        sort_field = self.request.query_params.get('order_by')
        if sort_field and sort_field.lstrip('-') in ('costo', 'n_progetti'):
            ret_sqs.query.order_by = []  # reset default order_by parameter set in definition
            ret_sqs = ret_sqs.order_by(sort_field)

        return ret_sqs

    def get_aggregate_page_url(self):
        return reverse('soggetto_detail', kwargs={'slug': self.kwargs['slug']})

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)

        aggregate_data = self.get_aggregate_data(request, *args, **kwargs)

        response.data['data_aggiornamento'] = aggregate_data['data_aggiornamento']
        response.data['aggregati'] = aggregate_data['aggregati']

        return response


class AggregatoGlobaleView(BaseAggregatoView):
    def get_aggregate_page_url(self):
        return reverse('globale')


class AggregatoTemaDetailView(BaseAggregatoView):
    def get_aggregate_page_url(self):
        return reverse('temasintetico_detail', kwargs={'slug': self.kwargs['slug']})

    def get_extra_context(self, object):
        return OrderedDict([
            ('nome_tema', object.descrizione_breve),
        ])


class AggregatoNaturaDetailView(BaseAggregatoView):
    def get_aggregate_page_url(self):
        return reverse('classificazioneazione_detail', kwargs={'slug': self.kwargs['slug']})

    def get_extra_context(self, object):
        return OrderedDict([
            ('nome_natura', object.descrizione_breve),
        ])


class AggregatoTerritorioDetailView(BaseAggregatoView):
    def get_aggregate_page_url(self):
        return reverse('territorio_detail', kwargs={'slug': self.kwargs['slug']})

    def get_extra_context(self, object):
        if self.kwargs['slug'] == 'ambito-estero':
            return OrderedDict([
                ('nome_territorio', 'Ambito Estero'),
                ('tipo_territorio', Territorio.TIPO.E),
                ('popolazione', ''),
            ])

        return OrderedDict([
            ('nome_territorio', object.denominazione),
            ('tipo_territorio', object.tipo),
            ('popolazione', object.popolazione_totale),
        ])


class AggregatoProgrammaDetailView(BaseAggregatoView):
    def get_aggregate_page_url(self):
        return reverse('programma_detail', kwargs={'pk': self.kwargs['pk']})

    def get_extra_context(self, object):
        return OrderedDict([
            ('nome_programma', object.descrizione),
            ('n_beneficiari', Soggetto.objects.filter(ruoli__progressivo_beneficiario__gt=0, ruoli__progetto__in=Progetto.objects.computabili().con_programmi([object])).distinct('codice_fiscale').count()),
        ])
