# -*- coding: utf-8 -*-
import json
import urllib.parse as urlparse
from collections import OrderedDict
from rest_framework import serializers
from rest_framework.reverse import reverse
from ..models import TemaSintetico, ClassificazioneAzione, Territorio, Localizzazione, Programma, Progetto, Ruolo,\
    Pagamento, Soggetto, Ambito, Impegno, ProgettoFaseProcedurale, ProgettoIndicatoreRealizzazione


class MyDecimalField(serializers.Field):
    def to_representation(self, value):
        return '{},{:<02d}'.format(*[int(x) for x in str(value).split('.')])


class AggregationURLField(serializers.Field):
    """
    Field that returns aggregation url for tema or natura or territorio.
    """
    url_name = None

    def __init__(self, identifier):
        self.url_name = 'api-aggregati-{}-detail'.format(identifier)
        super().__init__(self, source='*')

    def to_representation(self, obj):
        request = self.context.get('request')
        format = self.context.get('format')

        return reverse(self.url_name, kwargs={'slug': obj.slug}, request=request, format=format)


class FilterURLsField(serializers.Field):
    """
    Field that returns urls for progetti or soggetti with a given tema or natura or territorio or programma.
    """
    filter_name = None
    apply_to = None

    def __init__(self, filter_name, apply_to):
        self.filter_name = filter_name
        self.apply_to = apply_to if isinstance(apply_to, (list, tuple)) else (apply_to,)
        super().__init__(self, source='*')

    def to_representation(self, obj):
        request = self.context.get('request')
        format = self.context.get('format')

        ret = OrderedDict()
        for key, urlname in (('progetti', 'api-progetto-list'), ('soggetti', 'api-soggetto-list')):
            if key in self.apply_to:
                url = reverse(urlname, request=request, format=format)
                url_parts = urlparse.urlsplit(url)
                query_parts = urlparse.parse_qsl(url_parts.query)
                query_parts.append((self.filter_name, obj.codice if self.filter_name == 'programma' else obj.slug))
                url_parts = url_parts._replace(query=urlparse.urlencode(query_parts))

                ret[key] = url_parts.geturl()

        return ret


class ClassificazioneAzioneDetailModelSerializer(serializers.ModelSerializer):
    aggregati = AggregationURLField('classificazioneazione')
    filtri = FilterURLsField('natura', ('progetti', 'soggetti'))

    class Meta:
        model = ClassificazioneAzione
        fields = ('codice', 'slug', 'descrizione_breve', 'descrizione', 'descrizione_estesa', 'aggregati', 'filtri')


class ClassificazioneAzioneListModelSerializer(serializers.ModelSerializer):
    aggregati = AggregationURLField('classificazioneazione')
    filtri = FilterURLsField('natura', ('progetti', 'soggetti'))

    class Meta:
        model = ClassificazioneAzione
        fields = ('url', 'codice', 'slug', 'descrizione_breve', 'descrizione', 'aggregati', 'filtri')
        extra_kwargs = {
            'url': {'view_name': 'api-classificazioneazione-detail', 'lookup_field': 'slug'},
        }


class TemaSinteticoDetailModelSerializer(serializers.ModelSerializer):
    aggregati = AggregationURLField('temasintetico')
    filtri = FilterURLsField('tema', ('progetti', 'soggetti'))

    class Meta:
        model = TemaSintetico
        fields = ('codice', 'slug', 'descrizione_breve', 'descrizione', 'descrizione_estesa', 'aggregati', 'filtri')


class TemaSinteticoListModelSerializer(serializers.ModelSerializer):
    aggregati = AggregationURLField('temasintetico')
    filtri = FilterURLsField('tema', ('progetti', 'soggetti'))

    class Meta:
        model = TemaSintetico
        fields = ('url', 'codice', 'slug', 'descrizione_breve', 'descrizione', 'aggregati', 'filtri')
        extra_kwargs = {
            'url': {'view_name': 'api-temasintetico-detail', 'lookup_field': 'slug'},
        }


class ProgrammaDetailModelSerializer(serializers.ModelSerializer):
    filtri = FilterURLsField('programma', 'progetti')

    class Meta:
        model = Programma
        fields = ('codice', 'descrizione', 'descrizione_estesa', 'filtri')


class ProgrammaListModelSerializer(serializers.ModelSerializer):
    filtri = FilterURLsField('programma', 'progetti')

    class Meta:
        model = Programma
        fields = ('url', 'codice', 'descrizione', 'filtri')
        extra_kwargs = {
            'url': {'view_name': 'api-programma-detail'},
        }


class AmbitoModelSerializer(serializers.ModelSerializer):
    def to_representation(self, obj):
        return OrderedDict((k.lower(), v) for k, v in obj.csv_data.items() if k.lower() != 'cod_locale_progetto')

    class Meta:
        model = Ambito


class RuoloModelSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedRelatedField(view_name='api-soggetto-detail', lookup_field='slug', source='soggetto', read_only=True)
    denominazione = serializers.CharField(source='soggetto.denominazione')
    codice_fiscale = serializers.CharField(source='soggetto.codice_fiscale')
    indirizzo = serializers.CharField(source='soggetto.indirizzo')
    cap = serializers.CharField(source='soggetto.cap')
    ruoli = serializers.ListField()

    def to_representation(self, obj):
        obj.ruoli = [Ruolo.RUOLO[ruolo_code] for ruolo_code, ruolo_desc in Ruolo.inv_ruoli_dict().items() if getattr(obj, 'progressivo_{}'.format(ruolo_desc)) > 0]
        return super().to_representation(obj)

    class Meta:
        model = Ruolo
        fields = ('url', 'denominazione', 'codice_fiscale', 'indirizzo', 'cap', 'ruoli')


class Ruolo2ModelSerializer(serializers.ModelSerializer):
    denominazione = serializers.CharField(source='soggetto.denominazione')
    ruoli = serializers.ListField()

    def to_representation(self, obj):
        obj.ruoli = [Ruolo.RUOLO[ruolo_code] for ruolo_code, ruolo_desc in Ruolo.inv_ruoli_dict().items() if getattr(obj, 'progressivo_{}'.format(ruolo_desc)) > 0]
        return super().to_representation(obj)

    class Meta:
        model = Ruolo
        fields = ('denominazione', 'ruoli')


class TerritorioModelSerializer(serializers.ModelSerializer):
    aggregati = AggregationURLField('territorio')
    filtri = FilterURLsField('territorio', ('progetti', 'soggetti'))

    class Meta:
        model = Territorio
        fields = ('denominazione', 'denominazione_ted', 'tipo', 'slug', 'cod_reg', 'cod_prov', 'cod_com', 'aggregati', 'filtri')


class LocalizzazioneModelSerializer(serializers.ModelSerializer):
    def to_representation(self, obj):
        return {
            **TerritorioModelSerializer(context={'request': self.context.get('request')}).to_representation(obj.territorio),
            **super().to_representation(obj)
        }

    class Meta:
        model = Localizzazione
        fields = ('indirizzo', 'cap')


class ProgettoFaseProceduraleModelSerializer(serializers.ModelSerializer):
    oc_cod_fase = serializers.CharField(source='fase_procedurale.codice')
    oc_descr_fase = serializers.CharField(source='fase_procedurale.descrizione')

    class Meta:
        model = ProgettoFaseProcedurale
        exclude = ('id', 'progetto', 'fase_procedurale')


class ProgettoIndicatoreRealizzazioneModelSerializer(serializers.ModelSerializer):
    tipo_indicatore = serializers.CharField(source='indicatore_realizzazione.tipo')
    cod_indicatore = serializers.CharField(source='indicatore_realizzazione.codice')
    descr_indicatore = serializers.CharField(source='indicatore_realizzazione.descrizione')
    unita_misura = serializers.CharField(source='indicatore_realizzazione.unita_misura.codice')
    desc_unita_misura = serializers.CharField(source='indicatore_realizzazione.unita_misura.descrizione')
    valore_programmato = MyDecimalField()
    valore_realizzato = MyDecimalField()
    oc_flag_visual_indicatore = serializers.CharField(source='flag_visualizzazione')

    class Meta:
        model = ProgettoIndicatoreRealizzazione
        exclude = ('id', 'progetto', 'indicatore_realizzazione', 'flag_visualizzazione')


class ImpegnoModelSerializer(serializers.ModelSerializer):
    oc_data_impegni = serializers.DateField(source='data')
    impegni = MyDecimalField(source='ammontare')

    class Meta:
        model = Impegno
        fields = ('oc_data_impegni', 'impegni')


class PagamentoModelSerializer(serializers.ModelSerializer):
    oc_data_pagamenti = serializers.DateField(source='data')
    tot_pagamenti = MyDecimalField(source='ammontare')
    oc_tot_pagamenti_fsc = MyDecimalField(source='ammontare_fsc')
    oc_tot_pagamenti_pac = MyDecimalField(source='ammontare_pac')
    oc_tot_pagamenti_rendicontab_ue = MyDecimalField(source='ammontare_rendicontabile_ue')
    oc_codice_programma = serializers.CharField(source='codice_programma')

    class Meta:
        model = Pagamento
        fields = ('oc_data_pagamenti', 'tot_pagamenti', 'oc_tot_pagamenti_fsc', 'oc_tot_pagamenti_pac', 'oc_tot_pagamenti_rendicontab_ue', 'oc_codice_programma')


class ProgettoDetailModelSerializer(serializers.ModelSerializer):
    def to_representation(self, obj):
        return OrderedDict(
            [
                # (k.lower(), v) for k, v in json.loads(obj.csv_data, object_pairs_hook=OrderedDict).items()
                (k.lower(), v) for k, v in obj.csv_data.items()
            ] + [
                ('programmi', AmbitoModelSerializer(context={'request': self.context.get('request')}, many=True).to_representation(obj.ambiti)),
                ('soggetti', RuoloModelSerializer(context={'request': self.context.get('request')}, many=True).to_representation(obj.ruoli)),
                ('territori', LocalizzazioneModelSerializer(context={'request': self.context.get('request')}, many=True).to_representation(obj.localizzazione_set)),
                ('fasi_procedurali', ProgettoFaseProceduraleModelSerializer(context={'request': self.context.get('request')}, many=True).to_representation(obj.fasi_procedurali)),
                ('indicatori_realizzazione', ProgettoIndicatoreRealizzazioneModelSerializer(context={'request': self.context.get('request')}, many=True).to_representation(obj.indicatori_realizzazione)),
                ('lista_impegni', ImpegnoModelSerializer(context={'request': self.context.get('request')}, many=True).to_representation(obj.impegni)),
                ('lista_pagamenti', PagamentoModelSerializer(context={'request': self.context.get('request')}, many=True).to_representation(obj.pagamenti)),
            ]
        )

    class Meta:
        model = Progetto


class ProgettoListModelSerializer(serializers.ModelSerializer):
    def to_representation(self, obj):
        return OrderedDict(
            [
                ('url', reverse('api-progetto-detail', kwargs={'slug': obj.slug}, request=self.context.get('request'), format=self.context.get('format'))),
            ] + [
                # (k.lower(), v) for k, v in json.loads(obj.csv_data, object_pairs_hook=OrderedDict).items() if k.lower() in ('cod_locale_progetto', 'cup', 'cup_descr_natura', 'oc_tema_sintetico', 'oc_descrizione_programma', 'oc_titolo_progetto', 'finanz_totale_pubblico', 'tot_pagamenti', 'oc_data_inizio_effettiva')
                (k.lower(), v) for k, v in obj.csv_data.items() if k.lower() in ('cod_locale_progetto', 'cup', 'cup_descr_natura', 'oc_descr_ciclo', 'oc_finanz_tot_pub_netto', 'oc_stato_progetto', 'oc_tema_sintetico', 'oc_titolo_progetto', 'tot_pagamenti')
            ] + [
                ('percentuale_avanzamento', '{}%'.format(obj.percentuale_pagamenti)),
            ] + [
                # ('soggetti', set(reverse('api-soggetto-detail', kwargs={'slug': o.slug}, request=self.context.get('request'), format=self.context.get('format')) for o in obj.soggetti.all())),
                ('soggetti', Ruolo2ModelSerializer(context={'request': self.context.get('request')}, many=True).to_representation(obj.ruoli)),
                ('territori', set(o.slug for o in obj.territori.all())),
            ]
        )

    class Meta:
        model = Progetto


class ProgettoSearchResultSerializer(serializers.Serializer):
    def to_representation(self, obj):
        return ProgettoListModelSerializer(context={'request': self.context.get('request')}).to_representation(obj.object)


class SoggettoDetailModelSerializer(serializers.ModelSerializer):
    territorio = TerritorioModelSerializer()

    class Meta:
        model = Soggetto
        exclude = ('id',)


class SoggettoListModelSerializer(serializers.ModelSerializer):
    temi = serializers.ListField(source='temi_slug')
    ruoli = serializers.ListField(source='ruoli_slug')
    costo_pubblico = MyDecimalField(source='costo')
    progetti = serializers.CharField(source='n_progetti')

    class Meta:
        model = Soggetto
        fields = ('url', 'slug', 'denominazione', 'temi', 'ruoli', 'costo_pubblico', 'progetti')
        extra_kwargs = {
            'url': {'view_name': 'api-soggetto-detail', 'lookup_field': 'slug'},
        }


class SoggettoSearchResultSerializer(serializers.Serializer):
    def to_representation(self, obj):
        obj.object.temi_slug = TemaSintetico.objects.filter(progetti__soggetti=obj.object).values_list('slug', flat=True).distinct()
        obj.object.ruoli_slug = [Ruolo.inv_ruoli_dict()[x] for x in obj.ruolo]
        obj.object.costo = obj.costo
        obj.object.n_progetti = obj.n_progetti

        return SoggettoListModelSerializer(context={'request': self.context.get('request')}).to_representation(obj.object)
