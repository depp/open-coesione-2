# -*- coding: utf-8 -*-
from collections import OrderedDict
from django.db.models import Q, Count
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _
from itertools import chain
from .models import Programma


class Config(object):
    @cached_property
    def get_lista_programmi(self):
        programmi_by_ciclo = {}
        for programma in Programma.objects.programmi().annotate(n=Count('classificazione_set')).filter(n__gt=0).order_by('descrizione'):
            programmi_by_ciclo.setdefault(programma.ciclo_programmazione, []).append(programma)

        programmi0713 = programmi_by_ciclo.get('1', [])
        programmi1420 = programmi_by_ciclo.get('2', [])
        programmi2127 = programmi_by_ciclo.get('3', [])

        programmiPSCregionali = sorted(
            [p for p in chain.from_iterable(programmi_by_ciclo.values()) if p.codice in ('PSCABRUZZO', 'PSCBASILICATA', 'PSCCALABRIA', 'PSCCAMPANIA', 'PSCEMILROMAGNA', 'PSCFRIULI', 'PSCLAZIO', 'PSCLIGURIA', 'PSCLOMBARDIA', 'PSCMARCHE', 'PSCMOLISE', 'PSCBOLZANO', 'PSCTRENTO', 'PSCPIEMONTE', 'PSCPUGLIA', 'PSCSARDEGNA', 'PSCSICILIA', 'PSCTOSCANA', 'PSCUMBRIA', 'PSCVALLEAOSTA', 'PSCVENETO')],
            key=lambda x: x.descrizione
        )

        lista_programmi = {
            'fsc_0006_1': [
                programmiPSCregionali,
            ],
            'ue_fesr_0713': [
                [p for p in programmi0713 if ' FESR ' in p.descrizione_it.upper()],
            ],
            'ue_fse_0713': [
                [p for p in programmi0713 if ' FSE ' in p.descrizione_it.upper()],
            ],
            'fsc_0713_1': [
                # [p for p in programmi0713 if p.descrizione_it.upper().startswith('PAR') and p.codice not in ('2007TO003FA012', '2007FR002FA003', '2007LO002FA006', '2007TO002FA012', '2007TR002FA010')],
            ],
            'fsc_0713_2': [
                [p for p in programmi0713 if p.descrizione_it.upper().startswith(('PROGRAMMA ATTUATIVO', 'PROGRAMMA STRATEGICO')) and p.codice not in ('2007EM002FA003', '2007IT001FA004', '2007IT001FA005', '2007IT004FABC1')],
                # [p for p in programmi0713 if p.descrizione_it.upper().startswith('PROGRAMMA REGIONALE') and p.codice not in ('2007CL001FA008',)],
                [p for p in programmi0713 if p.descrizione_it.upper().startswith('PROGRAMMA NAZIONALE')],
                # [p for p in programmi0713 if p.descrizione_it.upper().startswith('PIANO STRALCIO')],
                [p for p in programmi0713 if p.descrizione_it.upper().startswith('PIANO STRAORDINARIO DI BONIFICA')],
                # [p for p in programmi0713 if p.descrizione_it.upper().startswith('PROGRAMMA OBIETTIVI DI SERVIZIO') and p.codice not in ('2007IT001FAOS5', '2007IT001FAOS6')],
                [p for p in programmi0713 if p.codice in ('2017NAPOLI-POZZ', '2018FREJUSFSC', '2012XMITRFSC005', '2017PPORTTARFSC', 'FSCMILITSPORT', '2015XMINTFSC016', 'FSCCOMUNI', '2017MEFDTFSC004')],
            ],
            'fsc_0713_3': [
                [p for p in programmi1420 if p.codice in ('PSCISTRUZIONE',)],
                programmiPSCregionali,
            ],
            'pac_0713': [
                [p for p in programmi0713 if p.descrizione_it.upper().startswith('PROGRAMMA PAC') and (' MINISTERO ' in p.descrizione_it.upper() or ' PCM ' in p.descrizione_it.upper() or ' GOVERNANCE ' in p.descrizione_it.upper())],
                [p for p in programmi0713 if p.descrizione_it.upper().startswith('PROGRAMMA PAC') and not (' MINISTERO ' in p.descrizione_it.upper() or ' PCM ' in p.descrizione_it.upper() or ' GOVERNANCE ' in p.descrizione_it.upper())],
            ],
            'pac_0713_x': [
                [p for p in programmi0713 if p.codice in ('2007IT001FA005', '2007IT005FAMG1', '2007SA002FA016', '2007IT001FA003')],
                [p for p in programmi0713 if p.codice in ('2007IT161PO001', '2007IT161PO007', '2007IT161PO009', '2007IT161PO011', '2007IT162PO016')],
                [p for p in programmi0713 if p.codice in ('2007IT051PO001')],
            ],
            'ue_fesr_1420': [
                [p for p in programmi1420 if ' FESR ' in p.descrizione_it.upper() and not ' FSE ' in p.descrizione_it.upper() and not ' INTERREG ' in p.descrizione_it.upper()],
            ],
            'ue_fesr_cte_1420': [
                [p for p in programmi1420 if ' INTERREG ' in p.descrizione_it.upper()],
            ],
            'ue_fesr_fse_1420': [
                [p for p in programmi1420 if ' FESR FSE ' in p.descrizione_it.upper()],
            ],
            'ue_fse_1420': [
                [p for p in programmi1420 if ' FSE ' in p.descrizione_it.upper() and not ' FESR ' in p.descrizione_it.upper()],
            ],
            'ue_iog_1420': [
                [p for p in programmi1420 if p.codice in ('2014IT05M9OP001',)],
            ],
            # 'ue_feamp_1420': [
            #     [p for p in programmi1420 if p.codice in ('2014IT14MFOP001',)],
            # ],
            # 'ue_feasr_1420': [
            #     [p for p in programmi1420 if ' FEASR ' in p.descrizione_it.upper()],
            # ],
            'fsc_1420_1': [
                [p for p in programmi1420 if p.codice in ('PSCSVILECONOM', 'PSCAGRICOLTURA', 'PSCTRANSECOLOG', 'PSCINFRASTRUT', 'PSCUNIVRICERCA', 'PSCISTRUZIONE', 'PSCSALUTE', 'PSCPCMSPORT', 'PSCCULTURA', 'PSCTURISMO')],
                programmiPSCregionali,
                [p for p in programmi1420 if p.codice in ('PSCCITMETBARI', 'PSCCIMETBOLOGNA', 'PSCCIMTCAGLIARI', 'PSCCIMETCATANIA', 'PSCCIMETFIRENZE', 'PSCCIMETGENOVA', 'PSCCIMETMESSINA', 'PSCCITMETMILANO', 'PSCCITMETNAPOLI', 'PSCCIMETPALERMO', 'PSCCITMETREGCAL', 'PSCCIMETVENEZIA')],
            ],
            'fsc_1420_2': [
                # [p for p in programmi1420 if p.codice in ('2016XXAMPSAP00', '2015XXXXXPSO000', 'FSCACTBENICONF')],
                [p for p in programmi1420 if p.codice in ('FSCACTBENICONF',)],
            ],
            'fsc_1420_3': [
                # [p for p in programmi1420 if p.codice in ('2016PATTIFIR', '2016PATTIMES')],
                # [p for p in programmi1420 if p.codice in ('2016PATTILAZ', '2016PATTIMOL', '2016PATTIPUG', '2016PATTISARD', '2016PATTISICI')],
            ],
            'fsc_1420_4': [
                [p for p in programmi1420 if p.codice in ('2018FSCGIOVIMP', 'FSCRESTOSUDAGR', 'FSCCRESCISUD', 'FONDOINFRSOCIAL', 'AREEURBDEGRFSC', '2015PCDPCFSC018', '2019AGCOEFSC001', '2014XXSMDFSC004', '2014PCDPCFSC003', 'CISBRINDILECCE', 'CISCALABRIA', 'CISVESPOMNAPOLI', 'COMMTARANTOFSC', 'CISFOGGIAFSC', 'CISMOLISEFSC', 'CSITERRAFUOCHI', 'FSCTERZOSETTORE', '2016MEFDTFSC001', '2018MATERAFSC', '2016XXPCMFSC002', 'FSCMISERICSVIL', '2019MISEDLCRE01')],
            ],
            'pac_poc_1420': [
                [p for p in programmi1420 if p.descrizione_it.upper().startswith('POC ')],
            ],
            'ord_1420': [
                [p for p in programmi1420 if p.descrizione_it.upper().startswith('STRATEGIA AREE INTERNE')],
                [p for p in programmi1420 if p.codice in ('AREEINTVVFF', '2020PCDPCINA001')],
            ],
            'fsc_2127': [
                programmi2127,
            ],
        }

        return lista_programmi


class GruppoProgrammi(object):
    GRUPPI_PROGRAMMI = {
        'fsc-0006': _('Programmi FSC 2000-2006'),
        'fsc-0006-1': _('Piani Sviluppo e Coesione 2000-2006'),
        'ue-0713': _('Programmi UE 2007-2013'),
        'ue-fesr-0713': _('Programmi UE FESR 2007-2013'),
        'ue-fse-0713': _('Programmi UE FSE 2007-2013'),
        'fsc-0713': _('Programmi FSC 2007-2013'),
        'fsc-0713-1': _('Programmi Attuativi Regionali 2007-2013'),
        'fsc-0713-2': _('Altri Programmi FSC 2007-2013'),
        'fsc-0713-3': _('Piani Sviluppo e Coesione 2007-2013'),
        'pac-0713': _('Programmi PAC 2007-2013'),
        'ue-1420': _('Programmi UE 2014-2020'),
        'ue-fesr-1420': _('Programmi UE FESR 2014-2020'),
        'ue-fesr-cte-1420': _('Programmi UE FESR CTE 2014-2020'),
        'ue-fesr-fse-1420': _('Programmi UE FESR FSE 2014-2020'),
        'ue-fse-1420': _('Programmi UE FSE 2014-2020'),
        'ue-iog-1420': _('Programmi UE IOG 2014-2020'),
        # 'ue-feamp-1420': _('Programmi UE FEAMP 2014-2020'),
        # 'ue-feasr-1420': _('Programmi UE FEASR 2014-2020'),
        'fsc-1420': _('Programmi FSC 2014-2020'),
        'fsc-1420-1': _('Piani Sviluppo e Coesione 2014-2020'),
        'fsc-1420-2': _('Piani nazionali 2014-2020'),
        'fsc-1420-3': _('Patti per lo sviluppo 2014-2020'),
        'fsc-1420-4': _('Altri Programmi FSC 2014-2020'),
        'pac-poc-1420': _('Programmi PAC/POC 2014-2020'),
        'ord-1420': _('Risorse ordinarie 2014-2020'),
        'fsc-2127': _('Programmi FSC 2021-2027'),
    }

    codice = None

    def __init__(self, codice):
        if codice in self.GRUPPI_PROGRAMMI:
            self.codice = codice
        else:
            raise ValueError('Wrong codice: {}'.format(codice))

    def __str__(self):
        return '{}'.format(self.descrizione)

    def get_search_filter_querystring(self):
        import urllib.parse
        return urllib.parse.urlencode({'gruppo_programmi': self.codice})

    @property
    def descrizione(self):
        return self.GRUPPI_PROGRAMMI[self.codice]

    @cached_property
    def programmi(self):
        lista_programmi = Config().get_lista_programmi

        if self.codice == 'fsc-0006':
            programmi = lista_programmi['fsc_0006_1']
        elif self.codice == 'ue-0713':
            programmi = lista_programmi['ue_fesr_0713'] + lista_programmi['ue_fse_0713']
        elif self.codice == 'fsc-0713':
            programmi = lista_programmi['fsc_0713_1'] + lista_programmi['fsc_0713_2'] + lista_programmi['fsc_0713_3']
        elif self.codice == 'ue-1420':
            programmi = lista_programmi['ue_fesr_1420'] + lista_programmi['ue_fesr_cte_1420'] + lista_programmi['ue_fesr_fse_1420'] + lista_programmi['ue_fse_1420'] + lista_programmi['ue_iog_1420']  #+ lista_programmi['ue_feamp_1420'] + lista_programmi['ue_feasr_1420']
        elif self.codice == 'fsc-1420':
            programmi = lista_programmi['fsc_1420_1'] + lista_programmi['fsc_1420_2'] + lista_programmi['fsc_1420_3'] + lista_programmi['fsc_1420_4']
        else:
            programmi = lista_programmi[self.codice.replace('-', '_')]

        return list(chain.from_iterable(programmi))

    @property
    def ciclo_programmazione(self):
        return self.programmi[0].ciclo_programmazione

    def get_ciclo_programmazione_display(self):
        return self.programmi[0].get_ciclo_programmazione_display()
