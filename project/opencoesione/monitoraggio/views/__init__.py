# -*- coding: utf-8 -*-
from django.views.decorators.clickjacking import xframe_options_exempt

from .aggregate import *
from .coesione import *
from .search import *

from django.db.models import Prefetch
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, TemplateView
from ..models import Progetto, Ambito, Ruolo, Territorio, ProgettoFaseProcedurale, MonitoraggioASOC, ProgettoIndicatoreRealizzazione
from ...models import CommunicationContent
from ...views import cached_context, JSONResponseMixin, XRobotsTagTemplateResponseMixin


class ProgettoDetailView(XRobotsTagTemplateResponseMixin, DetailView):
    model = Progetto

    def get_x_robots_tag(self):
        return 'noindex' if (self.object.is_privacy or (not self.object.is_active)) else False

    def get_queryset(self):
        return super().get_queryset()\
            .select_related('classificazione_azione__classificazione_superiore', 'tema_sintetico')\
            .prefetch_related(
                Prefetch('ambiti', queryset=Ambito.objects.select_related('fonte', 'classificazione_qsn__classificazione_superiore__classificazione_superiore', 'programma__classificazione_superiore__classificazione_superiore', 'tema_prioritario')),
                Prefetch('ruoli', queryset=Ruolo.objects.select_related('soggetto')),
                Prefetch('fasi_procedurali', queryset=ProgettoFaseProcedurale.objects.select_related('fase_procedurale')),
                Prefetch('indicatori_realizzazione', queryset=ProgettoIndicatoreRealizzazione.objects.filter(flag_visualizzazione='0').select_related('indicatore_realizzazione__unita_misura')),
                Prefetch('monitoraggi_asoc', queryset=MonitoraggioASOC.objects.select_related('istituto_comune')),
                'pagamenti', 'territori',
            )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['section'] = 'dati'

        if self.object.territori.all():
            numero_collaboratori = 5
            altri_progetti_nei_territori_qs = Progetto.objects.computabili().exclude(codice_locale=self.object.codice_locale).nei_territori(self.object.territori.all()).distinct().order_by('-finanz_totale_pubblico_netto')
            altri_progetti_nei_territori = [
                {
                    'titolo': _('Stesso tema'),
                    'lista': altri_progetti_nei_territori_qs.con_tema_sintetico(self.object.tema_sintetico)[:numero_collaboratori],
                },
                {
                    'titolo': _('Stessi attuatori'),
                    'lista': [],  # altri_progetti_nei_territori_qs.filter(soggetti__in=self.object.attuatori)[:numero_collaboratori],
                },
                {
                    'titolo': _("Stessa natura dell'investimento"),
                    'lista': altri_progetti_nei_territori_qs.con_classificazione_azione(self.object.classificazione_azione)[:numero_collaboratori],
                },
                {
                    'titolo': _('Stessi programmatori'),
                    'lista': [],  # altri_progetti_nei_territori_qs.filter(soggetti__in=self.object.programmatori)[:numero_collaboratori],
                },
            ]

            self.object.altri_progetti_nei_territori = [x for x in altri_progetti_nei_territori if x['lista']]

        pagamenti_per_anno = {x['anno']: x['ammontare'] for x in self.object.pagamenti.per_anno()}

        self.object.pagamenti_per_anno = []
        if pagamenti_per_anno:
            ammontare_cumulato = 0
            for anno in range(min(pagamenti_per_anno), max(pagamenti_per_anno) + 1):
                ammontare = pagamenti_per_anno.get(anno, 0)
                ammontare_cumulato += ammontare

                self.object.pagamenti_per_anno.append({
                    'anno': anno,
                    'ammontare': ammontare,
                    'ammontare_cumulato': ammontare_cumulato,
                    'percentuale_su_finanziamentopubblico': self.object.percentuale_su_finanziamentopubblico(ammontare_cumulato)
                })

        self.object.contenuti_comunicazione = CommunicationContent.objects.visibili().filter(references__type='2', references__code__in=(self.object.codice_locale, self.object.cup))

        return context


class ProgettoCSVDetailView(DetailView):
    model = Progetto

    def get(self, request, *args, **kwargs):
        import io
        from ...opendata.utils import get_latest_localfile

        self.object = self.get_object()

        clps = set()
        clps.add('COD_LOCALE_PROGETTO')
        clps.add(self.object.codice_locale)

        output = io.StringIO()

        with zipfile.ZipFile(get_latest_localfile('progetti_esteso.zip')) as zfile:
            with zfile.open(zfile.namelist().pop(0)) as f:
                for line in io.TextIOWrapper(f, 'UTF-8-sig'):
                    clp = line.split('";"', 2)[0].lstrip('"').replace('""', '"')
                    if clp in clps:
                        output.write(line)

        response = HttpResponse(output.getvalue(), content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(self.object.slug)

        return response


class ProgettoPagamentiCSVDetailView(DetailView):
    model = Progetto

    def get(self, request, *args, **kwargs):
        import csv
        from django.utils.formats import date_format, number_format

        def my_number_format(value):
            if value is None:
                return ''
            return number_format(value, decimal_pos=2)

        self.object = self.get_object()

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=pagamenti_{}.csv'.format(self.object.slug)

        writer = csv.writer(response, dialect='excel-semicolon')

        writer.writerow(['COD_LOCALE_PROGETTO', 'OC_DATA_PAGAMENTI', 'TOT_PAGAMENTI', 'OC_TOT_PAGAMENTI_RENDICONTAB_UE', 'OC_TOT_PAGAMENTI_FSC', 'OC_TOT_PAGAMENTI_PAC', 'OC_CODICE_PROGRAMMA'])

        for pagamento in self.object.pagamenti.all():
            writer.writerow([
                self.object.codice_locale,
                date_format(pagamento.data, 'd/m/Y'),
                my_number_format(pagamento.ammontare),
                my_number_format(pagamento.ammontare_rendicontabile_ue),
                my_number_format(pagamento.ammontare_fsc),
                my_number_format(pagamento.ammontare_pac),
                pagamento.codice_programma,
            ])

        return response


class AutocompleteTerritoriView(JSONResponseMixin, TemplateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        query = self.request.GET['q']

        territori = Territorio.objects.filter(denominazione__istartswith=query).order_by('-popolazione_totale')[0:20]

        context['territori'] = [{
            'denominazione': territorio.nome_con_provincia,
            'url': territorio.get_absolute_url(),
            'search_filter': territorio.get_search_filter_querystring(),
        } for territorio in territori]

        return context


class ProgrammaDocumentiDetailView(DetailView):
    model = Programma
    template_name = 'monitoraggio/programma_documenti_detail.html'

    def get_queryset(self):
        return super().get_queryset().programmi()

    def get_context_data(self, **kwargs):
        from ...models import BaseResource

        context = super().get_context_data(**kwargs)

        context['section'] = 'dati'

        context['has_progetti'] = Progetto.objects.only('is_active', 'flag_visualizzazione').computabili().con_programmi([self.object]).exists()
        context['has_contenuticomunicazione'] = CommunicationContent.objects.visibili().filter(references__type='1', references__code=self.object.codice).exists()

        attrs = ('documenti', 'collegamenti')
        context['categories'] = {k[1]: {x: [] for x in attrs} for k in ((None, None),) + BaseResource.CATEGORY_CHOICES}
        for attr in attrs:
            for obj in getattr(self.object, attr).all():
                context['categories'][obj.get_category_display()][attr].append(obj)

        return context


class ProgrammaContenutiComunicazioneDetailView(DetailView):
    model = Programma
    template_name = 'monitoraggio/programma_contenuticomunicazione_detail.html'

    def get_queryset(self):
        return super().get_queryset().programmi()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['section'] = 'dati'

        context['has_progetti'] = Progetto.objects.only('is_active', 'flag_visualizzazione').computabili().con_programmi([self.object]).exists()
        context['has_documenti'] = self.object.documenti.exists() or self.object.collegamenti.exists()

        context['contenuti_comunicazione'] = CommunicationContent.objects.visibili().filter(references__type='1', references__code=self.object.codice)

        return context


class ProgrammaWidgetDetailView(DetailView):
    model = Programma
    template_name = 'monitoraggio/programma_widget_detail.html'

    def get_queryset(self):
        return super().get_queryset().programmi().filter(pk__in=(o.pk for o in GruppoProgrammi('ue-1420').programmi))

    @cached_context
    def get_cached_context_data(self):
        import decimal

        progetti = Progetto.objects.computabili().con_programmi([self.object])

        return {
            'n_operazioni': progetti.count(),
            'n_beneficiari': Soggetto.objects.filter(ruoli__progressivo_beneficiario__gt=0, ruoli__progetto__in=progetti).distinct('codice_fiscale').count(),
            'costo_ammesso': sum(decimal.Decimal(ambito.csv_data['COSTO_RENDICONTABILE_UE'].replace(',', '.')) for ambito in Ambito.objects.filter(programma__classificazione_superiore__classificazione_superiore=self.object, **Progetto.computabili_filter_dict('progetto__'))),
        }

    def get_context_data(self, **kwargs):
        from ...opendata.models import Dataset

        context = super().get_context_data(**kwargs)

        context.update(self.get_cached_context_data())

        context['data_aggiornamento'] = datetime.datetime.strptime(settings.DATA_AGGIORNAMENTO, '%Y-%m-%d')

        try:
            context['data_file_url'] = reverse('localdataset_redirect', kwargs={'path': Dataset.objects.filter(section__slug='beneficiari_section').get(program=self.object).file})
        except Exception:
            context['data_file_url'] = '#'

        context['metadata_file_url'] = MetadataFile.objects.get(slug='beneficiari').file.url

        return context

    @xframe_options_exempt
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)
