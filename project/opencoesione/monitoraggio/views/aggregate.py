# -*- coding: utf-8 -*-
import datetime
from collections import defaultdict
from django.conf import settings
from django.db.models import Sum, Count, F, ExpressionWrapper, Func, Case, When, Value, DecimalField, CharField
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView, DetailView, ListView

from ..gruppo_programmi import GruppoProgrammi
from ..models import (
    ClassificazioneAzione, Pagamento, TemaSintetico, Progetto, Territorio, Programma, Focus, Soggetto, Ruolo, Impegno,
    Area, Fonte, ProgettoFocus, StrategiaS3, TerritorioS3, Ambito,
)
from ...models import CommunicationContent
from ...views import cached_context, XRobotsTagTemplateResponseMixin, WidgetEmbedCodeMixin, FlatPageMixin


def merge_dicts(*dict_args):
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


class AggregatoMixin(object):
    def get(self, request, *args, **kwargs):
        path = self.add_ciclo_programmazione_querystring(request.path)
        if path != request.get_full_path():
            from django.shortcuts import redirect
            return redirect(path, permanent=True)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['section'] = 'dati'

        if hasattr(self, 'get_comuni_csv_url'):
            context['comuni_csv_url'] = self.get_comuni_csv_url()

        context['search_url'] = self.get_search_url()

        context['ciclo_programmazione'] = self.get_ciclo_programmazione()

        context['data_aggiornamento'] = datetime.datetime.strptime(settings.DATA_AGGIORNAMENTO, '%Y-%m-%d')

        return context

    @staticmethod
    def add_totali(objects, totali, key='pk'):
        default = {k: 0 for k in ('totale_progetti', 'totale_finanziamenti', 'totale_pagamenti', 'finanziamenti_coesione', 'pagamenti_coesione')}
        totali_by_key = {x.pop('id'): x for x in totali}
        objects_with_totali = []
        for object in objects:
            object.totali = totali_by_key.get(getattr(object, key), default)
            objects_with_totali.append(object)
        return objects_with_totali

    def classificazioni_objs2dicts(self, objects):
        return [merge_dicts({'nome': o.descrizione_breve, 'slug': getattr(o, 'slug', None), 'url': self.get_item_url(o)}, o.totali) for o in objects]

    def territori_objs2dicts(self, objects, key):
        return [merge_dicts({'codice': getattr(o, key), 'nome': str(o), 'slug': getattr(o, 'slug', None), 'url': self.get_item_url(o), 'totale_finanziamenti_procapite': round(o.totali['totale_finanziamenti'] / o.popolazione_totale)}, o.totali) for o in objects if o.totali.get('totale_progetti', 0)]

    def get_ciclo_programmazione(self):
        ciclo_programmazione = self.request.GET.get('ciclo_programmazione')
        return ciclo_programmazione if ciclo_programmazione in settings.CICLO_PROGRAMMAZIONE else None

    def get_cicli_programmazione(self):
        cicli_programmazione = self.get_progetti_queryset(usa_ciclo_programmazione=False).values_list('ambiti__ciclo_programmazione', flat=True).distinct()
        return tuple(x for x in settings.CICLO_PROGRAMMAZIONE if x[0] in cicli_programmazione)

    def add_ciclo_programmazione_querystring(self, url, is_search_url=False):
        ciclo_programmazione = self.get_ciclo_programmazione()

        if ciclo_programmazione:
            import urllib.parse

            if is_search_url:
                query = {'selected_facets': 'ciclo_programmazione:{}'.format(ciclo_programmazione)}
            else:
                query = {'ciclo_programmazione': ciclo_programmazione}

            url = '{}{}{}'.format(url, '&' if '?' in url else '?', urllib.parse.urlencode(query, safe=':'))

        return url

    def get_search_url(self):
        search_url = '{}?q='.format(reverse('progetto_search'))

        search_url = self.add_ciclo_programmazione_querystring(search_url, is_search_url=True)

        if hasattr(self, 'get_search_filter_querystring'):
            search_url = '{}&{}'.format(search_url, self.get_search_filter_querystring())
        elif hasattr(self, 'object') and hasattr(self.object, 'get_search_filter_querystring'):
            search_url = '{}&{}'.format(search_url, self.object.get_search_filter_querystring())

        return search_url

    def get_item_url(self, item):
        return '{}&{}'.format(self.get_search_url(), item.get_search_filter_querystring())

    def get_progetti_queryset(self, usa_ciclo_programmazione=True, **kwargs):
        if usa_ciclo_programmazione:
            ciclo_programmazione = self.get_ciclo_programmazione()
        else:
            ciclo_programmazione = None
        return Progetto.objects.computabili(ciclo_programmazione, getattr(self, 'flag_visualizzazione', None))

    def get_aggregate_data(self):
        progetti = self.get_progetti_queryset()

        aggregate_data = progetti.totali()

        aggregate_data['stati_progetto'] = {x.pop('id'): x for x in progetti.totali_group_by('stato')}

        object = getattr(self, 'object', None)

        if not isinstance(object, (TemaSintetico, StrategiaS3)):
            aggregate_data['temi_principali'] = self.classificazioni_objs2dicts(self.add_totali(TemaSintetico.objects.all(), progetti.totali_group_by('tema_sintetico_id')))

        if not isinstance(object, ClassificazioneAzione):
            aggregate_data['classificazioniazione_principali'] = self.classificazioni_objs2dicts(self.add_totali(ClassificazioneAzione.objects.principali(), progetti.totali_group_by('classificazione_azione__classificazione_superiore_id')))

        if isinstance(object, (Area, StrategiaS3, TemaSintetico, Territorio)) or isinstance(self, TerritorioEsteroListView):
            from django.contrib.postgres.aggregates import StringAgg
            programmi = self.add_totali(Programma.objects.programmi().filter(classificazione_set__classificazione_set__ambiti__progetto__in=progetti).annotate(fonti=StringAgg('classificazione_set__classificazione_set__ambiti__fonte__descrizione_breve', delimiter=', ', distinct=True)), progetti.totali_group_by('ambiti__programma__classificazione_superiore__classificazione_superiore_id'))
            aggregate_data['programmi'] = [merge_dicts({'nome': o.descrizione, 'fonti': o.fonti, 'url': self.get_item_url(o)}, o.totali) for o in programmi if o.totali.get('totale_progetti', 0)]

        if not isinstance(object, (Area, Soggetto)) and not isinstance(self, TerritorioEsteroListView):
            if not isinstance(object, Territorio) or object.is_macroarea:
                territori = Territorio.objects.regioni()
                if isinstance(object, Territorio):
                    territori = territori.filter(**object.get_filter_dict())
                aggregate_data['regioni'] = self.territori_objs2dicts(self.add_totali(territori, progetti.distinct().totali_group_by('territori__cod_reg'), 'cod_reg'), 'cod_reg')

            if not isinstance(object, Territorio) or object.is_macroarea or object.is_regione:
                territori = Territorio.objects.province()
                if isinstance(object, Territorio):
                    territori = territori.filter(**object.get_filter_dict())
                aggregate_data['province'] = self.territori_objs2dicts(self.add_totali(territori, progetti.distinct().totali_group_by('territori__cod_prov'), 'cod_prov'), 'cod_prov')

            if isinstance(object, Territorio) and (object.is_regione or object.is_provincia):
                territori = Territorio.objects.comuni().filter(**object.get_filter_dict())
                aggregate_data['comuni'] = self.territori_objs2dicts(self.add_totali(territori, progetti.distinct().totali_group_by('territori__cod_com'), 'cod_com'), 'cod_com')

        if not isinstance(object, Soggetto):
            aggregate_data['top_soggetti_attuatori'] = Soggetto.objects.filter(ruoli__progressivo_attuatore__gt=0, progetti__in=progetti).annotate(totale=Sum('progetti__finanz_totale_pubblico_netto')).order_by('-totale')[:7]

        impegni_per_anno = {x['anno']: x['ammontare'] for x in Impegno.objects.filter(progetto__in=progetti).per_anno()}
        pagamenti_per_anno = {x['anno']: x['ammontare'] for x in Pagamento.objects.filter(progetto__in=progetti).per_anno()}

        anni = set(list(impegni_per_anno) + list(pagamenti_per_anno))

        aggregate_data['impegnipagamenti_per_anno'] = []
        if impegni_per_anno or pagamenti_per_anno:
            ammontare_impegni_cumulato = 0
            ammontare_pagamenti_cumulato = 0

            for anno in range(min(anni), max(anni) + 1):
                ammontare_impegni_cumulato += impegni_per_anno.get(anno, 0)
                ammontare_pagamenti_cumulato += pagamenti_per_anno.get(anno, 0)

                aggregate_data['impegnipagamenti_per_anno'].append({
                    'anno': anno,
                    'ammontare_impegni': ammontare_impegni_cumulato,
                    'ammontare_pagamenti': ammontare_pagamenti_cumulato,
                })

        aggregate_data['top_progetti_per_finanziamento'] = progetti.no_privacy().filter(finanz_totale_pubblico_netto__isnull=False).order_by('-finanz_totale_pubblico_netto', '-data_fine_effettiva')[:3]

        aggregate_data['ultimi_progetti_conclusi'] = progetti.no_privacy().filter(data_fine_effettiva__lte=datetime.datetime.now(), stato=Progetto.STATO.concluso).order_by('-data_fine_effettiva', '-finanz_totale_pubblico_netto')[:3]

        if hasattr(self, 'get_top_comuni_per_finanziamento_pro_capite_filter_dict'):
            aggregate_data['top_comuni_per_finanziamento_pro_capite'] = self.get_top_comuni_per_finanziamento_pro_capite(self.get_top_comuni_per_finanziamento_pro_capite_filter_dict(), 3)

        aggregate_data['cicli_programmazione'] = self.get_cicli_programmazione()

        return aggregate_data

    def get_top_comuni_per_finanziamento_pro_capite(self, filter_dict, limit=None):
        return Territorio.objects.comuni().filter(**merge_dicts(filter_dict, Progetto.computabili_filter_dict('progetti__', self.get_ciclo_programmazione(), getattr(self, 'flag_visualizzazione', None)))).annotate(totale=Sum('progetti__finanz_totale_pubblico_netto')).annotate(totale_pro_capite=ExpressionWrapper(F('totale') / F('popolazione_totale'), output_field=DecimalField())).order_by('-totale_pro_capite')[:limit]


class GlobaleView(FlatPageMixin, AggregatoMixin, TemplateView):
    template_name = 'monitoraggio/globale.html'
    flatpage_url = '/dati/'

    def get_item_url(self, item):
        return self.add_ciclo_programmazione_querystring(item.get_absolute_url())

    @cached_context
    def get_cached_context_data(self):
        return self.get_aggregate_data()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update(self.get_cached_context_data())

        return context


class GruppoProgrammiView(AggregatoMixin, TemplateView):
    template_name = 'monitoraggio/gruppoprogrammi_detail.html'

    def get_progetti_queryset(self, **kwargs):
        progetti = super().get_progetti_queryset(**kwargs).con_programmi(self.object.programmi)
        if self.object.codice in ('ue-fesr-1420', 'ue-fse-1420'):
            progetti = progetti.filter(ambiti__fondo_comunitario=self.object.codice[3:-5].upper())
        return progetti

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        return {'progetti__ambiti__programma__classificazione_superiore__classificazione_superiore__in': self.object.programmi}

    @cached_context
    def get_cached_context_data(self):
        context = self.get_aggregate_data()

        if self.object.codice in ('ue-fesr-0713', 'ue-fse-0713'):
            import csv
            import re
            from collections import OrderedDict
            from operator import itemgetter
            from ....opencoesione.models import DataFile

            def parse_float(num):
                try:
                    return float(num.replace('.', '').replace(',', '.'))
                except:
                    return None

            data_pagamenti_per_programma = datetime.date(2015, 12, 31)

            dotazioni_totali = csv.DictReader(open(DataFile.objects.get(slug='pagamenti_ammessi_data').file.path, 'r'), delimiter=';')

            pattern = re.compile('pagamenti ammessi (\d{4})1231')
            anni = [m.group(1) for m in map(pattern.match, dotazioni_totali.fieldnames) if m is not None]

            dotazioni_totali = list(dotazioni_totali)

            valori_per_anno = OrderedDict((x, {trend: {'dotazioni_totali': 0.0, 'pagamenti': 0.0} for trend in ('conv', 'cro')}) for x in anni)

            for trend in ('conv', 'cro'):
                programmi_per_trend = sorted(({'codice': programma.codice, 'descrizione': programma.descrizione} for programma in self.object.programmi if ' {} '.format(trend) in programma.descrizione_it.lower()), key=itemgetter('descrizione'))

                programmi_per_trend_codici = [programma['codice'] for programma in programmi_per_trend]

                dotazioni_totali_per_programma = {}
                pagamenti_per_programma = {}
                for row in dotazioni_totali:
                    programma_codice = row['OC_CODICE_PROGRAMMA']
                    if programma_codice in programmi_per_trend_codici:
                        data = data_pagamenti_per_programma.strftime('%Y%m%d')
                        try:
                            valore = row['DOTAZIONE TOTALE PROGRAMMA POST PAC {}'.format(data)]
                        except KeyError:
                            valore = row['DOTAZIONE TOTALE PROGRAMMA {}'.format(data)]

                        dotazioni_totali_per_programma[programma_codice] = parse_float(valore)

                        pagamenti_per_programma[programma_codice] = parse_float(row['pagamenti ammessi {}'.format(data)])

                pagamenti_per_programma_tutti = [{'programma': programma['descrizione'], 'dotazioni_totali': dotazioni_totali_per_programma[programma['codice']], 'pagamenti': pagamenti_per_programma[programma['codice']], 'rimanenti': max(0, dotazioni_totali_per_programma[programma['codice']] - pagamenti_per_programma[programma['codice']])} for programma in programmi_per_trend]
                pagamenti_per_programma_regionali = [x for x in pagamenti_per_programma_tutti if x['programma'].startswith('POR ') or x['programma'].startswith('ROP ')]
                pagamenti_per_programma_nazionali = [x for x in pagamenti_per_programma_tutti if not (x['programma'].startswith('POR ') or x['programma'].startswith('ROP '))]

                context['pagamenti_per_programma_{}'.format(trend)] = pagamenti_per_programma_regionali
                if len(pagamenti_per_programma_nazionali):
                    context['pagamenti_per_programma_{}'.format(trend)] += [{}] + pagamenti_per_programma_nazionali

                for row in dotazioni_totali:
                    programma_codice = row['OC_CODICE_PROGRAMMA']
                    if programma_codice in programmi_per_trend_codici:
                        for anno in valori_per_anno:
                            data = '{}1231'.format(anno)

                            valori_per_anno[anno][trend]['pagamenti'] += parse_float(row['pagamenti ammessi {}'.format(data)] or 0)

                            try:
                                valore = row['DOTAZIONE TOTALE PROGRAMMA POST PAC {}'.format(data)]
                            except KeyError:
                                valore = row['DOTAZIONE TOTALE PROGRAMMA {}'.format(data)]

                            valori_per_anno[anno][trend]['dotazioni_totali'] += parse_float(valore)

            for anno, valori in valori_per_anno.items():
                valori['totale'] = {
                    'pagamenti': sum(valori[trend]['pagamenti'] for trend in valori),
                    'dotazioni_totali': sum(valori[trend]['dotazioni_totali'] for trend in valori),
                }

            context['percentuale_pagamenti_per_anno'] = OrderedDict((datetime.date(int(anno), 12, 31), {trend: 100 * valori[trend]['pagamenti'] / valori[trend]['dotazioni_totali'] for trend in valori}) for anno, valori in valori_per_anno.items())

            context['data_pagamenti_per_programma'] = data_pagamenti_per_programma

        return context

    def get_context_data(self, **kwargs):
        try:
            self.object = GruppoProgrammi(codice=self.kwargs.get('slug'))
        except:
            raise Http404

        context = super().get_context_data(object=self.object, **kwargs)

        context.update(self.get_cached_context_data())

        return context


class ProgrammaDetailView(WidgetEmbedCodeMixin, AggregatoMixin, DetailView):
    model = Programma

    def get_queryset(self):
        return super().get_queryset().programmi()

    def get_progetti_queryset(self, **kwargs):
        return super().get_progetti_queryset(**kwargs).con_programmi([self.object])

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        return {'progetti__ambiti__programma__classificazione_superiore__classificazione_superiore': self.object}

    @cached_context
    def get_cached_context_data(self):
        return self.get_aggregate_data()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update(self.get_cached_context_data())

        context['has_documenti'] = self.object.documenti.exists() or self.object.collegamenti.exists()
        context['has_contenuticomunicazione'] = CommunicationContent.objects.visibili().filter(references__type='1', references__code=self.object.codice).exists()

        if self.object in GruppoProgrammi('ue-1420').programmi:
            context['widget_embed_code'] = self.get_widget_embed_code(reverse('programma_widget_detail', kwargs={'pk': self.object.codice}))
        else:
            context['widget_embed_code'] = False

        return context


class AreaDetailView(AggregatoMixin, DetailView):
    model = Area
    flag_visualizzazione = ('0', '9')
    regione = False

    def get_item_url(self, item):
        if isinstance(item, self.model):
            return self.add_ciclo_programmazione_querystring(item.get_absolute_url())
        else:
            return super().get_item_url(item)

    def get_search_filter_querystring(self):
        from django.http import QueryDict
        if self.regione:
            querystring = 'tipo_area_reg={}.{:02d}'.format(self.object.codice, self.regione.cod_reg)
        else:
            querystring = self.object.get_search_filter_querystring()
        params = QueryDict(querystring, mutable=True)
        params.update({'selected_facets': 'is_pubblicato:1'})
        params.update({'selected_facets': 'is_pubblicato:2'})
        return params.urlencode(safe=':')

    def get_queryset(self):
        return super().get_queryset().non_partizioni()

    def get_progetti_queryset(self, **kwargs):
        return super().get_progetti_queryset(**kwargs).in_area(self.object, self.regione)

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        if self.object.is_root:
            filter_dict = {'progetti__aree__classificazione_superiore': self.object}
            if self.regione:
                filter_dict.update(self.regione.get_filter_dict('progetti__aree__classificazione_set__comuni__'))
                filter_dict.update({'pk': F('progetti__aree__classificazione_set__comuni__pk')})
            return filter_dict
        else:
            return {'progetti__aree': self.object}

    @cached_context
    def get_cached_context_data(self):
        context = self.get_aggregate_data()

        progetti = self.get_progetti_queryset()

        context['fonti'] = self.classificazioni_objs2dicts(self.add_totali(Fonte.objects.all(), progetti.totali_group_by('ambiti__fonte_id')))

        if self.object.is_root:
            aree = self.add_totali(self.object.classificazione_set.prefetch_related('classificazione_set__comuni').order_by('descrizione'), progetti.totali_group_by('aree__codice'))
            context['lista_aree'] = [merge_dicts({'nome': o.descrizione, 'url': self.get_item_url(o), 'regioni': o.lista_regioni}, o.totali) for o in aree if o.totali.get('totale_progetti', 0)]
            context['aree'] = self.territori_objs2dicts(aree, 'codice')
        else:
            # context['comuni'] = self.territori_objs2dicts(self.add_totali(Territorio.objects.comuni().filter(aree__classificazione_superiore=self.object), progetti.distinct().totali_group_by('territori__cod_com'), 'cod_com'), 'cod_com')
            context['comuni'] = self.territori_objs2dicts(self.add_totali(Territorio.objects.comuni().filter(localizzazione__aree=self.object), progetti.distinct().totali_group_by('territori__cod_com'), 'cod_com'), 'cod_com')

        return context

    def get_context_data(self, **kwargs):
        regione_slug = self.kwargs.get('regione_slug')
        if self.object.is_root:
            regioni = Territorio.objects.regioni().filter(cod_reg__in=(
                x['cod_reg'] for x in Territorio.objects.comuni().filter(**Progetto.computabili_filter_dict('aree__classificazione_superiore__progetti__', self.get_ciclo_programmazione(), self.flag_visualizzazione)).values('cod_reg').annotate(cnt=Count('aree')).filter(cnt__gt=0).order_by()
            ))
            if regione_slug:
                self.regione = get_object_or_404(regioni, slug=regione_slug)
        elif regione_slug:
            raise Http404
        else:
            regioni = False

        context = super().get_context_data(lista_regioni=regioni, regione=self.regione, **kwargs)

        context.update(self.get_cached_context_data())

        return context


class StrategiaS3DetailView(AggregatoMixin, DetailView):
    model = StrategiaS3
    territorio_s3 = False
    territorio = False

    def get_item_url(self, item, territorio_s3=None):
        if isinstance(item, StrategiaS3) and territorio_s3:
            return self.add_ciclo_programmazione_querystring(reverse('strategias3_territorios3_detail', kwargs={'pk': item.pk, 'territorio_s3_pk': territorio_s3.pk}))
        elif isinstance(item, (StrategiaS3, TerritorioS3)):
            return self.add_ciclo_programmazione_querystring(item.get_absolute_url())
        elif isinstance(item, Territorio) and not (self.territorio or self.territorio_s3):
            viewname = 'strategias3root_territorio_detail' if self.object.is_root else 'strategias3_territorio_detail'
            return self.add_ciclo_programmazione_querystring(reverse(viewname, kwargs={'pk': self.object.pk, 'territorio_slug': item.slug}))
        else:
            return super().get_item_url(item)

    def get_search_filter_querystring(self):
        from django.http import QueryDict
        querystring = self.object.get_search_filter_querystring()
        params = QueryDict(querystring, mutable=True)
        if self.territorio_s3:
            params.update({'t_s3': self.territorio_s3.pk})
        if self.territorio:
            params.update(QueryDict(self.territorio.get_search_filter_querystring()))
        return params.urlencode(safe=':')

    def get_progetti_queryset(self, **kwargs):
        return super().get_progetti_queryset(**kwargs).in_strategia_s3(self.object, self.territorio_s3, self.territorio)

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        filter_dict = self.object.get_filter_dict('progetti__strategie_s3__')
        if self.territorio_s3:
            filter_dict.update({'progetti__strategie_s3__territorio': self.territorio_s3})
        return filter_dict

    @cached_context
    def get_cached_context_data(self):
        context = self.get_aggregate_data()

        progetti = self.get_progetti_queryset()

        context['fonti'] = self.classificazioni_objs2dicts(self.add_totali(Fonte.objects.all(), progetti.totali_group_by('ambiti__fonte_id')))

        if not self.object.is_traiettoria:
            context['lista_traiettorie_s3'] = [
                merge_dicts(
                    {
                        'nome': o.descrizione,
                        'url': self.get_item_url(o),
                        'specializzazione_nome': o.classificazione_superiore.descrizione,
                        'specializzazione_url': self.get_item_url(o.classificazione_superiore, o.territorio),
                        'territorio_nome': o.territorio.descrizione,
                        'territorio_url': self.get_item_url(o.territorio),
                    },
                    o.totali
                )
                for o in self.add_totali(StrategiaS3.objects.filter(tipo=StrategiaS3.TIPO.traiettoria, **self.object.get_filter_dict()).select_related('classificazione_superiore', 'territorio').order_by('descrizione'), progetti.totali_group_by('strategie_s3__codice')) if o.totali.get('totale_progetti', 0)
            ]

        return context

    def get_context_data(self, **kwargs):
        territorio_s3_pk = self.kwargs.get('territorio_s3_pk')
        if territorio_s3_pk:
            self.territorio_s3 = get_object_or_404(TerritorioS3, pk=territorio_s3_pk)

        territorio_slug = self.kwargs.get('territorio_slug')
        if territorio_slug:
            self.territorio = get_object_or_404(Territorio, slug=territorio_slug)

        specializzazioni_s3 = []
        for tipo_strategia_s3 in StrategiaS3.objects.filter(tipo=StrategiaS3.TIPO.tipo_strategia).prefetch_related('classificazione_set').order_by('codice'):
            specializzazioni_s3.append(tipo_strategia_s3)
            for specializzazione_s3 in tipo_strategia_s3.classificazione_set.all():
                specializzazioni_s3.append(specializzazione_s3)

        context = super().get_context_data(
            lista_specializzazioni_s3=specializzazioni_s3,
            territorio_s3=self.territorio_s3,
            territorio=self.territorio,
            **kwargs
        )

        context.update(self.get_cached_context_data())

        return context


class ClassificazioneAzioneDetailView(AggregatoMixin, DetailView):
    model = ClassificazioneAzione

    def get_queryset(self):
        return super().get_queryset().principali()

    def get_progetti_queryset(self, **kwargs):
        return super().get_progetti_queryset(**kwargs).con_classificazione_azione(self.object)

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        return {'progetti__classificazione_azione__classificazione_superiore': self.object}

    def get_comuni_csv_url(self):
        return reverse('classificazioneazione_comuni_csv', kwargs={'slug': self.object.slug})

    @cached_context
    def get_cached_context_data(self):
        return self.get_aggregate_data()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update(self.get_cached_context_data())

        return context


class TemaSinteticoDetailView(AggregatoMixin, DetailView):
    model = TemaSintetico

    def get_progetti_queryset(self, **kwargs):
        return super().get_progetti_queryset(**kwargs).con_tema_sintetico(self.object)

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        return {'progetti__tema_sintetico': self.object}

    def get_comuni_csv_url(self):
        return reverse('temasintetico_comuni_csv', kwargs={'slug': self.object.slug})

    @cached_context
    def get_cached_context_data(self):
        return self.get_aggregate_data()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update(self.get_cached_context_data())

        return context


class FocusDetailView(AggregatoMixin, DetailView):
    model = Focus
    flag_visualizzazione = ('0', '9')

    def get_queryset(self):
        return super().get_queryset().select_related('classificazione_superiore').prefetch_related('classificazione_set')

    def get_progetti_queryset(self, localizzabili=False, **kwargs):
        return super().get_progetti_queryset(**kwargs).nel_focus(self.object, localizzabili)

    def _get_cicli(self, progetti):
        cicli = {}
        ambiti = set()
        for item in progetti.annotate(ciclo_programmazione=F('progetti_focus__ciclo_programmazione'), ambito=F('progetti_focus__ambito')).values('ciclo_programmazione', 'ambito').annotate(totale_finanziamenti=Sum('finanz_totale_pubblico_netto')).order_by('-ciclo_programmazione'):
            ciclo = Ambito.inv_cicli_programmazione_dict()[item['ciclo_programmazione']].replace('_', '-')
            ambito = ProgettoFocus.AMBITO[item['ambito']]
            if ciclo not in cicli:
                cicli[ciclo] = {'ciclo': ciclo}
            cicli[ciclo][ambito] = item['totale_finanziamenti']
            ambiti.add(ambito)

        cicli = [{**{'Ciclo': ciclo['ciclo']}, **{x: ciclo.get(x, 0) for x in ambiti}} for ciclo in cicli.values()]

        return sorted(cicli, key=lambda x: x['Ciclo'], reverse=True)

    def _get_territori(self, totale_finanziamenti):
        def percentage(a, b):
            return min(round((a or 0) / b * 100, 2) if b else 0, 100)

        progetti = self.get_progetti_queryset(localizzabili=True)

        impegni_by_cod_reg = {x['cod_reg']: round(float(x['totale_impegni'] or 0)) for x in progetti.annotate(cod_reg=F('progetti_focus__regione__cod_reg')).values('cod_reg').annotate(totale_impegni=Sum('impegni__ammontare'))}

        regioni = [{'codice': o.cod_reg, 'nome': str(o), 'totale_progetti': o.totali['totale_progetti'], 'totale_finanziamenti': o.totali['totale_finanziamenti'], 'percentuale_impegni': percentage(impegni_by_cod_reg.get(o.cod_reg, 0), o.totali['totale_finanziamenti']), 'percentuale_pagamenti': percentage(o.totali['totale_pagamenti'], o.totali['totale_finanziamenti'])} for o in self.add_totali(Territorio.objects.regioni(), progetti.totali_group_by('progetti_focus__regione__cod_reg'), 'cod_reg')]

        totale_finanziamenti_localizzabili = sum(x['totale_finanziamenti'] for x in regioni)

        return {
            'regioni': regioni,
            'percentuale_finanziamenti_localizzabili': percentage(totale_finanziamenti_localizzabili, totale_finanziamenti),
            'percentuale_finanziamenti_mezzogiorno': percentage(progetti.filter(macro_area=Progetto.MACRO_AREA.mezzogiorno).aggregate(totale_finanziamenti=Sum('finanz_totale_pubblico_netto'))['totale_finanziamenti'], totale_finanziamenti_localizzabili),
        }

    def _get_classificazioniazione_principali(self, progetti):
        classi_finanziarie_keys = ['0-100K', '100K-500K', '500K-1mln', '1mln-2mln', '2mln-5mln', '5mln-10mln', '> 10mln']
        classi_finanziarie = progetti.annotate(
            classificazioneazione_id=F('classificazione_azione__classificazione_superiore_id'),
            classe_finanziaria=Case(
                When(finanz_totale_pubblico_netto__lte=100000, then=Value(classi_finanziarie_keys[0])),
                When(finanz_totale_pubblico_netto__lte=500000, then=Value(classi_finanziarie_keys[1])),
                When(finanz_totale_pubblico_netto__lte=1000000, then=Value(classi_finanziarie_keys[2])),
                When(finanz_totale_pubblico_netto__lte=2000000, then=Value(classi_finanziarie_keys[3])),
                When(finanz_totale_pubblico_netto__lte=5000000, then=Value(classi_finanziarie_keys[4])),
                When(finanz_totale_pubblico_netto__lte=10000000, then=Value(classi_finanziarie_keys[5])),
                default=Value(classi_finanziarie_keys[6]),
                output_field=CharField(),
            )
        ).values('classificazioneazione_id', 'classe_finanziaria').annotate(totale_finanziamenti=Sum('finanz_totale_pubblico_netto'), totale_progetti=Count('pk'))

        classi_finanziarie_by_classificazioneazione_id = {}
        for item in classi_finanziarie:
            classificazioneazione_id = item.pop('classificazioneazione_id')
            if classificazioneazione_id not in classi_finanziarie_by_classificazioneazione_id:
                classi_finanziarie_by_classificazioneazione_id[classificazioneazione_id] = {k: False for k in reversed(classi_finanziarie_keys)}
            classi_finanziarie_by_classificazioneazione_id[classificazioneazione_id][item.pop('classe_finanziaria')] = item

        avanzamento_procedurale = progetti.annotate(classificazioneazione_id=F('classificazione_azione__classificazione_superiore_id'), stato_procedurale2=Func(F('stato_procedurale'), Value('2'), Value('3'), function='replace')).values('classificazioneazione_id', 'stato_procedurale2').exclude(classificazioneazione_id__in=('01', '08')).annotate(totale_finanziamenti=Sum('finanz_totale_pubblico_netto')).order_by('stato_procedurale2')

        avanzamento_procedurale_by_classificazioneazione_id = {}
        for item in avanzamento_procedurale:
            classificazioneazione_id = item['classificazioneazione_id']
            if classificazioneazione_id not in avanzamento_procedurale_by_classificazioneazione_id:
                avanzamento_procedurale_by_classificazioneazione_id[classificazioneazione_id] = {x[0]: 0 for x in Progetto.STATO_PROCEDURALE if x[0] != '2'}
            avanzamento_procedurale_by_classificazioneazione_id[classificazioneazione_id][item['stato_procedurale2']] = item['totale_finanziamenti']

        return [{
            'nome': o.descrizione_breve,
            'codice': o.codice,
            'totale_finanziamenti': o.totali['totale_finanziamenti'],
            'classi_finanziarie': classi_finanziarie_by_classificazioneazione_id[o.codice],
            'avanzamento_procedurale': [(Progetto.STATO_PROCEDURALE[x[0]], x[1]) for x in avanzamento_procedurale_by_classificazioneazione_id.get(o.codice, {}).items()],
        } for o in self.add_totali(ClassificazioneAzione.objects.principali().exclude(codice='ND'), progetti.totali_group_by('classificazione_azione__classificazione_superiore_id')) if o.totali['totale_progetti']]

    @cached_context
    def get_cached_context_data(self):
        main_object = self.object if self.object.is_root else self.object.classificazione_superiore

        progetti = self.get_progetti_queryset()

        context = progetti.totali()

        context.update(progetti.aggregate(totale_impegni=Sum('impegni__ammontare')))

        context['cicli'] = self._get_cicli(progetti)

        context['classi'] = [merge_dicts({'nome': o.nome}, o.totali) for o in self.add_totali(self.object.classificazione_set.all(), progetti.totali_group_by('focus__id'))]

        context['territori'] = self._get_territori(context['totale_finanziamenti'])

        context['classificazioniazione_principali'] = self._get_classificazioniazione_principali(progetti)
        context['classificazioniazione_principali_con_avanzamento_procedurale'] = [x for x in context['classificazioniazione_principali'] if x['avanzamento_procedurale']]

        progetti_monoprogramma = progetti.annotate(programma_count=Count('ambiti__programma__classificazione_superiore__classificazione_superiore_id')).filter(programma_count=1)
        top_programmi = Programma.objects.programmi().filter(classificazione_set__classificazione_set__ambiti__progetto__in=progetti_monoprogramma).annotate(totale_finanziamenti=Sum('classificazione_set__classificazione_set__ambiti__progetto__finanz_totale_pubblico_netto')).order_by('-totale_finanziamenti')[:10]
        context['top_programmi'] = [{'nome': o.descrizione, 'ciclo_programmazione': Ambito.inv_cicli_programmazione_dict()[o.ciclo_programmazione].replace('_', '-'), 'totale_finanziamenti': o.totale_finanziamenti} for o in top_programmi]

        context['main_object'] = main_object

        focus_classi = [(self.add_ciclo_programmazione_querystring(o.get_absolute_url()), str(o), self.object.pk == o.pk) for o in main_object.classificazione_set.all()]
        context['focus_classi'] = [(self.add_ciclo_programmazione_querystring(main_object.get_absolute_url()), _('Tutte le classi'), self.object.pk == main_object.pk)] + focus_classi if focus_classi else False

        context['search_url'] = '{}?q=&{}'.format(reverse('progetto_search'), main_object.get_search_filter_querystring())

        context['ciclo_programmazione'] = self.get_ciclo_programmazione()

        context['cicli_programmazione'] = self.get_cicli_programmazione()
        if len(context['cicli_programmazione']) < 2:
            context['cicli_programmazione'] = False

        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update(self.get_cached_context_data())

        main_object = self.object if self.object.is_root else self.object.classificazione_superiore

        context['data_url'] = main_object.file_dati.file.url if main_object.file_dati else False
        context['metadata_url'] = main_object.file_metadati.file.url if main_object.file_metadati else False

        context['data_aggiornamento'] = main_object.data_aggiornamento or datetime.datetime.strptime(settings.DATA_AGGIORNAMENTO, '%Y-%m-%d')

        return context


class SoggettoDetailView(XRobotsTagTemplateResponseMixin, AggregatoMixin, DetailView):
    model = Soggetto

    def get_x_robots_tag(self):
        return 'noindex' if self.object.is_privacy else False

    def get_progetti_queryset(self, **kwargs):
        return super().get_progetti_queryset(**kwargs).del_soggetto(self.object)

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        return {'progetti__soggetti__pk': self.object.pk}

    @property
    def cache_enabled(self):
        return self.object.progetti.count() > settings.BIG_SOGGETTI_THRESHOLD

    @cached_context
    def get_cached_context_data(self):
        sum_dict = lambda *ds: {k: sum(d.get(k, 0) or 0 for d in ds) for k in ds[0]}

        context = self.get_aggregate_data()

        # == COLLABORATORI CON CUI SI SPARTISCONO PIÙ SOLDI =======================================

        context['top_collaboratori'] = Soggetto.objects.filter(progetti__ruoli__soggetto=self.object).exclude(pk=self.object.pk).annotate(totale=Count('pk')).order_by('-totale')[:7]

        # == TOTALI PER REGIONI (E NAZIONI) =======================================================

        progetti = self.get_progetti_queryset()

        # i progetti del soggetto localizzati in più territori (vengono considerati a parte per evitare di contarli più volte nelle aggregazioni)
        progetti_multilocalizzati_pks = [x['pk'] for x in progetti.values('pk').annotate(cnt=Count('territori__cod_reg', distinct=True)).filter(cnt__gt=1)]

        totali_non_multilocalizzati = {x.pop('id'): x for x in progetti.exclude(pk__in=progetti_multilocalizzati_pks).totali_group_by('territori__cod_reg')}

        totali_multilocalizzati_nazionali = {}
        totali_multilocalizzati_non_nazionali = {}
        for progetto in Progetto.objects.computabili(self.get_ciclo_programmazione()).filter(pk__in=progetti_multilocalizzati_pks).prefetch_related('territori'):
            totali_progetto = {'totale_finanziamenti': float(progetto.finanz_totale_pubblico_netto or 0), 'totale_pagamenti': float(progetto.pagamento or 0), 'totale_progetti': 1}

            if any(t.is_nazionale for t in progetto.territori.all()) and not any(t.is_estero for t in progetto.territori.all()):  # con almeno una localizzazione nazionale e nessuna estera
                totali_multilocalizzati_nazionali = sum_dict(totali_progetto, totali_multilocalizzati_nazionali)
            else:
                totali_multilocalizzati_non_nazionali = sum_dict(totali_progetto, totali_multilocalizzati_non_nazionali)

        if any(totali_multilocalizzati_nazionali.values()):
            totali_non_multilocalizzati[0] = sum_dict(totali_multilocalizzati_nazionali, totali_non_multilocalizzati.get(0, {}))

        territori = []

        for territorio in Territorio.objects.regioni(with_nations=True).filter(cod_reg__in=totali_non_multilocalizzati.keys()).order_by('-tipo', 'denominazione'):
            territorio.totali = totali_non_multilocalizzati[territorio.cod_reg]
            territori.append(territorio)

        # assegno a un territorio fittizio i progetti multilocalizzati senza localizzazione nazionale
        if any(totali_multilocalizzati_non_nazionali.values()):
            territorio = Territorio(denominazione=_('In più territori'), tipo='X')
            territorio.totali = totali_multilocalizzati_non_nazionali
            territori.append(territorio)

        context['territori'] = [merge_dicts({'nome': str(o), 'slug': o.slug, 'url': '#' if o.tipo == 'X' else self.get_item_url(o)}, o.totali) for o in territori]

        # == TOTALI PER RUOLO =====================================================================

        totali_by_ruolo_by_progetto = defaultdict(dict)

        for ruolo_cod, ruolo_desc in Ruolo.inv_ruoli_dict().items():
            for totali in self.object.ruoli.con_ruolo(ruolo_desc).filter(**Progetto.computabili_filter_dict('progetto__', self.get_ciclo_programmazione())).values('progetto_id').annotate(totale_finanziamenti=Sum('progetto__finanz_totale_pubblico_netto'), totale_pagamenti=Sum('progetto__pagamento'), totale_progetti=Count('progetto')):
                totali_by_ruolo_by_progetto[totali.pop('progetto_id')][ruolo_cod] = totali

        totali_by_ruolo = defaultdict(dict)

        for totaliprogetto_by_ruolo in totali_by_ruolo_by_progetto.values():
            codice = ''.join(totaliprogetto_by_ruolo.keys())  # in caso di più ruoli per uno stesso progetto si crea un nuovo codice
            totali = list(totaliprogetto_by_ruolo.values())[0]  # prendo il primo dei totali per ruolo, tanto DEVONO essere tutti uguali

            totali_by_ruolo[codice] = sum_dict(totali, totali_by_ruolo[codice])

        context['ruoli'] = [merge_dicts({'nome': ' / '.join(sorted([str(Ruolo.RUOLO[r]) for r in codice])), 'url': '{}&ruolo={}'.format(self.get_search_url(), codice)}, totali) for codice, totali in totali_by_ruolo.items()]

        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update(self.get_cached_context_data())

        return context


class TerritorioDetailView(AggregatoMixin, DetailView):
    model = Territorio

    def get_item_url(self, item):
        if isinstance(item, self.model):
            return self.add_ciclo_programmazione_querystring(item.get_absolute_url())
        else:
            return super().get_item_url(item)

    def get_queryset(self):
        return super().get_queryset().exclude(tipo=self.model.TIPO.E)

    def get_progetti_queryset(self, **kwargs):
        return super().get_progetti_queryset(**kwargs).nei_territori([self.object])

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        return self.object.get_filter_dict()

    def get_comuni_csv_url(self):
        if self.object.is_macroarea or self.object.is_regione or self.object.is_provincia:
            return reverse('territorio_comuni_csv', kwargs={'slug': self.object.slug})
        else:
            return None

    @cached_context
    def get_cached_context_data(self):
        return self.get_aggregate_data()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update(self.get_cached_context_data())

        return context


class TerritorioEsteroListView(AggregatoMixin, ListView):
    model = Territorio

    def get_search_filter_querystring(self):
        import urllib.parse
        return urllib.parse.urlencode({'territorio_tipo': self.model.TIPO.E})

    def get_queryset(self):
        return super().get_queryset().esteri()

    def get_progetti_queryset(self, **kwargs):
        return super().get_progetti_queryset(**kwargs).nei_territori(self.get_queryset())

    @cached_context
    def get_cached_context_data(self):
        context = self.get_aggregate_data()

        progetti = self.get_progetti_queryset()

        territori = self.get_queryset()

        multi_territori = {}
        for progetto in progetti.annotate(cnt=Count('territori')).filter(cnt__gt=1):
            key = ', '.join(sorted([t.denominazione for t in progetto.territori if t in territori]))
            multi_territori.setdefault(key, []).append(progetto.pk)

        progetti_multilocalizzati_pks = [item for sublist in multi_territori.values() for item in sublist]

        territori = self.add_totali(territori, progetti.exclude(pk__in=progetti_multilocalizzati_pks).totali_group_by('localizzazione__territorio_id'))

        for name, pks in multi_territori.items():
            territorio = self.model(denominazione=name, tipo=self.model.TIPO.E)
            territorio.totali = Progetto.objects.filter(pk__in=pks).totali()
            territori.append(territorio)

        context['territori'] = [merge_dicts({'nome': str(o), 'slug': o.slug, 'url': '#'}, o.totali) for o in territori if o.totali.get('totale_progetti', 0)]

        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update(self.get_cached_context_data())

        return context


class BaseComuniCSVDetailView(AggregatoMixin, DetailView):
    def get_comuni_filter_dict(self):
        return {}

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        return {}

    def get(self, request, *args, **kwargs):
        import csv
        from django.utils.formats import number_format

        self.object = self.get_object()

        comuni_con_pro_capite = self.get_top_comuni_per_finanziamento_pro_capite(self.get_top_comuni_per_finanziamento_pro_capite_filter_dict())
        altri_comuni = Territorio.objects.comuni().filter(**self.get_comuni_filter_dict()).exclude(pk__in=(x.pk for x in comuni_con_pro_capite))

        comuni = list(comuni_con_pro_capite) + list(altri_comuni)

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}_pro_capite.csv'.format(self.kwargs.get('slug', 'all'))

        writer = csv.writer(response, dialect='excel-semicolon')

        writer.writerow([_('Comune'), _('Provincia'), _('Finanziamento pro capite')])

        for comune in comuni:
            writer.writerow([
                comune.denominazione,
                comune.provincia.denominazione,
                number_format(getattr(comune, 'totale_pro_capite', 0), decimal_pos=2),
            ])

        return response


class ClassificazioneAzioneComuniCSVDetailView(BaseComuniCSVDetailView):
    model = ClassificazioneAzione

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        return {'progetti__classificazione_azione__classificazione_superiore': self.object}


class TemaSinteticoComuniCSVDetailView(BaseComuniCSVDetailView):
    model = TemaSintetico

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        return {'progetti__tema_sintetico': self.object}


class TerritorioComuniCSVDetailView(BaseComuniCSVDetailView):
    model = Territorio

    def get_comuni_filter_dict(self):
        return self.object.get_filter_dict()

    def get_top_comuni_per_finanziamento_pro_capite_filter_dict(self):
        return self.object.get_filter_dict()
