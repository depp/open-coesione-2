# -*- coding: utf-8 -*-
import datetime
import zipfile
from collections import OrderedDict
from django import forms
from django.conf import settings
from django.contrib.humanize.templatetags.humanize import intword, intcomma
from django.http import HttpResponse
from django.utils.functional import lazy
from django.utils.text import format_lazy
from django.utils.translation import ugettext_lazy as _
from ..gruppo_programmi import GruppoProgrammi
from ..models import Territorio, Soggetto, Ruolo, TemaSintetico, Progetto, ClassificazioneAzione, Fonte, Programma,\
    Focus, Area, StrategiaS3, TerritorioS3
from ...opendata.models import MetadataFile
from ....enhancedsearch.forms import MultiSelectWithRangeFacetedSearchForm
from ....enhancedsearch.views import MultiSelectWithRangeFacetedSearchView


def range_format(range_start, range_end, format='{}'):
    def humanize(value):
        return intcomma(intword(value))
    humanize_lazy = lazy(humanize, str)

    range_start, range_end = (format_lazy(format, humanize_lazy(r)) if r is not None else r for r in (range_start, range_end))

    if range_start is None:
        return format_lazy(_('fino a {}'), range_end)
    elif range_end is None:
        return format_lazy(_('oltre {}'), range_start)
    else:
        return format_lazy(_('da {} a {}'), range_start, range_end)


class OCMultiSelectWithRangeFacetedSearchForm(MultiSelectWithRangeFacetedSearchForm):
    cup = forms.CharField(required=False)
    soggetto = forms.CharField(required=False)
    ruolo = forms.CharField(required=False)
    gruppo_programmi = forms.CharField(required=False)
    programma = forms.CharField(required=False)
    tipo_area_reg = forms.CharField(required=False)
    area = forms.CharField(required=False)
    s3 = forms.CharField(required=False)
    t_s3 = forms.CharField(required=False)
    territorio_tipo = forms.CharField(required=False)
    territorio_com = forms.IntegerField(required=False)
    territorio_prov = forms.IntegerField(required=False)
    territorio_reg = forms.IntegerField(required=False)
    territorio_mac = forms.IntegerField(required=False)

    def search(self):
        sqs = super().search()

        if self.is_valid():
            for fld in self.fields:
                if fld not in ('q', 'ruolo'):
                    val = self.cleaned_data.get(fld)
                    if val or val == 0:
                        if fld == 'cup':
                            sqs = sqs.filter(cup_s__in=val.split('|'))
                        elif fld == 'soggetto':
                            codici_ruolo = [x[0] for x in Ruolo.RUOLO]

                            sqs = sqs.filter(soggetto__in=['{}|{}'.format(val, r) for r in codici_ruolo])

                            ruolo = self.cleaned_data.get('ruolo')
                            if ruolo:
                                for r in codici_ruolo:
                                    condition = {'soggetto': '{}|{}'.format(val, r)}
                                    if r in ruolo:
                                        sqs = sqs.filter(**condition)
                                    else:
                                        sqs = sqs.exclude(**condition)
                        elif fld == 'gruppo_programmi':
                            try:
                                gruppo_programmi = GruppoProgrammi(codice=val)
                            except:
                                pass
                            else:
                                sqs = sqs.filter(programma__in=[p.codice for p in gruppo_programmi.programmi])
                                if val in ('ue-fesr-1420', 'ue-fse-1420'):
                                    sqs = sqs.filter(fondo_comunitario=val[3:-5])
                        else:
                            sqs = sqs.filter(**{fld: val})

        return sqs


class OCMultiSelectWithRangeFacetedSearchViewSearchView(MultiSelectWithRangeFacetedSearchView):
    form_class = OCMultiSelectWithRangeFacetedSearchForm
    paginate_by = 10
    ordering = '-costo'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['section'] = 'dati'

        context['territorio_filter_base_url'] = self._get_params('territorio_com', 'territorio_prov', 'territorio_reg', 'territorio_mac')

        context['filters'] = []

        if 'query' in context and context['query']:
            context['filters'].append({'name': _('Testo contenuto'), 'value': context['query'], 'remove_url': self._get_params('q')})

        territorio_com = self.request.GET.get('territorio_com')
        territorio_prov = self.request.GET.get('territorio_prov')
        territorio_reg = self.request.GET.get('territorio_reg')
        territorio_mac = self.request.GET.get('territorio_mac')
        try:
            if territorio_com and territorio_com != '0':
                territorio = Territorio.objects.comuni().get(cod_com=territorio_com)
            elif territorio_prov and territorio_prov != '0':
                territorio = Territorio.objects.province().get(cod_prov=territorio_prov)
            elif territorio_reg:
                territorio = Territorio.objects.regioni(with_nations=True).get(cod_reg=territorio_reg)
            elif territorio_mac:
                territorio = Territorio.objects.macroaree().get(cod_mac=territorio_mac)
            else:
                territorio = None
        except:
            pass
        else:
            if territorio:
                context['filters'].append({'name': _('Territorio'), 'value': territorio.nome_con_provincia, 'remove_url': context['territorio_filter_base_url']})

        return context


class ProgettoSearchView(OCMultiSelectWithRangeFacetedSearchViewSearchView):
    template_name = 'monitoraggio/progetto_search.html'

    model = Progetto

    FACETS = ('is_active', 'is_pubblicato', 'natura', 'tema', 'fonte', 'tipo_area', 'stato', 'ciclo_programmazione', 'focus')

    RANGES = {
        'costo': {
            '0-0TO1K': {
                'qrange': '{* TO 1000]',
                'label': range_format(None, 1000, '€ {}'),
            },
            '1-1KTO10K': {
                'qrange': '{1000 TO 10000]',
                'label': range_format(1000, 10000, '€ {}'),
            },
            '2-10KTO100K': {
                'qrange': '{10000 TO 100000]',
                'label': range_format(10000, 100000, '€ {}'),
            },
            '3-100KTO1M': {
                'qrange': '{100000 TO 1000000]',
                'label': range_format(100000, 1000000, '€ {}'),
            },
            '4-1MTO10M': {
                'qrange': '{1000000 TO 10000000]',
                'label': range_format(1000000, 10000000, '€ {}'),
            },
            '5-10MTOINF': {
                'qrange': '{10000000 TO *]',
                'label': range_format(10000000, None, '€ {}'),
            },
        },
        'data_inizio': {
            '00-2024': {
                'qrange': '[2024-01-01T00:00:00Z TO *]',
                'label': '2024',
            },
            '01-2023': {
                'qrange': '[2023-01-01T00:00:00Z TO 2023-12-31T23:59:59Z]',
                'label': '2023',
            },
            '02-2022': {
                'qrange': '[2022-01-01T00:00:00Z TO 2022-12-31T23:59:59Z]',
                'label': '2022',
            },
            '03-2021': {
                'qrange': '[2021-01-01T00:00:00Z TO 2021-12-31T23:59:59Z]',
                'label': '2021',
            },
            '04-2020': {
                'qrange': '[2020-01-01T00:00:00Z TO 2020-12-31T23:59:59Z]',
                'label': '2020',
            },
            '05-2019': {
                'qrange': '[2019-01-01T00:00:00Z TO 2019-12-31T23:59:59Z]',
                'label': '2019',
            },
            '06-2018': {
                'qrange': '[2018-01-01T00:00:00Z TO 2018-12-31T23:59:59Z]',
                'label': '2018',
            },
            '07-2017': {
                'qrange': '[2017-01-01T00:00:00Z TO 2017-12-31T23:59:59Z]',
                'label': '2017',
            },
            '08-2016': {
                'qrange': '[2016-01-01T00:00:00Z TO 2016-12-31T23:59:59Z]',
                'label': '2016',
            },
            '09-2015': {
                'qrange': '[2015-01-01T00:00:00Z TO 2015-12-31T23:59:59Z]',
                'label': '2015',
            },
            '10-2014': {
                'qrange': '[2014-01-01T00:00:00Z TO 2014-12-31T23:59:59Z]',
                'label': '2014',
            },
            '11-2013': {
                'qrange': '[2013-01-01T00:00:00Z TO 2013-12-31T23:59:59Z]',
                'label': '2013',
            },
            '12-2012': {
                'qrange': '[2012-01-01T00:00:00Z TO 2012-12-31T23:59:59Z]',
                'label': '2012',
            },
            '13-2011': {
                'qrange': '[2011-01-01T00:00:00Z TO 2011-12-31T23:59:59Z]',
                'label': '2011',
            },
            '14-2010': {
                'qrange': '[2010-01-01T00:00:00Z TO 2010-12-31T23:59:59Z]',
                'label': '2010',
            },
            '15-2009': {
                'qrange': '[2009-01-01T00:00:00Z TO 2009-12-31T23:59:59Z]',
                'label': '2009',
            },
            '16-2008': {
                'qrange': '[2008-01-01T00:00:00Z TO 2008-12-31T23:59:59Z]',
                'label': '2008',
            },
            '17-2007': {
                'qrange': '[2007-01-01T00:00:00Z TO 2007-12-31T23:59:59Z]',
                'label': '2007',
            },
            '18-early': {
                'qrange': '[1970-01-02T00:00:00Z TO 2006-12-31T23:59:59Z]',
                'label': format_lazy(_('prima del {year}'), **{'year': '2007'}),
            },
            '19-nd': {
                'qrange': '[* TO 1970-01-01T00:00:00Z]',
                'label': _('non disponibile'),
            }
        },
    }

    def get(self, request, *args, **kwargs):
        if not any(x.startswith('is_pubblicato') for x in request.GET.getlist('selected_facets')) and not request.GET.get('r'):
            from django.http import HttpResponseRedirect
            uri = request.build_absolute_uri()
            return HttpResponseRedirect('{}{}selected_facets=is_pubblicato:1'.format(uri, '&' if '?' in uri else '?'))

        return super().get(request, *args, **kwargs)

    @staticmethod
    def _get_objects_by_pk(pks):
        return {str(key): value for key, value in Progetto.objects.select_related('tema_sintetico', 'classificazione_azione__classificazione_superiore').prefetch_related('territori').in_bulk(pks).items()}

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()

        if not 'is_active:0' in kwargs['selected_facets']:
            kwargs['selected_facets'] += ['is_active:1']

        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        param = 'programma'
        if param in self.request.GET:
            try:
                obj = Programma.objects.get(pk=self.request.GET[param])
            except:
                pass
            else:
                context['filters'].append({'name': _('Programma'), 'value': str(obj), 'remove_url': self._get_params(param)})

        param = 'gruppo_programmi'
        if param in self.request.GET:
            try:
                obj = GruppoProgrammi(codice=self.request.GET[param])
            except:
                pass
            else:
                context['filters'].append({'name': _('Programmi'), 'value': str(obj), 'remove_url': self._get_params(param)})

        params = ('soggetto', 'ruolo')
        if params[0] in self.request.GET:
            try:
                obj = Soggetto.objects.get(slug=self.request.GET[params[0]])
            except:
                pass
            else:
                name = str(obj)
                if params[1] in self.request.GET:
                    name += ' ({})'.format('/'.join(sorted(set(str(Ruolo.RUOLO[r]) for r in self.request.GET[params[1]] if r in Ruolo.RUOLO))))
                context['filters'].append({'name': _('Soggetto'), 'value': name, 'remove_url': self._get_params(*params)})

        param = 'tipo_area_reg'
        if param in self.request.GET:
            try:
                pk, cod_reg = self.request.GET[param].split('.')
                obj = Area.objects.tipi_aree().get(pk=pk)
                reg = Territorio.objects.regioni().get(cod_reg=cod_reg)
            except:
                pass
            else:
                context['filters'].append({'name': _('Aggregazione territoriale'), 'value': '{} ({})'.format(obj, reg), 'remove_url': self._get_params(param)})

        param = 'area'
        if param in self.request.GET:
            try:
                obj = Area.objects.aree().get(pk=self.request.GET[param])
            except:
                pass
            else:
                context['filters'].append({'name': _('Area'), 'value': str(obj), 'remove_url': self._get_params(param)})

        param = 's3'
        if param in self.request.GET:
            try:
                obj = StrategiaS3.objects.get(pk=self.request.GET[param])
            except:
                pass
            else:
                context['filters'].append({'name': obj.get_tipo_display, 'value': obj.descrizione, 'remove_url': self._get_params(param)})

        param = 't_s3'
        if param in self.request.GET:
            try:
                obj = TerritorioS3.objects.get(pk=self.request.GET[param])
            except:
                pass
            else:
                context['filters'].append({'name': _('Strategia territoriale'), 'value': obj.descrizione, 'remove_url': self._get_params(param)})

        context['params'] = self._get_params()

        context['search_within_non_active'] = 'is_active:0' in self.request.GET.getlist('selected_facets')

        # definizione struttura dati per visualizzazione faccette

        facets = OrderedDict()

        facets['ciclo_programmazione'] = self._build_facet_field_info('ciclo_programmazione', _('Ciclo di programmazione'), {k: (v, v) for k, v in settings.CICLO_PROGRAMMAZIONE})
        facets['natura'] = self._build_facet_field_info('natura', _("Natura dell'investimento"), {o.codice: (o.descrizione, o.descrizione_breve) for o in ClassificazioneAzione.objects.principali()})
        facets['tema'] = self._build_facet_field_info('tema', _('Tema'), {o.codice: (o.descrizione, o.descrizione_breve) for o in TemaSintetico.objects.all()})
        facets['fonte'] = self._build_facet_field_info('fonte', _('Fonte'), {o.codice: (o.descrizione, o.descrizione_breve) for o in Fonte.objects.all()})
        facets['tipo_area'] = self._build_facet_field_info('tipo_area', _('Aggregazione territoriale'), {o.codice: (o.descrizione, o.descrizione) for o in list(Area.objects.tipi_aree()) + list(StrategiaS3.objects.filter(classificazione_superiore__isnull=True))})
        facets['stato'] = self._build_facet_field_info('stato', _('Stato progetto'), {k: (v, v) for k, v in Progetto.STATO})
        facets['focus'] = self._build_facet_field_info('focus', _('Focus'), {o.slug: (o.nome, o.nome) for o in Focus.objects.all()})

        facets['data_inizio'] = self._build_range_facet_queries_info('data_inizio', _('Anno di inizio'))
        facets['costo'] = self._build_range_facet_queries_info('costo', _('Finanziamenti'))

        facets['is_pubblicato'] = self._build_facet_field_info('is_pubblicato', _('Visualizzazione'), {'1': (_('Progetti pubblicati'), _('Progetti pubblicati')), '0': (_('Progetti esclusi'), _('Progetti esclusi')), '2': (_('Progetti FEASR in Aree Interne'), _('Progetti FEASR in Aree Interne'))})

        try:
            facets['stato']['values'] = sorted(facets['stato']['values'], key=lambda x: x['key'], reverse=True)
        except KeyError:
            pass

        context['my_facets'] = facets

        context['metadata_file'] = MetadataFile.objects.get(slug='progetti')

        context['data_aggiornamento'] = datetime.datetime.strptime(settings.DATA_AGGIORNAMENTO, '%Y-%m-%d')

        return context


class SoggettoSearchView(OCMultiSelectWithRangeFacetedSearchViewSearchView):
    template_name = 'monitoraggio/soggetto_search.html'

    model = Soggetto

    FACETS = ('ruolo', 'natura', 'tema')

    RANGES = {
        'costo': {
            '0-0TO100K': {
                'qrange': '{* TO 100000]',
                'label': range_format(None, 100000, '€ {}'),
            },
            '1-100KTO1M': {
                'qrange': '{100000 TO 1000000]',
                'label': range_format(100000, 1000000, '€ {}'),
            },
            '2-1MTO10M': {
                'qrange': '{1000000 TO 10000000]',
                'label': range_format(1000000, 10000000, '€ {}'),
            },
            '3-10MTO100M': {
                'qrange': '{10000000 TO 100000000]',
                'label': range_format(10000000, 100000000, '€ {}'),
            },
            '4-100MTO1G': {
                'qrange': '{100000000 TO 1000000000]',
                'label': range_format(100000000, 1000000000, '€ {}'),
            },
            '5-1GTOINF': {
                'qrange': '{1000000000 TO *]',
                'label': range_format(1000000000, None, '€ {}'),
            },
        },
        'n_progetti': {
            '0-0TO10': {
                'qrange': '{* TO 10]',
                'label': range_format(None, 10),
            },
            '1-10TO100': {
                'qrange': '{10 TO 100]',
                'label': range_format(10, 100),
            },
            '2-100TO1K': {
                'qrange': '{100 TO 1000]',
                'label': range_format(100, 1000),
            },
            '3-1KTO10K': {
                'qrange': '{1000 TO 10000]',
                'label': range_format(1000, 10000),
            },
            '4-10KTOINF': {
                'qrange': '{10000 TO *]',
                'label': range_format(10000, None),
            },
        },
    }

    @staticmethod
    def _get_objects_by_pk(pks):
        return {str(key): value for key, value in Soggetto.objects.in_bulk(pks).items()}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        for object in context['object_list']:
            object.ruolo = [Ruolo.RUOLO[r] for r in object.ruolo]

        # definizione struttura dati per visualizzazione faccette

        facets = OrderedDict()

        facets['ruolo'] = self._build_facet_field_info('ruolo', _('Ruolo'), {k: (v, v) for k, v in Ruolo.RUOLO})
        facets['natura'] = self._build_facet_field_info('natura', _("Natura dell'investimento"), {o.codice: (o.descrizione, o.descrizione_breve) for o in ClassificazioneAzione.objects.principali()})
        facets['tema'] = self._build_facet_field_info('tema', _('Tema'), {o.codice: (o.descrizione, o.descrizione_breve) for o in TemaSintetico.objects.all()})

        facets['costo'] = self._build_range_facet_queries_info('costo', _('Finanziamenti'))
        facets['n_progetti'] = self._build_range_facet_queries_info('n_progetti', _('Numero di progetti'))

        context['my_facets'] = facets

        return context


class ProgettoCSVSearchView(ProgettoSearchView):
    def get_queryset(self):
        return super().get_queryset().values_list('pk', flat=True)

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            results = form.search()
        else:
            results = self.get_queryset()

        # if kwargs.get('limit_results', True) and results.count() > settings.N_MAX_DOWNLOADABLE_RESULTS:
        if kwargs.get('limit_results', True):
            import hashlib
            import os
            from django.http import QueryDict

            query = QueryDict('', mutable=True)
            for k, v in sorted(request.GET.lists()):
                query.setlist(k, v)

            params = query.urlencode(safe=':')
            if 'r' in query:
                del (query['r'])
            params1 = query.urlencode(safe=':')

            hash = hashlib.md5(params1.encode()).hexdigest()
            filename = os.path.join(settings.MEDIA_ROOT, 'searchcsv', '{}.zip'.format(hash))

            try:
                with open(filename, 'rb') as f:
                    response = HttpResponse(f.read(), content_type='application/x-zip-compressed')
                    response['Content-Disposition'] = 'attachment; filename=progetti.csv.zip'
                return response
            except FileNotFoundError:
                from django.contrib import messages
                from django.http import HttpResponseRedirect
                from django.urls import reverse
                from django.utils.formats import number_format
                from taskmanager.models import AppCommand, Task

                task, created = Task.objects.get_or_create(
                    name=params1,
                    command=AppCommand.objects.get(name='csvexport'),
                    defaults={
                        'arguments': '--query {},--filename {}'.format(params, filename)
                    }
                )
                if task.status == Task.STATUS_IDLE:
                    task.launch()
                    # messages.info(request, format_lazy(_('La tua richiesta supera i {} risultati e il sistema sta generando il file dei progetti che vuoi scaricare.\nClicca tra qualche minuto sul bottone per scaricare il file.'), number_format(settings.N_MAX_DOWNLOADABLE_RESULTS, force_grouping=True)))
                    messages.info(request, _('Il sistema sta generando il file dei progetti che vuoi scaricare.\nClicca tra qualche minuto sul bottone per scaricare il file.'))
                else:
                    messages.info(request, _('Il sistema sta ancora elaborando il file dei progetti che hai richiesto.\nRiprova tra poco a cliccare sul bottone per scaricare il file.'))

                return HttpResponseRedirect(reverse('progetto_search') + '?' + params)
        else:
            import io
            import tempfile
            from ...opendata.utils import get_latest_localfile

            chunk_size = 10000

            clps = set()
            clps.add('COD_LOCALE_PROGETTO')
            for i in range(0, len(results), chunk_size):
                chunked_results = results[i:i + chunk_size]
                clps.update(Progetto.objects.filter(pk__in=chunked_results).values_list('codice_locale', flat=True))

            response = HttpResponse(content_type='application/x-zip-compressed')
            response['Content-Disposition'] = 'attachment; filename=progetti.csv.zip'

            with tempfile.NamedTemporaryFile(mode='w', suffix='.csv', encoding='UTF-8-sig') as tfile:
                with zipfile.ZipFile(get_latest_localfile('progetti_esteso.zip')) as zfile:
                    with zfile.open(zfile.namelist().pop(0)) as f:
                        for line in io.TextIOWrapper(f, 'UTF-8-sig'):
                            clp = line.split('";"', 2)[0].lstrip('"').replace('""', '"')
                            if clp in clps:
                                tfile.write(line)

                tfile.flush()

                with zipfile.ZipFile(response, 'w', zipfile.ZIP_DEFLATED) as zfile:
                    zfile.write(tfile.name, arcname='progetti.csv')

            return response
