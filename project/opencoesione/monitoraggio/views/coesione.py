# -*- coding: utf-8 -*-
from decimal import Decimal
from django.views.generic.detail import BaseDetailView, DetailView

from . import merge_dicts
from ..models import TemaSinteticoCoesione, TerritorioCoesione, Territorio, Valutazione
from ...views import FlatPageListView


def get_aggregatedata_from_api(viewname, request, kwargs=None):
    import json
    import urllib.request
    from rest_framework.reverse import reverse

    source_url = reverse(viewname, request=request, kwargs=kwargs)
    source_url = source_url.replace('http://localhost:8000', 'https://opencoesione.gov.it')

    try:
        with urllib.request.urlopen(source_url) as response:
            data = json.loads(response.read().decode())
    except Exception:
        return {}
    else:
        return data['aggregati']


def get_totals_from_data(data):
    if data:
        totali = data['totali']
        return {
            'totale_finanziamenti': Decimal(totali['costo_pubblico'].replace(',', '.')),
            'totale_pagamenti': Decimal(totali['pagamenti'].replace(',', '.')),
            'totale_progetti': int(totali['progetti']),
        }
    else:
        return {}


def get_territori_from_data(data):
    return [merge_dicts({'slug': key, 'nome': values['label']}, get_totals_from_data(values)) for key, values in data.items()]


class TemaSinteticoCoesioneDetailView(DetailView):
    model = TemaSinteticoCoesione

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # context.update(Progetto.objects.computabili().con_tema_sintetico(self.object).totali())
        context.update(get_totals_from_data(get_aggregatedata_from_api('api-aggregati-temasintetico-detail', request=self.request, kwargs={'slug': self.object.slug})))

        context['objects'] = self.model.objects.all()

        context['section'] = 'coesione'

        return context


class TerritorioCoesioneDetailView(DetailView):
    model = TerritorioCoesione
    slug_field = 'territorio__slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        data = get_aggregatedata_from_api('api-aggregati-territorio-detail', request=self.request, kwargs={'slug': self.object.slug})
        context.update(get_totals_from_data(data))

        territori_by_type = {}
        if data:
            if self.object.territorio.is_macroarea:
                territori_by_type['regioni'] = get_territori_from_data(data['territori']['regioni'])
                regioni_slug2cod = {o.slug: o.cod_reg for o in Territorio.objects.regioni()}
                for territorio in territori_by_type['regioni']:
                    territorio['codice'] = regioni_slug2cod[territorio.pop('slug')]
            else:
                territori_by_type['comuni'] = get_territori_from_data(data['territori']['comuni'])
                comuni_slug2cod = {o.slug: o.cod_com for o in Territorio.objects.comuni()}
                for territorio in territori_by_type['comuni']:
                    territorio['codice'] = comuni_slug2cod[territorio.pop('slug')]
        context['territori_by_type'] = territori_by_type

        context['territori_temi_sintetici_coesione'] = self.object.territori_temi_sintetici_coesione_per_territorio.select_related('tema_sintetico_coesione')
        if data:
            totali_by_temasintetico = {key: get_totals_from_data(values) for key, values in data['temi'].items()}
            for territorio_tema_sintetico_coesione in context['territori_temi_sintetici_coesione']:
                territorio_tema_sintetico_coesione.totale_finanziamenti = totali_by_temasintetico[territorio_tema_sintetico_coesione.tema_sintetico_coesione.slug]['totale_finanziamenti']
                territorio_tema_sintetico_coesione.totale_finanziamenti_perc = round(100 * territorio_tema_sintetico_coesione.totale_finanziamenti / context['totale_finanziamenti'])

        context['objects'] = self.model.objects.all()

        context['section'] = 'coesione'

        return context


class ValutazioneMixin(object):
    HEADER_ROW = 3

    @staticmethod
    def get_reader():
        import csv
        from ...opendata.models import Dataset
        return csv.DictReader(open(Dataset.objects.get(slug='processi_valutativi').file.path, 'r', encoding='latin-1'), delimiter=';')

    @staticmethod
    def fix_reader_fieldnames(reader):
        temisintetici = TemaSinteticoCoesione.objects.values_list('descrizione_breve_it', flat=True)

        fieldnames = list(reader.fieldnames)
        for n in range(len(fieldnames)):
            if fieldnames[n][:2].isdigit() and fieldnames[n][2] == '-':
                fieldnames[n] = fieldnames[n][:3] + ('tema' if fieldnames[n][3:] in temisintetici else 'territorio')
        reader.fieldnames = fieldnames

    @staticmethod
    def get_object_label(obj):
        content_object = obj.content_object
        if isinstance(content_object, TerritorioCoesione):
            return '{:02d}-territorio'.format(content_object.territorio.cod_reg or (content_object.territorio.cod_mac + 28))
        else:
            return '{}-tema'.format(content_object.codice)

    def parse_data(self, object_list, current_object=None):
        if current_object:
            current_object_label = self.get_object_label(current_object)
        else:
            current_object_label = False

        reader = self.get_reader()

        self.fix_reader_fieldnames(reader)

        totali = {
            'in_preparazione': 0,
            'in_corso': 0,
            'completate': 0,
        }

        territori_id2obj = Territorio.objects.in_bulk(
            o.content_object.territorio_id for o in object_list if isinstance(o.content_object, TerritorioCoesione)
        )
        for obj in object_list:
            if isinstance(obj.content_object, TerritorioCoesione):
                obj.content_object.territorio = territori_id2obj[obj.content_object.territorio_id]
            obj.valutazioni = 0

        for row in reader:
            if row['OC'] == 'X' and (not current_object_label or (row[current_object_label] == 'X')):
                completata = bool(row['Completata'] == 'SI')
                incorso = bool(row['In_corso'] == 'SI')
                idx = 'completate' if completata else 'in_corso' if incorso else 'in_preparazione'

                totali[idx] += 1

                for obj in object_list:
                    if row[self.get_object_label(obj)] == 'X':
                        obj.valutazioni += 1

        totali['totale'] = sum(totali.values())

        return totali


class ValutazioneListView(ValutazioneMixin, FlatPageListView):
    model = Valutazione
    flatpage_url = '/coesione/valutazioni/'

    def get_queryset(self):
        return super().get_queryset().prefetch_related('content_object')

    def get_context_data(self, **kwargs):
        totali = self.parse_data(self.object_list)

        context = super().get_context_data(totali=totali, section='coesione', **kwargs)

        extra_contents = [x.strip() for x in context['flatpage'].extra_content.split('<hr />')]
        for n, attr in enumerate(('testo_dati', 'testo_temi', 'testo_territori')):
            try:
                setattr(context['flatpage'], attr, extra_contents[n])
            except IndexError:
                pass

        return context


class ValutazioneDetailView(ValutazioneMixin, DetailView):
    model = Valutazione

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        other_objects = self.model.objects.exclude(content_type=self.content_type)

        self.object.totali = self.parse_data(other_objects, self.object)

        context['other_objects'] = other_objects

        context['objects'] = self.object.content_object.__class__.objects.prefetch_related('valutazioni')

        context['section'] = 'coesione'

        return context


class ValutazioneCSVView(ValutazioneMixin, BaseDetailView):
    model = Valutazione

    def get(self, request, *args, **kwargs):
        import csv
        from django.http import HttpResponse

        self.object = self.get_object()

        object_label = self.get_object_label(self.object)

        reader = self.get_reader()

        rows = [reader.fieldnames]

        self.fix_reader_fieldnames(reader)

        rows += [row.values() for row in reader if row['OC'] == 'X' and row[object_label] == 'X']

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(self.object.slug)

        writer = csv.writer(response, dialect='excel-semicolon-minimal')
        writer.writerows(rows)

        return response
