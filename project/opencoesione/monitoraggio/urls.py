# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.views.decorators.cache import cache_page
from .views import *


urlpatterns = [
    # globale
    url(r'^$', GlobaleView.as_view(), name='globale'),

    # progetti
    url(r'^progetti/$', ProgettoSearchView.as_view(), name='progetto_search'),
    url(r'^progetti/csv/$', ProgettoCSVSearchView.as_view(), name='progetto_search_progetti_csv'),

    # dettaglio progetto
    url(r'^progetti/(?P<slug>[\w-]+)/$', ProgettoDetailView.as_view(), name='progetto_detail'),
    url(r'^progetti/pagamenti_(?P<slug>[\w-]+).csv$', ProgettoPagamentiCSVDetailView.as_view(), name='progetto_pagamenti_csv'),
    # url(r'^progetti/(?P<slug>[\w-]+).csv$', ProgettoCSVDetailView.as_view(), name='progetto_detail_csv'),

    # classificazione azione
    url(r'^nature/(?P<slug>[\w-]+)/$', ClassificazioneAzioneDetailView.as_view(), name='classificazioneazione_detail'),

    # tema sintetico
    url(r'^temi/(?P<slug>[\w-]+)/$', TemaSinteticoDetailView.as_view(), name='temasintetico_detail'),

    # focus
    url(r'^focus/(?P<slug>[\w.-]+)/$', FocusDetailView.as_view(), name='focus_detail'),

    # programma
    url(r'^programmi/(?P<pk>[\w-]+)/$', ProgrammaDetailView.as_view(), name='programma_detail'),
    url(r'^programmi/(?P<pk>[\w-]+)/contenuticomunicazione/$', ProgrammaContenutiComunicazioneDetailView.as_view(), name='programma_contenuticomunicazione_detail'),
    url(r'^programmi/(?P<pk>[\w-]+)/documenti/$', ProgrammaDocumentiDetailView.as_view(), name='programma_documenti_detail'),
    url(r'^programmi-widget/(?P<pk>[\w-]+)/$', ProgrammaWidgetDetailView.as_view(), name='programma_widget_detail'),

    # gruppo programmi
    url(r'^gruppi-programmi/(?P<slug>[\w-]+)/$', GruppoProgrammiView.as_view(), name='gruppo_programmi'),

    # strategie
    url(r'^strategie/', include([
        url(r'^(?P<pk>S3)/$', StrategiaS3DetailView.as_view(), name='strategias3root_detail'),
        url(r'^(?P<pk>S3)/(?P<territorio_s3_pk>0\d{2})/$', StrategiaS3DetailView.as_view(), name='strategias3root_territorios3_detail'),
        url(r'^(?P<pk>S3)/t/(?P<territorio_slug>[\w-]+)/$', StrategiaS3DetailView.as_view(), name='strategias3root_territorio_detail'),
        url(r'^S3/(?P<pk>[\w-]+)/$', StrategiaS3DetailView.as_view(), name='strategias3_detail'),
        url(r'^S3/(?P<pk>[\w-]+)/(?P<territorio_s3_pk>0\d{2})/$', StrategiaS3DetailView.as_view(), name='strategias3_territorios3_detail'),
        url(r'^S3/(?P<pk>[\w-]+)/t/(?P<territorio_slug>[\w-]+)/$', StrategiaS3DetailView.as_view(), name='strategias3_territorio_detail'),
        url(r'^(?P<pk>[\w-]+)/$', AreaDetailView.as_view(), name='area_detail'),
        url(r'^(?P<pk>[\w-]+)/(?P<regione_slug>[\w-]+)/$', AreaDetailView.as_view(), name='area_regione_detail'),
    ])),

    # soggetti
    url(r'^soggetti/$', SoggettoSearchView.as_view(), name='soggetto_search'),
    url(r'^soggetti/(?P<slug>[\w-]+)/$', SoggettoDetailView.as_view(), name='soggetto_detail'),

    # territori
    url(r'^territori/autocomplete/$', AutocompleteTerritoriView.as_view(), name='territori_autocomplete'),
    url(r'^territori/ambito-estero/$', TerritorioEsteroListView.as_view(), name='territorioestero_list'),
    url(r'^territori/(?P<slug>[\w-]+)/$', TerritorioDetailView.as_view(), name='territorio_detail'),

    # csv comuni procapite per classificazione azione
    url(r'^nature/(?P<slug>[\w-]+).csv$', cache_page(settings.CACHE_PAGE_DURATION_SECS)(ClassificazioneAzioneComuniCSVDetailView.as_view()), name='classificazioneazione_comuni_csv'),
    # csv comuni procapite per tema sintetico
    url(r'^temi/(?P<slug>[\w-]+).csv$', cache_page(settings.CACHE_PAGE_DURATION_SECS)(TemaSinteticoComuniCSVDetailView.as_view()), name='temasintetico_comuni_csv'),
    # csv comuni procapite per territorio
    url(r'^territori/(?P<slug>[\w-]+).csv$', cache_page(settings.CACHE_PAGE_DURATION_SECS)(TerritorioComuniCSVDetailView.as_view()), name='territorio_comuni_csv'),
]
