# -*- coding: utf-8 -*-
import datetime
import pandas as pd
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.core.management.base import BaseCommand
from django.db import transaction
from io import BytesIO
from urllib.request import urlopen
from ...models import Progetto


class Command(BaseCommand):
    """
    Data are imported from their CSV sources.
    """
    help = 'Import data from csv'

    import_types = {
        'sintesi-ponrec': {
            'import_method': '_update_sintesi_ponrec',
        },
        'sintesi-pongat': {
            'import_method': '_update_sintesi_pongat',
        },
    }

    def add_arguments(self, parser):
        parser.add_argument('--csv-file', dest='csv_file', default=None, help='Select csv file.')
        parser.add_argument('--import-type', dest='import_type', default=None, help='Type of import; choose among {}.'.format(', '.join('"{}"'.format(t) for t in self.import_types)))
        parser.add_argument('--encoding', dest='encoding', default='utf-8-sig', help='Set character encoding of input file.')
        parser.add_argument('--separator', dest='separator', default=';', help='Set separator of input file.')

    def handle(self, *args, **options):
        csvfile = options['csv_file']
        importtype = options['import_type']
        encoding = options['encoding']
        separator = options['separator']

        if not importtype in self.import_types:
            self.stdout.write(self.style.ERROR('Wrong --import-type option "{}". Choose among {}.'.format(importtype, ', '.join('"{}"'.format(t) for t in self.import_types))))
            exit(1)

        # read csv file
        try:
            self.stdout.write('Reading file {} ....'.format(csvfile))

            csv = urlopen(csvfile).read()
        except IOError:
            self.stdout.write(self.style.ERROR('It was impossible to open file {}'.format(csvfile)))
        else:
            df = pd.read_csv(
                BytesIO(csv.decode(encoding).encode('utf-8')),
                sep=separator,
                header=0,
                low_memory=True,
                dtype=object,
                encoding='utf-8',
                keep_default_na=False,
            )

            self.stdout.write('Done.')

            df.fillna('', inplace=True)
            df.drop_duplicates(inplace=True)

            self.stdout.write('Inizio import "{}" ({}).'.format(importtype, csvfile))

            start_time = datetime.datetime.now()

            method = getattr(self, str(self.import_types[importtype]['import_method']))
            method(df)

            duration = datetime.datetime.now() - start_time
            seconds = round(duration.total_seconds())

            self.stdout.write('Fine. Tempo di esecuzione: {:02d}:{:02d}:{:02d}.'.format(int(seconds // 3600), int((seconds % 3600) // 60), int(seconds % 60)))

    @transaction.atomic
    def _update_sintesi_ponrec(self, df):
        df = df[df['Sintesi'].str.strip() != '']

        df_count = len(df)

        report = {'updated': 0, 'not_updated': 0, 'not_found': 0}

        for n, (index, row) in enumerate(df.iterrows(), 1):
            codice = '1MISE{}'.format(row['CodiceLocaleProgetto'].strip())

            try:
                progetto = Progetto.objects.get(codice_locale=codice)
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto non trovato: {}. Skipping'.format(n, df_count, codice)))
                report['not_found'] += 1
            else:
                if progetto.sintesi:
                    self.stdout.write('{}/{} - Sintesi non aggiornata per il progetto: {}'.format(n, df_count, progetto))
                    report['not_updated'] += 1
                else:
                    progetto.sintesi = row['Sintesi'].strip()
                    progetto.sintesi_fonte = 'PONREC'
                    progetto.save()

                    self.stdout.write('{}/{} - Sintesi aggiornata per il progetto: {}'.format(n, df_count, progetto))
                    report['updated'] += 1

        self.stdout.write('Sintesi aggiornate: {updated}. Sintesi non aggiornate: {not_updated}. Progetti non trovati: {not_found}.'.format(**report))

    @transaction.atomic
    def _update_sintesi_pongat(self, df):
        df = df[df['Sintesi intervento'].str.strip() != '']

        df_count = len(df)

        report = {'updated': 0, 'not_updated': 0, 'not_found': 0, 'duplicated': 0}

        for n, (index, row) in enumerate(df.iterrows(), 1):
            codice = row['CUP'].strip()

            try:
                progetto = Progetto.objects.get(cup=codice)
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto non trovato: {}. Skipping'.format(n, df_count, codice)))
                report['not_found'] += 1
            except MultipleObjectsReturned:
                self.stdout.write(self.style.WARNING('{}/{} - Più progetti con codice: {}. Skipping'.format(n, df_count, codice)))
                report['duplicated'] += 1
            else:
                if progetto.sintesi:
                    self.stdout.write('{}/{} - Sintesi non aggiornata per il progetto: {}'.format(n, df_count, progetto))
                    report['not_updated'] += 1
                else:
                    progetto.sintesi = row['Sintesi intervento'].strip()
                    progetto.sintesi_fonte = 'PONGAT'
                    progetto.save()

                    self.stdout.write('{}/{} - Sintesi aggiornata per il progetto: {}'.format(n, df_count, progetto))
                    report['updated'] += 1

        self.stdout.write('Sintesi aggiornate: {updated}. Sintesi non aggiornate: {not_updated}. Progetti non trovati: {not_found}. CUP non univoci: {duplicated}.'.format(**report))
