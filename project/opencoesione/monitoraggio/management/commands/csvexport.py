# -*- coding: utf-8 -*-
import csv
import datetime
from django.core.management.base import BaseCommand
from django.test import RequestFactory
from django.urls import reverse

from ...views import ProgettoCSVSearchView


def setup_view(view, request, *args, **kwargs):
    """
    Mimic as_view() returned callable, but returns view instance.
    args and kwargs are the same you would pass to ``reverse()``
    """
    view.request = request
    view.args = args
    view.kwargs = kwargs

    # get the object instance using the view's get_object method
    if hasattr(view, 'get_object'):
        view.object = view.get_object()

    return view


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--query', dest='query', default=None)
        parser.add_argument('--filename', dest='filename', default=None)

    def handle(self, *args, **options):
        query = options['query']
        filename = options['filename']

        self.stdout.write('Inizio export.')

        start_time = datetime.datetime.now()

        url = reverse('progetto_search_progetti_csv')

        url = '{}?{}'.format(url, query)

        view = setup_view(ProgettoCSVSearchView(), RequestFactory().get(url))

        content = view.get(view.request, limit_results=False).getvalue()

        with open(filename, 'wb') as f:
            f.write(content)

        duration = datetime.datetime.now() - start_time
        seconds = round(duration.total_seconds())

        self.stdout.write('Fine. Tempo di esecuzione: {:02d}:{:02d}:{:02d}.'.format(int(seconds // 3600), int((seconds % 3600) // 60), int(seconds % 60)))
