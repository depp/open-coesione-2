# -*- coding: utf-8 -*-
import datetime
import glob
import itertools
import pandas as pd
import re
import zipfile
from collections import OrderedDict
from decimal import Decimal
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.core.management.base import BaseCommand
from django.db import transaction, connection
from django.db.utils import DatabaseError
from io import BytesIO
from pandas.errors import EmptyDataError
from ...models import (
    Progetto, Ruolo, Localizzazione, Pagamento, Impegno, Programma, ClassificazioneAzione, ClassificazioneOggetto,
    ClassificazioneQSN, TemaSintetico, TemaPrioritario, Fonte, MonitoraggioASOC, Soggetto, Territorio, Focus, Ambito,
    ProgettoFaseProcedurale, FaseProcedurale, Area, StrategiaS3, TerritorioS3,
    ProgettoIndicatoreRealizzazione, IndicatoreRealizzazione, IndicatoreRealizzazioneUnitaMisura
)


def strip(val):
    try:
        return val.strip()
    except AttributeError:
        return val


def zeropadded(val, n):
    try:
        val = int(val)
    except ValueError:
        pass
    else:
        val = '{:0{}d}'.format(val, n)
    return val


def convert_progetto_cup_cod_natura(val):
    if val.strip():
        val = zeropadded(val, 2)
        if val == '02':
            val = '01'
    else:
        val = 'ND'
    return val


def convert_progetto_cup_cod_tipologia(val):
    if val.strip():
        val = zeropadded(val, 2)
    else:
        val = 'ND'
    return val


def convert_progetto_cup_cod_settore(val):
    return zeropadded(val, 2)


def convert_progetto_cup_cod_sottosettore(val):
    return zeropadded(val, 2)


def convert_progetto_cup_cod_categoria(val):
    return zeropadded(val, 3)


def convert_progetto_oc_cod_tema_sintetico(val):
    return zeropadded(val, 2)


def convert_ambito_qsn_cod_tema_prioritario_ue(val):
    return zeropadded(val, 2)


def convert_ambito_oc_cod_fonte(val):
    return 'PAC0713' if val == 'PAC' else val


def convert_soggetto_oc_denominazione_sogg(val):
    return re.sub('\s{2,}', ' ', val).strip()
    # return val.encode('ascii', 'ignore').strip()


class Command(BaseCommand):
    """
    Data are imported from their CSV sources.
    """
    help = 'Import data from csv'

    import_types = {
        'progetti': {
            'files': ['progetti_{}.zip', 'inattivi/prog_inattivi_{}.zip'],
            'import_method': '_import_progetti',
            'converters': {
                'CUP_COD_NATURA': convert_progetto_cup_cod_natura,
                'CUP_COD_TIPOLOGIA': convert_progetto_cup_cod_tipologia,
                'CUP_COD_SETTORE': convert_progetto_cup_cod_settore,
                'CUP_COD_SOTTOSETTORE': convert_progetto_cup_cod_sottosettore,
                'CUP_COD_CATEGORIA': convert_progetto_cup_cod_categoria,
                'OC_COD_TEMA_SINTETICO': convert_progetto_oc_cod_tema_sintetico,
            },
        },
        'focus': {
            'files': ['focus_{}.zip'],
            'import_method': '_import_focus',
            'converters': None,
        },
        'ambiti': {
            'files': [
                'ambito_FESR0713_{}.zip',
                'ambito_FSE0713_{}.zip',
                'ambito_FSC0713_{}.zip',
                'ambito_PAC0713_{}.zip',
                'ambito_FESR1420_{}.zip',
                'ambito_FSE1420_{}.zip',
                'ambito_FEASR1420_{}.zip',
                'ambito_FSC1420_{}.zip',
                'ambito_IOG1420_{}.zip',
                'ambito_FEAMP1420_{}.zip',
                'ambito_PAC1420_{}.zip',
                'ambito_CTE1420_{}.zip',
                'ambito_SNAI1420_{}.zip',
                'ambito_FSC2127_{}.zip',
                'inattivi/ambito_FESR0713_inattivi_{}.zip',
                'inattivi/ambito_FSE0713_inattivi_{}.zip',
                'inattivi/ambito_FSC0713_inattivi_{}.zip',
                'inattivi/ambito_PAC0713_inattivi_{}.zip',
                'inattivi/ambito_FESR1420_inattivi_{}.zip',
                'inattivi/ambito_FSE1420_inattivi_{}.zip',
                'inattivi/ambito_FEAMP1420_inattivi_{}.zip',
                'inattivi/ambito_IOG1420_inattivi_{}.zip',
                'inattivi/ambito_FSC1420_inattivi_{}.zip',
                'inattivi/ambito_PAC1420_inattivi_{}.zip',
                'inattivi/ambito_FSC2127_inattivi_{}.zip',
            ],
            'import_method': '_import_ambiti',
            'converters': {
                'QSN_COD_TEMA_PRIORITARIO_UE': convert_ambito_qsn_cod_tema_prioritario_ue,
                'OC_COD_FONTE': convert_ambito_oc_cod_fonte,
            },
        },
        'soggetti': {
            'files': ['soggetti_{}.zip'],
            'import_method': '_import_soggetti',
            'converters': {
                'OC_DENOMINAZIONE_SOGG': convert_soggetto_oc_denominazione_sogg,
                'OC_CODICE_FISCALE_SOGG': strip,
            },
        },
        'aree_comuni': {
            'files': ['utility/comuni_aree_{}.csv'],
            'import_method': '_import_aree_comuni',
            'converters': None,
        },
        'aree_progetti': {
            'files': ['utility/progetti_aree_{}.csv'],
            'import_method': '_import_aree_progetti',
            'converters': None,
        },
        'strategie-s3': {
            'files': ['utility/progetti_s3_{}.csv'],
            'import_method': '_import_strategie_s3',
            'converters': {
                'COD_TRAIETTORIA_S3': lambda x: x.replace(' ', '_')
            },
        },
        'localizzazioni': {
            'files': ['localizzazioni_{}.zip', 'inattivi/loc_inattivi_{}.zip'],
            'import_method': '_import_localizzazioni',
            'converters': None,
        },
        'fasi': {
            'files': ['fasi_{}.zip', 'inattivi/fasi_inattivi_{}.zip'],
            'import_method': '_import_fasi',
            'converters': None,
        },
        'indicatori': {
            'files': ['indicatori_{}.zip', 'inattivi/ind_inattivi_{}.zip'],
            'import_method': '_import_indicatori',
            'converters': None,
        },
        'pagamenti': {
            'files': ['pagamenti_{}.zip', 'inattivi/pag_inattivi_{}.zip'],
            'import_method': '_import_pagamenti',
            'converters': None,
        },
        'impegni': {
            'files': ['impegni_{}.zip', 'inattivi/imp_inattivi_{}.zip'],
            'import_method': '_import_impegni',
            'converters': None,
        },
        'privacy-progetti': {
            'files': ['utility/prog_privacy_{}.csv', 'utility/prog_inattivi_privacy_{}.csv'],
            'import_method': '_update_privacy_progetti',
            'converters': None,
        },
        'privacy-soggetti': {
            'files': ['utility/sogg_privacy_{}.csv', 'utility/sogg_inattivi_privacy_{}.csv'],
            'import_method': '_update_privacy_soggetti',
            'converters': None,
        },
        'corrispondenze-progetti': {
            'files': ['utility/duplicati_{}.csv'],
            'import_method': '_import_corrispondenze_progetti',
            'converters': None,
        },
        'monitoraggi-asoc': {
            'files': ['progetti_ASOC.csv'],
            'import_method': '_import_progetti_asoc',
            'converters': None,
        },
    }

    def add_arguments(self, parser):
        parser.add_argument('--csv-path', dest='csv_path', default=None, help='Select csv files path.')
        parser.add_argument('--import-type', dest='import_type', default=None, help='Type of import; choose among {}.'.format(', '.join('"{}"'.format(t) for t in self.import_types)))
        parser.add_argument('--encoding', dest='encoding', default='utf-8-sig', help='Set character encoding of input file.')

    def handle(self, *args, **options):
        csvpath = options['csv_path']
        importtype = options['import_type']
        encoding = options['encoding']

        if not importtype in self.import_types:
            self.stdout.write(self.style.ERROR('Wrong --import-type option "{}". Choose among {}.'.format(importtype, ', '.join('"{}"'.format(t) for t in self.import_types))))
            exit(1)

        df = pd.DataFrame()

        # read csv files
        for file in self.import_types[importtype]['files']:
            csvfile = csvpath.rstrip('/') + '/' + file.format('202?????')

            files = sorted(glob.glob(csvfile))
            if files:
                csvfile = files[-1]
            else:
                self.stdout.write(self.style.ERROR('It was impossible to open file {}'.format(csvfile)))
                continue

            try:
                self.stdout.write('Reading file {}'.format(csvfile), ending='... ')

                if csvfile.endswith('.zip'):
                    with zipfile.ZipFile(csvfile) as zfile:
                        csv = zfile.read(zfile.namelist().pop(0)).decode(encoding).encode('utf-8')
                else:
                    import codecs
                    with codecs.open(csvfile, 'r', encoding) as cfile:
                        csv = bytes(cfile.read(), 'utf-8')
            except IOError:
                self.stdout.write(self.style.ERROR('It was impossible to open file {}'.format(csvfile)))
                continue
            else:
                try:
                    df_tmp = pd.read_csv(
                        BytesIO(csv),
                        sep=';',
                        header=0,
                        low_memory=True,
                        dtype=object,
                        encoding='utf-8',
                        keep_default_na=False,
                        converters=self.import_types[importtype]['converters'],
                    )
                except EmptyDataError:
                    self.stdout.write(self.style.ERROR('No columns to parse from file {}'.format(csvfile)))
                    continue
                else:
                    if importtype == 'progetti':
                        df_tmp['FLAG_ATTIVO'] = not ('inattivi' in file)

                    df = df.append(df_tmp, ignore_index=True)

                    del df_tmp

                    self.stdout.write(self.style.SUCCESS('OK'))

        if len(df) == 0:
            self.stdout.write(self.style.ERROR('Nessun dato da processare.'))
            return

        df.fillna('', inplace=True)
        df.drop_duplicates(inplace=True)

        self.stdout.write('Inizio import "{}" ({}).'.format(importtype, csvpath))

        start_time = datetime.datetime.now()

        method = getattr(self, str(self.import_types[importtype]['import_method']))
        method(df)

        duration = datetime.datetime.now() - start_time
        seconds = round(duration.total_seconds())

        self.stdout.write('Fine. Tempo di esecuzione: {:02d}:{:02d}:{:02d}.'.format(int(seconds // 3600), int((seconds % 3600) // 60), int(seconds % 60)))

    @transaction.atomic
    def _import_progetti_classificazioneazione(self, df):
        df1 = df[['CUP_COD_NATURA', 'CUP_DESCR_NATURA']].drop_duplicates()
        for index, row in df1.iterrows():
            classificazione, created = ClassificazioneAzione.objects.get_or_create(
                codice=self._get_id(row, ['CUP_COD_NATURA']),
                defaults={
                    'descrizione': 'ACQUISTO DI BENI E SERVIZI' if row['CUP_COD_NATURA'] == '01' else row['CUP_DESCR_NATURA'],
                    'tipo': ClassificazioneAzione.TIPO.natura,
                }
            )
            self._log(created, 'Creata classificazione azione natura: {}'.format(classificazione))

        df1 = df[['CUP_COD_NATURA', 'CUP_COD_TIPOLOGIA', 'CUP_DESCR_TIPOLOGIA']].drop_duplicates()
        for index, row in df1.iterrows():
            classificazione, created = ClassificazioneAzione.objects.get_or_create(
                codice=self._get_id(row, ['CUP_COD_NATURA', 'CUP_COD_TIPOLOGIA']),
                defaults={
                    'descrizione': row['CUP_DESCR_TIPOLOGIA'],
                    'tipo': ClassificazioneAzione.TIPO.tipologia,
                    'classificazione_superiore_id': self._get_id(row, ['CUP_COD_NATURA']),
                }
            )
            self._log(created, 'Creata classificazione azione natura_tipologia: {}'.format(classificazione))

    @transaction.atomic
    def _import_progetti_classificazioneoggetto(self, df):
        # only non-empty classifications are created
        df_filtered = df[(df['CUP_COD_SETTORE'].str.strip() != '') & (df['CUP_COD_SOTTOSETTORE'].str.strip() != '') & (df['CUP_COD_CATEGORIA'].str.strip() != '')]

        df1 = df_filtered[['CUP_COD_SETTORE', 'CUP_DESCR_SETTORE']].drop_duplicates()
        for index, row in df1.iterrows():
            classificazione, created = ClassificazioneOggetto.objects.get_or_create(
                codice=self._get_id(row, ['CUP_COD_SETTORE']),
                defaults={
                    'descrizione': row['CUP_DESCR_SETTORE'],
                    'tipo': ClassificazioneOggetto.TIPO.settore,
                }
            )
            self._log(created, 'Creata classificazione oggetto settore: {}'.format(classificazione))

        df1 = df_filtered[['CUP_COD_SETTORE', 'CUP_COD_SOTTOSETTORE', 'CUP_DESCR_SOTTOSETTORE']].drop_duplicates()
        for index, row in df1.iterrows():
            classificazione, created = ClassificazioneOggetto.objects.get_or_create(
                codice=self._get_id(row, ['CUP_COD_SETTORE', 'CUP_COD_SOTTOSETTORE']),
                defaults={
                    'descrizione': row['CUP_DESCR_SOTTOSETTORE'],
                    'tipo': ClassificazioneOggetto.TIPO.sottosettore,
                    'classificazione_superiore_id': self._get_id(row, ['CUP_COD_SETTORE']),
                }
            )
            self._log(created, 'Creata classificazione oggetto settore_sottosettore: {}'.format(classificazione))

        df1 = df_filtered[['CUP_COD_SETTORE', 'CUP_COD_SOTTOSETTORE', 'CUP_COD_CATEGORIA', 'CUP_DESCR_CATEGORIA']].drop_duplicates()
        for index, row in df1.iterrows():
            classificazione, created = ClassificazioneOggetto.objects.get_or_create(
                codice=self._get_id(row, ['CUP_COD_SETTORE', 'CUP_COD_SOTTOSETTORE', 'CUP_COD_CATEGORIA']),
                defaults={
                    'descrizione': row['CUP_DESCR_CATEGORIA'],
                    'tipo': ClassificazioneOggetto.TIPO.categoria,
                    'classificazione_superiore_id': self._get_id(row, ['CUP_COD_SETTORE', 'CUP_COD_SOTTOSETTORE']),
                }
            )
            self._log(created, 'Creata classificazione oggetto settore_sottosettore_categoria: {}'.format(classificazione))

    @transaction.atomic
    def _import_progetti_tema_sintetico(self, df):
        df1 = df[['OC_COD_TEMA_SINTETICO', 'OC_TEMA_SINTETICO']].drop_duplicates()
        for index, row in df1.iterrows():
            tema, created = TemaSintetico.objects.get_or_create(
                codice=row['OC_COD_TEMA_SINTETICO'],
                defaults={
                    'descrizione': row['OC_TEMA_SINTETICO'],
                }
            )
            self._log(created, 'Creato tema sintetico: {}'.format(tema))

    def _import_progetti(self, df):
        self._truncate_table(Progetto)

        self._import_progetti_classificazioneazione(df)
        self._import_progetti_classificazioneoggetto(df)
        self._import_progetti_tema_sintetico(df)

        stato_desc2cod = {x[1]: x[0] for x in Progetto.STATO}
        statoprocedurale_desc2cod = {x[1]: x[0] for x in Progetto.STATO_PROCEDURALE}
        macroarea_desc2cod = {x[1]: x[0] for x in Progetto.MACRO_AREA}

        df_count = len(df)

        transaction.set_autocommit(False)

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                values = {}

                values['codice_locale'] = row['COD_LOCALE_PROGETTO']

                # values['ciclo_programmazione'] = row['OC_COD_CICLO']

                values['is_active'] = row['FLAG_ATTIVO']

                values['flag_visualizzazione'] = row['OC_FLAG_VISUALIZZAZIONE']

                values['cup'] = row['CUP'].strip()

                values['titolo'] = row['OC_TITOLO_PROGETTO'].strip()

                values['sintesi'] = row['OC_SINTESI_PROGETTO'].strip()

                values['stato'] = stato_desc2cod.get(row['OC_STATO_PROGETTO'])

                values['stato_procedurale'] = statoprocedurale_desc2cod.get(row['OC_STATO_PROCEDURALE'])

                values['macro_area'] = macroarea_desc2cod.get(row['OC_MACROAREA'])

                for key in ('finanz_totale_pubblico', 'finanz_da_reperire', 'economie_totali', 'economie_totali_pubbliche'):
                    values[key] = self._get_value(row, key.upper(), 'decimal')

                for key in ('finanz_ue', 'finanz_stato_pac', 'finanz_stato_fsc', 'finanz_regione', 'finanz_provincia', 'finanz_comune', 'finanz_risorse_liberate', 'finanz_altro_pubblico', 'finanz_stato_estero', 'finanz_privato'):
                    values[key] = self._get_value(row, 'OC_{}_NETTO'.format(key.upper()), 'decimal')

                # for key in ('costo_coesione', 'pagamenti_coesione'):
                #     values[key] = self._get_value(row, 'OC_{}'.format(key.upper()), 'decimal')

                values['finanz_stato_fondo_di_rotazione'] = self._get_value(row, 'OC_FINANZ_STATO_FONDO_ROT_NETTO', 'decimal')

                values['finanz_stato_altri_provvedimenti'] = self._get_value(row, 'OC_FINANZ_STATO_ALTRI_PROV_NETTO', 'decimal')

                values['finanz_totale_pubblico_netto'] = self._get_value(row, 'OC_FINANZ_TOT_PUB_NETTO', 'decimal')

                values['pagamento'] = self._get_value(row, 'TOT_PAGAMENTI', 'decimal')

                values['data_inizio'] = self._get_value(row, 'OC_DATA_INIZIO_PROGETTO', 'date')
                values['data_fine_prevista'] = self._get_value(row, 'OC_DATA_FINE_PROGETTO_PREVISTA', 'date')
                values['data_fine_effettiva'] = self._get_value(row, 'OC_DATA_FINE_PROGETTO_EFFETTIVA', 'date')

                values['data_aggiornamento'] = self._get_value(row, 'DATA_AGGIORNAMENTO', 'date')

                values['classificazione_azione_id'] = self._get_id(row, ['CUP_COD_NATURA', 'CUP_COD_TIPOLOGIA'])

                values['classificazione_oggetto_id'] = self._get_id(row, ['CUP_COD_SETTORE', 'CUP_COD_SOTTOSETTORE', 'CUP_COD_CATEGORIA'])

                values['tema_sintetico_id'] = self._get_id(row, ['OC_COD_TEMA_SINTETICO'])

                values['csv_data'] = OrderedDict(row)
            except Exception as e:
                self.stdout.write(self.style.ERROR('{}/{} - ERRORE progetto {}: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'], e)))
            else:
                try:
                    with transaction.atomic():
                        Progetto.objects.create(**values)
                except DatabaseError as e:
                    self.stdout.write(self.style.ERROR('{}/{} - ERRORE progetto {}: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'], e)))
                else:
                    self.stdout.write('{}/{} - Creato progetto: {}'.format(n, df_count, row['COD_LOCALE_PROGETTO']))

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{} -----------------> Committing.' .format(n))
                transaction.commit()

        transaction.set_autocommit(True)

    def _import_focus(self, df):
        for focus in Focus.objects.all():
            focus.progetti.clear()

        transaction.set_autocommit(False)

        # only non-empty focus are created
        df1 = df[df['OC_FOCUS'].str.strip() != '']['OC_FOCUS'].drop_duplicates()

        focus_slugs = set(itertools.chain.from_iterable(item.replace('#', '').split(' ') for index, item in df1.iteritems()))

        for focus_slug in focus_slugs:
            focus, created = Focus.objects.get_or_create(
                slug=focus_slug,
                defaults={
                    'nome': focus_slug.replace('_', ' ').capitalize(),
                }
            )
            self._log(created, 'Creato focus: {}'.format(focus))

        transaction.commit()

        focus_slug2obj = {focus.slug: focus for focus in Focus.objects.all()}

        df_count = len(df)

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                progetto = Progetto.objects.get(codice_locale=row['COD_LOCALE_PROGETTO'])
                self.stdout.write(self.style.NOTICE('{}/{} - Progetto: {}'.format(n, df_count, progetto)))
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto non trovato: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'])))
            else:
                try:
                    with transaction.atomic():
                        progetto.focus.set([focus_slug2obj[x] for x in row['OC_FOCUS'].replace('#', '').strip().split(' ') if x])
                except DatabaseError as e:
                    self.stdout.write(self.style.ERROR('{}/{} - ERRORE relazioni focus->progetto {}: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'], e)))
                else:
                    self.stdout.write('{}/{} - Create relazioni focus->progetto: {}'.format(n, df_count, row['COD_LOCALE_PROGETTO']))

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{} -----------------> Committing.' .format(n))
                transaction.commit()

        transaction.set_autocommit(True)

    @transaction.atomic
    def _import_ambiti_programma(self, df, lev0, lev1, lev2):
        if lev0['codice'] in df and lev1['codice'] in df and lev2['codice'] in df:
            # only non-empty classifications are created
            df_filtered = df[(df[lev0['codice']].str.strip() != '') & (df[lev1['codice']].str.strip() != '') & (df[lev2['codice']].str.strip() != '')]

            df1 = df_filtered[[lev0['codice'], lev0['descrizione'], 'OC_COD_FONTE']].drop_duplicates()
            for index, row in df1.iterrows():
                programma, created = Programma.objects.get_or_create(
                    codice=self._get_id(row, [lev0['codice']], '/'),
                    defaults={
                        'descrizione': row[lev0['descrizione']],
                        'tipo': lev0['tipo'],
                        'ciclo_programmazione': self._get_ciclo_programmazione_programma(row['OC_COD_FONTE']),
                    }
                )
                self._log(created, 'Creato {}: {}'.format(programma.tipo, programma))

            df1 = df_filtered[[lev0['codice'], lev1['codice'], lev1['descrizione'], 'OC_COD_FONTE']].drop_duplicates()
            for index, row in df1.iterrows():
                programma, created = Programma.objects.get_or_create(
                    codice=self._get_id(row, [lev0['codice'], lev1['codice']], '/'),
                    defaults={
                        'descrizione': row[lev1['descrizione']],
                        'tipo': lev1['tipo'],
                        'ciclo_programmazione': self._get_ciclo_programmazione_programma(row['OC_COD_FONTE']),
                        'classificazione_superiore_id': self._get_id(row, [lev0['codice']], '/'),
                    }
                )
                self._log(created, 'Creato {}: {}'.format(programma.tipo, programma))

            df1 = df_filtered[[lev0['codice'], lev1['codice'], lev2['codice'], lev2['descrizione'], 'OC_COD_FONTE']].drop_duplicates()
            for index, row in df1.iterrows():
                programma, created = Programma.objects.get_or_create(
                    codice=self._get_id(row, [lev0['codice'], lev1['codice'], lev2['codice']], '/'),
                    defaults={
                        'descrizione': row[lev2['descrizione']],
                        'tipo': lev2['tipo'],
                        'ciclo_programmazione': self._get_ciclo_programmazione_programma(row['OC_COD_FONTE']),
                        'classificazione_superiore_id': self._get_id(row, [lev0['codice'], lev1['codice']], '/'),
                    }
                )
                self._log(created, 'Creato {}: {}'.format(programma.tipo, programma))

    @transaction.atomic
    def _import_ambiti_classificazioneqsn(self, df):
        # only non-empty classifications are created
        df_filtered = df[(df['QSN_COD_PRIORITA'].str.strip() != '') & (df['QSN_COD_OBIETTIVO_GENERALE'].str.strip() != '') & (df['QSN_CODICE_OBIETTIVO_SPECIFICO'].str.strip() != '')]

        df1 = df_filtered[['QSN_COD_PRIORITA', 'QSN_DESCRIZIONE_PRIORITA']].drop_duplicates()
        for index, row in df1.iterrows():
            classificazione, created = ClassificazioneQSN.objects.get_or_create(
                codice=row['QSN_COD_PRIORITA'],
                defaults={
                    'descrizione': row['QSN_DESCRIZIONE_PRIORITA'],
                    'tipo': ClassificazioneQSN.TIPO.priorita,
                }
            )
            self._log(created, 'Creata priorità QSN: {}'.format(classificazione))

        df1 = df_filtered[['QSN_COD_PRIORITA', 'QSN_COD_OBIETTIVO_GENERALE', 'QSN_DESCR_OBIETTIVO_GENERALE']].drop_duplicates()
        for index, row in df1.iterrows():
            classificazione, created = ClassificazioneQSN.objects.get_or_create(
                codice=row['QSN_COD_OBIETTIVO_GENERALE'],
                defaults={
                    'descrizione': row['QSN_DESCR_OBIETTIVO_GENERALE'],
                    'tipo': ClassificazioneQSN.TIPO.obiettivo_generale,
                    'classificazione_superiore_id': row['QSN_COD_PRIORITA'],
                }
            )
            self._log(created, 'Creato obiettivo generale QSN: {}'.format(classificazione))

        df1 = df_filtered[['QSN_COD_PRIORITA', 'QSN_COD_OBIETTIVO_GENERALE', 'QSN_CODICE_OBIETTIVO_SPECIFICO', 'QSN_DESCR_OBIETTIVO_SPECIFICO']].drop_duplicates()
        for index, row in df1.iterrows():
            classificazione, created = ClassificazioneQSN.objects.get_or_create(
                codice=row['QSN_CODICE_OBIETTIVO_SPECIFICO'],
                defaults={
                    'descrizione': row['QSN_DESCR_OBIETTIVO_SPECIFICO'],
                    'tipo': ClassificazioneQSN.TIPO.obiettivo_specifico,
                    'classificazione_superiore_id': row['QSN_COD_OBIETTIVO_GENERALE'],
                }
            )
            self._log(created, 'Creato obiettivo specifico QSN: {}'.format(classificazione))

    @transaction.atomic
    def _import_ambiti_tema_prioritario(self, df):
        # only non-empty classifications are created
        df1 = df[df['QSN_COD_TEMA_PRIORITARIO_UE'].str.strip() != ''][['QSN_COD_TEMA_PRIORITARIO_UE', 'QSN_DESCR_TEMA_PRIORITARIO_UE']].drop_duplicates()
        for index, row in df1.iterrows():
            tema, created = TemaPrioritario.objects.get_or_create(
                codice=row['QSN_COD_TEMA_PRIORITARIO_UE'],
                defaults={
                    'descrizione': row['QSN_DESCR_TEMA_PRIORITARIO_UE'],
                }
            )
            self._log(created, 'Creato tema prioritario: {}'.format(tema))

    @transaction.atomic
    def _import_ambiti_fonte(self, df):
        # only non-empty classifications are created
        df1 = df[df['OC_COD_FONTE'].str.strip() != ''][['OC_COD_FONTE', 'OC_DESCR_FONTE']].drop_duplicates()
        for index, row in df1.iterrows():
            fonte, created = Fonte.objects.get_or_create(
                codice=row['OC_COD_FONTE'],
                defaults={
                    'descrizione': row['OC_DESCR_FONTE'],
                }
            )
            self._log(created, 'Creata fonte: {}'.format(fonte))

    def _import_ambiti(self, df):
        self._truncate_table(Ambito)

        def confdict(x):
            if x == 'programma':
                codice = 'OC_CODICE_PROGRAMMA'
                descrizione = 'OC_DESCRIZIONE_PROGRAMMA'
            elif x == 'asse':
                codice = 'PO_CODICE_ASSE'
                descrizione = 'PO_DENOMINAZIONE_ASSE'
            elif x == 'obiettivo_operativo':
                codice = 'PO_COD_OBIETTIVO_OPERATIVO'
                descrizione = 'PO_OBIETTIVO_OPERATIVO'
            else:
                codice = 'COD_{}'.format(x.upper())
                descrizione = 'DESCR_{}'.format(x.upper())

            return {'codice': codice, 'descrizione': descrizione, 'tipo': getattr(Programma.TIPO, x)}

        programma_cols_ids = [
            ['programma', 'asse', 'obiettivo_operativo'],
            ['programma', 'linea', 'azione'],
            ['programma', 'asse', 'obiettivo_specifico'],
            ['programma', 'priorita', 'tipo_intervento'],
            ['programma', 'settore_strategico_fsc', 'asse_tematico_fsc'],
            ['programma', 'asse_tematico_pac', 'linea_azione_pac'],
            ['programma', 'priorita', 'misura'],
            ['programma', 'area_snai', 'ambito_snai'],
            ['programma', 'area_tematica_psc', 'sett_interv_psc'],
        ]

        for programma_cols_id in programma_cols_ids:
            self._import_ambiti_programma(df, *[confdict(x) for x in programma_cols_id])

        self._import_ambiti_classificazioneqsn(df)
        self._import_ambiti_tema_prioritario(df)
        self._import_ambiti_fonte(df)

        df_obj = df.select_dtypes(['object'])
        df[df_obj.columns] = df_obj.apply(lambda x: x.str.strip(' '))
        df = df.drop_duplicates()

        gb = df.groupby(['COD_LOCALE_PROGETTO'] + [confdict(x)['codice'] for x in set(itertools.chain.from_iterable(programma_cols_ids)) if confdict(x)['codice'] in df], as_index=False)

        df_count = gb.ngroups

        transaction.set_autocommit(False)

        for n, (_, df1) in enumerate(gb, 1):
            for nn, (index, row1) in enumerate(df1.iterrows(), 1):
                if nn == 1:
                    row = OrderedDict(row1)
                else:
                    row = OrderedDict((x, row[x] or row1[x]) for x in row)

            try:
                progetto = Progetto.objects.get(codice_locale=row['COD_LOCALE_PROGETTO'])
                self.stdout.write(self.style.NOTICE('{}/{} - Progetto: {}'.format(n, df_count, progetto)))
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto non trovato: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'])))
            else:
                try:
                    values = {}

                    values['progetto'] = progetto

                    values['ciclo_programmazione'] = row['OC_COD_CICLO']

                    values['flag_pac'] = row['OC_FLAG_PAC']

                    values['fondo_comunitario'] = getattr(Ambito.FONDO_COMUNITARIO, row.get('QSN_FONDO_COMUNITARIO') or row.get('FONDO_COMUNITARIO'), None)

                    values['classificazione_qsn_id'] = self._get_id(row, ['QSN_CODICE_OBIETTIVO_SPECIFICO'])

                    values['tema_prioritario_id'] = self._get_id(row, ['QSN_COD_TEMA_PRIORITARIO_UE'])

                    values['fonte_id'] = self._get_id(row, ['OC_COD_FONTE'])

                    values['programma_id'] = next((id for id in (self._get_id(row, [confdict(x)['codice'] for x in programma_cols_id], '/') for programma_cols_id in programma_cols_ids) if id), None)

                    for key in ('costo_coesione', 'pagamenti_coesione'):
                        values[key] = self._get_value(row, 'OC_{}'.format(key.upper()), 'decimal')

                    values['csv_data'] = row
                except Exception as e:
                    self.stdout.write(self.style.ERROR('{}/{} - ERRORE ambito {}: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'], e)))
                else:
                    try:
                        with transaction.atomic():
                            Ambito.objects.create(**values)
                    except DatabaseError as e:
                        self.stdout.write(self.style.ERROR('{}/{} - ERRORE ambito {}: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'], e)))
                    else:
                        self.stdout.write('{}/{} - Creato ambito: {}'.format(n, df_count, row['COD_LOCALE_PROGETTO']))

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{} -----------------> Committing.' .format(n))
                transaction.commit()

        transaction.set_autocommit(True)

    def _import_soggetti(self, df):
        self._truncate_table(Soggetto)
        self._truncate_table(Ruolo)

        # creazione soggetti

        # df1 = df[['OC_DENOMINAZIONE_SOGG', 'OC_CODICE_FISCALE_SOGG', 'COD_FORMA_GIURIDICA_SOGG', 'COD_ATECO_SOGG', 'COD_COMUNE_SEDE_SOGG', 'INDIRIZZO_SOGG', 'CAP_SOGG']].drop_duplicates()
        # df1 = df[['OC_DENOMINAZIONE_SOGG', 'OC_CODICE_FISCALE_SOGG', 'COD_COMUNE_SEDE_SOGG', 'INDIRIZZO_SOGG', 'CAP_SOGG']].drop_duplicates()
        # df1 = df.groupby(['OC_DENOMINAZIONE_SOGG', 'OC_CODICE_FISCALE_SOGG'], as_index=False).first()
        # df1 = df[['OC_DENOMINAZIONE_SOGG', 'OC_CODICE_FISCALE_SOGG', 'COD_COMUNE_SEDE_SOGG', 'INDIRIZZO_SOGG', 'CAP_SOGG']].groupby(['OC_DENOMINAZIONE_SOGG', 'OC_CODICE_FISCALE_SOGG'], as_index=False).agg(lambda x: x.value_counts().index[0])

        gb = df.groupby(['OC_DENOMINAZIONE_SOGG', 'OC_CODICE_FISCALE_SOGG'], as_index=False)

        df_count = gb.ngroups

        transaction.set_autocommit(False)

        for n, (_, df1) in enumerate(gb, 1):
            df1 = df1.groupby(['OC_DENOMINAZIONE_SOGG', 'OC_CODICE_FISCALE_SOGG', 'COD_COMUNE_SEDE_SOGG', 'INDIRIZZO_SOGG', 'CAP_SOGG'], as_index=False).size().reset_index(name='SIZE').sort_values('SIZE', ascending=False)

            df2 = df1[(df1['COD_COMUNE_SEDE_SOGG'].str.strip() != '') & (df1['INDIRIZZO_SOGG'].str.strip() != '')]
            if df2.empty:
                df2 = df1[df1['COD_COMUNE_SEDE_SOGG'].str.strip() != '']
                if df2.empty:
                    df2 = df1

            row = df2.iloc[0]

            denominazione = row['OC_DENOMINAZIONE_SOGG']
            codice_fiscale = row['OC_CODICE_FISCALE_SOGG']

            try:
                territorio = Territorio.objects.get_from_istat_code(row['COD_COMUNE_SEDE_SOGG'])
            except ObjectDoesNotExist:
                territorio = None

            try:
                with transaction.atomic():
                    Soggetto.objects.create(
                        denominazione=denominazione,
                        codice_fiscale=codice_fiscale,
                        indirizzo=self._get_value(row, 'INDIRIZZO_SOGG'),
                        cap=self._get_value(row, 'CAP_SOGG'),
                        territorio=territorio,
                    )
            except DatabaseError as e:
                self.stdout.write(self.style.ERROR('{}/{} - ERRORE soggetto {} ({}): {}. Skipping.'.format(n, df_count, denominazione, codice_fiscale, e)))
            else:
                self.stdout.write('{}/{} - Creato soggetto: {} ({})'.format(n, df_count, denominazione, codice_fiscale))

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{} -----------------> Committing.'.format(n))
                transaction.commit()

        # creazione ruoli

        ruolo_cod2desc = Ruolo.inv_ruoli_dict()

        # df1 = df[['COD_LOCALE_PROGETTO', 'OC_DENOMINAZIONE_SOGG', 'OC_CODICE_FISCALE_SOGG', 'SOGG_COD_RUOLO', 'SOGG_PROGR_RUOLO']].drop_duplicates()
        df1 = df.groupby(['COD_LOCALE_PROGETTO', 'OC_DENOMINAZIONE_SOGG', 'OC_CODICE_FISCALE_SOGG', 'SOGG_COD_RUOLO'], as_index=False).first()

        df_count = len(df1)

        for n, (index, row) in enumerate(df1.iterrows(), 1):
            try:
                progetto = Progetto.objects.get(codice_locale=row['COD_LOCALE_PROGETTO'])
                self.stdout.write(self.style.NOTICE('{}/{} - Progetto: {}'.format(n, df_count, progetto)))
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto non trovato: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'])))
            else:
                denominazione = row['OC_DENOMINAZIONE_SOGG']
                codice_fiscale = row['OC_CODICE_FISCALE_SOGG']

                try:
                    soggetto = Soggetto.objects.get(denominazione=denominazione, codice_fiscale=codice_fiscale)
                    self.stdout.write(self.style.NOTICE('{}/{} - Soggetto: {} ({})'.format(n, df_count, soggetto.denominazione, soggetto.codice_fiscale)))
                except ObjectDoesNotExist:
                    self.stdout.write(self.style.WARNING('{}/{} - Soggetto non trovato: {} ({}). Skipping.'.format(n, df_count, denominazione, codice_fiscale)))
                else:
                    try:
                        with transaction.atomic():
                            Ruolo.objects.update_or_create(
                                progetto=progetto,
                                soggetto=soggetto,
                                defaults={
                                    'progressivo_{}'.format(ruolo_cod2desc[row['SOGG_COD_RUOLO']]): row['SOGG_PROGR_RUOLO']
                                },
                            )
                    except DatabaseError as e:
                        self.stdout.write(self.style.ERROR('{}/{} - ERRORE ruolo {} {} ({}): {}. Skipping.'.format(n, df_count, progetto, soggetto, row['SOGG_COD_RUOLO'], e)))
                    else:
                        self.stdout.write('{}/{} - Creato ruolo: {} {} ({})'.format(n, df_count, progetto, soggetto, row['SOGG_COD_RUOLO']))

                    del progetto
                    del soggetto

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{} -----------------> Committing.'.format(n))
                transaction.commit()

        transaction.set_autocommit(True)

    @transaction.atomic
    def _import_aree_comuni(self, df):
        cache = {
            'comune': {'{:03d}{:06d}'.format(o.cod_reg, o.cod_com): o for o in  Territorio.objects.comuni()},
            'area': Area.objects.in_bulk(),
        }

        area_cols = ['COD_TIPO_AREA', 'DENOM_TIPO_AREA', 'COD_AREA', 'DENOM_AREA', 'COD_PARTIZIONE', 'DENOM_PARTIZIONE']

        df['COD_PARTIZIONE'] = df[['COD_AREA', 'COD_PARTIZIONE']].apply(lambda x: '.'.join(x), axis=1)

        df['COD_ISTAT_COMUNE'] = df[['COD_REGIONE', 'COD_PROVINCIA', 'COD_COMUNE']].apply(lambda x: ''.join(x), axis=1)

        df = df[area_cols + ['COD_ISTAT_COMUNE']]

        gb = df.groupby(area_cols, as_index=False)

        df_count = gb.ngroups

        for n, (key, df1) in enumerate(gb, 1):
            area_data = dict(zip(area_cols, key))

            area = None

            for key in ('TIPO_AREA', 'AREA', 'PARTIZIONE'):
                codice = area_data['COD_{}'.format(key)]

                if codice not in cache['area']:
                    cache['area'][codice] = Area.objects.create(
                        codice=codice,
                        descrizione=area_data['DENOM_{}'.format(key)],
                        tipo=getattr(Area.TIPO, key.lower()),
                        classificazione_superiore=area,
                    )
                    created = True
                else:
                    created = False

                area = cache['area'][codice]

                self._log(created, '{}/{} - Creata area: {} ({})'.format(n, df_count, area, area.tipo))

            area.comuni.set(cache['comune'][cod_istat] for cod_istat in df1['COD_ISTAT_COMUNE'])

    @transaction.atomic
    def _import_aree_progetti(self, df):
        df1 = df[['COD_AREA', 'COD_LOCALE_PROGETTO']].drop_duplicates()

        cache = {
            'progetto': {o.codice_locale: o for o in Progetto.objects.filter(codice_locale__in=df1['COD_LOCALE_PROGETTO'].drop_duplicates().tolist())},
            'area': Area.objects.aree().in_bulk(df1['COD_AREA'].drop_duplicates().tolist()),
        }

        gb = df1.groupby(['COD_AREA'], as_index=False)

        df_count = gb.ngroups

        for n, (key, df2) in enumerate(gb, 1):
            try:
                area = cache['area'][key]
                self.stdout.write(self.style.NOTICE('{}/{} - Area: {} ({})'.format(n, df_count, area, area.tipo)))
            except KeyError:
                self.stdout.write(self.style.WARNING('{}/{} - Area non trovata: {}. Skipping.'.format(n, df_count, key)))
            else:
                try:
                    area.progetti.set(cache['progetto'][clp] for clp in df2['COD_LOCALE_PROGETTO'])
                except Exception as e:
                    self.stdout.write(self.style.ERROR('{}/{} - ERRORE relazioni area->progetti {}: {}. Skipping.'.format(n, df_count, area, e)))
                else:
                    self.stdout.write('{}/{} - Create relazioni area->progetti: {}'.format(n, df_count, area))

        import json

        df1 = df[df['GEORIFERIMENTO'].str.strip() != ''][['COD_LOCALE_PROGETTO', 'GEORIFERIMENTO']].drop_duplicates()

        df_count = len(df1)

        for n, (index, row) in enumerate(df1.iterrows(), 1):
            progetto = cache['progetto'][row['COD_LOCALE_PROGETTO']]
            progetto.geo_data = json.loads(row['GEORIFERIMENTO'].replace("'", '"').replace('][', '], ['))
            progetto.save()

            self.stdout.write(self.style.NOTICE('{}/{} - Aggiornato geo_data per il progetto: {}'.format(n, df_count, progetto)))

    @transaction.atomic
    def _import_strategie_s3(self, df):
        df1 = df[['COD_TERRITORIO_S3', 'DESCR_TERRITORIO_S3']].drop_duplicates()
        for index, row in df1.iterrows():
            territorio_s3, created = TerritorioS3.objects.get_or_create(
                codice=row['COD_TERRITORIO_S3'],
                defaults={
                    'descrizione': row['DESCR_TERRITORIO_S3'],
                }
            )
            self._log(created, 'Creato territorio S3: {}'.format(territorio_s3))

        strategia_s3, created = StrategiaS3.objects.get_or_create(
            codice='S3',
            defaults={
                'descrizione': 'Strategia di Sviluppo Intelligente',
                'tipo': StrategiaS3.TIPO.strategia,
            }
        )
        self._log(created, 'Creata strategia S3: {}'.format(strategia_s3))

        df1 = df['TIPO_S3'].drop_duplicates()
        for index, item in df1.iteritems():
            strategia_s3, created = StrategiaS3.objects.get_or_create(
                codice=item,
                defaults={
                    'descrizione': {'NAZIONALE': 'STRATEGIA NAZIONALE', 'REGIONALE': 'STRATEGIE REGIONALI'}[item],
                    'tipo': StrategiaS3.TIPO.tipo_strategia,
                    'classificazione_superiore_id': 'S3',
                }
            )
            self._log(created, 'Creato tipo strategia S3: {}'.format(strategia_s3))

        df1 = df[['COD_SPECIALIZZAZIONE_S3', 'DESCR_SPECIALIZZAZIONE_S3', 'TIPO_S3']].drop_duplicates()
        for index, row in df1.iterrows():
            strategia_s3, created = StrategiaS3.objects.get_or_create(
                codice=row['COD_SPECIALIZZAZIONE_S3'],
                defaults={
                    'descrizione': row['DESCR_SPECIALIZZAZIONE_S3'],
                    'tipo': StrategiaS3.TIPO.specializzazione,
                    'classificazione_superiore_id': row['TIPO_S3'],
                }
            )
            self._log(created, 'Creata area di specializzazione S3: {}'.format(strategia_s3))

        df1 = df[['COD_TRAIETTORIA_S3', 'DESCR_TRAIETTORIA_S3', 'COD_SPECIALIZZAZIONE_S3', 'COD_TERRITORIO_S3']].drop_duplicates()
        for index, row in df1.iterrows():
            strategia_s3, created = StrategiaS3.objects.get_or_create(
                codice=row['COD_TRAIETTORIA_S3'],
                defaults={
                    'descrizione': row['DESCR_TRAIETTORIA_S3'],
                    'tipo': StrategiaS3.TIPO.traiettoria,
                    'classificazione_superiore_id': row['COD_SPECIALIZZAZIONE_S3'],
                    'territorio_id': row['COD_TERRITORIO_S3'],
                }
            )
            self._log(created, 'Creata traiettoria di sviluppo S3: {}'.format(strategia_s3))

        cache = {
            'progetto': {o.codice_locale: o for o in Progetto.objects.filter(codice_locale__in=df['COD_LOCALE_PROGETTO'].drop_duplicates().tolist())},
            'traiettoria_s3': {o.codice: o for o in StrategiaS3.objects.filter(codice__in=df['COD_TRAIETTORIA_S3'].drop_duplicates().tolist())},
        }

        df1 = df[['COD_TRAIETTORIA_S3', 'COD_LOCALE_PROGETTO']].drop_duplicates()
        gb = df1.groupby(['COD_TRAIETTORIA_S3'], as_index=False)

        df_count = gb.ngroups

        for n, (key, df2) in enumerate(gb, 1):
            traiettoria_s3 = cache['traiettoria_s3'][key]
            try:
                traiettoria_s3.progetti.set(cache['progetto'][clp] for clp in df2['COD_LOCALE_PROGETTO'] if clp in cache['progetto'])
            except Exception as e:
                self.stdout.write(self.style.ERROR('{}/{} - ERRORE relazioni traiettoria s3->progetti {}: {}. Skipping.'.format(n, df_count, traiettoria_s3, e)))
            else:
                self.stdout.write('{}/{} - Create relazioni traiettoria s3->progetti: {}'.format(n, df_count, traiettoria_s3))

    @transaction.atomic
    def _import_localizzazioni_territori(self, df):
        keywords = ['OC_TERRITORIO_PROG', 'COD_REGIONE', 'DEN_REGIONE']

        # only complete and non-empty classifications are created
        if all(k in df.columns.values for k in keywords):
            df1 = df[df['OC_TERRITORIO_PROG'].isin([Territorio.TIPO.E, Territorio.TIPO.N])][keywords].drop_duplicates()
            for index, row in df1.iterrows():
                territorio, created = Territorio.objects.get_or_create(
                    cod_reg=int(row['COD_REGIONE']),
                    cod_prov=0,
                    cod_com=0,
                    tipo=row['OC_TERRITORIO_PROG'],
                    defaults={
                        'denominazione': row['DEN_REGIONE']
                    }
                )
                self._log(created, 'Creato territorio: {} ({})'.format(territorio, territorio.tipo))

    def _import_localizzazioni(self, df):
        self._truncate_table(Localizzazione)

        # elimino le righe senza localizzazione
        df = df[df['COD_COMUNE'] != '999']

        # i territori esteri o nazionali non sono in Territorio di default
        self._import_localizzazioni_territori(df)

        df_count = len(df)

        insert_list = []
        area_dict = {}

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                progetto = Progetto.objects.get(codice_locale=row['COD_LOCALE_PROGETTO'])
                self.stdout.write(self.style.NOTICE('{}/{} - Progetto: {}'.format(n, df_count, progetto)))
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto non trovato: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'])))
            else:
                territorio = None

                tipo_territorio = self._get_value(row, 'OC_TERRITORIO_PROG') or Territorio.TIPO.C

                if not tipo_territorio in (Territorio.TIPO.E, Territorio.TIPO.N):
                    if row['COD_REGIONE'] in ('000',):
                        tipo_territorio = Territorio.TIPO.N
                    elif row['COD_PROVINCIA'] in ('000', '900', 'P{}'.format(row['COD_REGIONE'])):
                        tipo_territorio = Territorio.TIPO.R
                    elif row['COD_COMUNE'] in ('000', '900'):
                        tipo_territorio = Territorio.TIPO.P

                if not tipo_territorio in Territorio.TIPO:
                    self.stdout.write(self.style.WARNING('{}/{} - Tipo di territorio sconosciuto o errato: {}. Skipping.'.format(n, df_count, tipo_territorio)))
                else:
                    lookup = {}
                    lookup['tipo'] = tipo_territorio
                    lookup['cod_reg'] = int(row['COD_REGIONE'])

                    if tipo_territorio == Territorio.TIPO.R:
                        pass
                    elif tipo_territorio == Territorio.TIPO.P:
                        lookup['cod_prov'] = int(row['COD_PROVINCIA'])
                    elif tipo_territorio == Territorio.TIPO.C:
                        lookup['cod_prov'] = int(row['COD_PROVINCIA'])
                        lookup['cod_com'] = '{}{}'.format(int(row['COD_PROVINCIA']), row['COD_COMUNE'])
                    else:
                        lookup['cod_prov'] = 0
                        lookup['cod_com'] = 0

                    try:
                        territorio = Territorio.objects.get(**lookup)
                        self.stdout.write(self.style.NOTICE('{}/{} - Territorio: {}'.format(n, df_count, territorio)))
                    except ObjectDoesNotExist:
                        if tipo_territorio == Territorio.TIPO.C:
                            del lookup['cod_prov']
                            del lookup['cod_com']

                            lookup['denominazione'] = row['DEN_COMUNE']

                            try:
                                territorio = Territorio.objects.get(**lookup)
                                self.stdout.write(self.style.NOTICE('{}/{} - Territorio di tipo "Comune" individuato attraverso la denominazione: {}.'.format(n, df_count, territorio)))
                            except ObjectDoesNotExist:
                                pass

                    if not territorio:
                        self.stdout.write(self.style.WARNING('{}/{} - Territorio non trovato: {} [{}]/{} [{}]/{} [{}] ({}). Skipping.'.format(n, df_count, row['DEN_COMUNE'], row['COD_COMUNE'], row['DEN_PROVINCIA'], row['COD_PROVINCIA'], row['DEN_REGIONE'], row['COD_REGIONE'], tipo_territorio)))

                if territorio:
                    insert_list.append(
                        Localizzazione(
                            progetto=progetto,
                            territorio=territorio,
                            indirizzo=self._get_value(row, 'INDIRIZZO_PROG'),
                            cap=self._get_value(row, 'CAP_PROG'),
                        )
                    )

                    if row['COD_AREA_INTERNA'].strip():
                        area_dict[(progetto, territorio)] = row['COD_AREA_INTERNA']

                    self.stdout.write('{}/{} - Creata localizzazione progetto: {}'.format(n, df_count, insert_list[-1]))

                    del progetto
                    del territorio

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{}/{} -----------------> Salvataggio in corso.'.format(n, df_count))
                Localizzazione.objects.bulk_create(insert_list)
                insert_list = []

        cache = {
            'area': Area.objects.aree().in_bulk(df[df['COD_AREA_INTERNA'].str.strip() != '']['COD_AREA_INTERNA'].drop_duplicates().tolist()),
        }

        field = 'aree'

        ThroughModel = getattr(Localizzazione, field).through

        cnt = len(area_dict)

        insert_list = []

        for n, ((progetto, territorio), cod_area) in enumerate(area_dict.items(), 1):
            for localizzazione in Localizzazione.objects.filter(progetto=progetto, territorio=territorio):
                insert_list.append(ThroughModel(**{getattr(localizzazione, field).source_field_name: localizzazione, getattr(localizzazione, field).target_field_name: cache['area'][cod_area]}))
                self.stdout.write('{}/{} - Creata relazione localizzazione->area: {}'.format(n, cnt, insert_list[-1]))

            if (n % 5000 == 0) or (n == cnt):
                self.stdout.write('{}/{} -----------------> Salvataggio in corso.'.format(n, cnt))
                ThroughModel.objects.bulk_create(insert_list)
                insert_list = []

    def _import_fasi_fasiprocedurali(self, df):
        # only non-empty fasi are created
        df1 = df[['OC_COD_FASE', 'OC_DESCR_FASE']].drop_duplicates()
        for index, row in df1.iterrows():
            faseprocedurale, created = FaseProcedurale.objects.get_or_create(
                codice=row['OC_COD_FASE'],
                defaults={
                    'descrizione': row['OC_DESCR_FASE'],
                    'is_esecutiva': row['OC_DESCR_FASE'].lower().startswith('esecuzione'),
                }
            )
            self._log(created, 'Creata fase procedurale: {}'.format(faseprocedurale))

    def _import_fasi(self, df):
        self._truncate_table(ProgettoFaseProcedurale)

        df = df[df['OC_COD_FASE'].str.strip() != ''].groupby(['COD_LOCALE_PROGETTO', 'OC_COD_FASE'], as_index=False).first().sort_values(['COD_LOCALE_PROGETTO', 'OC_COD_FASE'], ascending=[True, True])

        self._import_fasi_fasiprocedurali(df)

        df_count = len(df)

        insert_list = []

        created = 0

        progetto = Progetto()

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                if progetto.codice_locale != row['COD_LOCALE_PROGETTO']:
                    progetto = Progetto.objects.get(codice_locale=row['COD_LOCALE_PROGETTO'])
                    self.stdout.write(self.style.NOTICE('{}/{} - Progetto: {}'.format(n, df_count, progetto)))
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto non trovato: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'])))
            else:
                insert_list.append(
                    ProgettoFaseProcedurale(
                        progetto=progetto,
                        fase_procedurale_id=row['OC_COD_FASE'],
                        data_inizio_prevista=self._get_value(row, 'DATA_INIZIO_PREVISTA', 'date'),
                        data_inizio_effettiva=self._get_value(row, 'DATA_INIZIO_EFFETTIVA', 'date'),
                        data_fine_prevista=self._get_value(row, 'DATA_FINE_PREVISTA', 'date'),
                        data_fine_effettiva=self._get_value(row, 'DATA_FINE_EFFETTIVA', 'date'),
                    )
                )
                self.stdout.write('{}/{} - Creata fase: {}'.format(n, df_count, insert_list[-1]))

                created += 1

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{}/{} -----------------> Salvataggio in corso.'.format(n, df_count))
                ProgettoFaseProcedurale.objects.bulk_create(insert_list)
                insert_list = []

        self.stdout.write('Sono stati create {} fasi su {}.'.format(created, df_count))

    def _import_indicatori_indicatorirealizzazione(self, df):
        df1 = df[['UNITA_MISURA', 'DESCR_UNITA_MISURA']].drop_duplicates()
        for index, row in df1.iterrows():
            indicatorerealizzazioneunitamisura, created = IndicatoreRealizzazioneUnitaMisura.objects.get_or_create(
                codice=row['UNITA_MISURA'],
                defaults={
                    'descrizione': row['DESCR_UNITA_MISURA'],
                }
            )
            self._log(created, 'Creata unità di misura degli indicatori di realizzazione: {}'.format(indicatorerealizzazioneunitamisura))

        df1 = df[['COD_INDICATORE', 'DESCR_INDICATORE', 'TIPO_INDICATORE', 'UNITA_MISURA']].drop_duplicates()
        for index, row in df1.iterrows():
            indicatorerealizzazione, created = IndicatoreRealizzazione.objects.get_or_create(
                codice=row['COD_INDICATORE'],
                defaults={
                    'descrizione': row['DESCR_INDICATORE'],
                    'tipo': row['TIPO_INDICATORE'],
                    'unita_misura_id': row['UNITA_MISURA'],
                }
            )
            self._log(created, 'Creato indicatore di realizzazione: {}'.format(indicatorerealizzazione))

    def _import_indicatori(self, df):
        self._truncate_table(ProgettoIndicatoreRealizzazione)

        df = df[df['DESCR_INDICATORE'].str.strip() != ''].sort_values(['COD_LOCALE_PROGETTO', 'COD_INDICATORE'], ascending=[True, True])

        self._import_indicatori_indicatorirealizzazione(df)

        df_count = len(df)

        insert_list = []

        created = 0

        progetto = Progetto()

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                if progetto.codice_locale != row['COD_LOCALE_PROGETTO']:
                    progetto = Progetto.objects.get(codice_locale=row['COD_LOCALE_PROGETTO'])
                    self.stdout.write(self.style.NOTICE('{}/{} - Progetto: {}'.format(n, df_count, progetto)))
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto non trovato: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'])))
            else:
                insert_list.append(
                    ProgettoIndicatoreRealizzazione(
                        progetto=progetto,
                        indicatore_realizzazione_id=row['COD_INDICATORE'],
                        valore_programmato=self._get_value(row, 'VALORE_PROGRAMMATO', 'decimal'),
                        valore_realizzato=self._get_value(row, 'VALORE_REALIZZATO', 'decimal'),
                        flag_visualizzazione=row['OC_FLAG_VISUAL_INDICATORE'],
                    )
                )
                self.stdout.write('{}/{} - Creato indicatore: {}'.format(n, df_count, insert_list[-1]))

                created += 1

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{}/{} -----------------> Salvataggio in corso.'.format(n, df_count))
                ProgettoIndicatoreRealizzazione.objects.bulk_create(insert_list)
                insert_list = []

        self.stdout.write('Sono stati creati {} indicatori su {}.'.format(created, df_count))

    def _import_pagamenti(self, df):
        self._truncate_table(Pagamento)

        df = df[df['TOT_PAGAMENTI'].str.strip() != ''].sort_values(['COD_LOCALE_PROGETTO', 'OC_DATA_PAGAMENTI'], ascending=[True, True])

        df_count = len(df)

        insert_list = []

        created = 0

        progetto = Progetto()

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                if progetto.codice_locale != row['COD_LOCALE_PROGETTO']:
                    progetto = Progetto.objects.get(codice_locale=row['COD_LOCALE_PROGETTO'])
                    self.stdout.write(self.style.NOTICE('{}/{} - Progetto: {}'.format(n, df_count, progetto)))

                    # importi_cumulati = {'TOT_PAGAMENTI': 0, 'OC_TOT_PAGAMENTI_FSC': 0, 'OC_TOT_PAGAMENTI_PAC': 0, 'OC_TOT_PAGAMENTI_RENDICONTAB_UE': 0}
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto non trovato: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'])))
            else:
                # for k in importi_cumulati:
                #     importi_cumulati[k] += self._get_value(row, k, 'decimal')

                insert_list.append(
                    Pagamento(
                        progetto=progetto,
                        data=self._get_value(row, 'OC_DATA_PAGAMENTI', 'date'),
                        # ammontare=importi_cumulati['TOT_PAGAMENTI'],
                        # ammontare_fsc=importi_cumulati['OC_TOT_PAGAMENTI_FSC'],
                        # ammontare_pac=importi_cumulati['OC_TOT_PAGAMENTI_PAC'],
                        # ammontare_rendicontabile_ue=importi_cumulati['OC_TOT_PAGAMENTI_RENDICONTAB_UE'],
                        ammontare=self._get_value(row, 'TOT_PAGAMENTI', 'decimal'),
                        ammontare_fsc=self._get_value(row, 'OC_TOT_PAGAMENTI_FSC', 'decimal'),
                        ammontare_pac=self._get_value(row, 'OC_TOT_PAGAMENTI_PAC', 'decimal'),
                        ammontare_rendicontabile_ue=self._get_value(row, 'OC_TOT_PAGAMENTI_RENDICONTAB_UE', 'decimal'),
                        codice_programma=self._get_value(row, 'OC_CODICE_PROGRAMMA'),
                    )
                )
                self.stdout.write('{}/{} - Creato pagamento: {}'.format(n, df_count, insert_list[-1]))

                created += 1

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{}/{} -----------------> Salvataggio in corso.'.format(n, df_count))
                Pagamento.objects.bulk_create(insert_list)
                insert_list = []

        self.stdout.write('Sono stati creati {} pagamenti su {}.'.format(created, df_count))

    def _import_impegni(self, df):
        self._truncate_table(Impegno)

        df = df[df['IMPEGNI'].str.strip() != ''].sort_values(['COD_LOCALE_PROGETTO', 'OC_DATA_IMPEGNI'], ascending=[True, True])

        df_count = len(df)

        insert_list = []

        created = 0

        progetto = Progetto()

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                if progetto.codice_locale != row['COD_LOCALE_PROGETTO']:
                    progetto = Progetto.objects.get(codice_locale=row['COD_LOCALE_PROGETTO'])
                    self.stdout.write(self.style.NOTICE('{}/{} - Progetto: {}'.format(n, df_count, progetto)))
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto non trovato: {}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'])))
            else:
                insert_list.append(
                    Impegno(
                        progetto=progetto,
                        data=self._get_value(row, 'OC_DATA_IMPEGNI', 'date'),
                        ammontare=self._get_value(row, 'IMPEGNI', 'decimal'),
                    )
                )
                self.stdout.write('{}/{} - Creato impegno: {}'.format(n, df_count, insert_list[-1]))

                created += 1

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{}/{} -----------------> Salvataggio in corso.'.format(n, df_count))
                Impegno.objects.bulk_create(insert_list)
                insert_list = []

        self.stdout.write('Sono stati creati {} impegni su {}.'.format(created, df_count))

    def _update_privacy_progetti(self, df):
        df_count = len(df)

        ids = []
        tot_updated = 0

        for n, (index, row) in enumerate(df.iterrows(), 1):
            ids.append(row['COD_LOCALE_PROGETTO'])

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{}/{} - Aggiornamento del flag privacy in corso ....' .format(n, df_count))
                updated = Progetto.objects.filter(codice_locale__in=ids).update(is_privacy=True)
                self.stdout.write('{}/{} - Fatto. Record aggiornati: {}.'.format(n, df_count, updated))

                ids = []
                tot_updated += updated

        self.stdout.write('Totale record aggiornati: {}.'.format(tot_updated))

    @transaction.atomic
    def _update_privacy_soggetti(self, df):
        ruolo_cod2desc = Ruolo.inv_ruoli_dict()

        df_count = len(df)

        tot_updated = 0

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                soggetto = Soggetto.objects.get(ruoli__progetto__codice_locale=row['COD_LOCALE_PROGETTO'], **{'ruoli__progressivo_{}'.format(ruolo_cod2desc[row['SOGG_COD_RUOLO']]): row['SOGG_PROGR_RUOLO']})
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Soggetto non trovato: {}/{}/{}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'], row['SOGG_COD_RUOLO'], row['SOGG_PROGR_RUOLO'])))
            except MultipleObjectsReturned:
                self.stdout.write(self.style.WARNING('{}/{} - Più di un soggetto trovato: {}/{}/{}. Skipping.'.format(n, df_count, row['COD_LOCALE_PROGETTO'], row['SOGG_COD_RUOLO'], row['SOGG_PROGR_RUOLO'])))
            else:
                soggetto.is_privacy = True
                soggetto.save()

                self.stdout.write(self.style.NOTICE('{}/{} - Aggiornato flag privacy per il soggetto: {}'.format(n, df_count, soggetto)))
                tot_updated += 1

        self.stdout.write('Totale record aggiornati: {}.'.format(tot_updated))

    @transaction.atomic
    def _import_corrispondenze_progetti(self, df):
        df_count = len(df)

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                progetto_attuatore = Progetto.objects.get(codice_locale=row['COD_LOCALE_PROGETTO'])
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto attuatore non trovato: {}. Skipping'.format(n, df_count, row['COD_LOCALE_PROGETTO'])))
            else:
                try:
                    progetto_attuato = Progetto.objects.get(codice_locale=row['CLP_DUPLICATO'])
                except ObjectDoesNotExist:
                    self.stdout.write(self.style.WARNING('{}/{} - Progetto attuato non trovato: {}. Skipping'.format(n, df_count, row['CLP_DUPLICATO'])))
                else:
                    progetto_attuatore.progetti_attuati.add(progetto_attuato)
                    progetto_attuato.is_active = progetto_attuato.is_active and (progetto_attuato.flag_visualizzazione == '1')
                    progetto_attuato.save()

                    self.stdout.write('{}/{} - Creata corrispondenza {} --> {}' .format(n, df_count, progetto_attuato, progetto_attuatore))

    @transaction.atomic
    def _import_progetti_asoc(self, df):
        self._truncate_table(MonitoraggioASOC)

        df_count = len(df)

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                progetto = Progetto.objects.get(codice_locale=row['COD_LOCALE_PROGETTO'])
            except ObjectDoesNotExist:
                self.stdout.write(self.style.WARNING('{}/{} - Progetto non trovato: {}. Skipping'.format(n, df_count, row['COD_LOCALE_PROGETTO'])))
            else:
                provincia_den2cod = {o.denominazione: o.cod_prov for o in Territorio.objects.province()}

                try:
                    territorio = Territorio.objects.comuni().get(cod_prov=provincia_den2cod.get(row['PROVINCIA_ISTITUTO']), denominazione__iexact=row['COMUNE_ISTITUTO'])
                except ObjectDoesNotExist:
                    self.stdout.write(self.style.WARNING('{}/{} - Territorio non trovato: {}/{}. Skipping'.format(n, df_count, row['PROVINCIA_ISTITUTO'], row['COMUNE_ISTITUTO'])))
                except MultipleObjectsReturned:
                    self.stdout.write(self.style.WARNING('{}/{} - Più di un territorio trovato: {}/{}. Skipping'.format(n, df_count, row['PROVINCIA_ISTITUTO'], row['COMUNE_ISTITUTO'])))
                else:
                    MonitoraggioASOC.objects.create(
                        progetto=progetto,
                        titolo_progetto=self._get_value(row, 'OC_TITOLO_PROGETTO'),
                        edizione_asoc=self._get_value(row, 'EDIZIONE_ASOC'),
                        istituto_nome=self._get_value(row, 'ISTITUTO'),
                        istituto_comune=territorio,
                        team=self._get_value(row, 'TEAM_ASOC'),
                        blog_url=self._get_value(row, 'LINK_BLOG'),
                        monithon_url=self._get_value(row, 'LINK_MONITHON'),
                        elaborato_url=self._get_value(row, 'LINK_ELABORATO'),
                        asoc_stories_url=self._get_value(row, 'LINK_ASOC_STORIES'),
                        experience_url=self._get_value(row, 'LINK_EXPERIENCE'),
                    )

                    self.stdout.write('{}/{} - Creato monitoraggio ASOC per il progetto: {}'.format(n, df_count, progetto))

    def _truncate_table(self, model):
        table = model._meta.db_table

        self.stdout.write('Truncate table "{}"'.format(table), ending='... ')

        try:
            with connection.cursor() as cursor:
                cursor.execute('TRUNCATE TABLE "{}" RESTART IDENTITY CASCADE;'.format(table))
        except Exception as e:
            self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
            exit(1)
        else:
            self.stdout.write(self.style.SUCCESS('OK'))

    def _log(self, created, msg):
        if created:
            self.stdout.write(msg)
        else:
            self.stdout.write(self.style.NOTICE(msg.replace('Creat', 'Trovat')))

    @staticmethod
    def _get_value(row, key, type='string'):
        if key in row:
            value = str(row[key]).strip()
            if value:
                if type == 'decimal':
                    value = Decimal(value.replace(',', '.'))
                elif type == 'date':
                    value = datetime.datetime.strptime(value, '%Y%m%d')

                return value

        return None

    @staticmethod
    def _get_id(row, keys, sep='.'):
        values = [row.get(k, '').strip() for k in keys]
        if all(values):
            return sep.join(values)
        return None

    @staticmethod
    def _get_ciclo_programmazione_programma(cod_fonte):
        if cod_fonte.endswith('2127'):
            return '3'
        elif cod_fonte.endswith(('1420', 'NAZORD')):
            return '2'
        else:
            return '1'
