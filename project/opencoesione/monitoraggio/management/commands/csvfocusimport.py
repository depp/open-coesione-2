# -*- coding: utf-8 -*-
import csv
import os

from django.core.management.base import BaseCommand
from django.db.models import Count

from ...models import Progetto, ProgettoFocus, Focus, Territorio


CSV_REQUIRED_COLUMNS = ('COD_LOCALE_PROGETTO', 'OC_FOCUS', 'OC_AMBITO', 'OC_COD_REGIONE', 'OC_COD_CICLO', 'DATA_AGGIORNAMENTO')


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--filename', dest='filename', default=None)

    def handle(self, *args, **options):
        filename = options['filename']

        with open(filename, 'r') as f:
            reader = csv.DictReader(f)
            data = list(reader)
            fieldnames = reader.fieldnames

        os.remove(filename)

        headers_diff = set(CSV_REQUIRED_COLUMNS).difference(set(fieldnames))
        if headers_diff:
            self.log_error('Formato colonne non valido. Le colonne mancanti sono: {}'.format(list(headers_diff)))
            return

        self.log_info('Inizializzazione della procedura')

        progettiwithfocus_pks = set(Progetto.objects.annotate(cnt=Count('focus')).filter(cnt__gt=0).values_list('pk', flat=True))

        progettiwithfocus_pks = self.load_data(data, progettiwithfocus_pks)

        self.rebuild_index(list(progettiwithfocus_pks))

        self.clear_cache()

    def load_data(self, data: list, progettiwithfocus_pks: set) -> set:
        from django.db import connection

        focus_slug2obj = {o.slug: o for o in Focus.objects.all()}
        regione_cod2obj = {'{:03d}'.format(o.cod_reg): o for o in Territorio.objects.regioni()}

        self.log_info('Azzeramento delle relazioni focus-progetti')

        with connection.cursor() as cursor:
            cursor.execute('TRUNCATE TABLE "{}" RESTART IDENTITY CASCADE;'.format(ProgettoFocus._meta.db_table))

        created = 0

        aggiornamenti = {}

        for start, end, total, data_chunk in self.chunks(data, 5000):
            self.log_info('Correlazione ai focus dei progetti {}-{}/{}'.format(start + 1, end, total))

            progetto_clp2pk = {x['codice_locale']: x['pk'] for x in Progetto.objects.filter(codice_locale__in=(r['COD_LOCALE_PROGETTO'] for r in data_chunk)).values('pk', 'codice_locale')}

            objs = []
            for row in data_chunk:
                try:
                    for slug in row['OC_FOCUS'].replace('#', '').strip().split(' '):
                        focus_slug = slug.split('.')[0]
                        for slg in {focus_slug, slug}:
                            if slg not in focus_slug2obj:
                                focus_slug2obj[slg] = Focus.objects.create(slug=slg, nome=slg.replace('_', ' ').replace('.', ' ').capitalize(), classificazione_superiore=focus_slug2obj.get(focus_slug))

                        objs.append(ProgettoFocus(
                            progetto_id=progetto_clp2pk[row['COD_LOCALE_PROGETTO']],
                            focus=focus_slug2obj[slug],
                            ciclo_programmazione=row['OC_COD_CICLO'],
                            ambito=row['OC_AMBITO'],
                            regione=regione_cod2obj.get(row['OC_COD_REGIONE']),
                        ))

                        aggiornamenti.setdefault(row['DATA_AGGIORNAMENTO'], set()).add(focus_slug)
                except KeyError:
                    self.log_warning('{} - progetto non trovato'.format(row['COD_LOCALE_PROGETTO']))
                except Exception as e:
                    self.log_error('{} - {}'.format(row['COD_LOCALE_PROGETTO'], e))

            objs = ProgettoFocus.objects.bulk_create(objs)

            created += len(objs)

            progettiwithfocus_pks.update(set(progetto_clp2pk.values()))

        self.log_info('Sono state create {} relazioni focus-progetti'.format(created))

        from datetime import datetime
        for data_aggiornamento, focus_slugs in aggiornamenti.items():
            data_aggiornamento = datetime.strptime(data_aggiornamento, '%Y%m%d')
            objs = Focus.objects.filter(slug__in=focus_slugs)
            objs.update(data_aggiornamento=data_aggiornamento)
            self.log_info('È stata impostata la data di aggiornamento {} per i focus: {}'.format(data_aggiornamento, ', '.join('"{}"'.format(o) for o in objs.order_by('nome'))))

        return progettiwithfocus_pks

    def rebuild_index(self, progettiwithfocus_pks: list):
        from ...search_indexes import ProgettoIndex

        index = ProgettoIndex()

        for start, end, total, progettiwithfocus_pks_chunk in self.chunks(progettiwithfocus_pks, 5000):
            self.log_info('Indicizzazione dei progetti {}-{}/{}'.format(start + 1, end, total))
            index.get_backend().update(index, index.index_queryset().filter(pk__in=progettiwithfocus_pks_chunk))

    def clear_cache(self):
        from django.conf import settings
        from django.core.cache import cache
        from django.utils import translation

        self.log_info('Azzeramento delle cache dei focus')

        objs = Focus.objects.all()

        for language in settings.LANGUAGES:
            with translation.override(language[0]):
                for obj in objs:
                    for ciclo_programmazione in [''] + [x[0] for x in settings.CICLO_PROGRAMMAZIONE]:
                        url = obj.get_absolute_url() + ('?ciclo_programmazione={}'.format(ciclo_programmazione) if ciclo_programmazione else '')
                        cache.delete('context{}'.format(url))

    @staticmethod
    def chunks(lst, chunk_size):
        total = len(lst)
        for start in range(0, total, chunk_size):
            end = min(start + chunk_size, total)
            yield start, end, total, lst[start:end]

    def log_info(self, msg):
        self.stdout.write('INFO: {}'.format(msg))

    def log_error(self, msg):
        self.stdout.write('ERROR: {}'.format(msg))

    def log_warning(self, msg):
        self.stdout.write('WARNING: {}'.format(msg))

    def log_debug(self, msg):
        self.stdout.write('DEBUG: {}'.format(msg))
