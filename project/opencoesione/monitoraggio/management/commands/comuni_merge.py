# -*- coding: utf-8 -*-
import datetime
import pandas as pd
from django.core.management.base import BaseCommand
from django.db import transaction
from ...models import Territorio


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('--filename', dest='filename', default=None, help='Select file name.')

    @transaction.atomic
    def handle(self, *args, **options):
        filename = options['filename']

        self.stdout.write('Inizio import dal file {}.'.format(filename))

        start_time = datetime.datetime.now()

        new_col_names = ['COD_REGIONE_NEW', 'COD_PROVINCIA_NEW', 'COD_COMUNE_NEW', 'DEN_REGIONE_NEW', 'DEN_PROVINCIA_NEW', 'DEN_COMUNE_NEW']

        # df = pd.read_excel(filename)
        df = pd.read_csv(filename, sep=';')
        gb = df.groupby(new_col_names, as_index=False)

        df_count = gb.ngroups

        for n, (new, df1) in enumerate(gb, 1):
            new = dict(zip(new_col_names, new))

            try:
                self.stdout.write('{}/{} - Accorpamento di {} comuni nel comune: {}'.format(n, df_count, len(df1), new['DEN_COMUNE_NEW']), ending='... ')

                territori_old = Territorio.objects.filter(cod_com__in=(r['COD_PROVINCIA_OLD'] * 1000 + r['COD_COMUNE_OLD'] for _, r in df1.iterrows()))

                if len(territori_old) < len(df1):
                    raise Exception('territorio non trovato')

                self.stdout.write('[{}]'.format(', '.join(str(t) for t in territori_old)), ending='... ')

                values = {
                    'denominazione': new['DEN_COMUNE_NEW'],
                    'cod_reg': new['COD_REGIONE_NEW'],
                    'cod_prov': new['COD_PROVINCIA_NEW'],
                    'cod_com': new['COD_PROVINCIA_NEW'] * 1000 + new['COD_COMUNE_NEW'],
                    'tipo': Territorio.TIPO.C,
                    'popolazione_totale': sum(t.popolazione_totale for t in territori_old),
                    'popolazione_femminile': sum(t.popolazione_femminile for t in territori_old),
                    'popolazione_maschile': sum(t.popolazione_maschile for t in territori_old),
                }

                territorio_new = Territorio.objects.create(**values)

                for territorio_old in territori_old:
                    territorio_old.localizzazione_set.all().update(territorio=territorio_new)

                    if territorio_old.cod_reg != territorio_new.cod_reg:
                        territorio_old.regione.popolazione_totale -= territorio_old.popolazione_totale
                        territorio_old.regione.popolazione_femminile -= territorio_old.popolazione_femminile
                        territorio_old.regione.popolazione_maschile -= territorio_old.popolazione_maschile
                        territorio_old.regione.save()

                        territorio_new.regione.popolazione_totale += territorio_old.popolazione_totale
                        territorio_new.regione.popolazione_femminile += territorio_old.popolazione_femminile
                        territorio_new.regione.popolazione_maschile += territorio_old.popolazione_maschile
                        territorio_new.regione.save()

                    if territorio_old.cod_prov != territorio_new.cod_prov:
                        territorio_old.provincia.popolazione_totale -= territorio_old.popolazione_totale
                        territorio_old.provincia.popolazione_femminile -= territorio_old.popolazione_femminile
                        territorio_old.provincia.popolazione_maschile -= territorio_old.popolazione_maschile
                        territorio_old.provincia.save()

                        territorio_new.provincia.popolazione_totale += territorio_old.popolazione_totale
                        territorio_new.provincia.popolazione_femminile += territorio_old.popolazione_femminile
                        territorio_new.provincia.popolazione_maschile += territorio_old.popolazione_maschile
                        territorio_new.provincia.save()

                    territorio_old.delete()
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
            else:
                self.stdout.write(self.style.SUCCESS('OK'))

        duration = datetime.datetime.now() - start_time
        seconds = round(duration.total_seconds())

        self.stdout.write('Fine. Tempo di esecuzione: {:02d}:{:02d}:{:02d}.'.format(int(seconds // 3600), int((seconds % 3600) // 60), int(seconds % 60)))
