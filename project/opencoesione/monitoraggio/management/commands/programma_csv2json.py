# -*- coding: utf-8 -*-
import datetime
import glob
import json
import os

import pandas as pd
import zipfile

from django.conf import settings
from django.core.management.base import BaseCommand
from io import BytesIO

from django.utils import translation
from pandas.errors import EmptyDataError
from ...models import ClassificazioneAzione, TemaSintetico, Territorio, Progetto


def convert_cup_cod_natura(val):
    if not val.strip():
        val = 'ND'
    elif val == '02':
        val = '01'
    return val


def convert_multivalues(val):
    return ':::{}:::'.format(val)


def convert_float(val):
    try:
        return float(val.replace('.', '').replace(',', '.'))
    except ValueError:
        return None


class Command(BaseCommand):
    """
    Data are exported to JSON from their CSV sources.
    """
    help = 'Export data from csv to json'

    def add_arguments(self, parser):
        parser.add_argument('--programma', dest='codice_programma', default=None, help='Select program code.')
        parser.add_argument('--csv-path', dest='csv_path', default=None, help='Select csv files path.')
        parser.add_argument('--encoding', dest='encoding', default='utf-8-sig', help='Set character encoding of input file.')

    def handle(self, *args, **options):
        start_time = datetime.datetime.now()

        codice_programma = options['codice_programma']
        csvpath = options['csv_path']
        encoding = options['encoding']

        if codice_programma == '2014IT14MFOP001':
            zfilename = 'progetti_esteso_FEAMP_2014_2020_{}.zip'
        elif codice_programma in ('2014IT06RDRP005', '2014IT06RDRP010', '2014IT06RDRP012', '2014IT06RDRP013', '2014IT06RDRP018', '2014IT06RDRP019', '2014IT06RDRP020'):
            zfilename = 'progetti_esteso_FEASR_2014_2020_{}.zip'
        else:
            zfilename = 'progetti_esteso_{}.zip'

        zfilename = csvpath.rstrip('/') + '/opendata/' + zfilename.format('202?????')

        files = sorted(glob.glob(zfilename))

        if not files:
            self.stdout.write(self.style.ERROR('Impossibile aprire il file {}.'.format(zfilename)))
        else:
            zfilename = files[-1]

            try:
                self.stdout.write('Lettura del file {}.'.format(zfilename))

                with zipfile.ZipFile(zfilename) as zfile:
                    csv = zfile.read(zfile.namelist().pop(0)).decode(encoding).encode('utf-8')
            except IOError:
                self.stdout.write(self.style.ERROR('Impossibile aprire il file {}.'.format(zfilename)))
            else:
                try:
                    df = pd.read_csv(
                        BytesIO(csv),
                        sep=';',
                        header=0,
                        low_memory=True,
                        dtype=object,
                        encoding='utf-8',
                        keep_default_na=False,
                        converters={
                            'CUP_COD_NATURA': convert_cup_cod_natura,
                            'COD_REGIONE': convert_multivalues,
                            'COD_PROVINCIA': convert_multivalues,
                            'OC_FINANZ_TOT_PUB_NETTO': convert_float,
                            'TOT_PAGAMENTI': convert_float,
                        },
                    )
                except EmptyDataError:
                    self.stdout.write(self.style.ERROR('Nessuna colonna da analizzare nel file {}.'.format(zfilename)))
                else:
                    df = df[df['OC_CODICE_PROGRAMMA'] == codice_programma][['OC_DESCRIZIONE_PROGRAMMA', 'COD_LOCALE_PROGETTO', 'OC_COD_TEMA_SINTETICO', 'CUP_COD_NATURA', 'COD_REGIONE', 'COD_PROVINCIA', 'OC_STATO_PROGETTO', 'OC_FINANZ_TOT_PUB_NETTO', 'TOT_PAGAMENTI']].drop_duplicates()

                    stati_progetto = [(x[0], str(x[1])) for x in Progetto.STATO]

                    for language in ('it', 'en'):
                        translation.activate(language)

                        jfilename = os.path.join(settings.MEDIA_ROOT, 'programma_{}_{}.json'.format(codice_programma, language))

                        self.stdout.write('Esportazione nel file {}.'.format(jfilename))

                        data = {
                            'contesto': {
                                'nome_programma': df['OC_DESCRIZIONE_PROGRAMMA'].iloc[0],
                                # 'n_beneficiari': df['OC_TOTALE_BENEFICIARI'].sum(),
                            },
                            'data_aggiornamento': os.path.splitext(zfilename)[0].split('_')[-1],
                            'aggregati': {
                                **self._get_totali(df),
                                'stati_progetti': {
                                    Progetto.inv_stati_dict()[x[0]]: {'label': translation.ugettext(x[1]), **self._get_totali(df[df['OC_STATO_PROGETTO'] == x[1]])} for x in stati_progetto
                                },
                                'temi': {
                                    o.slug: {'label': o.descrizione_breve, **self._get_totali(df[df['OC_COD_TEMA_SINTETICO'] == o.codice])} for o in TemaSintetico.objects.all()
                                },
                                'nature': {
                                    o.slug: {'label': o.descrizione_breve, **self._get_totali(df[df['CUP_COD_NATURA'] == o.codice])} for o in ClassificazioneAzione.objects.principali()
                                },
                                'territori': {
                                    'regioni': {
                                        o.slug: {'label': str(o), **self._get_totali(df[df['COD_REGIONE'].str.contains(':::{:03d}:::'.format(o.cod_reg))])} for o in Territorio.objects.regioni()
                                    },
                                    'province': {
                                        o.slug: {'label': str(o), **self._get_totali(df[df['COD_PROVINCIA'].str.contains(':::{:03d}{:03d}:::'.format(o.cod_reg, o.cod_prov))])} for o in Territorio.objects.province()
                                    },
                                },
                            },
                        }

                        with open(jfilename, 'w') as jfile:
                            json.dump(data, jfile)

        duration = datetime.datetime.now() - start_time
        seconds = round(duration.total_seconds())

        self.stdout.write('Fine. Tempo di esecuzione: {:02d}:{:02d}:{:02d}.'.format(int(seconds // 3600), int((seconds % 3600) // 60), int(seconds % 60)))

    def _get_totali(self, df):
        floatformat = lambda x: '{},00'.format(int(round(x, 0)))
        return {
            'totali': {
                'costo_pubblico': floatformat(df['OC_FINANZ_TOT_PUB_NETTO'].sum()),
                'pagamenti': floatformat(df['TOT_PAGAMENTI'].sum()),
                'progetti': str(len(df)),
            }
        }
