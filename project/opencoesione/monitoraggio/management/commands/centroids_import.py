# -*- coding: utf-8 -*-
import datetime
import json
import logging
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.core.management.base import BaseCommand
from urllib.request import urlopen
from ...models import Territorio


class Command(BaseCommand):
    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument('--json-url', dest='url', default=None, help='URL of JSON.')

    def handle(self, *args, **options):
        verbosity = options['verbosity']
        if verbosity == '0':
            self.logger.setLevel(logging.ERROR)
        elif verbosity == '1':
            self.logger.setLevel(logging.WARNING)
        elif verbosity == '2':
            self.logger.setLevel(logging.INFO)
        elif verbosity == '3':
            self.logger.setLevel(logging.DEBUG)

        url = options['url']

        start_time = datetime.datetime.now()

        try:
            self.logger.info('Lettura file {} ...'.format(url))

            results = json.loads(urlopen(url).read().decode('utf-8'))['results']
        except IOError:
            self.logger.error('Impossibile leggere il file {}'.format(url))
        else:
            self.logger.info('Fatto.')

            result_count = len(results)

            updated_count = 0

            self.logger.info('Aggiornamento centroidi ...')

            for n, result in enumerate(results, 1):
                try:
                    territorio = Territorio.objects.get(cod_reg=result['cod_reg'], cod_prov=result['cod_pro'], cod_com=result['cod_com'])
                except ObjectDoesNotExist:
                    self.logger.warning('{}/{} - Territorio non trovato: {}/{}/{}. Skipping.'.format(n, result_count, result['cod_reg'], result['cod_pro'], result['cod_com']))
                except MultipleObjectsReturned:
                    self.logger.warning('{}/{} - Più di un territorio trovato: {}/{}/{}. Skipping.'.format(n, result_count, result['cod_reg'], result['cod_pro'], result['cod_com']))
                else:
                    territorio.centroid_lat = result['centroid']['lat']
                    territorio.centroid_lon = result['centroid']['lon']
                    territorio.save()

                    self.logger.debug('{}/{} - Aggiornato centroide per il territorio: {}'.format(n, result_count, territorio))
                    updated_count += 1

                if (n % 500 == 0) or (n == result_count):
                    self.logger.info('{}/{}'.format(n, result_count))

            self.logger.info('Fatto. Totale record aggiornati: {} su {}.'.format(updated_count, result_count))

        duration = datetime.datetime.now() - start_time
        seconds = round(duration.total_seconds())

        self.logger.info('Fine. Tempo di esecuzione: {:02d}:{:02d}:{:02d}.'.format(int(seconds // 3600), int((seconds % 3600) // 60), int(seconds % 60)))
