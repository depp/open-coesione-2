# -*- coding: utf-8 -*-
import datetime
import logging
from django.conf import settings
from django.core.cache import cache
from django.core.management.base import BaseCommand
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.test import RequestFactory
from django.utils import translation
from ...views import GlobaleView, GruppoProgrammiView, ProgrammaDetailView, ClassificazioneAzioneDetailView,\
    TemaSinteticoDetailView, TerritorioDetailView, SoggettoDetailView, ProgrammaWidgetDetailView


def setup_view(view, request, *args, **kwargs):
    """
    Mimic as_view() returned callable, but returns view instance.
    args and kwargs are the same you would pass to ``reverse()``
    """
    view.request = request
    view.args = args
    view.kwargs = kwargs

    # get the object instance using the view's get_object method
    if hasattr(view, 'get_object'):
        view.object = view.get_object()

    return view


class Command(BaseCommand):
    """
    Generate cached content by visiting pages, uses request.
    """

    help = 'Generate cached content for aggregate pages'

    page_types = {
        'globale': {
            'view_class': GlobaleView,
            'url_name': 'globale',
        },
        'temi': {
            'view_class': TemaSinteticoDetailView,
            'url_name': 'temasintetico_detail',
        },
        'nature': {
            'view_class': ClassificazioneAzioneDetailView,
            'url_name': 'classificazioneazione_detail',
        },
        'gruppiprogrammi': {
            'view_class': GruppoProgrammiView,
            'url_name': 'gruppo_programmi',
        },
        'programmi': {
            'view_class': ProgrammaDetailView,
            'url_name': 'programma_detail',
        },
        'programmiwidget': {
            'view_class': ProgrammaWidgetDetailView,
            'url_name': 'programma_widget_detail',
        },
        'soggetti': {
            'view_class': SoggettoDetailView,
            'url_name': 'soggetto_detail',
        },
        'territori': {
            'view_class': TerritorioDetailView,
            'url_name': 'territorio_detail',
        },
    }

    def add_arguments(self, parser):
        parser.add_argument('--type', dest='type', default=None, help='Page type; choose among {}.'.format(', '.join('"{}"'.format(t) for t in self.page_types)))

    logger = logging.getLogger('project')

    def handle(self, *args, **options):
        verbosity = options['verbosity']
        if verbosity == '0':
            self.logger.setLevel(logging.ERROR)
        elif verbosity == '1':
            self.logger.setLevel(logging.WARNING)
        elif verbosity == '2':
            self.logger.setLevel(logging.INFO)
        elif verbosity == '3':
            self.logger.setLevel(logging.DEBUG)

        page_type = options['type']

        if page_type and page_type not in self.page_types:
            self.logger.error('Wrong --type option "{}". Choose among {}.'.format(page_type, ', '.join('"{}"'.format(t) for t in self.page_types)))
        else:
            start_time = datetime.datetime.now()

            page_types = [page_type] if page_type else self.page_types.keys()

            for page_type in page_types:
                self.logger.info('Inizio generazione cache per "{}".'.format(page_type))

                method = getattr(self, 'handle_{}'.format(page_type))
                method(self.page_types[page_type])

            duration = datetime.datetime.now() - start_time
            seconds = round(duration.total_seconds())

            self.logger.info('Fine. Tempo di esecuzione: {:02d}:{:02d}:{:02d}.'.format(int(seconds // 3600), int((seconds % 3600) // 60), int(seconds % 60)))

    def handle_globale(self, params):
        self._cache_computation(params, {})

    def handle_temi(self, params):
        from ....monitoraggio.models import TemaSintetico

        for slug in TemaSintetico.objects.all().values_list('slug', flat=True):
            self._cache_computation(params, {'slug': slug})

    def handle_nature(self, params):
        from ....monitoraggio.models import ClassificazioneAzione

        for slug in ClassificazioneAzione.objects.principali().values_list('slug', flat=True):
            self._cache_computation(params, {'slug': slug})

    def handle_gruppiprogrammi(self, params):
        from ...gruppo_programmi import GruppoProgrammi

        for slug in GruppoProgrammi.GRUPPI_PROGRAMMI.keys():
            self._cache_computation(params, {'slug': slug})

    def handle_programmi(self, params):
        from ....monitoraggio.models import Programma

        for programma in Programma.objects.programmi().annotate(n=Count('classificazione_set__classificazione_set__ambiti__progetto', distinct=True)).filter(n__gt=settings.BIG_PROGRAMMI_THRESHOLD):
            self._cache_computation(params, {'pk': programma.pk})

    def handle_programmiwidget(self, params):
        from ...gruppo_programmi import GruppoProgrammi
        from ....monitoraggio.models import Programma

        for programma in Programma.objects.programmi().annotate(n=Count('classificazione_set__classificazione_set__ambiti__progetto', distinct=True)).filter(n__gt=settings.BIG_PROGRAMMI_THRESHOLD, pk__in=(o.pk for o in GruppoProgrammi('ue-1420').programmi)):
            self._cache_computation(params, {'pk': programma.pk})

    def handle_soggetti(self, params):
        from ....monitoraggio.models import Soggetto

        for soggetto in Soggetto.objects.annotate(n=Count('progetti')).filter(n__gt=settings.BIG_SOGGETTI_THRESHOLD):
            self._cache_computation(params, {'slug': soggetto.slug})

    def handle_territori(self, params):
        from ....monitoraggio.models import Territorio

        for slug in list(Territorio.objects.macroaree().values_list('slug', flat=True)) + list(Territorio.objects.regioni().values_list('slug', flat=True)) + list(Territorio.objects.province().values_list('slug', flat=True)):
            self._cache_computation(params, {'slug': slug})

    def _cache_computation(self, params, url_cond):
        for language in settings.LANGUAGES:
            translation.activate(language[0])

            for ciclo_programmazione in [''] + [x[0] for x in settings.CICLO_PROGRAMMAZIONE]:
                url = reverse(params['url_name'], kwargs=url_cond)

                if ciclo_programmazione:
                    url = '{}?ciclo_programmazione={}'.format(url, ciclo_programmazione)

                cache.delete('context{}'.format(url))

                try:
                    self.logger.info(u'Generazione della cache per: "{}"...'.format(url))

                    view = setup_view(params['view_class'](), RequestFactory().get(url), **url_cond)
                    view.get_context_data()
                except Exception as e:
                    self.logger.error(u'ERRORE: {}.'.format(e))
                else:
                    self.logger.info(u'Fatto.')
