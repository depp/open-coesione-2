# -*- coding: utf-8 -*-
import logging
import os
import requests
import shutil
import zipfile
from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand
from pathlib import Path


class Command(BaseCommand):
    """
    A BaseCommand to import data from CSVs into the database using Django ORM.
    """
    help = 'Import all data by running each command in the proper order. ' \
        'NOTE: existing data will be destroyed.'
    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument('data_url', nargs=1, help='URL to the zip file containing all the data (dropbox)')
        parser.add_argument('--data-path', dest='data_path', default='./data', help='Path to the directory containing downloaded data on the server')
        parser.add_argument('--import-sections', dest='import_sections', default=None, help='Sections to import, separated by "@"')
        parser.add_argument('--no-download', dest='download', action='store_false', help='Avoid download, since data are already there (dev-only)')
        parser.add_argument('--opendata-only', dest='opendata_only', action='store_true', help='Perform only opendata update operations')

    def handle(self, *args, **options):
        verbosity = options['verbosity']
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        data_url = options['data_url'][0]
        update_date = settings.DATA_AGGIORNAMENTO
        data_path = os.path.join(options['data_path'], update_date)
        opendata_path = os.path.join(settings.MEDIA_ROOT, 'open_data')

        # prepare data_path
        os.makedirs(data_path, exist_ok=True)

        # grab the shared dropbox folder as zipfile and extract all files in data_path
        if options['download']:
            self.logger.info('Fetching data from Dropbox ...')

            response = requests.get(data_url, stream=True)
            response.raise_for_status()
            with open('data.zip', 'wb') as data_zip:
                for block in response.iter_content(8192, decode_unicode=True):
                    data_zip.write(block)
            with zipfile.ZipFile('data.zip') as zfile:
                zfile.extractall(data_path)

            os.unlink('data.zip')

        if not options['opendata_only']:
            self.update_database(data_path, options['import_sections'], verbosity)
            # self.update_feampjson(data_path, options['import_sections'], verbosity)

        self.update_opendata(data_path, opendata_path)

    def update_opendata(self, data_path, opendata_path):
        self.logger.info('Update open_data files ...')

        # remove opendata_path
        shutil.rmtree(opendata_path)

        # copy opendata directory except opendata/focus in opendata_path
        shutil.copytree(os.path.join(data_path, 'opendata'), opendata_path)
        shutil.rmtree(os.path.join(opendata_path, 'focus'))

        for p in Path(data_path).glob('*.zip'):
            shutil.copy(p, opendata_path)

        for p in Path(data_path).glob('*.parquet'):
            shutil.copy(p, opendata_path)

        for p in Path(opendata_path).glob('focus_*.zip'):
            p.unlink()

        for p in Path(os.path.join(opendata_path, 'feasr')).glob('*.csv'):
            p.unlink()

        self.logger.info('Create open_data/programmi files ...')

        call_command('split_progetti_esteso_by_programma')

    def update_database(self, data_path, import_sections, verbosity):
        try:
            for section in [
                'progetti',
                # 'focus',
                'ambiti',
                'soggetti',
                'aree_comuni',
                'aree_progetti',
                'strategie-s3',
                'localizzazioni',
                'fasi',
                'indicatori',
                'pagamenti',
                'impegni',
                'privacy-progetti',
                'privacy-soggetti',
                'corrispondenze-progetti',
                'sintesi-ponrec',
                'sintesi-pongat',
            ]:
                if not import_sections or section in import_sections.split('@'):
                    self.logger.info('Importing {} ...'.format(section))

                    if section == 'sintesi-ponrec':
                        call_command(
                            'csvremoteimport',
                            import_type=section,
                            verbosity=verbosity,
                            csv_file='http://www.ponrec.it/opendata/ponrec_opendata.csv',
                            separator=',',
                        )
                    elif section == 'sintesi-pongat':
                        call_command(
                            'csvremoteimport',
                            import_type=section,
                            verbosity=verbosity,
                            csv_file='http://www.agenziacoesione.gov.it/opencms/export/sites/dps/it/documentazione/pongat/Beneficiari/Beneficiari_PON_GAT_dati_31_10_2015.csv',
                            encoding='latin1',
                        )
                    else:
                        call_command(
                            'csvimport',
                            import_type=section,
                            verbosity=verbosity,
                            csv_path=data_path,
                        )

            from ....opendata.models import LocalDataset
            LocalDataset.objects.update(updated_at=settings.DATA_AGGIORNAMENTO)
        except (KeyboardInterrupt, SystemExit):
            return '\nInterrupted by the user.'
        else:
            return 'Done!'

    def update_feampjson(self, data_path, import_sections, verbosity):
        if not import_sections or 'feamp-json' in import_sections.split('@'):
            self.logger.info('Exporting FEAMP json ...')

            call_command(
                'programma_csv2json',
                codice_programma='2014IT14MFOP001',
                verbosity=verbosity,
                csv_path=data_path,
            )
