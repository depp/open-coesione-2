# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2021-12-07 13:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('monitoraggio', '0025_auto_20211123_1349'),
    ]

    operations = [
        migrations.AddField(
            model_name='ambito',
            name='ambito',
            field=models.CharField(choices=[('CTE', 'CTE'), ('FEAMP', 'FEAMP'), ('FEASR', 'FEASR'), ('FESR', 'FESR'), ('FSC', 'FSC'), ('FSE', 'FSE'), ('IOG', 'IOG'), ('PAC', 'PAC'), ('SNAI', 'SNAI')], default='FESR', max_length=5),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='progetto',
            name='macro_area',
            field=models.CharField(choices=[('C', 'Centro-Nord'), ('M', 'Mezzogiorno'), ('E', 'Estero'), ('N', 'Ambito Nazionale'), ('X', 'Altro')], db_index=True, default='C', max_length=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='ambito',
            name='fondo_comunitario',
            field=models.CharField(blank=True, choices=[('FESR', 'Fondo Europeo di Sviluppo Regionale (FESR)'), ('FSE', 'Fondo Sociale Europeo (FSE)'), ('FEASR', 'Fondo Europeo Agricolo per lo Sviluppo Rurale (FEASR)'), ('FSC', 'Fondo per lo Sviluppo e la Coesione (FSC)'), ('PAC', "Piano d'Azione Coesione (PAC)"), ('IOG', 'Iniziativa Occupazione Giovani (IOG)'), ('FEAMP', 'Fondo Europeo per gli Affari Marittimi e la Pesca (FEAMP)')], max_length=5, null=True),
        ),
    ]
