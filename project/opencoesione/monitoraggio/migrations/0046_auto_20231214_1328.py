# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2023-12-14 12:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


def set_file_relations(apps, schema_editor):
    Focus = apps.get_model('monitoraggio', 'Focus')
    Dataset = apps.get_model('opendata', 'Dataset')
    MetadataFile = apps.get_model('opendata', 'MetadataFile')

    dataset_map = {o.slug: o for o in Dataset.objects.all()}
    metadata_map = {o.slug: o for o in MetadataFile.objects.all()}

    for obj in Focus.objects.filter(classificazione_superiore__isnull=True):
        slug = obj.slug.replace('_', '')
        slug = {'attrattivitaturistica': 'turismo', 'innovazionericerca': 'ricerca', 'covid': 'covid19'}.get(slug, slug)

        obj.file_metadati = metadata_map['focus_' + slug]

        if slug in ('cultura', 'ricerca'):
            slug += '_2'
        elif slug in ('beniconfiscati', 'turismo'):
            slug += '_3'
        else:
            slug += '_1'

        obj.file_dati = dataset_map['focus_' + slug]

        obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('monitoraggio', '0045_auto_20230727_2300'),
        ('opendata', '0003_auto_20231206_1301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='focus',
            name='file_dati',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='opendata.Dataset'),
        ),
        migrations.AlterField(
            model_name='focus',
            name='file_metadati',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='opendata.MetadataFile'),
        ),
        migrations.RunPython(
            set_file_relations
        ),
    ]
