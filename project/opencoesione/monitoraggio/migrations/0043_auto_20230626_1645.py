# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2023-06-26 14:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('monitoraggio', '0042_auto_20230621_2326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='temasinteticoindicatore',
            name='titolo',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='temasinteticoindicatore',
            name='titolo_dati',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='temasinteticoindicatore',
            name='titolo_dati_en',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='temasinteticoindicatore',
            name='titolo_dati_it',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='temasinteticoindicatore',
            name='titolo_en',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='temasinteticoindicatore',
            name='titolo_it',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
