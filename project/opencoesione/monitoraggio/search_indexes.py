# -*- coding: utf-8 -*-
import datetime
from itertools import chain

from django.db.models import Prefetch
from haystack import indexes

from .models import Progetto, Ruolo, Soggetto, TemaSintetico, ClassificazioneAzione, Ambito


class ProgettoIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True, stored=False)

    is_active = indexes.FacetBooleanField(model_attr='is_active', stored=False)
    is_pubblicato = indexes.FacetCharField(stored=False)

    soggetto = indexes.FacetMultiValueField(stored=False)

    territorio_tipo = indexes.MultiValueField(stored=False)
    territorio_com = indexes.MultiValueField(stored=False)
    territorio_prov = indexes.MultiValueField(stored=False)
    territorio_reg = indexes.MultiValueField(stored=False)
    territorio_mac = indexes.MultiValueField(stored=False)

    programma = indexes.MultiValueField(stored=False)
    fondo_comunitario = indexes.MultiValueField(stored=False)

    ciclo_programmazione = indexes.FacetMultiValueField(stored=False)

    natura = indexes.FacetCharField(stored=False)
    tema = indexes.FacetCharField(stored=False)
    fonte = indexes.FacetMultiValueField(stored=False)
    stato = indexes.FacetCharField(model_attr='stato', stored=False)

    tipo_area = indexes.FacetMultiValueField(stored=False)
    tipo_area_reg = indexes.MultiValueField(stored=False)
    area = indexes.MultiValueField(stored=False)

    s3 = indexes.MultiValueField(stored=False)
    t_s3 = indexes.MultiValueField(stored=False)

    focus = indexes.FacetMultiValueField(stored=False)

    data_inizio = indexes.FacetDateField(stored=False)
    costo = indexes.FacetFloatField(model_attr='finanz_totale_pubblico_netto', stored=False)
    perc_pagamento = indexes.FacetFloatField(stored=False)

    def get_model(self):
        return Progetto

    def index_queryset(self, using=None):
        return self.get_model().objects.prefetch_related(
            Prefetch('ambiti', queryset=Ambito.objects.select_related('fonte', 'programma')),
            'territori',
            'focus',
            'aree__classificazione_set__comuni',
            'strategie_s3__classificazione_superiore__classificazione_superiore',
        )

    def prepare(self, obj):
        self.prepared_data = super().prepare(obj)

        self.prepared_data['cup_s'] = obj.cup or ''

        return self.prepared_data

    def prepare_is_pubblicato(self, obj):
        if obj.flag_visualizzazione == '0':
            return '1'
        elif obj.flag_visualizzazione == '9':
            return '2'
        else:
            return '0'

    def prepare_soggetto(self, obj):
        soggetti = []
        for r in obj.ruoli.values('soggetto__slug', *['progressivo_{}'.format(v) for v in Ruolo.inv_ruoli_dict().values()]):
            for ruolo_cod, ruolo_desc in Ruolo.inv_ruoli_dict().items():
                if r['progressivo_{}'.format(ruolo_desc)] > 0:
                    soggetti.append('{}|{}'.format(r['soggetto__slug'], ruolo_cod))
        return soggetti

    def prepare_territorio_tipo(self, obj):
        return [o.tipo for o in obj.territori.all()]

    def prepare_territorio_com(self, obj):
        return [o.cod_com for o in obj.territori.all()]

    def prepare_territorio_prov(self, obj):
        return [o.cod_prov for o in obj.territori.all()]

    def prepare_territorio_reg(self, obj):
        return [o.cod_reg for o in obj.territori.all()]

    def prepare_territorio_mac(self, obj):
        return [o.cod_mac for o in obj.territori.all()]

    def prepare_programma(self, obj):
        return [o.codice.split('/')[0] for o in obj.programmi]

    def prepare_fondo_comunitario(self, obj):
        return set(o.fondo_comunitario for o in obj.ambiti.all() if o.fondo_comunitario)

    def prepare_ciclo_programmazione(self, obj):
        return set(o.ciclo_programmazione for o in obj.ambiti.all())

    def prepare_natura(self, obj):
        return obj.classificazione_azione_id.split('.')[0]

    def prepare_tema(self, obj):
        return obj.tema_sintetico_id

    def prepare_fonte(self, obj):
        fonti = [o.codice for o in obj.fonti]
        if any(o.flag_pac != '0' for o in obj.ambiti.all()) and not 'PAC' in fonti:
            fonti.append('PAC')
        return fonti

    def prepare_tipo_area(self, obj):
        return set(
            [o.classificazione_superiore_id for o in obj.aree.all()] + [o.classificazione_superiore.classificazione_superiore.classificazione_superiore_id for o in obj.strategie_s3.all()]
        )

    def prepare_tipo_area_reg(self, obj):
        return set('{}.{:02d}'.format(o.classificazione_superiore_id, comune.cod_reg) for o in obj.aree.all() for comune in o.lista_comuni)

    def prepare_area(self, obj):
        return [o.codice for o in obj.aree.all()]

    def prepare_s3(self, obj):
        return set(chain.from_iterable([[o.codice, o.classificazione_superiore_id, o.classificazione_superiore.classificazione_superiore_id] for o in obj.strategie_s3.all()]))

    def prepare_t_s3(self, obj):
        return set(o.territorio_id for o in obj.strategie_s3.all())

    def prepare_focus(self, obj):
        return [o.slug.split('.')[0] for o in obj.focus.all()]

    def prepare_data_inizio(self, obj):
        return obj.data_inizio or datetime.datetime.strptime('19700101', '%Y%m%d')

    def prepare_perc_pagamento(self, obj):
        return obj.percentuale_pagamenti


class SoggettoIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True, stored=False)

    territorio_com = indexes.MultiValueField(stored=False)
    territorio_prov = indexes.MultiValueField(stored=False)
    territorio_reg = indexes.MultiValueField(stored=False)

    ruolo = indexes.FacetMultiValueField()
    natura = indexes.FacetMultiValueField(stored=False)
    tema = indexes.FacetMultiValueField(stored=False)

    costo = indexes.FacetFloatField()
    n_progetti = indexes.FacetIntegerField()

    def get_model(self):
        return Soggetto

    def index_queryset(self, using=None):
        return self.get_model().objects.select_related('territorio')

    def prepare_territorio_com(self, obj):
        if obj.territorio:
            return obj.territorio.cod_com

    def prepare_territorio_prov(self, obj):
        if obj.territorio:
            return obj.territorio.cod_prov

    def prepare_territorio_reg(self, obj):
        if obj.territorio:
            return obj.territorio.cod_reg

    def prepare_ruolo(self, obj):
        return [ruolo_cod for ruolo_cod, ruolo_desc in Ruolo.inv_ruoli_dict().items() if obj.ruoli.con_ruolo(ruolo_desc).exists()]

    def prepare_natura(self, obj):
        return list(ClassificazioneAzione.objects.principali().filter(classificazione_set__progetti__soggetti=obj, **Progetto.computabili_filter_dict('classificazione_set__progetti__')).values_list('codice', flat=True).distinct())

    def prepare_tema(self, obj):
        return list(TemaSintetico.objects.filter(progetti__soggetti=obj, **Progetto.computabili_filter_dict('progetti__')).values_list('codice', flat=True).distinct())

    def prepare_costo(self, obj):
        return Progetto.objects.computabili().del_soggetto(obj).totali()['totale_finanziamenti']

    def prepare_n_progetti(self, obj):
        return Progetto.objects.computabili().del_soggetto(obj).totali()['totale_progetti']
