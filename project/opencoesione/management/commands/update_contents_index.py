# -*- coding: utf-8 -*-
import logging
from django.core.management import call_command
from django.core.management.base import BaseCommand

from ...search_indexes import SEARCH_MODELS as CONTENTS_SEARCH_MODELS
from ...opendata.search_indexes import SEARCH_MODELS as OPENDATA_SEARCH_MODELS

SEARCH_MODELS = CONTENTS_SEARCH_MODELS + OPENDATA_SEARCH_MODELS


class Command(BaseCommand):
    """
    A BaseCommand to rebuild contents index.
    """
    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options['verbosity']
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        try:
            self.logger.info('Update indexes...')

            for model in SEARCH_MODELS:
                opts = model._meta
                call_command('update_index', '{}.{}'.format(opts.app_label, opts.model_name))
        except (KeyboardInterrupt, SystemExit):
            self.logger.info('Interrupted by the user.')
        else:
            self.logger.info('Done!')
