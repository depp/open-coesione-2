# -*- coding: utf-8 -*-
from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline
from django.contrib.flatpages.admin import FlatPageAdmin
# from django.contrib.flatpages.forms import FlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site
from django.forms import MultipleHiddenInput
from modeltranslation.admin import TabbedTranslationAdmin, TranslationTabularInline, TranslationStackedInline, TranslationGenericTabularInline
import nested_admin
from .models import *
from .utils import export_select_fields_csv_action
from ..tagging.admin import TagAdmin, TagInline
from ..tagging.models import Tag


# class CustomFlatpageAdminForm(FlatpageForm):
#     class Meta:
#         widgets = {
#             'content_it': CKEditorUploadingWidget(),
#             'content_en': CKEditorUploadingWidget(),
#             'extra_content_it': CKEditorUploadingWidget(),
#             'extra_content_en': CKEditorUploadingWidget(),
#             'sites': MultipleHiddenInput()
#         }


class StoriaTerritorioInlineForm(forms.ModelForm):
    def has_changed(self):
        return not self.instance.pk or super().has_changed()


class FileInline(TranslationGenericTabularInline):
    model = File
    exclude = ('category',)
    verbose_name = 'Documento'
    verbose_name_plural = 'Documenti'
    extra = 0


class LinkInline(TranslationGenericTabularInline):
    model = Link
    exclude = ('category',)
    verbose_name = 'Collegamento'
    verbose_name_plural = 'Collegamenti'
    extra = 0


class StoriaTerritorioTemaSinteticoCoesioneInline(nested_admin.NestedTabularInline):
    model = StoriaTerritorioTemaSinteticoCoesione
    verbose_name_plural = 'temi della coesione'
    extra = 0


class StoriaTerritorioTerritorioCoesioneInline(nested_admin.NestedTabularInline):
    model = StoriaTerritorioTerritorioCoesione
    verbose_name_plural = 'territori della coesione'
    extra = 0


class StoriaTerritorioInline(nested_admin.NestedGenericTabularInline):
    model = StoriaTerritorio
    form = StoriaTerritorioInlineForm
    verbose_name_plural = 'storia del territorio'
    min_num = 1
    max_num = 1
    inlines = [StoriaTerritorioTemaSinteticoCoesioneInline, StoriaTerritorioTerritorioCoesioneInline]

    # def has_add_permission(self, request):
    #     return False

    def has_delete_permission(self, request, obj=None):
        return False


class FixNestedInlineMixin(object):
    sortable_options = {
        'disabled': True,
    }


class Tag2Inline(FixNestedInlineMixin, TagInline):
    pass


class File2Inline(FixNestedInlineMixin, FileInline):
    pass


class ProjectStoryTextInline(FixNestedInlineMixin, TranslationStackedInline):
    model = ProjectStoryText
    extra = 0
    verbose_name = 'testo'
    verbose_name_plural = 'testi'

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'description':
            kwargs['widget'] = CKEditorUploadingWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class OpportunitaProgrammaGruppoInline(TranslationTabularInline):
    model = OpportunitaProgrammaGruppo
    extra = 0
    max_num = 2


class CommunicationContentReferenceInline(admin.TabularInline):
    model = CommunicationContentReference
    extra = 0
    verbose_name = 'riferimento'
    verbose_name_plural = 'riferimenti'


class MenuItemAdmin(TabbedTranslationAdmin):
    list_display = ('description', 'menu', 'priority', 'url')
    list_editable = ('priority',)


class CarouselItemAdmin(TabbedTranslationAdmin):
    list_display = ('title', 'overtitle', 'link', 'priority', 'visible')
    list_editable = ('priority', 'visible')
    list_filter = ('visible',)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'text':
            kwargs['widget'] = CKEditorWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class PillolaAdmin(nested_admin.NestedModelAdminMixin, TabbedTranslationAdmin):
    date_hierarchy = 'published_at'
    list_display = ('title', 'published_at')
    prepopulated_fields = {'slug': ('title',)}
    inlines = [File2Inline, Tag2Inline, StoriaTerritorioInline]

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'abstract':
            kwargs['widget'] = CKEditorWidget()
        elif db_field.name == 'description':
            kwargs['widget'] = CKEditorUploadingWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class VideoAdmin(TabbedTranslationAdmin):
    date_hierarchy = 'published_at'
    list_display = ('title', 'published_at')
    prepopulated_fields = {'slug': ('title',)}

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'abstract':
            kwargs['widget'] = CKEditorWidget()
        elif db_field.name == 'description':
            kwargs['widget'] = CKEditorUploadingWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class ASOCStoryAdmin(nested_admin.NestedModelAdminMixin, VideoAdmin):
    inlines = [StoriaTerritorioInline]


class VideoStoryAdmin(nested_admin.NestedModelAdminMixin, VideoAdmin):
    inlines = [StoriaTerritorioInline]


class ProjectStoryAdmin(nested_admin.NestedModelAdminMixin, VideoAdmin):
    inlines = [ProjectStoryTextInline, StoriaTerritorioInline]


class NewsAdmin(TabbedTranslationAdmin):
    date_hierarchy = 'published_at'
    list_display = ('title', 'published_at')
    prepopulated_fields = {'slug': ('title',)}
    inlines = [TagInline]

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'body':
            kwargs['widget'] = CKEditorUploadingWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class PressReviewAdmin(TabbedTranslationAdmin):
    date_hierarchy = 'published_at'
    list_display = ('title', 'source', 'author', 'published_at')


class ItemAdmin(TabbedTranslationAdmin):
    list_display = ('title', 'start_date', 'priority')
    list_editable = ('priority',)
    inlines = [FileInline]

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'body':
            kwargs['widget'] = CKEditorUploadingWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class FAQAdmin(TabbedTranslationAdmin):
    list_display = ('domanda', 'priorita', 'highlight')
    list_editable = ('priorita', 'highlight')
    list_filter = ('highlight',)
    prepopulated_fields = {'slug': ('domanda',)}
    inlines = [TagInline]

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'risposta':
            kwargs['widget'] = CKEditorWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)


class DataFileAdmin(TabbedTranslationAdmin):
    list_display = ('slug', 'name', 'file', 'updated_at')

    def get_readonly_fields(self, request, obj=None):
        if not request.user.is_superuser:
            return self.readonly_fields + ('slug',)
        return super().get_readonly_fields(request, obj)


class OpportunitaProgrammaAdmin(TabbedTranslationAdmin):
    list_display = ('nome', 'tipo', 'priorita')
    list_editable = ('priorita',)
    list_filter = ('tipo',)
    inlines = [OpportunitaProgrammaGruppoInline]

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'immagine':
            from .svg_image_form_field import SvgAndImageFormField
            kwargs['form_class'] = SvgAndImageFormField
        return super().formfield_for_dbfield(db_field, **kwargs)


class CommunicationContentAdmin(TabbedTranslationAdmin):
    date_hierarchy = 'created_at'
    list_display = ('title', 'type', 'status', 'administration', 'priority', 'created_at', 'updated_at')
    list_editable = ('priority',)
    list_filter = ('status', 'type')
    inlines = [CommunicationContentReferenceInline]

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields['type'].required = True
        return form


class ContactMessageAdmin(admin.ModelAdmin):
    date_hierarchy = 'sent_at'
    list_display = ('sender', 'email', 'organization', 'sent_at')
    actions = [
        export_select_fields_csv_action(
            'Esporta i selezionati in formato CSV',
            fields=[
                ('sender', 'Nome'),
                ('email', 'E-mail'),
                ('organization', 'Organizzazione'),
                ('location', 'Località'),
                ('reason', 'Motivo'),
                ('body', 'Messaggio'),
                ('sent_at', 'Data invio'),
            ],
            header=True
        ),
    ]


class CustomTagAdmin(TagAdmin, TabbedTranslationAdmin):
    pass


class CustomFlatPageAdmin(FlatPageAdmin, TabbedTranslationAdmin):
    # form = CustomFlatpageAdminForm
    list_filter = []

    def get_list_display(self, request):
        list_display = super().get_list_display(request)
        if request.user.is_superuser:
            list_display += ('template_name',)
        return list_display

    def get_fieldsets(self, request, obj=None):
        fields = ('url', 'title', 'content', 'extra_content', 'sites')
        if request.user.is_superuser:
            fields += ('template_name',)
        self.fieldsets = ((None, {'fields': fields}),)
        return super().get_fieldsets(request, obj)

    def get_readonly_fields(self, request, obj=None):
        if not request.user.is_superuser and obj:
            return self.readonly_fields + ('url',)
        return super().get_readonly_fields(request, obj)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in ('content', 'extra_content'):
            kwargs['widget'] = CKEditorUploadingWidget()
        return super().formfield_for_dbfield(db_field, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'sites':
            kwargs['widget'] = MultipleHiddenInput()
            kwargs['initial'] = [Site.objects.get_current()]
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(MenuItem, MenuItemAdmin)
admin.site.register(CarouselItem, CarouselItemAdmin)
admin.site.register(Pillola, PillolaAdmin)
admin.site.register(ASOCStory, ASOCStoryAdmin)
admin.site.register(VideoStory, VideoStoryAdmin)
admin.site.register(ProjectStory, ProjectStoryAdmin)
admin.site.register(VideoTutorial, VideoAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(PressReview, PressReviewAdmin)
admin.site.register(Document, ItemAdmin)
admin.site.register(Event, ItemAdmin)
admin.site.register(EventVideo, ItemAdmin)
admin.site.register(Reuse, ItemAdmin)
admin.site.register(Workshop, ItemAdmin)
admin.site.register(FAQ, FAQAdmin)
admin.site.register(DataFile, DataFileAdmin)
admin.site.register(OpportunitaProgramma, OpportunitaProgrammaAdmin)
admin.site.register(CommunicationContent, CommunicationContentAdmin)
admin.site.register(ContactMessage, ContactMessageAdmin)
admin.site.unregister(Tag)
admin.site.register(Tag, CustomTagAdmin)
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, CustomFlatPageAdmin)
admin.site.unregister(Site)
