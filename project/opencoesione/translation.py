# -*- coding: utf-8 -*-
from django.contrib.flatpages.models import FlatPage
from modeltranslation.translator import register, TranslationOptions
from .models import (
    File, Link, MenuItem, CarouselItem, Pillola, News, PressReview, Event, FAQ, DataFile, Reuse,
    Workshop, Document, EventVideo, OpportunitaProgramma, OpportunitaProgrammaGruppo, ASOCStory, VideoStory,
    VideoTutorial, ProjectStory, ProjectStoryText, CommunicationContent
)
from ..tagging.models import Tag


@register(File)
class FileTranslationOptions(TranslationOptions):
    fields = ('description', 'file')


@register(Link)
class LinkTranslationOptions(TranslationOptions):
    fields = ('description',)


@register(MenuItem)
class MenuItemTranslationOptions(TranslationOptions):
    fields = ('description',)


@register(CarouselItem)
class CarouselItemTranslationOptions(TranslationOptions):
    fields = ('title', 'overtitle', 'text', 'link', 'image')


@register(Pillola)
class PillolaTranslationOptions(TranslationOptions):
    fields = ('title', 'abstract', 'description', 'image')


@register(ASOCStory)
@register(VideoStory)
@register(VideoTutorial)
@register(ProjectStory)
class VideoTranslationOptions(TranslationOptions):
    fields = ('title', 'abstract', 'description', 'video')


@register(ProjectStoryText)
class ProjectStoryTextTranslationOptions(TranslationOptions):
    fields = ('title', 'description')


@register(News)
class NewsTranslationOptions(TranslationOptions):
    fields = ('title', 'body')


@register(PressReview)
class PressReviewTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(Document)
@register(Event)
@register(EventVideo)
@register(Reuse)
@register(Workshop)
class ItemTranslationOptions(TranslationOptions):
    fields = ('title', 'body', 'location')


@register(FAQ)
class FAQTranslationOptions(TranslationOptions):
    fields = ('domanda', 'risposta')


@register(DataFile)
class DataFileTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(OpportunitaProgramma)
@register(OpportunitaProgrammaGruppo)
class OpportunitaProgrammaTranslationOptions(TranslationOptions):
    fields = ('nome',)


@register(CommunicationContent)
class CommunicationContentTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'url')


@register(Tag)
class TagTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(FlatPage)
class FlatPageTranslationOptions(TranslationOptions):
    fields = ('title', 'content', 'extra_content')
