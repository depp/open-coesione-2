# -*- coding: utf-8 -*-
import csv
import os
from _csv import register_dialect, QUOTE_ALL, QUOTE_MINIMAL
from django.http import HttpResponse


class excel_semicolon(csv.excel):
    """Extends excel Dialect in order to set semicolon as delimiter."""
    delimiter = ';'
    quoting = QUOTE_ALL
register_dialect('excel-semicolon', excel_semicolon)


class excel_semicolon_minimal(csv.excel):
    """Extends excel Dialect in order to set semicolon as delimiter."""
    delimiter = ';'
    quoting = QUOTE_MINIMAL
register_dialect('excel-semicolon-minimal', excel_semicolon_minimal)


def export_select_fields_csv_action(description='Export selected objects as CSV file', fields=None, exclude=None, header=True):
    """
    This function returns an export csv action

    'fields' is a list of tuples denoting the field and label to be exported. Labels
    make up the header row of the exported file if header=True.

        fields=[
                ('field1', 'label1'),
                ('field2', 'label2'),
                ('field3', 'label3'),
            ]
    You can use the ORM lookup '__' syntax to access related fields,
    but the existance of the field is not checked in advance.
    Errors are trapped in this case and '' is returned as value.

    'exclude' is a flat list of fields to exclude. If 'exclude' is passed,
    'fields' will not be used. Either use 'fields' or 'exclude.'

        exclude=['field1', 'field2', field3]

    'header' is whether or not to output the column names as the first row

    Based on: http://djangosnippets.org/snippets/2020/
    """
    def extended_getattr(obj, attribute_name):
        if obj is None:
            return ''

        if '__' in attribute_name:
            (a1, a2) = attribute_name.split('__', 1)
            try:
                o = getattr(obj, a1)
            except AttributeError:
                o = None
            return extended_getattr(o, a2)
        else:
            try:
                return getattr(obj, attribute_name)
            except AttributeError:
                return ''

    def export_as_csv(modeladmin, request, queryset):
        """
        Generic csv export admin action.
        based on http://djangosnippets.org/snippets/1697/
        """
        opts = modeladmin.model._meta
        standard_field_names = [field.name for field in opts.fields]

        labels = []

        if exclude:
            field_names = [v for v in standard_field_names if v not in exclude]
        elif fields:
            field_names = [k for k, v in fields if k in standard_field_names or '__' in k]
            labels = [v for k, v in fields if k in standard_field_names or '__' in k]
        else:
            field_names = standard_field_names

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(str(opts).replace('.', '_'))

        writer = csv.writer(response, dialect='excel-semicolon')

        if header:
            if labels:
                writer.writerow(labels)
            else:
                writer.writerow(field_names)

        for obj in queryset:
            writer.writerow([extended_getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = description
    return export_as_csv


def generate_ckeditor_filename(upload_name):
    from ckeditor_uploader.utils import storage, slugify_filename
    from django.conf import settings

    if getattr(settings, 'CKEDITOR_UPLOAD_SLUGIFY_FILENAME', True):
        upload_name = slugify_filename(upload_name)

    name =  os.path.join(settings.CKEDITOR_UPLOAD_PATH, upload_name)
    if storage.exists(name):
        storage.delete(name)

    return upload_name
