# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse
from itertools import groupby
from .models import Indicatore, Ripartizione, IndicatoreRegionale


def indicatori(request):
    data = {
        'locations': {
            str(ripartizione.id): {
                'name': ripartizione.descrizione,
            } for ripartizione in Ripartizione.objects.with_value().order_by('id')
        },
        'topics': {
            tema.codice: {
                'name': tema.descrizione_breve,
                'indicators': {
                    indicatore.codice: {
                        'title': indicatore.titolo,
                        'subtitle': indicatore.sottotitolo,
                    } for indicatore in indicatori
                }
            } for tema, indicatori in groupby(Indicatore.objects.with_value().order_by('tema__priorita', 'codice').select_related('tema'), key=lambda x: x.tema)
        },
    }

    return HttpResponse(json.dumps(data), content_type='application/json')


def indicatori_regionali(request, indicatore_id):
    data = {
        ripartizione: [{
            'year': int(indicatore_regionale.anno),
            'value': float(indicatore_regionale.valore),
        } for indicatore_regionale in indicatori_regionali] for ripartizione, indicatori_regionali in groupby(IndicatoreRegionale.objects.filter(indicatore=indicatore_id).exclude(valore='').order_by('indicatore', 'ripartizione', 'anno'), key=lambda x: x.ripartizione_id)
    }

    return HttpResponse(json.dumps(data), content_type='application/json')
