# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import indicatori, indicatori_regionali


urlpatterns = [
    url(r'^indicatori/$', indicatori, name='indicatori'),
    url(r'^indicatori-regionali/(?P<indicatore_id>\d{3})/$', indicatori_regionali, name='indicatori_regionali'),
]
