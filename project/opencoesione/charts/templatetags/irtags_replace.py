# -*- coding: utf-8 -*-
import re
from django import template
from django.contrib.humanize.templatetags.humanize import intcomma
from django.core.cache import cache
from itertools import groupby
from ..models import IndicatoreRegionale

register = template.Library()


def get_data_from_db():
    def get_first_last(lst):
        return {'first': lst[0], 'last': lst[-1]}

    data = {
        int(indicatore): {
            int(ripartizione): get_first_last([{
                'year': int(indicatore_regionale.anno),
                'value': round(float(indicatore_regionale.valore), 1),
            } for indicatore_regionale in indicatori_regionali]) for ripartizione, indicatori_regionali in groupby(ripartizioni, key=lambda x: x.ripartizione_id)
        } for indicatore, ripartizioni in groupby(IndicatoreRegionale.objects.exclude(valore='').order_by('indicatore', 'ripartizione', 'anno'), key=lambda x: x.indicatore_id)
    }

    return data


@register.filter
def irtags_replace(text):
    def get_repl(match_obj):
        if match_obj.group() is not None:
            repl = data.get(int(match_obj.group(2)), {}).get(int(match_obj.group(3)), {}).get(match_obj.group(5), {}).get(match_obj.group(4))
            if repl is None:
                return ''
            elif isinstance(repl, float):
                return intcomma(repl)
            else:
                return str(repl)

    data = cache.get_or_set('indicatoriregionali', get_data_from_db, timeout=3600)

    return re.sub(r'(\${\s*(\d{1,3})\.(\d{1,2})\.(value|year)\.(first|last)\s*})', get_repl, text)
