# -*- coding: utf-8 -*-
from django import template
from django.conf import settings
from ...monitoraggio.models import TemaSintetico

register = template.Library()


@register.inclusion_tag('templatetags/charts/indicatori_tematici.html', takes_context=True)
def indicatori_tematici(context, tema=None, territorio=None):
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')

    if not territorio:
        location_id = 0
    elif territorio.is_macroarea:
        location_id = territorio.cod_mac + 28
    else:
        location_id = territorio.cod_reg

    return {
        'temi': [tema] if tema else TemaSintetico.objects.all(),
        'location_id': location_id,
        sezikai_ctx_var: context[sezikai_ctx_var],
    }
