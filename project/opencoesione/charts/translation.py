# -*- coding: utf-8 -*-
from modeltranslation.translator import register, TranslationOptions
from .models import Indicatore


@register(Indicatore)
class IndicatoreTranslationOptions(TranslationOptions):
    fields = ('titolo', 'sottotitolo')
