# -*- coding: utf-8 -*-
import logging
import ssl

import pandas as pd
import re
import urllib.request as urllib2
import zipfile
from django.conf import settings
from django.core.management import BaseCommand, CommandError
from django.db import transaction
from io import BytesIO
from ...models import Indicatore, IndicatoreRegionale, Ripartizione
from ....monitoraggio.models import TemaSintetico


# ISTAT resource as URL
ISTAT_ARCHIVE_FILE_PATH = 'https://www.istat.it/storage/politiche-sviluppo/Archivio_unico_indicatori_regionali.zip'
ISTAT_FILE_NAME = 'Archivio_unico_indicatori_regionali.csv'
ISTAT_FILE_ENCODING = 'utf-8-sig'
# ISTAT_FILE_ENCODING = 'latin1'

# csv fields
CSV_CODE = 'COD_INDICATORE'
CSV_TOPIC = 'OC_TEMA_SINTETICO'
CSV_LOCATION = 'ID_RIPARTIZIONE'
CSV_LOCATION_DESCRIPTION = 'DESCRIZIONE_RIPARTIZIONE'
CSV_YEAR = 'ANNO_RIFERIMENTO'
CSV_VALUE = 'VALORE'
CSV_TITLE = 'TITOLO'
CSV_SUBTITLE = 'SOTTOTITOLO'

CSV_REQUIRED_COLUMNS = (CSV_CODE, CSV_TITLE, CSV_SUBTITLE, CSV_LOCATION, CSV_LOCATION_DESCRIPTION, CSV_YEAR, CSV_VALUE, CSV_TOPIC)

# elaboration helpers
VALID_INDEXES = list(settings.INDICATORI_VALIDI.keys())
VALID_REGIONS = list(range(1, 21)) + [23, 29, 30]

INDEX_TO_TOPIC = settings.INDICATORI_VALIDI

CURRENT = str(settings.ROOT_PATH.path('.current_istat_zip'))  # keeps info on the latest istat archive processed


def convert_topic(topic):
    topic_map = {
        'Rinnovamento urbano e rurale': 'Rinnovamento urbano  e rurale',
        'Rafforzamento delle capacità della PA': 'Rafforzamento capacità della PA',
    }

    topic = topic.strip()
    return topic_map[topic] if topic in topic_map else topic


def convert_value(value):
    # clean string
    value = re.sub(r'[^\d,.]', '', value).strip(',.')
    if value:
        # split for avoid thousand separator and different locale comma/dot symbol
        parts = re.split(r'[,.]', value)
        if len(parts) == 1:
            float_str = parts[0]
        else:
            float_str = '{}.{}'.format(''.join(parts[0:-1]), parts[-1])
        value = float(float_str)
        value = str(value)

    return value


class Command(BaseCommand):
    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument('--force-update', dest='forceupdate', default=False, action='store_true', help='Force extraction of archive')

    def handle(self, *args, **options):
        verbosity = options['verbosity']
        if verbosity == '0':
            self.logger.setLevel(logging.ERROR)
        elif verbosity == '1':
            self.logger.setLevel(logging.WARNING)
        elif verbosity == '2':
            self.logger.setLevel(logging.INFO)
        elif verbosity == '3':
            self.logger.setLevel(logging.DEBUG)

        try:
            request = urllib2.Request(ISTAT_ARCHIVE_FILE_PATH)

            if not options.get('forceupdate'):
                request.add_header('If-None-Match', self.stored_etag)

            archivefile = urllib2.urlopen(request, context=ssl._create_unverified_context())
        except (urllib2.HTTPError, Exception) as e:
            if isinstance(e, urllib2.HTTPError) and e.code == 304:
                self.logger.debug('Archivio già elaborato. Nessun aggiornamento disponibile. Usare --force-update per rielaborare')
            else:
                self.logger.error(e)
        else:
            try:
                import datetime
                from email.utils import parsedate
                date = datetime.datetime(*parsedate(archivefile.headers['Last-Modified'])[:6])
            except Exception as e:
                self.logger.error(e)
            else:
                from ....opendata.models import RemoteDataset
                RemoteDataset.objects.filter(slug='istat').update(updated_at=date)

            try:
                self.process_archive(archivefile)
            except Exception as e:
                self.logger.error(e)
            else:
                self.stored_etag = archivefile.headers['etag']

    def process_archive(self, archivefile):
        self.logger.debug('Elaborazione archivio: {0}'.format(archivefile.url))

        buffer = BytesIO(archivefile.read())
        zfile = zipfile.ZipFile(buffer)
        # csv_stream = zfile.read(ISTAT_FILE_NAME)
        csv_stream = zfile.read([filename for filename in zfile.namelist() if filename.endswith('.csv')][0])
        self.split_csv(csv_stream)
        zfile.close()

    def split_csv(self, csv_stream):
        df = pd.read_csv(
            BytesIO(csv_stream),
            sep=';',
            # sep='\t',
            header=0,
            low_memory=True,
            dtype=object,
            encoding=ISTAT_FILE_ENCODING,
            keep_default_na=False,
            converters={
                CSV_CODE: lambda x: x.strip().zfill(3),
                CSV_LOCATION: lambda x: int(x.strip()),
                CSV_TOPIC: convert_topic,
                CSV_VALUE: convert_value,
                CSV_YEAR: lambda x: int(x.strip()),
            },
        )

        # check if is valid headers
        headers_diff = set(CSV_REQUIRED_COLUMNS).difference(set(df))
        if headers_diff:
            raise CommandError('Formato colonne non valido. Le colonne mancanti sono: {}'.format(list(headers_diff)))

        # tema_desc2obj = {o.descrizione: o for o in TemaSintetico.objects.all()}
        tema_cod2obj = {o.codice: o for o in TemaSintetico.objects.all()}

        df_i = df[[CSV_CODE, CSV_TITLE, CSV_SUBTITLE, CSV_TOPIC]].drop_duplicates()
        df_i_count = len(df_i)

        for n, (index, row) in enumerate(df_i.iterrows(), 1):
            # obj = self.get_and_update_or_create('{}/{}'.format(n, df_i_count), Indicatore, codice=row[CSV_CODE], defaults={'titolo': row[CSV_TITLE], 'sottotitolo': row[CSV_SUBTITLE], 'tema': tema_desc2obj[row[CSV_TOPIC]]})
            obj = self.get_and_update_or_create('{}/{}'.format(n, df_i_count), Indicatore, codice=row[CSV_CODE], defaults={'titolo': row[CSV_TITLE], 'sottotitolo': row[CSV_SUBTITLE], 'tema': tema_cod2obj.get(INDEX_TO_TOPIC.get(row[CSV_CODE]))})

        transaction.commit()

        df_r = df[[CSV_LOCATION, CSV_LOCATION_DESCRIPTION]].drop_duplicates()
        df_r_count = len(df_r)

        for n, (index, row) in enumerate(df_r.iterrows(), 1):
            obj = self.get_and_update_or_create('{}/{}'.format(n, df_r_count), Ripartizione, id=row[CSV_LOCATION], defaults={'descrizione': row[CSV_LOCATION_DESCRIPTION]})

        transaction.commit()

        IndicatoreRegionale.objects.update(da_eliminare=True)
        transaction.commit()

        # df = df[df[CSV_CODE].isin(VALID_INDEXES) & df[CSV_LOCATION].isin(VALID_REGIONS) & df[CSV_TOPIC].isin(tema_desc2obj)]
        df = df[df[CSV_CODE].isin(VALID_INDEXES) & df[CSV_LOCATION].isin(VALID_REGIONS)]
        df_count = len(df)

        transaction.set_autocommit(False)

        for n, (index, row) in enumerate(df.iterrows(), 1):
            obj = self.get_and_update_or_create('{}/{}'.format(n, df_count), IndicatoreRegionale, indicatore_id=row[CSV_CODE], ripartizione_id=row[CSV_LOCATION], anno=row[CSV_YEAR], defaults={'valore': row[CSV_VALUE]})

            obj.da_eliminare = False
            obj.save()

            if (n % 5000 == 0) or (n == df_count):
                transaction.commit()

        da_eliminare = IndicatoreRegionale.objects.filter(da_eliminare=True)
        da_eliminare_count = len(da_eliminare)

        for n, obj in enumerate(da_eliminare, 1):
            obj.delete()
            self.logger.info('{}/{} - Eliminato {}: {}'.format(n, da_eliminare_count, IndicatoreRegionale._meta.verbose_name_raw, obj))

        transaction.commit()

        transaction.set_autocommit(True)

    @property
    def stored_etag(self):
        try:
            with open(CURRENT, 'r') as current:
                value = current.read()
        except EnvironmentError:
            value = ''

        return value

    @stored_etag.setter
    def stored_etag(self, value):
        with open(CURRENT, 'w') as current:
            current.write(value)

    def get_and_update_or_create(self, baselog, model, **kwargs):
        obj, created = model.objects.get_or_create(**kwargs)

        defaults = kwargs.get('defaults', {})
        if created:
            self.logger.info('{} - Creato {}: {}'.format(baselog, model._meta.verbose_name_raw, obj))
        elif all([getattr(obj, k) == v for k, v in defaults.items()]):
            self.logger.debug('{} - Trovato {}: {}'.format(baselog, model._meta.verbose_name_raw, obj))
        else:
            obj_old = str(obj)
            for k, v in defaults.items():
                setattr(obj, k, v)
            obj.save()
            self.logger.info('{} - Modificato {}: {} -> {}'.format(baselog, model._meta.verbose_name_raw, obj_old, obj))

        return obj
