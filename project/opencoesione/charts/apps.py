# -*- coding: utf-8 -*-
from django.apps import AppConfig


class MonitoraggioConfig(AppConfig):
    name = 'project.opencoesione.charts'
