# -*- coding: utf-8 -*-
from django.db import models
from ..monitoraggio.models import TemaSintetico


class ChartsQuerySet(models.QuerySet):
    def with_value(self):
        return self.filter(indicatori_regionali__isnull=False).distinct()


class Indicatore(models.Model):
    codice = models.CharField(max_length=3, primary_key=True)
    titolo = models.CharField(max_length=150)
    sottotitolo = models.CharField(max_length=500)
    tema = models.ForeignKey(TemaSintetico, related_name='indicatori', null=True, on_delete=models.CASCADE)

    objects = ChartsQuerySet.as_manager()

    def __str__(self):
        return '{} - [{}] {} ({})'.format(self.tema.descrizione if self.tema else '(senza tema sintetico)', self.codice, self.titolo, self.sottotitolo)


class Ripartizione(models.Model):
    id = models.PositiveSmallIntegerField(primary_key=True)
    descrizione = models.CharField(max_length=100)

    objects = ChartsQuerySet.as_manager()

    def __str__(self):
        return '[{}] {}'.format(self.id, self.descrizione)


class IndicatoreRegionale(models.Model):
    indicatore = models.ForeignKey(Indicatore, related_name='indicatori_regionali')
    ripartizione = models.ForeignKey(Ripartizione, related_name='indicatori_regionali')
    anno = models.CharField(max_length=4)
    valore = models.CharField(max_length=20)
    da_eliminare = models.BooleanField(default=False, db_index=True)

    def __str__(self):
        return '{} / {} / {}: {}'.format(self.indicatore, self.ripartizione, self.anno, self.valore)

    class Meta:
        unique_together = ('indicatore', 'ripartizione', 'anno')
        index_together = [
            ['indicatore', 'ripartizione', 'anno'],
        ]
