# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.flatpages.models import FlatPage
from django.template.loader import render_to_string
from django.utils import translation
from haystack import indexes

from .models import ASOCStory, FAQ, News, Pillola, ProjectStory, VideoStory, VideoTutorial
from .monitoraggio.models import TemaSinteticoCoesione, TerritorioCoesione

SEARCH_MODELS = (
    ASOCStory, FAQ, FlatPage, News, Pillola, ProjectStory, TemaSinteticoCoesione, TerritorioCoesione, VideoStory,
    VideoTutorial,
)


class ModeltranslationIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, stored=False)
    text_it = indexes.CharField(stored=False)
    text_en = indexes.CharField(stored=False)

    def prepare(self, obj):
        self.prepared_data = super().prepare(obj)

        opts = self.get_model()._meta
        template_name = 'search/indexes/{}/{}_text.txt'.format(opts.app_label, opts.model_name)

        self.prepared_data['text'] = render_to_string(template_name, {'object': obj})
        for language, _ in settings.LANGUAGES:
            with translation.override(language):
                self.prepared_data['text_{}'.format(language)] = render_to_string(template_name, {'object': obj})

        return self.prepared_data


class BaseIndex(ModeltranslationIndex):
    type = indexes.FacetCharField(stored=False)

    published_at = indexes.DateField(stored=False)

    def prepare(self, obj):
        self.prepared_data = super().prepare(obj)

        opts = self.get_model()._meta

        self.prepared_data['type'] = opts.model_name

        self.prepared_data['published_at'] = getattr(obj, 'published_at', None)

        return self.prepared_data


class ASOCStoryIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return ASOCStory


class FAQIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return FAQ


class FlatPageIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return FlatPage


class NewsIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return News


class PillolaIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return Pillola


class ProjectStoryIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return ProjectStory

    def index_queryset(self, using=None):
        return self.get_model().objects.prefetch_related('texts')


class TemaSinteticoCoesioneIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return TemaSinteticoCoesione

    def index_queryset(self, using=None):
        return self.get_model().objects.prefetch_related('indicatori_sintesi')


class TerritorioCoesioneIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return TerritorioCoesione

    def index_queryset(self, using=None):
        return self.get_model().objects.select_related('territorio')


class VideoStoryIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return VideoStory


class VideoTutorialIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return VideoTutorial
