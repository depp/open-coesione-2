# -*- coding: utf-8 -*-
"""
Base settings for OpenCoesione project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
from django.http import HttpResponsePermanentRedirect
from django.middleware.locale import LocaleMiddleware
from django.templatetags.static import static
from django.utils.functional import lazy
from django.utils.text import format_lazy
from django.utils.translation import ugettext_lazy as _
from model_utils import Choices
import environ
import os
from pathlib import Path

# PATH CONFIGURATION
# Build paths inside the project like this: os.path.join(ROOT_PATH, ...)
ROOT_PATH = environ.Path(__file__) - 2  # (oc2/project/settings.py - 2 = oc2/)
PROJECT_NAME = 'OpenCoesione'
PROJECT_PACKAGE = 'project' or PROJECT_NAME
PROJECT_PATH = ROOT_PATH.path(PROJECT_PACKAGE)
APPS_PATH = PROJECT_PATH
CONFIG_PATH = ROOT_PATH.path('config')
RESOURCES_PATH = ROOT_PATH.path('resources')

LOCALE_PATHS = [
    str(PROJECT_PATH.path('locale')),
]

# Add our project to our python path, this way we don't need to type our project
# name in our dotted import paths:
# os.path.append(PROJECT_PATH)
# END PATH CONFIGURATION

env = environ.Env()

# .env file, should load only in development environment
# Operating System Environment variables have precedence over variables defined in the .env file,
# that is to say variables from the .env files will only be used if not defined
# as environment variables.
READ_DOT_ENV_FILE = env.bool('DJANGO_READ_DOT_ENV_FILE', default=True)

if READ_DOT_ENV_FILE:
    dot_env_path = str(CONFIG_PATH.path('.env'))
    dot_env_file = Path(dot_env_path)
    if dot_env_file.is_file():
        env.read_env(str(dot_env_file))

# DEBUG CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DEBUG', False)
DEBUG_TOOLBAR = env.bool('DEBUG_TOOLBAR', DEBUG)
# END DEBUG CONFIGURATION


# MANAGER CONFIGURATION
ADMIN_EMAIL = env('ADMIN_EMAIL', default='admin@{project_name}.com'.format(project_name=PROJECT_NAME))
ADMIN_NAME = env('ADMIN_NAME', default=ADMIN_EMAIL.split('@')[0])

# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    (ADMIN_NAME, ADMIN_EMAIL),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# See: https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL', default=ADMIN_EMAIL)
# END MANAGER CONFIGURATION


# DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': env.db(default='sqlite://:memory:')
}
# END DATABASE CONFIGURATION


# GENERAL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = 'Europe/Rome'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'it'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

USE_THOUSAND_SEPARATOR = True

LANGUAGES = [
    ('it', 'Italiano'),
    ('en', 'English'),
]
# END GENERAL CONFIGURATION


# MEDIA CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = env('MEDIA_ROOT', default=str(RESOURCES_PATH.path('media')))
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'
FILE_UPLOAD_MAX_MEMORY_SIZE = 1024 * 1024 * 50  # 50 MB
FILE_UPLOAD_PERMISSIONS = 0o644
# END MEDIA CONFIGURATION


# STATIC FILE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = env('STATIC_ROOT', default=str(RESOURCES_PATH.path('static')))
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    str(PROJECT_PATH.path('static')),
)
# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
# END STATIC FILE CONFIGURATION


# SECRET CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# SECURITY WARNING: keep the secret key used in production secret!
# Note: This not-so-secret-key should only be used for development and testing.
SECRET_KEY = env('SECRET_KEY', default='not-so-secret')
# END SECRET CONFIGURATION


# SITE CONFIGURATION
# Hosts/domain names that are valid for this site
# See https://docs.djangoproject.com/en/1.11/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default='*')
# END SITE CONFIGURATION


# FIXTURE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (str(RESOURCES_PATH.path('fixtures')),)
# END FIXTURE CONFIGURATION


# TEMPLATE CONFIGURATION
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            str(APPS_PATH.path('templates')),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',

                'sekizai.context_processors.sekizai',

                'project.context_processor.main_settings',
            ],
        },
    },
]
# END TEMPLATE CONFIGURATION


########## MIDDLEWARE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#middleware-classes
MIDDLEWARE = (
    # Default Django middleware.
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    'project.middleware.AdvancedXFrameOptionsMiddleware',
#    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'project.middleware.force_default_language_middleware',

    'django.middleware.locale.LocaleMiddleware',
)
########## END MIDDLEWARE CONFIGURATION


# URL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = 'project.urls'
# END URL CONFIGURATION


# APP CONFIGURATION
DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Useful template tags:
    'django.contrib.humanize',

    # Django helper
    'django_extensions',

    'django.contrib.flatpages',
)

# Apps specific for this project go here.
LOCAL_APPS = (
    'rest_framework',
    'corsheaders',
    'haystack',
    'modeltranslation',
    'ckeditor',
    'ckeditor_uploader',
    'sekizai',
    'honeypot',
    'embed_video',
    'nested_admin',
    # 'nocaptcha_recaptcha',

    'project.enhancedsearch',
    'project.opencoesione',
    'project.opencoesione.charts',
    'project.opencoesione.monitoraggio',
    'project.opencoesione.opendata',
    'project.tagging',
    'project.urlshortener',

    # Admin panel and documentation:
    'django.contrib.admin',
    # 'django.contrib.admindocs',

    'taskmanager',
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS
# END APP CONFIGURATION


# AUTHENTICATION CONFIGURATION
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)
# END AUTHENTICATION CONFIGURATION


# LOGGING CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGS_PATH = env(
    'LOGS_PATH',
    default=os.path.normpath(str(RESOURCES_PATH))
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s',
            'datefmt': '%d/%b/%Y %H:%M:%S'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.normpath(
                os.path.join(LOGS_PATH, 'oc2.log')
            ),
            'maxBytes': 1024 * 1024 * 10,  # 10 MB
            'backupCount': 7,
            'formatter': 'verbose',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
    },
    'loggers': {
        'django': {
            'handlers': [],
            # 'handlers': ['file', 'console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'project': {
            'handlers': ['file', 'console'],
            'level': 'INFO',
            'propagate': True,
        }
    },
}
# END LOGGING CONFIGURATION


# TOOLBAR CONFIGURATION
# See: http://django-debug-toolbar.readthedocs.org/en/latest/installation.html#explicit-setup
if DEBUG_TOOLBAR:
    INSTALLED_APPS += (
        'debug_toolbar',
    )

    MIDDLEWARE += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )

    DEBUG_TOOLBAR_PATCH_SETTINGS = False

    # http://django-debug-toolbar.readthedocs.org/en/latest/installation.html
    INTERNAL_IPS = env(
        'DEBUG_TOOLBAR_INTERNAL_IPS',
        default='127.0.0.1'
    ).split(',')
# END TOOLBAR CONFIGURATION


# WSGI CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'wsgi.application'
# END WSGI CONFIGURATION


# TESTING CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/releases/1.6/#new-test-runner
TEST_RUNNER = 'django.test.runner.DiscoverRunner'
# END TESTING CONFIGURATION


# CACHES CONFIGURATION
###### ##### CACHES CONFIGURATION
if env.bool('USE_REDIS_CACHE', False):
    CACHES = {
        'default': {
            'BACKEND': 'django_redis.cache.RedisCache',
            'LOCATION': env('REDIS_URL', default='redis://redis:6379/0'),
            'OPTIONS': {
                'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            },
            'TIMEOUT': None
        }
    }
    SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
    SESSION_CACHE_ALIAS = 'default'
else:
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        }
    }

# CACHES = {
#     'default': env.cache(default='locmemcache://'),
# }
CACHE_PAGE_DURATION_SECS = 86400 * 365
# END CACHE CONFIGURATION


# SESSION CONFIGURATION
# SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
# SESSION_CACHE_ALIAS = 'default'
# END SESSION CONFIGURATION

LOGIN_URL = '/admin/login/'

# PASSWORD VALIDATION CONFIGURATION
# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
# END PASSWORD VALIDATION CONFIGURATION


########## HAYSTACK CONFIGURATION
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': env(
            'SOLR_URL',
            default='http://localhost:8080/solr/open_coesione_2'
        ),
        'TIMEOUT': 60 * 5,
        'INCLUDE_SPELLING': True,
        'BATCH_SIZE': 100,
    },
}
HAYSTACK_SEARCH_RESULTS_PER_PAGE = 50
HAYSTACK_SIGNAL_PROCESSOR = 'project.opencoesione.search_utils.MyRealtimeSignalProcessor'
########## END HAYSTACK CONFIGURATION


########## MODELTRANSLATION CONFIGURATION
MODELTRANSLATION_FALLBACK_LANGUAGES = ('it', 'en')
########## END MODELTRANSLATION CONFIGURATION


########## CKEDITOR CONFIGURATION
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar_OC': [
            ['Styles', 'Bold', 'Italic', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],
            ['Link', 'Unlink', 'Anchor'],
            ['Image', 'Table', 'HorizontalRule', 'SpecialChar', 'Iframe', 'Blockquote'],
            ['Source'], ['Maximize'],
        ],
        'toolbar': 'OC',
        'extraPlugins': ','.join(
            [
                'image2',
            ]
        ),
        'image2_alignClasses': ['image-left', 'image-center', 'image-right'],
        'stylesSet': [
            {
                'name': 'Blocco titolo',
                'element': 'div',
                'attributes': {
                    'class': 'title',
                },
            },
            {
                'name': 'Blocco dataset',
                'element': 'div',
                'attributes': {
                    'class': 'dataset',
                },
            },
            # {
            #     'name': 'Testo sottolineato',
            #     'element': 'span',
            #     'styles': {
            #         'text-decoration': 'underline',
            #     }
            # },
            # {
            #     'name': 'Testo cancellato',
            #     'element': 'span',
            #     'styles': {
            #         'text-decoration': 'line-through',
            #     }
            # },
            {
                'name': 'Citazione in rilievo a sinistra',
                'element': 'blockquote',
                'attributes': {
                    'class': 'pull-quote-left',
                    'role': 'presentation',
                    'aria-hidden': 'true',
                },
            },
            {
                'name': 'Citazione in rilievo a destra',
                'element': 'blockquote',
                'attributes': {
                    'class': 'pull-quote-right',
                    'role': 'presentation',
                    'aria-hidden': 'true',
                },
            },
        ],
        'extraAllowedContent': 'div(dataset,title);blockquote(pull-quote-left,pull-quote-right)',
        # 'allowedContent': {
        #     '$1': {
        #         'styles': False,
        #     },
        # },
        'disallowedContent': 'script; *[on*]; span; font',
        'contentsCss': lazy(static, str)('css/ckeditor.css'),
    },
}
CKEDITOR_UPLOAD_PATH = 'uploads/'
CKEDITOR_BROWSE_SHOW_DIRS = True
CKEDITOR_RESTRICT_BY_DATE = False
CKEDITOR_FILENAME_GENERATOR = 'project.opencoesione.utils.generate_ckeditor_filename'
########## END CKEDITOR CONFIGURATION


########## reCAPTCHA CONFIGURATION
# This is your public and private API keys as provided by reCAPTCHA
# NORECAPTCHA_SITE_KEY = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
# NORECAPTCHA_SECRET_KEY = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
########## END reCAPTCHA CONFIGURATION


########## REST_FRAMEWORK CONFIGURATION
REST_FRAMEWORK = {
    'DATE_FORMAT': '%Y%m%d',
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        # 'rest_framework.renderers.JSONPRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.AnonRateThrottle',
        'rest_framework.throttling.UserRateThrottle'
    ),
    'DEFAULT_THROTTLE_RATES': {
        'anon': '12/minute',  # 1 req every 5 seconds
        'user': '2/second',  # un-throttled  (default: '1/second')
    },
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 25,
}
########## END REST_FRAMEWORK CONFIGURATION

########## X_FRAME_OPTIONS CONFIGURATION for ascuoladiopencoesione.it embedding
X_FRAME_OPTIONS_ALLOWED_ORIGINS = ('127.0.0.1:8010', 'www.ascuoladiopencoesione.it', 'ascuoladiopencoesione.it')
X_FRAME_OPTIONS_ALLOWED_URLS = ('/progetti/', )
########## END X_FRAME_OPTIONS CONFIGURATION


########## UWSGI_TASKMANAGER CONFIGURATION
UWSGI_TASKMANAGER_N_LINES_IN_REPORT_INLINE = 10
UWSGI_TASKMANAGER_N_REPORTS_INLINE = 3
UWSGI_TASKMANAGER_SHOW_LOGVIEWER_LINK = True
UWSGI_TASKMANAGER_USE_FILTER_COLLAPSE = True
UWSGI_TASKMANAGER_SAVE_LOGFILE = True
########## END UWSGI_TASKMANAGER CONFIGURATION

########## OPENCOESIONE CONFIGURATION
DATA_AGGIORNAMENTO = env('DATA_AGGIORNAMENTO')
DATA_PUBBLICAZIONE_AGGIORNAMENTO = env('DATA_PUBBLICAZIONE_AGGIORNAMENTO')

CICLO_PROGRAMMAZIONE = Choices(
    ('9', '2000_2006', _('FSC da ciclo 2000-2006')),
    ('1', '2007_2013', format_lazy(_('Ciclo di programmazione {}'), '2007-2013')),
    ('2', '2014_2020', format_lazy(_('Ciclo di programmazione {}'), '2014-2020')),
    ('3', '2021_2027', format_lazy(_('Ciclo di programmazione {}'), '2021-2027')),
)

# N_MAX_DOWNLOADABLE_RESULTS = 100000

BIG_SOGGETTI_THRESHOLD = 2000
BIG_PROGRAMMI_THRESHOLD = 500

# INDICATORI_VALIDI = [
#     '007', '013', '015', '016', '018', '020', '022', '024', '026', '030', '032', '052', '053', '054', '062', '063',
#     '067', '071', '073', '076', '077', '080', '081', '084', '085', '093', '096', '099', '102', '106', '110', '111',
#     '112', '113', '134', '142', '143', '144', '145', '152', '165', '168', '174', '178', '230', '242', '245', '251',
#     '252', '256', '282', 'UV3', 'UV6',
# ]

INDICATORI_VALIDI = {
    '013': '08',
    '015': '08',
    '016': '08',
    '030': '06',
    '032': '06',
    '046': '07',
    '052': '05',
    '053': '05',
    '054': '03',
    '062': '02',
    '063': '11',
    '067': '11',
    '071': '02',
    '073': '13',
    '077': '11',
    '081': '04',
    '084': '05',
    '085': '04',
    '093': '01',
    '099': '11',
    '102': '11',
    '104': '11',
    '105': '06',
    '118': '07',
    '129': '07',
    '142': '09',
    '148': '03',
    '165': '06',
    '168': '03',
    '174': '08',
    '178': '08',
    '230': '08',
    '241': '03',
    '251': '01',
    '285': '09',
    '339': '11',
    '372': '06',
    '374': '04',
    '398': '03',
    '402': '08',
    '408': '11',
    '414': '09',
    '415': '09',
    '416': '01',
    '424': '13',
    '450': '07',
    '469': '13',
    '511': '05',
    '523': '01',
    '539': '05',
    '542': '13',
    '550': '11',
    '551': '11',
    '631': '09',
    '633': '09',
}

CONTACTS_EMAIL = env.list('CONTACTS_EMAIL', default=[ADMIN_EMAIL])

USE_EMAIL_SMTP = env.bool('USE_EMAIL_SMTP', default=True)

if USE_EMAIL_SMTP:
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = env('EMAIL_HOST', default='localhost')
    EMAIL_HOST_USER = env('EMAIL_HOST_USER', default='')
    EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD', default='')
    EMAIL_PORT = 587
    EMAIL_USE_TLS = True
else:
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = os.path.join(LOGS_PATH, 'email')

URLSHORTENER_DOMAIN = 'opencoesione.gov.it'

HONEYPOT_FIELD_NAME = 'telephone'

CORS_ORIGIN_ALLOW_ALL = True
########## END OPENCOESIONE CONFIGURATION


LocaleMiddleware.response_redirect_class = HttpResponsePermanentRedirect
