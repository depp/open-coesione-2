# -*- coding: utf-8 -*-
from haystack.forms import SearchForm


format_facet_field = lambda x: '{{!ex={0}_tag}}{0}'.format(x)


class MultiSelectWithRangeFacetedSearchForm(SearchForm):
    def __init__(self, *args, **kwargs):
        self.selected_facets = kwargs.pop('selected_facets', [])
        super().__init__(*args, **kwargs)

    def no_query_found(self):
        return self.searchqueryset.all()

    def search(self):
        sqs = super().search()

        for field, values in self._parse_selected_facets(self.selected_facets).items():
            if values:
                field = '{{!tag={0}_tag}}{0}'.format(field)
                sqs = sqs.narrow('{}:({})'.format(field, ' OR '.join(v if v.startswith(('[', '{')) and v.endswith((']', '}')) else '"{}"'.format(sqs.query.clean(v)) for v in values)))

        return sqs

    def _parse_selected_facets(self, facets):
        from collections import defaultdict

        parsed_facets = defaultdict(list)

        for facet in facets:
            if ':' in facet:
                field, value = facet.split(':', 1)
                parsed_facets[field].append(value)

        return parsed_facets
