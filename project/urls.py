# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.flatpages import views
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.defaults import server_error
from django.views.generic import RedirectView

from .opencoesione.models import Document, Event, Reuse, Workshop
from .opencoesione.monitoraggio.views import (
    TemaSinteticoCoesioneDetailView, TerritorioCoesioneDetailView,
    ValutazioneListView, ValutazioneDetailView, ValutazioneCSVView,
)
from .opencoesione.views import *

admin.autodiscover()


urlpatterns = [
    url(r'^robots.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),

    url(r'^admin/', admin.site.urls),
    url(r'^taskmanager/', include('taskmanager.urls')),
    url(r'^_nested_admin/', include('nested_admin.urls')),

    # url shortener
    url(r'^su/', include('project.urlshortener.urls')),

    # ckeditor uploader
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    # backward compatibility
    url(r'^progetti/(?P<path>tipologie|temi|programmi|gruppo-programmi)/(?P<slug>.+)$', BackwardCompatibilityRedirectView.as_view()),
    url(r'^territori/(?P<path>regioni|province|comuni)/(?P<slug>.+)$', BackwardCompatibilityRedirectView.as_view()),
    url(r'^(?P<path>pillola)/(?P<slug>.+)$', BackwardCompatibilityRedirectView.as_view()),

    url(r'^(?P<lang>it|en)/(?P<path>progetti|nature|temi|focus|programmi|programmi-widget|gruppo-programmi|strategie|soggetti|territori)/(?P<slug>.+)$', BackwardCompatibility2RedirectView.as_view()),

    url(r'^fonti-di-finanziamento/$', RedirectView.as_view(permanent=True, url='/it/risorse_2007_2013/')),
    url(r'^pac/$', RedirectView.as_view(permanent=True, url='/it/programmi_2007_2013/')),

    url(r'^faq/en/$', RedirectView.as_view(permanent=True, url='/en/faq/')),
    url(r'^progetto/en/$', RedirectView.as_view(permanent=True, url='/en/progetto/')),
    url(r'^access_indicators/$', RedirectView.as_view(permanent=True, url='/en/indicatori_di_accesso/')),

    url(r'^500/$', server_error),
]

urlpatterns += i18n_patterns(
    # home
    url(r'^$', TemplateView.as_view(template_name='homepage.html'), name='home'),

    # coesione
    url(r'^coesione/', include([
        url(r'^temi/(?P<slug>[\w-]+)/$', TemaSinteticoCoesioneDetailView.as_view(), name='temasinteticocoesione_detail'),
        url(r'^territori/(?P<slug>[\w-]+)/$', TerritorioCoesioneDetailView.as_view(), name='territoriocoesione_detail'),
        url(r'^valutazioni/$', ValutazioneListView.as_view(), name='valutazione_list'),
        url(r'^valutazioni/(?P<slug>[\w-]+)/$', ValutazioneDetailView.as_view(), name='valutazione_detail'),
        url(r'^valutazioni/csv/(?P<slug>[\w-]+)/$', ValutazioneCSVView.as_view(), name='valutazione_csv'),
    ])),

    url(r'^cerca/$', ContentSearchView.as_view(), name='search'),

    # dati
    url(r'^dati/', include('project.opencoesione.monitoraggio.urls')),

    # pillole
    url(r'^pillole/$', PillolaListView.as_view(), name='pillola_list'),
    url(r'^pillole/(?P<slug>[\w-]+)/$', PillolaDetailView.as_view(), name='pillola_detail'),

    # storie di progetto
    url(r'^storie_territorio/$', ProjectStoryListView.as_view(), name='projectstory_list'),
    url(r'^storie_territorio/(?P<slug>[\w-]+)/$', ProjectStoryDetailView.as_view(), name='projectstory_detail'),

    # ASOC stories
    url(r'^ASOCStories/$', ASOCStoryListView.as_view(), name='asocstory_list'),
    url(r'^ASOCStories/(?P<slug>[\w-]+)/$', ASOCStoryDetailView.as_view(), name='asocstory_detail'),

    # video racconti
    url(r'^video_racconti/$', VideoStoryListView.as_view(), name='videostory_list'),
    url(r'^video_racconti/(?P<slug>[\w-]+)/$', VideoStoryDetailView.as_view(), name='videostory_detail'),

    # video tutorial
    url(r'^scopri/video/$', VideoTutorialListView.as_view(), name='videotutorial_list'),
    url(r'^scopri/video/(?P<slug>[\w-]+)/$', VideoTutorialDetailView.as_view(), name='videotutorial_detail'),

    # news
    url(r'^news/rss/$', LatestNewsFeed(), name='news_feed'),
    url(r'^news/$', NewsListView.as_view(), name='news_list'),
    url(r'^news/(?P<slug>[\w-]+)/$', NewsDetailView.as_view(), name='news_detail'),

    # rassegna stampa
    url(r'^rassegna-stampa/$', PressReviewListView.as_view(), name='pressreview_list'),

    # contenuti di comunicazione
    url(r'^contenuti_comunicazione/$', CommunicationContentListView.as_view(), name='communicationcontent_list'),
    url(r'^contenuti_comunicazione_segnalazione/$', CommunicationContentCreateView.as_view(), name='communicationcontent_form'),

    # scopri
    url(r'^scopri/$', ItemListView.as_view(model=Event, flatpage_url='/scopri/'), name='event_list'),
    # url(r'^scopri/video/$', ItemListView.as_view(model=EventVideo, flatpage_url='/scopri/video/'), name='eventvideo_list'),

    # documenti istituzionali
    url(r'^documenti_istituzionali/$', ItemListView.as_view(model=Document, flatpage_url='/documenti_istituzionali/'), name='document_list'),

    # riuso
    url(r'^riuso/$', ItemListView.as_view(model=Reuse, flatpage_url='/riuso/'), name='reuse_list'),

    # laboratori sui dati
    url(r'^laboratori_dati/$', ItemListView.as_view(model=Workshop, flatpage_url='/laboratori_dati/'), name='workshop_list'),

    # faq
    url(r'^faq/$', FAQListView.as_view(), name='faq_list'),

    # opportunità 2021-2027
    url(r'^opportunita_2021_2027/$', OpportunitaProgrammaListView.as_view(), name='opportunitaprogramma_list'),

    url(r'^contatti/$', ContactMessageView.as_view(template_name='opencoesione/contatti.html'), name='contatto_form'),

    url(r'^indicatori_di_accesso/csv/(?P<n>[123])$', IndicatoriAccessoCSVView.as_view(), name='indicatori_accesso_csv'),

    url(r'^risorse_treemap/(?P<ciclo>0713|1420|2127)/(?P<tipo>fonte|territorio)$', RisorseTreemapView.as_view(template_name='opencoesione/risorse_treemap.html'), name='risorse_treemap'),

    url(r'^set_programmi_widget/$', SetProgrammiWidgetFormView.as_view(template_name='opencoesione/set_programmi_widget_form.html'), name='set_programmi_widget_form'),

    url(r'^programmi_(?P<ciclo>0713|1420|2127)(_(?P<extra>[a-z]+))?_widget/(?P<ambito>[+\w-]+)$', xframe_options_exempt(TemplateView.as_view(template_name='opencoesione/programmi_widget.html')), name='programmi_widget'),

    url(r'^documenti/(?P<path>.+)$', DocumentsRedirectView.as_view(), name='documents_clean'),

    # # segnalazione progetto
    # url(r'^progetti/segnalazione/$', SegnalazioneFormView.as_view(), name='progetti_segnalazione'),
    # url(r'^progetti/segnalazione/completa/$', TemplateView.as_view(template_name='segnalazione/completata.html'), name='progetti_segnalazione_completa'),
    # url(r'^progetti/segnalazione/(?P<pk>\d+)/$', SegnalazioneDetailView.as_view(), name='progetti_segnalazione_pubblicata'),

    # opendata
    url(r'^opendata/', include('project.opencoesione.opendata.urls')),

    # api
    url(r'^api/', include('project.opencoesione.monitoraggio.api.urls')),

    # charts
    url(r'^charts/', include('project.opencoesione.charts.urls')),

    # redirect
    url(r'^progetti_2014_2020/$', RedirectView.as_view(permanent=True, url='/beneficiari_operazioni_2014_2020/')),

    # flatpages
    url(r'^(?P<url>.*/)$', views.flatpage, name='flatpage')
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# static and media urls not works with DEBUG = True, see static function.
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
