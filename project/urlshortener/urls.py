# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import ShortURLRedirectView


urlpatterns = [
    url(r'^(?P<code>\w+)$', ShortURLRedirectView.as_view(), name='urlshortener-shorturl'),
]
