# -*- coding: utf-8 -*-
from django.apps import AppConfig


class UrlshortenerConfig(AppConfig):
    name = 'project.urlshortener'
