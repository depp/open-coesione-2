# -*- coding: utf-8 -*-
from urllib.parse import urlparse

from django.conf import settings
from django.middleware.clickjacking import XFrameOptionsMiddleware


def force_default_language_middleware(get_response):
    """
    Ignore Accept-Language HTTP headers

    This will force the I18N machinery to always choose settings.LANGUAGE_CODE
    as the default initial language, unless another one is set via sessions or cookies

    Should be installed *before* any middleware that checks request.META['HTTP_ACCEPT_LANGUAGE'],
    namely django.middleware.locale.LocaleMiddleware
    """

    def middleware(request):
        if 'HTTP_ACCEPT_LANGUAGE' in request.META:
            del request.META['HTTP_ACCEPT_LANGUAGE']

        response = get_response(request)

        return response

    return middleware


class AdvancedXFrameOptionsMiddleware(XFrameOptionsMiddleware):

    def process_response(self, request, response):
        """Intercept X_FRAME_OPTIONS_ALLOWED_ORIGINS and X_FRAME_OPTIONS_ALLOWED_URLS settings

        If the request comes from an approved origin and targets one of the approved urls, then
        set the headers:
        - X-Frame-Options allow-from domain.com
        - Content-Security-Policy frame-ancestors domain.com

        This enables iframe embedding both in newer and older browsers, as stated in
        https://stackoverflow.com/questions/30731290/how-to-set-x-frame-options-allow-from-in-nginx-correctly

        Otherwise, set the X-Frame-Options header with the default value
        (settings.X_FRAME_OPTIONS or 'SAMEORIGIN')

        :param request:
        :param response:
        :return:
        """
        if response.get('Content-Security-Policy') is not None and response.get('X-Frame-Options') is not None:
            return response

        referer = request.META.get('HTTP_REFERER', None)
        url_referer = urlparse(referer)
        referer_host = url_referer.netloc
        referer_str = "{0}://{1}/".format(url_referer.scheme, url_referer.netloc)

        allowed = False

        if referer_host and referer_host in settings.X_FRAME_OPTIONS_ALLOWED_ORIGINS:
            allowed = True

        allowed_urls = getattr(settings, 'X_FRAME_OPTIONS_ALLOWED_URLS', [])
        if allowed_urls:
            allowed = False
            for allowed_url in allowed_urls:
                if allowed_url in request.path:
                    allowed = True
                    break

        if allowed:
            response['X-Frame-Options'] = "allow-from {0}".format(referer_str)
            response['Content-Security-Policy'] = "frame-ancestors {0}".format(referer_str)
            return response
        else:
            return super(AdvancedXFrameOptionsMiddleware, self).process_response(request, response)
