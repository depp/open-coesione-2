# -*- coding: utf-8 -*-
from django.contrib.contenttypes.fields import GenericRelation, GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django_extensions.db.fields import AutoSlugField


class Tag(models.Model):
    name = models.CharField(max_length=100, verbose_name='termine')
    slug = models.SlugField(max_length=100, unique=True)
    priority = models.PositiveSmallIntegerField(default=0, verbose_name='priorità')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['priority']


class TaggedItem(models.Model):
    tag = models.ForeignKey(Tag, related_name='tagged_items')
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    def __str__(self):
        return '{}'.format(self.tag)

    class Meta:
        verbose_name = 'tag'


class TagManager(models.Manager):
    def get_queryset(self):
        ctype = ContentType.objects.get_for_model(self.model)
        return Tag.objects.filter(tagged_items__content_type=ctype).distinct()


class TagMixin(models.Model):
    tagged_items = GenericRelation(TaggedItem)

    objects = models.Manager()
    model_tags = TagManager()

    def tags(self):
        return Tag.objects.filter(id__in=(tagged_item.tag_id for tagged_item in self.tagged_items.all()))

    class Meta:
        abstract = True
