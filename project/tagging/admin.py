# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline
from .models import *


class TagInline(GenericTabularInline):
    model = TaggedItem
    extra = 0


class TagAdmin(admin.ModelAdmin):
    list_display = ('name', 'priority')
    list_editable = ('priority',)
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Tag, TagAdmin)
