# -*- coding: utf-8 -*-
from django.apps import AppConfig


class TaggingConfig(AppConfig):
    name = 'project.tagging'
