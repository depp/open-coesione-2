# -*- coding: utf-8 -*-
from collections import defaultdict
from .opencoesione.models import MenuItem


def main_settings(request):
    host = request.META['HTTP_HOST']

    menus = defaultdict(list)
    for menuitem in MenuItem.objects.select_related('page').only('menu', 'description', 'page__url'):
        menus[menuitem.menu].append({'name': menuitem.description, 'url': menuitem.url, 'identifier': menuitem.identifier})

    return {
        'MENUS': menus,
        'IS_PRODUCTION': all(h not in host for h in ('localhost', 'staging')),
    }
