{{ object.title|default_if_none:'' }}
{{ object.description|default_if_none:''|striptags|safe }}
{% for item in object.texts.all %}
{{ item.title|default_if_none:'' }}
{{ item.description|default_if_none:''|striptags|safe }}
{% endfor %}
