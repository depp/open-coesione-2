{{ object.descrizione_breve|default_if_none:'' }}
{{ object.descrizione_estesa|default_if_none:''|striptags|safe }}
{{ object.testo_dati|default_if_none:''|striptags|safe }}
{% for item in object.indicatori_sintesi.all %}
{{ item.titolo|default_if_none:'' }}
{{ item.testo|default_if_none:''|striptags|safe }}
{{ item.titolo_dati|default_if_none:'' }}
{% endfor %}
