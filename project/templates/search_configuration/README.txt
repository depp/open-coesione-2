A symbolic link is used to trick django management command
``build_solr_schema`` into generating a schema according to
Solr 3 or Solr 7 definitions, which are non compatible.
