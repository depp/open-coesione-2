var renderMapCarousel = function(id) {
    var map_container = $('#map-container');

    OCMap.init({
        containerId: 'map-container',
        width: 555,
        height: 720,
        limitsPath: map_container.data('limitspath'),
        layer: id,
        getFeatures: function (limits, layer) {
            return [
                topojson.feature(limits, limits.objects['regioni']),
                topojson.feature(limits, limits.objects[layer])
            ];
        },
        minZoomScale: map_container.data('minzoomscale'),
        disableDrag: true,
        onComplete: function () {
            var carousel_id = '#' + id;
            map_container.find('.base-layer').addClass('carousel-indicators').find('.area').each(function () {
                var elem = $(carousel_id).find('.carousel-item[data-code="' + $(this).data('code') + '"]');
                $(this).attr({
                    'data-bs-target': carousel_id,
                    'data-bs-slide-to': elem.index()
                }).toggleClass('active', elem.hasClass('active'));
            });
        }
    });
}

$(document).ready(function() {
    $('#territori').find('.carousel').bind('slide.bs.carousel', function (e) {
        var index = $(e.relatedTarget).index();
        $('[data-bs-target="#' + $(this).prop('id') + '"]').each(function() {
            $(this).toggleClass('active', $(this).data('bs-slide-to') === index);
        });
    });
});
