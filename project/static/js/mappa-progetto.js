ProgettoMap = (function() {
    var _init = function(containerID, geoJSON) {
        var container = $(containerID);

        var initMap = function() {
            var normalizeHeights = function() {
                container.height($('#map-container').height());
            };

            normalizeHeights();

            $(window).on('resize orientationchange', function() {
                normalizeHeights();
            });

            var geoJSONLayer = L.geoJSON(geoJSON, {style: {interactive: false}, pointToLayer: function(geoJsonPoint, latlng) {return L.circleMarker(latlng);}});

            var map = L.map(container.attr('id'), {scrollWheelZoom: false});

            map
                .addLayer(L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'}))
                .addLayer(geoJSONLayer)
                .addControl(new L.Control.Fullscreen({position: 'bottomright'}))
                .fitBounds(geoJSONLayer.getBounds())
                // .setZoom(map.getZoom() - 1, {animate: false})
                .on('fullscreenchange', function() {
                    if (map.isFullscreen()) {
                        map.scrollWheelZoom.enable();
                    } else {
                        map.scrollWheelZoom.disable();
                    }
                });
        };

        var intId = setInterval(function() {
            if ($('#map-container #mapg').length) {
                clearInterval(intId);
                initMap();
            }
        }, 10);
    };

    return {
        init: _init
    };
})();
