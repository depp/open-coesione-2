$(document).ready(function() {
    $('table.communicationcontents').addClass('display').dataTable({
        language: {
            url: (document.documentElement.lang  === 'it') ? '//cdn.datatables.net/plug-ins/1.10.15/i18n/Italian.json' : null
        },
        orderMulti: false,
        order: [],
        // columns: [
        //     null,
        //     null,
        //     {searchable: false},
        //     null,
        //     null,
        //     null,
        //     null
        // ],
        pageLength: 20,
        lengthChange: false
    });
});
