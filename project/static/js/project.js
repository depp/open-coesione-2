$(document).ready(function() {
    OC.init({
        highcharts_presets: config_highcharts_presets,
        typeahead_presets: config_typeahead_presets
    });

    $('.dropdown').each(function() {
        var dropdown = $(this);
        dropdown.find('.dropdown-menu').on('click', 'a', function() {
            dropdown.find('.dropdown-toggle').text($(this).text());
        });
    });

    var sm_options = (document.documentElement.lang  === 'it') ? {buttontxtmore: 'Leggi tutto', buttontxtless: 'Leggi meno'} : {};
    $('[data-sm-minrows]').each(function() {
        $(this).showMore($.extend(true, sm_options, {
            minheight: $(this).data('sm-minrows') * parseInt($(this).css('line-height').replace('px', ''))
        }));
    });
});

$(window).on('load', function() {
    $('#highlights').carouselHeightsNormalization();
    $('#faq_carousel').carouselHeightsNormalization();

    $('.totale.pagamenti').each(function() {
        var perc = $(this).data('percentuale-pagamenti');

        if (perc > 0) {
            $('<span></span>').addClass('perc-label').html(perc + '%').appendTo($(this)).css({left: Math.min(perc, 100) + '%', marginLeft: function() {
                if (perc > 97) {
                    $(this).addClass('r');
                    return '-' + $(this).width() + 'px';
                } else if (perc > 2) {
                    $(this).addClass('m');
                    return '-' + $(this).width() / 2 + 'px';
                } else {
                    return '0';
                }
            }});
        }
    });

    $('#message-modal').modal('show');
});

(function($) {
    // Normalize Carousel Heights - pass in Bootstrap Carousel items.
    $.fn.carouselHeightsNormalization = function() {
        var items = $(this).find('.carousel-item'), //grab all slides
            heights = [], //create empty array to store height values
            tallest; //create variable to make note of the tallest slide

        var normalizeHeights = function() {
            items.each(function() { //add heights to array
                heights.push($(this).outerHeight());
            });
            tallest = Math.max.apply(null, heights); //cache largest value
            items.each(function() {
                $(this).css('min-height', tallest + 'px');
            });
        };

        normalizeHeights();

        $(window).on('resize orientationchange', function () {
            //reset vars
            tallest = 0;
            heights.length = 0;

            items.each(function() {
                $(this).css('min-height', '0'); //reset min-height
            });
            normalizeHeights(); //run it again
        });
    };
}(jQuery));

(function($) {
    /*
    Plugin: ShowMore
    author: dtasic@gmail.com
    */
    $.fn.showMore = function(options) {
        'use strict';

        var currentelem = 1;

        this.each(function() {
            var currentid = '';
            var element = $(this);
            var auto = parseInt(element.innerHeight()) / 2;
            var fullheight = element.innerHeight();
            var maxWidth = element.css('width');
            var settings = $.extend({
                minheight: auto,
                buttontxtmore: 'show more',
                buttontxtless: 'show less',
                buttoncss: 'showmore-button',
                animationspeed: auto
            }, options);

            currentid = (element.attr('id') !== undefined) ? element.attr('id') : currentelem;
            element.wrap('<div id="showmore-' + currentid + '" data-showmore' + (maxWidth === '0px' ? '' : ' style="max-width: ' + maxWidth + ';"') + '></div>');

            if (element.parent().not('[data-showmore]')) {
                if (fullheight > settings.minheight) {
                    element.css('min-height', settings.minheight).css('max-height', settings.minheight).css('overflow', 'hidden');
                    var showMoreButton = $('<div />', {
                        id: 'showmore-button-' + currentid,
                        class: settings.buttoncss,
                        click: function() {
                            if (element.css('max-height') !== 'none') {
                                element.css('height', settings.minheight).css('max-height', '').animate({height: fullheight}, settings.animationspeed, function () { showMoreButton.html(settings.buttontxtless); });
                            } else {
                                element.animate({height: settings.minheight}, settings.animationspeed, function () { showMoreButton.html(settings.buttontxtmore); element.css('max-height', settings.minheight); });
                            }
                        },
                        html: settings.buttontxtmore
                    });

                    element.after(showMoreButton);
                }

                currentelem++;
            }
        });

        return this;
    };
}(jQuery));
