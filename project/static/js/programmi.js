(function (window, document, $, undefined) {
    var opts;

    $.fn.dataTable.SelectionFilter = function(oSettings, oOpts) {
        opts = oOpts;

		var classes = oSettings.oClasses;
		var tableId = oSettings.sTableId;

		var label = oSettings.aoColumns[opts.filterColumn].sTitle;

        var filterMenu = [];
        $(oSettings.aoData).each(function () {
            var val = this._aData[opts.filterColumn];
            if ($.inArray(val, filterMenu) === -1) {
                filterMenu.push(val);
            }
        });
        filterMenu.sort();

        var select = $('<select/>', {
            'name': tableId + '_selectionFilter',
            'aria-controls': tableId
        });

        select.append($('<option>').attr('value', '').text(opts.allText));
        $(filterMenu).each(function () {
            select.append($('<option>').attr('value', this).text(this));
        });

		var filter = $('<div/>', {
            'id': tableId + '_selectionFilter',
            'class': classes.sFilter
        }).append($('<label/>').append(label + ': ' + select[0].outerHTML));

        $('select', filter)
            .val(oSettings._sFilterValue)
            .on('change', function () {
                oSettings._sFilterValue = $(this).val();
                $('#' + tableId).dataTable().fnDraw();
            });

        return filter[0];
    };

    $.fn.dataTableExt.afnFiltering.push(
        function (oSettings, aData, iDataIndex) {
            var val = $('select[name="' + oSettings.sTableId + '_selectionFilter"]').val();
            return val ? aData[opts.filterColumn] === val : true;
        }
    );

    $.fn.dataTableExt.aoFeatures.push({
        fnInit: function (oSettings) {
            return new $.fn.dataTable.SelectionFilter(oSettings, oSettings.oInit.selectionFilter);
        },
        cFeature: 'r'
    });
})(window, document, jQuery);

$(document).ready(function() {
    var prefix = '!';
    var hash = document.location.hash.replace('#' + prefix, '#');
    var isItalian = (document.documentElement.lang  === 'it');

    function initDataTable() {
        $('table.display').not('.dataTable').filter(':visible').dataTable({
            language: {
                url: isItalian ? '//cdn.datatables.net/plug-ins/1.10.15/i18n/Italian.json' : null
            },
            orderMulti: false,
            order: [[1, 'asc']],
            columns: [
                null,
                {visible: false},
                {searchable: false, className: 'dt-body-right'},
                {searchable: false, className: 'dt-body-right'},
                null,
                null,
                null
            ],
            pageLength: 30,
            lengthMenu: [10, 30, 50, 70, 100],
            lengthChange: false,
            selectionFilter: {
                filterColumn: 1,
                allText: isItalian ? 'Tutti i programmi' : 'All programs'
            },
            responsive: true
        });
    }

    if (typeof $().tab === 'function') {
        $('#programmi .nav-tabs').on('shown.bs.tab', 'a', function () {
            initDataTable();
            document.location.hash = $(this).attr('href').replace('#', '#' + prefix);
        }).find('a[href="' + hash + '"]').tab('show');
    }

    initDataTable();
});
