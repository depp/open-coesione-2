IndicatorsCharts = (function() {
    var _topics = null,
        _locations = null,
        _indicators = null,
        _locationIDs = null,
        _indicatorID = null,
        _regionalIndicatorsURL = null,
        _topic_name = null,
        _topic_selector = null,
        _indicator_selector = null,
        _location_selector = null,
        _location_reset = null,
        _chart_exporter = null,
        _chart_container = null,
        _chart_instance = null,
        _series = {};

    var _init = function(containerID, locationID, indicatorsURL, regionalIndicatorsURL) {
        var container = $(containerID);

        _chart_container = container.find('.chart-canvas').first();

        _topic_name = container.find('.topic-name').first();

        _topic_selector = container.find('.topic-selector').first();
        _indicator_selector = container.find('.indicator-selector select').first();
        _location_selector = container.find('.location-selector select').first();
        _location_reset = container.find('.location-selector button').first();

        _chart_exporter = container.find('.chart-exporter').first();

        _regionalIndicatorsURL = regionalIndicatorsURL;

        _topic_selector.on('shown.bs.tab', 'a', function() {
            _initTopic($(this).data('topic'));
        });

        _indicator_selector.on('change', function() {
            _initIndicator($(this).val());
        });

        _location_selector.on('change', function() {
            var locationID = $(this).val();

            if (locationID) {
                var series = $.grep(_chart_instance.series, function(elem) { return _getLocationID(elem.name) === locationID; });
                if (series.length) {
                    series[0].setVisible();
                } else {
                    series = _filterSeries(_series[_indicatorID], [locationID])[0];
                    if (_chart_instance.series.length < _chart_instance.options.colors.length) {
                        _chart_instance.addSeries($.extend(true, {}, series, {
                            color: _chart_instance.options.colors[_chart_instance.series.length]
                        }));
                    } else {
                        _chart_instance.series[_chart_instance.series.length - 1].update(series)
                    }
                }
            }
        });

        _location_reset.on('click', function() {
            _location_selector.val('');
            if (_chart_instance) {
                while (_chart_instance.series.length > _locationIDs.length) {
                    _chart_instance.series.pop().remove();
                }
            }
        });

        _chart_exporter.on('click', function() {
            if (_chart_instance) {
                _chart_instance.exportChartLocal(null, {
                    chart: {
                        backgroundColor: '#FFFFFF'
                    },
                    title: {
                        text: _topic_name.text().toUpperCase() + '<br/>' + _indicators[_indicatorID]['title']
                    }
                });
            }
        });

        $.getJSON(indicatorsURL, function(data) {
            _topics = data['topics'];
            _locations = data['locations'];

            _locationIDs = [_getLocationID('Italia')];
            if (locationID) {
                _locationIDs.push(locationID.toString());
            }

            $.each(_locations, function(idx, location) {
                if ($.inArray(idx, _locationIDs) === -1) {
                    _location_selector.append($('<option>', {
                        value: idx,
                        text: location['name']
                    }));
                }
            });

            _initTopic(_chart_container.data('topic'));
        });
    };

    var _initTopic = function(topicID) {
        _topic_name.text(_topics[topicID]['name']);

        _indicators = _topics[topicID]['indicators'];

        _indicator_selector.children().remove();
        $.each(_indicators, function(idx, indicator) {
            _indicator_selector.append($('<option>', {
                value: idx,
                text: indicator['title']
            }));
        });

        _initIndicator(_indicator_selector.val());
    };

    var _initIndicator = function(indicatorID) {
        _location_selector.val('');

        _indicatorID = indicatorID;

        if (_indicatorID in _series) {
            _printChart();
        } else {
            $.getJSON(_regionalIndicatorsURL.replace('000', _indicatorID), function(data) {
                _series[_indicatorID] = [];

                $.each(data, function(locationID, locationValues) {
                    _series[_indicatorID].push({
                        name: _locations[locationID]['name'],
                        data: locationValues.map(function(x) { return [Date.UTC(x['year'], 0, 1), x['value']] }),
                        locationID: locationID
                    });
                });

                _printChart();
            });
        }
    };

    var _printChart = function() {
        _chart_instance && _chart_instance.destroy();

        _chart_instance = _chart_container.highcharts($.extend(true, {}, config_highcharts_presets.default, {
            chart: {
                type: 'spline',
                spacingBottom: 50
            },
            plotOptions: {
                series: {
                    marker: {
                        symbol: 'circle',
                        fillColor: '#ffffff',
                        lineWidth: 2,
                        lineColor: null,
                        radius: 6
                    }
                }
            },
            colors: ['#68875b', '#00c8ca', '#1a8275', '#03bf69', '#cfc241'],
            series: _filterSeries(_series[_indicatorID], _locationIDs),
            legend: {
                margin: 12,
                itemStyle: {
                    fontSize: '14px'
                }
            },
            xAxis: {
                showLastLabel: true,
                type: 'datetime',
                units: [
                    ['year', null]
                ]
            },
            title: {
                margin: 45
            },
            subtitle: {
                text: _indicators[_indicatorID]['subtitle']
            },
            credits: {
                enabled: true,
                text: 'Fonte: ISTAT',
                href: null,
                position: {
                    y: -10
                },
                style: {
                    cursor: 'default',
                    fontSize: '12px',
                    paddingTop: '10px'
                }
            },
            tooltip: {
                shared: true,
                headerFormat: '{point.key}<br/>',
                pointFormat: '{series.name}: {point.y:,.2f}<br/>'
            }
        })).highcharts();
    };

    var _getLocationID = function(name) {
        for (var key in _locations) {
            if (_locations.hasOwnProperty(key)) {
                if (_locations[key]['name'] === name) {
                    return key;
                }
            }
        }
        return -1;
    };

    var _filterSeries = function(series, locationIDs) {
        return $.grep(series, function(elem) {
            return $.inArray(elem['locationID'], locationIDs) > -1;
        });
    };

    return {
        init: _init
    };
})();
