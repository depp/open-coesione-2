var ciclo_programmazione_color_map = {
    '2000-2006': '#D8C99B',
    '2007-2013': '#164193',
    '2014-2020': '#559CB5',
    '2021-2027': '#94778B',
};

var focus_nature_color_map = {
    '01': '#FFAE29',
    '03': '#251E59',
    '06': '#00A2B8',
    '07': '#FF3950',
    '08': '#00B896',
};

var highcharts_focus_nature_classe_finanziaria_base = {
    chart: {
        type: 'bar',
        height: 260,
        events: {
            load: function () {
                for (var i = 0; i < this.series.length; i++) {
                    this.series[i].update({
                        color: focus_nature_color_map[this.series[i].name.split(' | ')[1]],
                        name: this.series[i].name.split(' | ')[0]
                    });
                }
            }
        }
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            borderWidth: 0,
            maxPointWidth: 28,
            groupPadding: 0,
            states: {
                hover: {
                    enabled: false
                },
                inactive: {
                    enabled: false
                }
            }
        }
    },
    legend: {
        enabled: false
    },
    xAxis: {
        lineWidth: 0,
        labels: {
            align: 'right',
            reserveSpace: true,
            style: {
                color: '#000000',
                fontSize: '12px'
            }
        }
    },
    yAxis: {
        visible: false
    }
};

$.extend(true, config_highcharts_presets, {
    // highcharts_focus_ambiti: {
    //     chart: {
    //         type: 'packedbubble',
    //         height: 220,
    //         marginBottom: 30
    //     },
    //     responsive: {
    //         rules: [{
    //             condition: {
    //                 maxWidth: 220
    //             },
    //             chartOptions: {
    //                 chart: {
    //                     height: 220,
    //                 },
    //                 plotOptions: {
    //                     series: {
    //                         minSize: '30%',
    //                         maxSize: '70%',
    //                     }
    //                 },
    //             },
    //         }]
    //     },
    //     plotOptions: {
    //         series: {
    //             minSize: '10%',
    //             maxSize: '80%',
    //             marker: {
    //                 fillOpacity: 1,
    //                 states: {
    //                     hover: {
    //                         enabled: false
    //                     }
    //                 }
    //             },
    //             draggable: false,
    //             layoutAlgorithm: {
    //                 enableSimulation: false,
    //                 parentNodeLimit: true,
    //                 seriesInteraction: false,
    //                 splitSeries: true
    //             },
    //             dataLabels: {
    //                 enabled: true,
    //                 format: '{point.name}',
    //                 filter: {
    //                     property: 'y',
    //                     operator: '>',
    //                     value: 10000000
    //                 },
    //                 style: {
    //                     color: '#333333',
    //                     textOutline: 'none',
    //                     textTransform: 'uppercase',
    //                     fontWeight: 'bold'
    //                 }
    //             }
    //         }
    //     },
    //     legend: {
    //         enabled: false
    //     },
    //     tooltip: {
    //         formatter: function () {
    //             if (this.point.name) {
    //                 return this.point.name.toUpperCase() + ': € ' + Highcharts.numberFormat(this.point.value, 0);
    //             } else {
    //                 return false;
    //             }
    //         }
    //     }
    // },
    highcharts_focus_classi: {
        data: {
            // fix for Chrome bug: https://bugs.chromium.org/p/chromium/issues/detail?id=124398
            parseDate: function(val) { return val; }
        },
        chart: {
            type: 'treemap',
            height: 140
        },
        plotOptions: {
            series: {
                colorByPoint: true,
                dataLabels: {
                    enabled: true,
                    style: {
                        color: '#FFFFFF',
                        textOutline: 'none',
                        textTransform: 'uppercase',
                        fontWeight: 'bold'
                    }
                },
                levels: [
                    {
                        level: 1,
                        borderColor: '#FFFFFF'
                    }
                ]
            }
        },
        colors: ['#123061', '#193E7B', '#1F4B96', '#2659B0', '#2E67CA', '#3675E5', '#3E83FF', '#609FFA', '#81B6F7', '#9FCAF6', '#BCDCF6', '#D7EBF8', '#F1F8FC'],
        tooltip: {
            pointFormat: '{point.name}<br/>€ {point.value}'
        }
    },
    highcharts_focus_regioni_attuazione: {
        chart: {
            type: 'line',
            polar: true,
            height: 420,
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 20,
            events: {
                load: function () {
                    var xAxis = this.xAxis['0'];
                    var xAxisLength = xAxis.names.length;
                    for (var i = 0; i < xAxisLength; i++) {
                        xAxis.addPlotBand({
                            innerRadius: '100%',
                            outerRadius: '130%',
                            from: i,
                            to: i + 1,
                            color: getGradientColor('#9BC9F6', '#F8F8F8', i / (xAxisLength - 1))
                        });
                    }
                }
            }
        },
        plotOptions: {
            series: {
                marker: false,
                pointPlacement: 'on'
            }
        },
        series: [{
            dashStyle: 'shortdash'
        }, {
            dashStyle: 'solid'
        }],
        colors: ['#0066CC'],
        legend: {
            align: 'left',
            verticalAlign: 'top',
            margin: 10
        },
        xAxis: {
            tickmarkPlacement: 'on',
            labels: {
                formatter: function() {
                    var val = this.value;
                    if (typeof val === 'string') {
                        val = val.split('/')[0]
                    }
                    return val;
                },
                style: {
                    fontSize: '11px'
                }
            }
        },
        yAxis: {
            tickInterval: 50,
            min: 0,
            max: 120,
            endOnTick: false,
            showLastLabel: true,
            labels: {
                format: '{value}%'
            }
        },
        tooltip: {
            shared: true,
            headerFormat: '{point.key}<br/>',
            pointFormat: '{series.name}: {point.y:,.2f}%<br/>'
        }
    },
    highcharts_focus_nature: {
        chart: {
            type: 'bar',
            height: 310,
            marginRight: 60,
            events: {
                load: function () {
                    var series = this.series['0'];
                    for (var i = 0; i < series.data.length; i++) {
                        series.points[i].update({color: focus_nature_color_map[series.points[i].codice.toString().padStart(2, '0')]});
                    }
                }
            }
        },
        data: {
            seriesMapping: [{
                codice: 1
            }]
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                maxPointWidth: 40,
                groupPadding: 0,
                dataLabels: {
                    enabled: true,
                    useHTML: true,
                    formatter: function () {
                        return '€ ' + humanize(this.y);
                    },
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        color: '#000000'
                    }
                },
                states: {
                    hover: {
                        enabled: false
                    }
                }
            }
        },
        legend: {
            enabled: false
        },
        xAxis: {
            lineWidth: 0,
            labels: {
                align: 'left',
                reserveSpace: true,
                style: {
                    color: '#000000',
                    fontSize: '12px'
                }
            }
        },
        yAxis: {
            visible: false
        },
        tooltip: {
            enabled: false
        }
    },
    highcharts_focus_nature_classe_finanziaria_progetti: $.extend(true, {}, highcharts_focus_nature_classe_finanziaria_base, {
        yAxis: {
            reversed: true
        },
        tooltip: {
            pointFormat: '{series.name}<br/>{point.y}'
        }
    }),
    highcharts_focus_nature_classe_finanziaria_finanziamenti: $.extend(true, {}, highcharts_focus_nature_classe_finanziaria_base, {
        xAxis: {
            opposite: true
        },
        tooltip: {
            pointFormat: '{series.name}<br/>€ {point.y}'
        }
    }),
    highcharts_focus_top_programmi: {
        chart: {
            type: 'bar',
            height: 280,
            marginRight: 30,
            events: {
                load: function () {
                    var series = this.series['0'];
                    var ticks = this.xAxis['0'].ticks;
                    var cicli_programmazione = new Set();
                    for (var i = 0; i < series.data.length; i++) {
                        series.points[i].update({color: ciclo_programmazione_color_map[series.points[i].ciclo_programmazione]});
                        ticks[i].label.css({right: (series.data[i].dataLabel.width + 10) + 'px'});
                        cicli_programmazione.add(series.points[i].ciclo_programmazione);
                    }

                    var container = $('<ul class="highcharts_legend"></ul>').insertBefore($(this.renderTo));
                    $.each(ciclo_programmazione_color_map, function (key, val) {
                        if (cicli_programmazione.has(key)) {
                            container.append('<li><span style="background-color:' + val + '"></span>' + key + '</li>');
                        }
                    });
                }
            }
        },
        data: {
            seriesMapping: [{
                ciclo_programmazione: 1
            }]
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                maxPointWidth: 22,
                groupPadding: 0,
                opacity: 0.6,
                dataLabels: {
                    enabled: true,
                    useHTML: true,
                    formatter: function () {
                        return '€ ' + humanize(this.y);
                    },
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        color: '#000000'
                    }
                },
                states: {
                    hover: {
                        enabled: false
                    }
                }
            }
        },
        legend: {
            enabled: false
        },
        xAxis: {
            lineWidth: 0,
            labels: {
                x: 10,
                align: 'left',
                useHTML: true,
                style: {
                    width: null,
                    pointerEvents: 'none',
                    textOverflow: 'ellipsis',
                    color: '#000000',
                    fontSize: '12px'
                }
            }
        },
        yAxis: {
            visible: false
        },
        tooltip: {
            useHTML: true,
            pointFormat: '{point.name}<br/>€ {point.y}'
        }
    }
});

function getGradientColor(start_color, end_color, percent) {
    var intcolors;

    var colors = $.map([start_color, end_color], function (val) {
        // strip the leading # if it's there
        val = val.replace(/^\s*#|\s*$/g, '');

        // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
        if (val.length === 3) {
            val = val.replace(/(.)/g, '$1$1');
        }

        return val;
    });

    var color = '#';
    for (var i = 0; i < 3; i++) {
        intcolors = $.map(colors, function (val) {
            return parseInt(val.substr(2 * i, 2), 16);
        });
        color += (((intcolors[1] - intcolors[0]) * percent) + intcolors[0]).toString(16).split('.')[0].padStart(2, '0');
    }

    return color;
}

function humanize(val) {
    if (val >= Math.pow(10, 9))
        return Highcharts.numberFormat(val / Math.pow(10, 9), 1) + ' ' + Highcharts.defaultOptions.lang.numericSymbols[2];
    else if (val >= Math.pow(10, 6))
        return Highcharts.numberFormat(val / Math.pow(10, 6), 1) + ' ' + Highcharts.defaultOptions.lang.numericSymbols[1];
    else
        return Highcharts.numberFormat(val, Number.isInteger(val) ? 0 : 2);
}

StatoProceduraleChart = (function() {
    var _svg = null;
    var _tip = null;

    var _data = [];

    var _xScale = null;
    var _yScale = null;

    var _width = null;
    var _height = 290;

    var _nRects = 40;

    var _sum_values = function(values) {
        return values.reduce(function(acc, val) { return acc + parseFloat(val); }, 0);
    };

    var _init = function(tableID) {
        var table = d3.select(tableID);

        if (table.empty()) {
            return;
        }

        var container = d3.select(table.node().parentNode)
            .insert('div', tableID)
            .attr('id', tableID.replace('#', '') + '_chart');

        var columns = table.selectAll('thead th').datum(function() {
            return d3.select(this).text();
        }).data().slice(2);

        var data = table.selectAll('tbody tr').datum(function() {
            return d3.select(this).selectAll('td').datum(function() {
                return d3.select(this).text();
            }).data();
        }).data();

        _data = data.map(function(item) {
            var name = item[0];
            var color = focus_nature_color_map[item[1]];
            var values = item.slice(2);
            var total = _sum_values(values);

            return {
                name: name,
                values: d3.range(_nRects).map(function(n) {
                    var index;
                    for (var i = 0; i < values.length; i++) {
                        if (Math.round(_sum_values(values.slice(i)) / total * _nRects) >= n + 1) {
                            index = i;
                        }
                    }
                    return {
                        index: index,
                        value: Math.round(values[index] / total * 100),
                        color: getGradientColor('#FFFFFF', color, Math.pow(index, 1.5) / Math.pow(values.length - 1, 1.5))
                    }
                })
            }
        });

        _width = container.node().getBoundingClientRect().width;

        _yScale = d3.scaleBand().domain(_data.map(function(d, i) { return i; })).range([0, _height]).paddingInner(0.1);

        _tip = d3.tip()
            .attr('class', 'd3-tip1')
            .offset([-8, 0])
            .html(function(d) {
                return columns[d.index] + ': ' + d.value + '%';
            });

        _svg = container.append('svg')
            .attr('xmlns', 'http://www.w3.org/2000/svg')
            .attr('width', _width)
            .attr('height', _height)
            .attr('viewBox', '0 0 ' + _width + ' ' + _height)
            .call(_tip);

        d3.select(window).on('resize.statoprocedurale', function() {
            _width = container.node().getBoundingClientRect().width;

            _svg
                .attr('width', _width)
                .attr('viewBox', '0 0 ' + _width + ' ' + _height)
                .selectAll('g').remove();

            _drawChart();
        });

        _drawChart();
    };

    var _drawChart = function() {
        _xScale = d3.scaleBand().domain(d3.range(_nRects)).range([0, _width]).padding(0.25).align(0);

        var g = _svg.selectAll('g')
            .data(_data)
            .enter().append('g')
            .attr('transform', function(d, i) { return 'translate(0,' + _yScale(i) + ')'; });

        g.append('text')
            .attr('x', 0)
            .attr('y', 20)
            .style('cursor', 'default')
            .text(function(d) { return d.name; });

        g.selectAll('rect')
            .data(function(d) { return d.values; })
            .enter().append('rect')
            .attr('fill', function(d) { return d.color; })
            .attr('stroke-width', 1)
            .attr('stroke', function(d) { return d.index === 0 ? '#C2C2C2' : d.color; })
            .attr('x', function(d, i) { return _xScale(i); })
            .attr('y', 30)
            .attr('width', _xScale.bandwidth())
            .attr('height', Math.min(_yScale.bandwidth() - 30, 80))
            .on('mouseover', _tip.show)
            .on('mouseout', _tip.hide);
    };

    return {
        init: _init
    };
})();

CicloAmbitoChart = (function() {
    var _svg = null;
    var _tip = null;

    var _roots = [];
    var _max_value = null;

    var _width = null;
    var _height = 258;

    var _init = function(tableID) {
        var table = d3.select(tableID);

        var container = d3.select(table.node().parentNode)
            .insert('div', tableID)
            .attr('id', tableID.replace('#', '') + '_chart');

        var columns = table.selectAll('thead th').datum(function() {
            return d3.select(this).text();
        }).data().slice(1);

        var data = table.selectAll('tbody tr').datum(function() {
            return d3.select(this).selectAll('td').datum(function() {
                return d3.select(this).text();
            }).data();
        }).data();

        data.forEach(function(row) {
            var child = {name: row.shift(), children: []};
            row.forEach(function(item, idx) {
                if (item > 0) {
                    child.children.push({name: columns[idx], value: item});
                }
            })
            _roots.push(child);
        });

        _roots = _roots.map(function(item) {
            return d3.hierarchy(item).sum(function(d) {
                return d.value;
            }).sort(function(a, b) {
                return b.value - a.value;
            })
        });
        _max_value = Math.max.apply(Math, _roots.map(function(item) { return item.value; }));

        _width = container.node().getBoundingClientRect().width;

        var formatter = d3.format(',d');

        _tip = d3.tip()
            .attr('class', 'd3-tip2')
            .offset([-8, 0])
            .html(function(d) {
                return d.data.name + ': €' + formatter(d.value);
            });

        _svg = container.append('svg')
            .attr('xmlns', 'http://www.w3.org/2000/svg')
            .attr('width', _width)
            .attr('height', _height)
            .attr('viewBox', '0 0 ' + _width + ' ' + _height)
            .call(_tip);

        d3.select(window).on('resize.cicloambito', function() {
            _width = container.node().getBoundingClientRect().width;

            _svg
                .attr('width', _width)
                .attr('viewBox', '0 0 ' + _width + ' ' + _height)
                .selectAll('g').remove();

            _drawChart();
        });

        _drawChart();
    };

    var _drawChart = function() {
        var topspace = 28;
        var margin = 2;
        var width = _width / _roots.length;
        var height = _height - topspace;
        var size = d3.scaleLinear().domain([0, _max_value]).range([2, Math.min(width, height) / 2 - 10]);
        var label_width_threshold = 180;

        _roots.forEach(function(root, idx) {
            var ratio = root.value / _max_value;
            var pack = d3.pack().size([width * ratio - 2 * margin, height * ratio - 2 * margin]).padding(5).radius(function(d) { return size(d.value) });
            var layout = pack(root);

            var dx = width / 2 - layout.x + idx * width;
            var dy = height / 2 - layout.y + topspace;
            var g = _svg.append('g').attr('transform', 'translate(' + dx + ',' + dy + ')');

            var node = g.selectAll('.node')
                .data(layout.descendants())
                .enter().append('g')
                .attr('class', function(d) { return d.children ? 'node' : 'leaf node'; })
                .attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; });

            node.append('circle')
                .attr('r', function(d) { return d.r; })
                .attr('fill', function(d) { return d.children ? ciclo_programmazione_color_map[d.data.name] : '#FFFFFF'; });

            node.filter(function(d) { return d.children; })
                .append('text')
                .attr('dy', -(height / 2 + 12))
                .attr('text-anchor', 'middle')
                .attr('fill', '#333333')
                .style('cursor', 'default')
                .style('pointer-events', 'none')
                .style('font-size', '16px')
                .style('font-style', 'italic')
                .text(function(d) { return d.data.name + (width < label_width_threshold ? '' : ' - '); })
                .append('tspan')
                .attr('x', width < label_width_threshold ? '0' : 'auto')
                .attr('dy', width < label_width_threshold ? '1.2em' : '0')
                .attr('fill', function(d) { return ciclo_programmazione_color_map[d.data.name]; })
                .text(function(d) { return '€ ' + humanize(d.value) });

            node.filter(function(d) { return !d.children; })
                .on('mouseover', _tip.show)
                .on('mouseout', _tip.hide)
                .append('text')
                .attr('dy', '0.3em')
                .attr('text-anchor', 'middle')
                .attr('fill', '#333333')
                .style('cursor', 'default')
                .style('pointer-events', 'none')
                .style('font-size', '11px')
                .style('font-weight', 'bold')
                .text(function(d) { return d.data.name.length <= (d.r / 3) ? d.data.name : ''; });
        })
    };

    return {
        init: _init
    };
})();
