$(document).ready(function() {
    $('div.chart-container').each(function() {
        var self = $(this);

        var container = self.find('div.chart-canvas')[0];
        var table = self.find('table.chart-table');
        var selector = self.find('select.chart-selector');

        var dates = table.find('thead th').slice(2).map(function() { return $(this).text() });

        var chart = Highcharts.chart(container, $.extend(true, {}, config_highcharts_presets.default, {
            chart: {
                type: 'column'
            },
            colors: ['#003399', '#C45355', '#228822'],
            legend: {
                align: 'left',
                verticalAlign: 'top',
                layout: 'vertical',
                backgroundColor: '#FFFFFF',
                itemMarginTop: 3,
                itemMarginBottom: 3,
                floating: true,
                x: 80,
                y: 10,
                shadow: true
            },
            xAxis: {
                categories: dates,
                labels: {
                    rotation: -45,
                    align: 'right'
                }
            },
            yAxis: {
                floor: 0,
                ceiling: 140,
                tickInterval: 20
            },
            tooltip: {
                pointFormat: '{series.name} {point.category}: {point.y:.0f}%'
            }
        }));

        var chart_series = {};

        table.find('tbody tr').each(function() {
            var row = $(this).find('td');
            var program = $(row[0]).text();
            var data_type = $(row[1]).text();

            if (!(program in chart_series)) {
                chart_series[program] = {}
            }
            chart_series[program][data_type] = row.slice(2).map(function() { return parseFloat($(this).text() || 0.0) });
        });

        $.each(chart_series, function(key, val) {
            selector.append($('<option>', { value : key }).text(key));
        });

        selector.on('change', function() {
            var program = $(this).val();

            if (chart.series.length) {
                $.each(chart.series, function(key, value) {
                    chart.series[key].setData(chart_series[program][chart.series[key].name], false);
                });
                chart.redraw();
            } else {
                $.each(chart_series[program], function(key, value) {
                    chart.addSeries({
                        name: key,
                        data: $.extend(true, {}, value)
                    })
                });
            }
        }).trigger('change');

        self.show();
    });
});
