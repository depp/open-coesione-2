(function () {
    $('#middle-footer a[href*="/informativa_privacy/"]').parent().after('<li><a href="#" data-cc="c-settings">' + ((document.documentElement.lang  === 'it') ? 'Preferenze cookie' : 'Cookie preferences') + '</a></li>');

    // const services = {
    //     'youtube': 'https://www.youtube.com/embed/',
    //     'slideshare': '//www.slideshare.net/slideshow/embed_code/key/',
    //     'infogram': 'https://e.infogram.com/',
    // }
    //
    // $.each(services, function (service, src) {
    //     $('iframe[src^="' + src + '"]').each(function () {
    //         var attrs = ['width', 'height'];
    //
    //         var self = $(this);
    //         var div = self.wrap('<div/>').parent();
    //
    //         var params = self.attr('src').replace(src, '').split('?');
    //
    //         div.attr('data-service', service);
    //         div.attr('data-id', params[0]);
    //         if (params.length > 1) {
    //             div.attr('data-params', params[1]);
    //         }
    //
    //         for (var i = 0; i < attrs.length; i++) {
    //             if (self.attr(attrs[i])) {
    //                 div.css(attrs[i], self.attr(attrs[i]) + 'px');
    //                 div.attr('data-frame-' + attrs[i], self.attr(attrs[i]));
    //             }
    //         }
    //
    //         self.remove();
    //     });
    // })

    const cc = initCookieConsent();
    const ifm = iframemanager();

    ifm.run({
        currLang: document.documentElement.getAttribute('lang'),
        showBtn: false,
        services: {
            youtube: {
                embedUrl: 'https://www.youtube-nocookie.com/embed/{data-id}',
                thumbnailUrl: 'https://i3.ytimg.com/vi/{data-id}/hqdefault.jpg',
                iframe: {
                    allow: 'fullscreen',
                },
                cookie: {
                    name: 'cc_youtube'
                },
                languages: {
                    it: {
                        notice: 'Questo video incorporato via YouTube utilizza cookie di terze parti ed è attualmente disabilitato. È possibile attivarne il funzionamento tramite le preferenze cookie del sito',
                        loadBtn: 'Carica il video',
                        loadAllBtn: 'Non chiedere nuovamente'
                    },
                    en: {
                        notice: 'This video embedded via YouTube uses third party cookies and is currently disabled. It is possible to view it through the cookie preferences',
                        loadBtn: 'Load video',
                        loadAllBtn: "Don't ask again"
                    }
                }
            },
            slideshare: {
                embedUrl: 'https://www.slideshare.net/slideshow/embed_code/key/{data-id}',
                iframe: {
                    allow: 'fullscreen',
                },
                cookie: {
                    name: 'cc_slideshare'
                },
                languages: {
                    it: {
                        notice: 'Questa presentazione incorporata via SlideShare utilizza cookie di terze parti ed è attualmente disabilitata. È possibile attivarne il funzionamento tramite le preferenze cookie del sito',
                        loadBtn: 'Carica la presentazione',
                        loadAllBtn: 'Non chiedere nuovamente'
                    },
                    en: {
                        notice: 'This presentation embedded via SlideShare uses third party cookies and is currently disabled. It is possible to view it through the cookie preferences',
                        loadBtn: 'Load presentation',
                        loadAllBtn: "Don't ask again"
                    }
                }
            },
            infogram: {
                embedUrl: 'https://e.infogram.com/{data-id}',
                iframe: {
                    allow: 'fullscreen',
                },
                cookie: {
                    name: 'cc_infogram'
                },
                languages: {
                    it: {
                        notice: 'Questa presentazione incorporata via Infogram utilizza cookie di terze parti ed è attualmente disabilitata. È possibile attivarne il funzionamento tramite le preferenze cookie del sito',
                        loadBtn: 'Carica la presentazione',
                        loadAllBtn: 'Non chiedere nuovamente'
                    },
                    en: {
                        notice: 'This presentation embedded via Infogram uses third party cookies and is currently disabled. It is possible to view it through the cookie preferences',
                        loadBtn: 'Load presentation',
                        loadAllBtn: "Don't ask again"
                    }
                }
            },
            facebook: {
                embedUrl: 'https://www.facebook.com/plugins/video.php',
                iframe: {
                    allow: 'fullscreen',
                },
                cookie: {
                    name: 'cc_facebook'
                },
                languages: {
                    it: {
                        notice: 'Questo video incorporato via Facebook utilizza cookie di terze parti ed è attualmente disabilitato. È possibile attivarne il funzionamento tramite le preferenze cookie del sito',
                        loadBtn: 'Carica il video',
                        loadAllBtn: 'Non chiedere nuovamente'
                    },
                    en: {
                        notice: 'This video embedded via Facebook uses third party cookies and is currently disabled. It is possible to view it through the cookie preferences',
                        loadBtn: 'Load video',
                        loadAllBtn: "Don't ask again"
                    }
                }
            }
        }
    });

    cc.run({
        current_lang: document.documentElement.getAttribute('lang'),
        autoclear_cookies: true,
        page_scripts: true,
        hide_from_bots: true,
        gui_options: {
            consent_modal: {
                layout: 'bar',
                position: 'top left',
                transition: 'slide'
            },
            settings_modal: {
                layout: 'bar',
                position: 'left',
                transition: 'slide'
            }
        },
        onAccept: function (cookie) {
            if (cc.allowedCategory('thirdparty')) {
                ifm.acceptService('all');
            }
        },
        onChange: function (cookie, changed_preferences) {
            if (cc.allowedCategory('thirdparty')) {
                ifm.acceptService('all');
            } else {
                ifm.rejectService('all');
            }
        },
        languages: {
            'it': {
                consent_modal: {
                    title: 'Preferenze cookie',
                    description: 'Questo sito utilizza cookie tecnici e di profilazione.<br />Cliccando su: &ldquo;ACCETTA TUTTI&rdquo; presti il consenso all&rsquo;uso di tutti i tipi di cookie; su &ldquo;RIFIUTA TUTTI&rdquo; puoi proseguire la navigazione con i soli cookie tecnici necessari al corretto funzionamento del sito.<br />Puoi gestire le preferenze, in qualsiasi momento, accedendo al pannello delle preferenze cookie, raggiungibile dal footer.<br />Per saperne di pi&ugrave; consulta la nostra <a href="/it/informativa_privacy/">informativa privacy</a>.<br /><button type="button" data-cc="c-settings" class="cc-link">Vai alle preferenze cookie</button>',
                    primary_btn: {
                        text: 'ACCETTA TUTTI',
                        role: 'accept_all'
                    },
                    secondary_btn: {
                        text: 'RIFIUTA TUTTI',
                        role: 'accept_necessary'
                    }
                },
                settings_modal: {
                    title: 'Preferenze cookie',
                    save_settings_btn: 'Salva le impostazioni',
                    accept_all_btn: 'ACCETTA TUTTI',
                    reject_all_btn: 'RIFIUTA TUTTI',
                    close_btn_label: 'Chiudi',
                    blocks: [
                        {
                            title: '',
                            // description: 'Questo sito utilizza cookie tecnici e di terze parti.<br />Puoi liberamente prestare, rifiutare o revocare il tuo consenso, in qualsiasi momento, accedendo al pannello delle preferenze cookie, raggiungibile dal footer.<br />Per saperne di più consulta le note legali.'
                            description: 'Questo sito utilizza cookie tecnici e di profilazione.<br />Cliccando su: &ldquo;ACCETTA TUTTI&rdquo; presti il consenso all&rsquo;uso di tutti i tipi di cookie; su &ldquo;RIFIUTA TUTTI&rdquo; puoi proseguire la navigazione con i soli cookie tecnici necessari al corretto funzionamento del sito.<br />Puoi gestire le preferenze, in qualsiasi momento, accedendo al pannello delle preferenze cookie, raggiungibile dal footer.<br />Per saperne di pi&ugrave; consulta la nostra <a href="/it/informativa_privacy/">informativa privacy</a>.'
                        }, {
                            title: 'Cookie tecnici necessari',
                            description: 'Cookie tecnici necessari per il funzionamento del sito',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            }
                        }, {
                            title: 'Cookie di terze parti',
                            description: 'Cookie che consentono la riproduzione dei video Youtube e delle presentazioni SlideShare o Infogram',
                            toggle: {
                                value: 'thirdparty',
                                enabled: false,
                                readonly: false
                            },
                        }
                    ]
                }
            },
            'en': {
                consent_modal: {
                    title: 'Cookie preferences',
                    description: 'This site uses technical and profiling cookies.<br />By clicking on &ldquo;ACCEPT ALL&rdquo; you give your consent to use all the cookies; by clicking on &ldquo;REJECT ALL&rdquo; you can browse this site with only technical cookies essential to its functioning.<br />At any moment you can change your personal preferences by using the cookie preference panel, accessible from the footer.<br />Learn more of the <a href="/en/informativa_privacy/">privacy policy</a>.<br /><button type="button" data-cc="c-settings" class="cc-link">Go to cookie preferences</button>',
                    primary_btn: {
                        text: 'ACCEPT ALL',
                        role: 'accept_all'
                    },
                    secondary_btn: {
                        text: 'REJECT ALL',
                        role: 'accept_necessary'
                    }
                },
                settings_modal: {
                    title: 'Cookie preferences',
                    save_settings_btn: 'Save settings',
                    accept_all_btn: 'ACCEPT ALL',
                    reject_all_btn: 'REJECT ALL',
                    close_btn_label: 'Close',
                    blocks: [
                        {
                            title: '',
                            // description: 'This site uses technical and third-party cookies.<br />You can freely give, refuse or revoke your consent, at any time, using the cookie preference panel, accessible from the footer.<br />Learn more in the legal notes.',
                            description: 'This site uses technical and profiling cookies.<br />By clicking on &ldquo;ACCEPT ALL&rdquo; you give your consent to use all the cookies; by clicking on &ldquo;REJECT ALL&rdquo; you can browse this site with only technical cookies essential to its functioning.<br />At any moment you can change your personal preferences by using the cookie preference panel, accessible from the footer.<br />Learn more of the <a href="/en/informativa_privacy/">privacy policy</a>.',
                        }, {
                            title: 'Strictly necessary cookies',
                            description: 'These cookies are essential for the proper functioning of the website. Without these cookies, the website would not work properly',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            }
                        }, {
                            title: 'Third party cookies',
                            description: 'These cookies allow the website to run Youtube videos and SlideShare or Infogram presentations',
                            toggle: {
                                value: 'thirdparty',
                                enabled: false,
                                readonly: false
                            }
                        }
                    ]
                }
            }
        }
    });
})();
