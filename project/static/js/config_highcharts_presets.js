if (document.documentElement.lang  === 'it') {
    Highcharts.setOptions({
        lang: {
            numericSymbols: [null, ' mln', ' mld'],
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });
} else {
    Highcharts.setOptions({
        lang: {
            numericSymbols: [null, ' mln', ' bln'],
            thousandsSep: ','
        }
    });
}

var config_highcharts_presets = {
    default: {
        chart: {
            backgroundColor: null,
            spacingLeft: 0,
            spacingRight: 0,
            style: {
                fontFamily: '"Lato", -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif'
            }
        },
        plotOptions: {
            series: {
                animation: false,
                events: {
                    legendItemClick: function(e) {
                        e.preventDefault();
                    }
                }
            }
        },
        legend: {
            margin: 0,
            itemStyle: {
                fontWeight: 'normal',
                cursor: 'default'
            },
            itemHoverStyle: null
        },
        xAxis: {
            tickLength: 0,
            minorGridLineWidth: 0
        },
        yAxis: {
            minorGridLineWidth: 0,
            title: {
                text: null
            }
        },
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '',
            backgroundColor: '#000000',
            borderRadius: 10,
            borderWidth: 0,
            shadow: false,
            style: {
                fontSize: '14px',
                color: '#FFFFFF'
            }
        },
        navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    },
    highcharts_treemap: {
        chart: {
            type: 'treemap',
            height: 450
        },
        plotOptions: {
            series: {
                cursor: 'pointer',
                colorByPoint: true,
                point: {
                    events: {
                        legendItemClick: function(e) {
                            location.href = $(this.name).attr('href');
                            e.preventDefault();
                        },
                        click: function(e) {
                            location.href = $(this.name).attr('href');
                        }
                    }
                },
                dataLabels: {
                    enabled: false
                },
                levels: [
                    {
                        level: 1,
                        borderColor: '#FFFFFF'
                    }
                ]
            }
        },
        colors: ['#123061', '#193E7B', '#1F4B96', '#2659B0', '#2E67CA', '#3675E5', '#3E83FF', '#609FFA', '#81B6F7', '#9FCAF6', '#BCDCF6', '#D7EBF8', '#F1F8FC'],
        series: [{
            legendType: 'point',
            showInLegend: true
        }],
        legend: {
            align: 'left',
            verticalAlign: 'top',
            margin: 20,
            padding: 0,
            itemMarginBottom: 6,
            symbolHeight: 8,
            labelFormatter: function() {
                var total = 0;
                for (var i = 0; i < this.series.data.length; i++) {
                    total += this.series.data[i].value;
                }
                return this.name + ' ' + Highcharts.numberFormat((this.value / total) * 100, 0) + '%';
            },
            itemStyle: {
                fontSize: '14px',
                textDecoration: 'none',
                cursor: 'pointer'
            },
            itemHoverStyle: {
                textDecoration: 'underline'
            }
        },
        tooltip: {
            pointFormat: '{point.name}<br/>{point.value}'
        }
    },
    highcharts_pie: {
        chart: {
            type: 'pie',
            height: 200,
            width: 200,
            events: {
                load: function() {
                    var renderTo = $(this.renderTo);
                    renderTo.dataLegend({
                        container: $('<table id="' + this.data.options.table + '_highcharts_legend" class="highcharts_legend"></table>').insertBefore(renderTo),
                        formatter: function(data) {
                            return '<tr><td><span style="background-color:' + data.color + '"></span>' + data.name + '</td><td class="amount">' + Highcharts.numberFormat(data.y, 0) + '</td></tr>';
                        }
                    });
                }
            }
        },
        plotOptions: {
            series: {
                cursor: true,
                showInLegend: true,
                dataLabels: {
                    enabled: false
                },
                point: {
                    events: {
                        legendItemClick: function(e) {
                            location.href = $(this.name).attr('href');
                            e.preventDefault();
                        },
                        click: function(e) {
                            location.href = $(this.name).attr('href');
                        }
                    }
                }
            }
        },
        colors: ['#123061', '#1F4B96', '#2E67CA', '#3E83FF', '#81B6F7', '#BCDCF6', '#F1F8FC'],
        legend: {
            enabled: false
        },
        xAxis: {
            type: 'category'
        },
        tooltip: {
            pointFormat: '{point.name}<br/>{point.percentage:.0f}%'
        }
    },
    highcharts_impegnipagamenti: {
        chart: {
            type: 'line',
            height: 300
        },
        plotOptions: {
            series: {
                marker: {
                    symbol: 'circle',
                    fillColor: '#FFFFFF',
                    lineWidth: 2,
                    lineColor: null,
                    radius: 4
                }
            }
        },
        colors: ['#2659B0', '#3E83FF'],
        legend: {
            align: 'left',
            verticalAlign: 'top',
            margin: 40,
            itemStyle: {
                fontSize: '13px',
                fontWeight: 'bold',
                textTransform: 'uppercase'
            }
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -90,
                align: 'right'
            }
        },
        tooltip: {
            shared: true,
            headerFormat: '{point.key}<br/>',
            pointFormat: '{series.name}: € {point.y}<br/>'
        }
    },
    highcharts_spesacertificata: {
        chart: {
            type: 'line',
            height: 500,
            animation: false,
            events: {
                load: function() {
                    var xAxis = this.xAxis['0'];
                    var groupedCategories = [];
                    var month;
                    var year;
                    var yearGroup;
                    var currentYear;
                    $.each(xAxis.names, function(idx, item) {
                        month = item.split(' ')[0];
                        year = item.split(' ')[1];
                        if (yearGroup !== year) {
                            yearGroup = year;
                            currentYear = {name: year, categories: []};
                            groupedCategories.push(currentYear);
                            xAxis.addPlotLine({color: '#E6E6E6', value: idx - 0.5, width: 1});
                        }
                        currentYear.categories.push(month.substring(0, 1).toLowerCase());
                    });
                    xAxis.update({categories: groupedCategories});
                }
            }
        },
        series: [
            {
                color: '#228822',
                connectNulls: true,
                marker: {
                    symbol: 'square',
                    radius: 3,
                    lineWidth: 0
                },
                lineWidth: 2
            },
            {
                color: '#C45355',
                connectNulls: true,
                marker: {
                    symbol: 'circle',
                    radius: 3,
                    lineWidth: 0
                },
                lineWidth: 2
            },
            {
                color: '#003399',
                marker: {
                    symbol: 'triangle',
                    radius: 3,
                    lineWidth: 0
                },
                lineWidth: 0
            }
        ],
        legend: {
            align: 'left',
            verticalAlign: 'top',
            layout: 'vertical',
            backgroundColor: '#FFFFFF',
            itemMarginTop: 5,
            itemMarginBottom: 5,
            floating: true,
            x: 80,
            y: 10,
            shadow: true
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: 0,
                style: {
                    fontSize: '8px'
                }
            }
        },
        yAxis: {
            floor: 0,
            ceiling: 110,
            tickInterval: 10
        },
        tooltip: {
            shared: true,
            headerFormat: '{point.key}<br/>',
            pointFormat: '{series.name}: {point.y:,.1f}%<br/>'
        }
    },
    highcharts_indicatoriaccesso: {
        chart: {
            type: 'line',
            height: 400,
            animation: false,
            events: {
                load: function() {
                    var chart = this;
                    var selector = $('<select></select>');
                    $.each(chart.series, function(idx, series) {
                        selector.append($('<option>', {
                            value: idx,
                            text: series.name
                        }));
                    });
                    selector.appendTo(
                        $('<form></form>').insertBefore(chart.container)
                    ).on('change', function() {
                        var value = parseInt($(this).val());
                        $.each(chart.series, function(idx, series) {
                            series.setVisible(idx === value, false);
                        });
                        chart.redraw();
                    }).trigger('change');
                }
            }
        },
        plotOptions: {
            series: {
                marker: {
                    symbol: 'circle',
                    radius: 3
                }
            }
        },
        colors: ['#123061'],
        legend: {
            enabled: false
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            min: 0
        },
        tooltip: {
            formatter: function() {
                var data = this.series.data;
                var decimals = 0;
                for (var i = 0; i < data.length && decimals === 0; i++) {
                    if (data[i].y % 1) {
                        decimals = 2;
                    }
                }
                return this.key + ': ' + Highcharts.numberFormat(this.y, decimals);
            }
        }
    },
    highcharts_programmi_pagamentiperanno: {
        chart: {
            type: 'line'
        },
        plotOptions: {
            series: {
                marker: {
                    symbol: 'circle',
                    fillColor: '#FFFFFF',
                    lineWidth: 2,
                    lineColor: null,
                    radius: 4
                }
            }
        },
        colors: ['#123061', '#2659B0', '#3E83FF'],
        legend: {
            align: 'left',
            verticalAlign: 'top',
            margin: 40,
            itemStyle: {
                fontSize: '13px',
                fontWeight: 'bold',
                textTransform: 'uppercase'
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            min: 0
        },
        tooltip: {
            shared: true,
            headerFormat: '{point.key}<br/>',
            pointFormat: '{series.name}: {point.y:,.2f}%<br/>'
        }
    },
    highcharts_programmi_pagamentiperprogramma: {
        chart: {
            type : 'column'
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                stacking: 'normal'
            }
        },
        series: [{
            color: '#2659B0',
            index: 2
        }, {
            color: '#3E83FF',
            index: 1
        }, {
            showInLegend: false,
            visible: false,
            index: 0
        }],
        legend: {
            reversed: true
        },
        xAxis: {
            type: 'category',
            labels: {
                formatter: function() {
                    var words = this.value.replace(/ FESR|FSE|CONV|CRO|ERDF|ESF|RCE /g, ' ').split(/[\s]+/);
                    var numWordsPerLine = 2;
                    var str = [];

                    for (var i = 0; i < words.length; i++) {
                        if (i > 0 && i % numWordsPerLine === 0)
                            str.push('<br/>');

                        str.push(words[i]);
                    }

                    return str.join(' ').replace(/ <br\/> /g, '<br/>');
                },
                rotation: -45,
                align: 'right'
            }
        },
        yAxis: {
            min: 0
        },
        tooltip: {
            formatter: function() {
                if (this.key) {
                    var series = this.series.chart.series,
                        x = this.point.x,
                        str = this.key;

                    for (var i = series.length - 1; i >= 0; i--) {
                        str += '<br/>' + series[i].name + ': € ' + Highcharts.numberFormat(series[i].data[x].y, 0)
                    }

                    return str;
                } else {
                    return false;
                }
            }
        }
    },
    highcharts_progetto_pagamenti: {
        chart: {
            height: 200
        },
        plotOptions: {
            series: {
                states: {
                    hover: {
                        enabled: false
                    }
                }
            }
        },
        data: {
            seriesMapping: [{
                x: 0,
                y: 1
            }, {
                x: 0,
                y: 2,
                v: 3
            }]
        },
        series: [{
            type: 'column',
            color: '#E6E6E6',
            tooltip: {
                pointFormat: '€ {point.y}'
            }
        }, {
            type: 'line',
            color: '#2659B0',
            tooltip: {
                pointFormat: '€ {point.y}<br/>({point.v:,.0f}%)'
            }
        }],
        legend: {
            enabled: false
        },
        xAxis: {
            type: 'category',
            lineWidth: 0,
            labels: {
                style: {
                    fontSize: '14px',
                    fontWeight: 'bold'
                }
            }
        },
        yAxis: {
            visible: false
        }
    },
    highcharts_stati_avanzamento: {
        chart: {
            type : 'bar',
            height: 130
        },
        plotOptions: {
            series: {
                stacking: 'percent',
                pointWidth: 50,
                borderWidth: 0,
                borderRadius: 0,
                dataLabels: {
                    enabled: true,
                    style: {
                        fontSize: '1.375rem',
                        color: '#FFFFFF',
                        textOutline: 0
                    },
                    formatter: function() {
                        return this.y ? this.y : null;
                    }
                }
            }
        },
        colors: ['#FF6A00', '#0066CC', '#26AB35'],
        legend: {
            useHTML:true,
            align: 'left',
            verticalAlign: 'top',
            padding: 2,
            symbolHeight: 8,
            itemStyle: {
                fontSize: '18px',
                lineHeight: '1.4'
            }
        },
        xAxis: {
            type: 'category',
            visible: false
        },
        yAxis: {
            reversed: true,
            visible: false,
            min: 0
        },
        tooltip: {
            enabled: false
        }
    }
};

config_highcharts_presets['highcharts_treemap_valutazioni'] = $.extend(true, {}, config_highcharts_presets['highcharts_treemap'], {
    legend: {
        labelFormatter: function() {
            return this.name + ' ' + Highcharts.numberFormat(this.value, 0);
        }
    },
    tooltip: {
        formatter: function() {
            return this.point.name + '<br/>' + (document.documentElement.lang  === 'it' ? 'Valutazioni' : 'Evaluations') + ': ' + this.point.value;
        }
    }
});

(function($) {
    $.fn.dataLegend = function(options) {
        return this.each(function() {
            var highcharts = $(this).highcharts();

            var data_array = highcharts.series[0].data;
//            var tooltip = highcharts.tooltip;

            var container = $(options.container);
            var formatter = options.formatter;

            container.html('');
            for (var i = 0; i < data_array.length; i++) {
                container.append(formatter(data_array[i]));
            }

            container.find('a').each(function(idx, obj) {
                var data = data_array[idx];
                $(obj).hover(
                    function() {
                        data.setState('hover');
//                        tooltip.refresh(data);
                    },
                    function() {
                        data.setState('');
//                        tooltip.hide();
                    }
                );
            });
        });
    };
}(jQuery));
