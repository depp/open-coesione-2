if (document.documentElement.lang  === 'it') {
    d3.formatDefaultLocale({
        decimal: ',',
        thousands: '.',
        grouping: [3],
        currency: ['€', '']
    });
}

function local(val) {
    return val.split('|')[document.documentElement.lang  === 'it' ? 0 : 1] || '';
}

function treemap(containerID, csvURL) {
    var container = d3.select(containerID);

    var width = container.node().getBoundingClientRect().width,
        height = width,
        minSqPixToShow = 5000;

    /* This one is used to map colors from L1 values in the .csv so modify it in case of top level domain */
    var domainL1 = ['Fondo Sviluppo e Coesione|Development and Cohesion Fund', 'Fondi Strutturali (Risorse UE)|Fondi Strutturali (EU resources)', 'Fondi Strutturali (Cofinanziamento Nazionale)|Fondi Strutturali (national co-financing)', 'PAC|PAC', 'CTE|ETC'];

    var color = d3.scale.ordinal()
        .domain(domainL1.map(local))
        .range(['#206243', '#003399', '#66A789', '#83BCA1', '#6785C2']);
        // .range(['#206243', '#509474', '#66A789', '#83BCA1', '#A1CFB9', '#BEE5D3', '#DAF9EA']);

    var treemap = d3.layout.treemap()
        .size([width, height])
        .sort(function(a, b) {
            return a.value - b.value;
        });

    var svg = container.append('svg')
        .attr('class', 'treemap')
        .attr('width', width)
        .attr('height', height)
        .append('g')
        .attr('transform', 'translate(-.5,-.5)');

    function wrap(d) {
        var text = d3.select(this),
        words = d.name.split(/\s+/).reverse(),
        word,
        line = [],
        lineHeight = 1.1,
        tspan = text.append('tspan').attr('dy', lineHeight + 'em').attr('x', d.dx / 2),
        width = d.dx - 8;

        while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(' '));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(' '));
                line = [word];
                tspan = text.append('tspan').attr('dy', lineHeight + 'em').attr('x', d.dx / 2).text(word);
            }
        }
    }

    function leafWrap(d) {
        var formatter = d3.format(',.1f'),
        text = d3.select(this),
        words = d.name.split(/\s+|\-/).reverse(),
        word,
        line = [],
        lineHeight = 1.1,
        tspan = text.append('tspan').attr('dy', 0).attr('x', d.dx / 2),
        width = d.dx - 8;

        while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(' '));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(' '));
                line = [word];
                tspan = text.append('tspan').attr('dy', lineHeight + 'em').attr('x', d.dx / 2).text(word);
            }
        }
        text.append('tspan').attr('dy', lineHeight + 'em').attr('x', d.dx / 2).text(formatter(+d.value));
    }

    function drawTreemap() {
        /* Then add the graph itself, skip level 0 to have the level 1 on top to be able to get the mouseover event on them */
        var cell = svg.selectAll('g')
            .data(treemap.nodes(json).filter(function(d) {
                return d.depth !== 0
            }))
            .enter().append('g')
            .attr('class', function(d) { /* set a class, leaf for last level, D<level> for the others */
                if (d.children) {
                    if (d.name === 'CTE' || d.name === 'ETC' || d.name === 'FEAD') {
                        return 'D1 exception'
                    } else {
                        return 'D' + d.depth
                    }
                } else {
                    return 'leaf';
                }
            })
            .attr('transform', function(d) {
                return 'translate(' + d.x + ',' + d.y + ')';
            })
            .sort(function(a, b) { /* sort to get the deepest level first so we will have the level 1 on top and be able to get onmouseover event on it */
                return b.depth - a.depth;
            });

	    /* append rect and manage fills to all the non top-level cells */
        cell.filter(function(d) {
            return d.depth > 1;
        })
	    .append('rect')
        .attr('width', function(d) {
            return d.dx - 2;
        })
        .attr('height', function(d) {
            return d.dy - 2;
        })
        .style('fill', function(d) {
            if (d.depth === 3) return color(d.parent.parent.name); /* level 3 element got a color picked from level 1 parent name */
            return 'none'; /* others are transparents */
        });

        /* Set the text in leaf: name and value */
        d3.selectAll('.leaf')
            .filter(function(d) {
                return d.dx * d.dy > minSqPixToShow;
            }) /* no text in that area when too small */
            .append('text')
            .attr('x', function(d) {
                return d.dx / 2;
            })
            .attr('y', function(d) {
                return d.dy / 2;
            })
            .attr('text-anchor', 'middle')
            .attr('fill', 'black')
            .each(leafWrap);

        /* Set the text in second level rectangle: name only, color is picked from parent name */
        d3.selectAll('.D2').append('text')
            .filter(function(d) {
                return d.dx * d.dy > minSqPixToShow;
            })
            .attr('x', function(d) {
                return d.dx / 2;
            })
            .attr('y', 3)
            .attr('dy', 0)
            .attr('text-anchor', 'middle')
            .attr('fill', function(d) {
                return color(d.parent.name);
            })
            .each(wrap);

    	/* Append a rect and a text in the the top level elements link */
        var d1Cells = d3.selectAll('.D1');

    	d1Cells.append('rect')
            .attr('width', function(d) {
                return d.dx - 2;
            })
            .attr('height', function(d) {
                return d.dy - 2;
            })
            .style('fill', function(d) {
                return color(d.name); /* level 1 element got a color picked from their name */
            });

        d1Cells.append('text')
            .attr('x', function(d) {
                return d.dx / 2;
            })
            .attr('y', function(d) {
                return d.dy / 2;
            })
            .attr('text-anchor', 'middle')
            .each(leafWrap);
    }

    var json = {name: '', children: []};

    d3.csv(csvURL, function(rows) {
        /* convert the CSV file into a json tree,
         each object has a name and children property, children is an array of object. Leaf have name and value property */
        rows.forEach(function(row) {
            var l1 = local(row['L1']),
                l2 = local(row['L2']),
                l3 = local(row['L3']),
                value = row['value'];

            var l1_node, l2_node;

            var levelFound = false;

            json.children.forEach(function(d) {
                if (d.name === l1) {
                    levelFound = true;
                    l1_node = d;
                }
            });

            if (!levelFound) {
                l1_node = {name: l1, children: []};
                json.children.push(l1_node);
            }

            levelFound = false;

            l1_node.children.forEach(function(d) {
                if (d.name === l2) {
                    levelFound = true;
                    l2_node = d;
                }
            });

            if (!levelFound) {
                l2_node = {name: l2, children: []};
                l1_node.children.push(l2_node);
            }

            l2_node.children.push({name: l3, value: value});
        });

        drawTreemap();

        /* on windows resize, we shall redraw the treemap */
        d3.select(window).on('resize', function() {
            width = container.node().getBoundingClientRect().width; /* get the new size */
            height = width;
            treemap.size([width, height]);
            d3.select('svg.treemap')
                .attr('width', width)  /* modify svg element width and height */
                .attr('height', height)
                .select('g').selectAll('g').remove(); /* delete all children node in svg/g/* */
            drawTreemap(); /* draw it */
        });
    });
}
