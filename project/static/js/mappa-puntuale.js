$(document).ready(function() {
    var map_container = d3.select('#map-container');

    OCMap.init({
        containerId: 'map-container',
        width: 555,
        height: 720,
        limitsPath: map_container.attr('data-limitspath'),
        layer: 'regioni',
        getFeatures: function(limits, layer) {
            return [
                topojson.feature(limits, limits.objects[layer]),
                topojson.feature(limits, limits.objects[layer])
            ];
        },
        minZoomScale: map_container.attr('data-minzoomscale'),
        markers: map_container.attr('data-coords') ? [map_container.attr('data-coords')] : d3.select('#location-list').selectAll('a[data-coords]').nodes().map(function(d) {
            return d3.select(d).attr('data-coords')
        })
    });

    map_container = $('#map-container');

    $('#location-list').find('a[data-coords]').each(function(idx, obj) {
        $(obj).hover(
            function() {
                map_container.find('circle').eq(idx).attr('r', 14);
            },
            function() {
                map_container.find('circle').eq(idx).attr('r', 8);
            }
        );
    });

    $('#tematizzazione').on('shown.bs.tab', 'a', function() {
        map_container.appendTo($('.map-outer:visible'));
    });
    map_container.show().appendTo($('.map-outer:visible'));
});
