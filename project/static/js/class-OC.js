OC = (function () {
    var _config = null;

    var _init = function (OcClassConfig) {
        _config = OcClassConfig;

        $('body').on('click', 'a[href="#"]', function (e) {
            e.preventDefault();
        });

        $('table.visually-hidden a').attr('tabindex', -1);

        $('a[href^="http"]')
            .attr('target', '_blank')
            .append('<span class="visually-hidden"> (' + (document.documentElement.lang  === 'it' ? 'si apre in una nuova finestra': 'opens in new window') + ')</span>')
            .each(function() {
                var rels = ($(this).attr('rel') || '').split(' ');
                if (!rels.includes('noopener')) {
                    rels.push('noopener');
                    $(this).attr('rel', rels.join(' ').trim());
                }
            });

        // $('#tools,#focus_tools').find('select').on('change', function () {
        //     window.location.href = $(this).val();
        // });

        _activateBootstrapTabs();
        _renderChartsFromTables();
        _initAutocompleters();
    };

    var _renderChartsFromTables = function () {
        var cls = 'hc-done';

        $.each(_config.highcharts_presets, function (selector, value) {
            $('table.' + selector).not('.' + cls).each(function () {
                var tableID = $(this).prop('id');
                var highchartsID = tableID + '_highcharts';

                // create a wrapper for the chart and attach it to the DOM
                $('<div></div>').attr('id', highchartsID).attr('aria-hidden', 'true')
                    .addClass('highcharts_wrapper')
                    .addClass(selector + '_wrapper')
                    .insertBefore(this);

                // render the chart
                Highcharts.chart(highchartsID, $.extend(true,
                    { data: { table: tableID } },
                    _config.highcharts_presets.default,
                    _config.highcharts_presets[selector]
                ));

                $(this).addClass(cls);
            });
        });
    };

    var _renderMapFromTables = function () {
        var map_container = $('.map-outer:visible');

        var map_layer = map_container.find('ul.map-layer a.active').data('value');
        var map_column = map_container.find('ul.map-column a.active').data('value');

        if (map_layer && map_column) {
            if (typeof renderMapFromTables === 'function') {
                renderMapFromTables(map_layer, map_column);
            }
            $('#map-container').appendTo(map_container).show();
        }
    };

    var _activateBootstrapTabs = function () {
        var prefix = '!';
        $('#tematizzazione').on('show.bs.tab', 'a', function (e) {
            window.location.hash = e.target.hash.replace('#', '#' + prefix);
            $('#tematizzazione').find('a.active').removeClass('active');
            _renderChartsFromTables();
        }).on('shown.bs.tab', 'a', function () {
            _renderMapFromTables();
        }).find('a[href="' + window.location.hash.replace(prefix, '') + '"]').tab('show');

        $('.map-selectors').find('ul').each(function () {
            var ul = $(this);

            if (ul.find('li').length < 2) {
                ul.hide();
            }

            ul.find('a').on('click', function () {
                $(this).tab('show');
            }).on('shown.bs.tab', function () {
                if (ul.closest('.map-selectors').is(':visible')) {
                    if (ul.hasClass('map-partition')) {
                        var val = $(this).data('value');
                        $('#map-container').find('.area').each(function () {
                            $(this).toggle(val === 'all' || $(this).hasClass(val));
                        });
                    } else if (ul.hasClass('map-carousel')) {
                        if (typeof renderMapCarousel === 'function') {
                            renderMapCarousel($(this).data('value'));
                        }
                    } else {
                        _renderMapFromTables();
                        $('.map-selectors').find('ul.map-partition a').removeClass('active').closest('li').first().find('a').addClass('active');
                    }
                }
            }).first().tab('show');
        });

        $('#indicatori-accesso').on('show.bs.collapse', '.collapse', function () {
            _renderChartsFromTables();
        });
    };

    var _initAutocompleters = function () {
        $.each(_config.typeahead_presets, function (selector, value) {
            var extPreset = $.extend(true, {},
                _config.typeahead_presets.default,
                _config.typeahead_presets[selector]
            );
            $('input.js-typeahead.' + selector).each(function () {
                $(this).typeahead(extPreset);
            }).prop('disabled', false);
        });
    };

    return {
        init: _init
    };
})();
