/*
    Author Guglielmo Celata <guglielmo@openpolis.it>

    Copyright 2018 Fondazione Openpolis ETS

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * OCMap singleton
 * an abstract, reusable object, that enables the user
 * to produce interactive d3-based maps (SVG), starting
 * from a topojson limits file and an indicator CSV file
 *
 * it reads:
 * - a topojson limits file, with at least 2 layers (com/prov or prov/reg),
 * - a csv file with indicator values for all areas in the layer
 *   and a shared id for the areas
 *
 * it produces a thematized choropleth map,
 * colored according to jenks classification,
 * which is draggable, zoomable, browseable (tooltips on mouse over areas)
 * and exportable as downloadable PNGs
 *
 * To use OCMap in an html page:
 *
 * - define html elements used by it in the DOM
 *     <div id="container" class="container-fluid outerdiv">
 *       <noscript>
 *           <div class="nojs">Javascript is either disabled or not supported in your browser. Please enable it or use a Javascript enabled browser.</div>
 *       </noscript>
 *
 *       <p id="loading-indicator" class="text-center lead">
 *           <i class="fa fa-spinner fa-spin fa-5x"></i><br/>Caricamento dei dati della mappa ...
 *       </p>
 *
 *       <svg></svg>
 *
 *       <!-- the positioning and zooming tools -->
 *       <div class="map-controls" role="group" style="display:none">
 *           <button class="map-control-button map-control-zoomin" title="Zoom In">
 *               <span class="glyphicon glyphicon-plus"></span>
 *           </button>
 *           <button class="map-control-button map-control-zoomout" title="Zoom Out">
 *               <span class="glyphicon glyphicon-minus"></span>
 *           </button>
 *           <button class="map-control-button map-control-zoomreset" title="Zoom Reset">
 *               <span class="glyphicon glyphicon-resize-full"></span>
 *           </button>
 *          <button class="map-control-button map-control-limitstoggle" title="Mostra/Nascondi confini">
 *               <span class="glyphicon glyphicon-record"></span>
 *           </button>
 *           <button class="map-control-button map-control-export" title="Esporta come immagine">
 *               <span class="glyphicon glyphicon-camera"></span>
 *           </button>
 *       </div>
 *
 *   </div>
 *
 * - load JS dependencies
 * <script src="https://unpkg.com/d3@4" charset="utf-8"></script>
 * <script src="https://unpkg.com/topojson-client@3"></script>
 * <script src="https://unpkg.com/d3-tip"></script>
 * <script src="/js/geostats.min.js"></script>
 * <script src="/js/canvas-toBlob.js"></script>
 * <script src="/js/FileSaver.min.js"></script>
 * <script src="/js/d3-ocmap.js"></script>
 *
 * - activate the thematizer after having defined
 *   all configuration options;
 * <script type="application/javascript">
 *       // all data and methods referencing
 *       // layers details MUST go here
 *       var OCMapConfig = {
 *       };
 *
 *       // activate the thematizer
 *       $(document).ready(function(){
 *           OCMap.init(OCMapConfig);
 *       });
 *   </script>
 */
OCMap = (function() {
    var _containerId,
        _mapWidth = 600,
        _mapHeight = 400,
        _limitsPath,
        _layer,
        _getFeatures,
        _superLimitsShown = true,
        _minZoomScale = 1,
        _maxZoomScale = 12,
        _indicatorData,
        _populateIndicatorById,
        _colorize,
        _nColorClasses = 5,
        _classificationMethod = "getClassJenks",
        _colorRange,
        _minDomain,
        _maxDomain,
        _tooltip,
        _legend,
        _activePathsFilter = false,
        _areaClassName = "area",
        _areaClickHandler,
        _markers,
        _onComplete,
        _disableDrag;

    var _init = function(config) {
        _containerId = config.containerId || _containerId;
        _mapWidth = config.width || _mapWidth;
        _mapHeight = config.height || _mapHeight;
        _limitsPath = config.limitsPath;
        _layer = config.layer;
        _getFeatures = config.getFeatures;
        _superLimitsShown = config.superLimitsShownAtStart || _superLimitsShown;
        _minZoomScale = config.minZoomScale || _minZoomScale;
        _maxZoomScale = config.maxZoomScale || _maxZoomScale;
        _indicatorData = config.indicatorData;
        _populateIndicatorById = config.populateIndicatorById;
        _colorize = config.colorize;
        _nColorClasses = config.nColorClasses || _nColorClasses;
        _classificationMethod = config.classificationMethod || _classificationMethod;
        _colorRange = config.colorRange;
        _minDomain = config.minDomain;
        _maxDomain = config.maxDomain;
        _tooltip = config.tooltip;
        _legend = config.legend;
        _activePathsFilter = config.activePathsFilter || _activePathsFilter;
        _areaClassName = config.areaClassName || _areaClassName;
        _areaClickHandler = config.areaClickHandler;
        _markers = config.markers;
        _onComplete = config.onComplete;
        _disableDrag = config.disableDrag;

        d3.queue()
            .defer(d3.json, _limitsPath)
            .await(_ready);
    };

    var _ready = function(error, limits) {
        if (error) throw error;

        function superLimitsWidth() {
            return _superLimitsShown ? 0.3 : 0
        }

        // extract base and super layer from topojson limits
        // uses _getFeatures() function, specified in config
        // that knows details about the limits layers
        var features = _getFeatures(limits, _layer);
        var superLayer = features[0],
            baseLayer = features[1];

        // function to be applied to create path
        var projection = d3.geoMercator().fitSize([_mapWidth, _mapHeight], baseLayer);
        var path = d3.geoPath(projection);

        var container = d3.select("#" + _containerId);

        var svg = container.select("svg")
            .html("")
            .attr("viewBox", "0 0 " + _mapWidth + " " + _mapHeight);

        // add map container
        var mapg = svg.append("g").attr("id", "mapg");

        // base layer (multiple paths) eg: comuni
        mapg.append("g").attr("class", "base-layer")
            .selectAll("path")
                .data(baseLayer.features)
                .enter().append("path")
                .attr("class", _areaClassName)
                .attr("data-code", function(d) {
                    return d.properties.cod;
                })
                .attr("d", path);

        // super layer (single path) eg: province
        mapg.append("path").attr("class", "super-layer")
            .datum(superLayer)
            .style("fill", "none")
            .style("stroke", "white")
            .style("stroke-width", superLimitsWidth)
            .style("stroke-linejoin", "round")
            .style("pointer-events", "none")
            .attr("d", path);

        if (_indicatorData) {
            var indicatorById = _populateIndicatorById(_indicatorData);
            var colorDomain;

            if (Object.keys(indicatorById).length) {
                // uses https://github.com/simogeo/geostats to compute Jenks classes
                var indicatorValues = Object.keys(indicatorById).map(function(key) {
                    return +indicatorById[key];
                });
                var indicatorSerie = new geostats(indicatorValues);

                _nColorClasses = Math.min(_nColorClasses, Math.max(indicatorValues.length - 1, 2));
                // _nColorClasses = Math.min(_nColorClasses, indicatorValues.length);

                // define series domain
                colorDomain = indicatorSerie[_classificationMethod](_nColorClasses);

                // fix
                colorDomain = colorDomain.filter(function(item, pos, self) {
                    return self.indexOf(item) === pos;
                });
                var minIndicatorValues = Math.min.apply(Math, indicatorValues);
                while (colorDomain[0] < minIndicatorValues) {
                    colorDomain.shift();
                }

                // _minDomain defaults to min value in series,
                // but can be overridden, setting the minDomain config value
                if (_minDomain === undefined) {
                    _minDomain = colorDomain[0];
                } else if (colorDomain[0] > _minDomain) {
                    colorDomain[0] = _minDomain;
                }

                // _maxDomain defaults to max value in series,
                // but can be overridden, setting the maxDomain config value
                if (_maxDomain === undefined) {
                    _maxDomain = colorDomain[_nColorClasses];
                } else if (colorDomain[_nColorClasses] < _maxDomain) {
                    colorDomain[_nColorClasses] = _maxDomain;
                }

                // fix
                if (colorDomain[colorDomain.length - 1] === undefined) {
                    colorDomain.pop();
                }
            } else {
                colorDomain = [];
            }

            // prepare scales and colors
            var colorRange = _colorRange || d3.schemeOrRd[colorDomain.length + 1];

            var color = d3.scaleThreshold()
                .domain(colorDomain)
                .range(colorRange);

            var paths = mapg.select(".base-layer").selectAll("path");
            if (_activePathsFilter) {
                paths = paths.filter(_activePathsFilter);
            }
            paths
                .style("fill", function(d) {
                    return _colorize(d, _layer, color, indicatorById);
                })
                .on("mouseover", function(d) {
                    tool_tip.show(d);
                })
                .on("mouseout", function(d) {
                    tool_tip.hide(d);
                })
                .on("click", _areaClickHandler);

            // setup tooltip
            // See original documentation for more details on styling:
            // http://labratrevenge.com/d3-tip/
            var tool_tip = d3.tip()
                .attr("class", "d3-tip")
                .offset([-8, 0])
                .html(function(d) {
                    return _tooltip(limits, d, _layer, indicatorById);
                });

            // add tooltip behavior
            d3.selectAll(".d3-tip").remove();
            mapg.call(tool_tip);

            // add legend
            if (typeof _legend === "function") {
                _legend(color, svg);
            }
        }

        if (_markers) {
            _markers = _markers.map(function(v) {
                return v.split(',');
            });

            mapg.selectAll("circle")
                .data(_markers).enter()
                .append("circle")
                .attr("class", "marker")
                .attr("r", 8)
                .attr("cx", function(d) {
                    return projection(d)[0];
                })
                .attr("cy", function(d) {
                    return projection(d)[1];
                });
        }

        var bbox = limits.bbox;
        var center = projection([(bbox[2] + bbox[0]) / 2, (bbox[3] + bbox[1]) / 2]);

        // add zoom interaction
        function zoomed() {
            var transform = d3.event.transform;

            mapg.select(".super-layer").attr("transform", transform);
            mapg.selectAll(".area").attr("transform", transform);
            mapg.selectAll(".marker").attr("transform", transform);
        }

        var zoom = d3.zoom()
            .scaleExtent([_minZoomScale, _maxZoomScale])
            .on("zoom", zoomed);

        var transform = d3.zoomIdentity
            .translate(_mapWidth / 2, _mapHeight / 2)
            .scale(_minZoomScale)
            .translate(-center[0], -center[1]);

        mapg.call(zoom).call(zoom.transform, transform);

        mapg.on("dblclick.zoom", null);
        if (_disableDrag) {
            mapg.on("mousedown.zoom", null);
        }

        var controls = container.select(".map-controls");

        controls.style("display", null);

        controls.select(".map-control-zoomin").on("click", function() {
            mapg.transition().duration(500).call(zoom.scaleBy, 1.618);
        });

        controls.select(".map-control-zoomout").on("click", function() {
            mapg.transition().duration(500).call(zoom.scaleBy, 0.618);
        });

        controls.select(".map-control-zoomreset").on("click", function() {
            mapg.transition().duration(500).call(zoom.transform, transform);
        });

        controls.select(".map-control-limitstoggle").on("click", function() {
            _superLimitsShown = !_superLimitsShown;
            mapg.select(".super-layer").style("stroke-width", superLimitsWidth);
        });

        controls.select(".map-control-export").on("click", function() {
            SVGExport.init(svg, 2 * _mapWidth, 2 * _mapHeight, "map.png")
        });

        if (_onComplete && typeof _onComplete === 'function') {
            _onComplete();
        }
    };

    return {
        init: _init
    };
})();

SVGExport = (function() {
    var _init = function(svg, width, height, filename) {
        _svgString2Image(_getSVGString(svg.node().cloneNode(true), width, height), width, height, save); // passes Blob and filesize String to the callback

        function save(dataBlob) {
            saveAs(dataBlob, filename); // FileSaver.js function
        }
    };

    function _getSVGString(svgNode, width, height) {
        svgNode.setAttribute('xlink', 'http://www.w3.org/1999/xlink');
        svgNode.setAttribute('width', width);
        svgNode.setAttribute('height', height);

        appendCSS(getCSSStyles2(svgNode), svgNode);

        var serializer = new XMLSerializer();

        var svgString = serializer.serializeToString(svgNode);
        svgString = svgString.replace(/(\w+)?:?xlink=/g, 'xmlns:xlink='); // Fix root xlink without namespace
        svgString = svgString.replace(/NS\d+:href/g, 'xlink:href'); // Safari NS namespace fix

        return svgString;

        function getCSSStyles2(parentElement) {
            var extractedCSSText = '';
            for (var i = 0; i < document.styleSheets.length; i++) {
                var s = document.styleSheets[i];

                try {
                    if (!s.cssRules) continue;
                } catch(e) {
                    if (e.name !== 'SecurityError') throw e; // for Firefox
                    continue;
                }

                var cssRules = s.cssRules;
                for (var r = 0; r < cssRules.length; r++) {
                    if (cssRules[r].selectorText && (cssRules[r].selectorText.indexOf('#map-container svg')) !== -1) {
                        extractedCSSText += cssRules[r].cssText.replace('#map-container svg', 'svg');
                    }
                }
            }

            return extractedCSSText;
        }

        function getCSSStyles(parentElement) {
            var i, c, r, selectorTextArr = [];

            // Add Parent element Id and Classes to the list
            selectorTextArr.push('#' + parentElement.id);
            for (c = 0; c < parentElement.classList.length; c++) {
                if (!contains('.' + parentElement.classList[c], selectorTextArr)) {
                    selectorTextArr.push('.' + parentElement.classList[c]);
                }
            }

            // Add Children element Ids and Classes to the list
            var nodes = parentElement.getElementsByTagName('*');
            for (i = 0; i < nodes.length; i++) {
                var id = nodes[i].id;
                if (!contains('#' + id, selectorTextArr)) {
                    selectorTextArr.push('#' + id);
                }

                var classes = nodes[i].classList;
                for (c = 0; c < classes.length; c++) {
                    if (!contains('.' + classes[c], selectorTextArr)) {
                        selectorTextArr.push('.' + classes[c]);
                    }
                }
            }

            // Extract CSS Rules
            var extractedCSSText = '';
            for (i = 0; i < document.styleSheets.length; i++) {
                var s = document.styleSheets[i];

                try {
                    if (!s.cssRules) continue;
                } catch(e) {
                    if (e.name !== 'SecurityError') throw e; // for Firefox
                    continue;
                }

                var cssRules = s.cssRules;
                for (r = 0; r < cssRules.length; r++) {
                    if (contains(cssRules[r].selectorText, selectorTextArr)) {
                        extractedCSSText += cssRules[r].cssText;
                    }
                }
            }

            return extractedCSSText;

            function contains(str,arr) {
                return arr.indexOf(str) !== -1;
            }
        }

        function appendCSS(cssText, element) {
            var styleElement = document.createElement('style');
            styleElement.setAttribute('type', 'text/css');
            styleElement.innerHTML = cssText;
            var refNode = element.hasChildNodes() ? element.children[0] : null;
            element.insertBefore(styleElement, refNode);
        }
    }

    function _svgString2Image(svgString, width, height, callback) {
        var svgData = 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(svgString))); // Convert SVG string to data URL

        var canvas = document.createElement('canvas');
        var context = canvas.getContext('2d');

        canvas.width = width;
        canvas.height = height;

        var image = new Image();
        image.onload = function() {
            context.clearRect (0, 0, width, height);
            context.drawImage(image, 0, 0, width, height);

            canvas.toBlob(function(blob) {
                if (callback) callback(blob);
            });
        };

        image.src = svgData;
    }

    return {
        init: _init
    };
})();
