$(document).ready(function() {
    var prefix = '!';
    var hash = document.location.hash.replace(prefix, '');

    $('#opendatatree,#item-list.faq-list .col-md-8').one('shown.bs.collapse', '.collapse', function() {
        if (hash) {
            $('html,body').scrollTop($('a[href="' + hash + '"]').offset().top);
        }
    }).on('shown.bs.collapse', '.collapse', function() {
        document.location.hash = '#' + prefix + $(this).attr('id');
    }).find(hash + '.collapse:first').collapse('show');
});
