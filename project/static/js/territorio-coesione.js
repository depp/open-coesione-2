$(document).ready(function() {
    const r = 38;
    const b = 5;

    d3.selectAll(".percentuale").each(function() {
        const data = [
            {class: "bkg", endAngle: 2 * Math.PI},
            {class: "arc", endAngle: d3.select(this).attr("data-perc") / 100 * 2 * Math.PI},
        ];

        const arcGen = d3.arc()
            .innerRadius(r - b)
            .outerRadius(r)
            .startAngle(0);

        d3.select(this).select("svg")
            .attr("width", 2 * r)
            .attr("height", 2 * r)
            .attr("viewBox", "0 0 " + 2 * r + " " + 2 * r)
            .selectAll("path")
            .data(data).enter()
            .append("path")
            .attr("d", arcGen)
            .attr("stroke-width", 0)
            .attr("class", function(d) {return d.class;})
            .attr("transform", "translate(" + r + "," + r + ")");
    });

    if ($.fn.slick) {
        $('.temi .slider').slick({
            dots: true,
            arrows: false,
            touchMove: false,
            autoplay: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            speed: 2000,
            autoplaySpeed: 4000,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
            ],
        });
    }
});
