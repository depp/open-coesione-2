$(document).ready(function() {
    $('table.display').dataTable({
        language: {
            url: (document.documentElement.lang  === 'it') ? '//cdn.datatables.net/plug-ins/1.10.15/i18n/Italian.json' : null
        },
        order: []
    });
});
