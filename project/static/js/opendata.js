$(document).ready(function() {
    $('#opendatatree').on('show.bs.collapse', '.collapse', function(e) {
        $(e.target).parent('.opendata-section').addClass('show');
    }).on('hide.bs.collapse', '.collapse', function(e) {
        $(e.target).parent('.opendata-section').removeClass('show');
    }).find('table').on('show.bs.collapse', '.collapse', function() {
        $(this).siblings().collapse('hide');
    });
});
