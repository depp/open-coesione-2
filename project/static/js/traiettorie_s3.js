$(document).ready(function() {
    function initDataTable() {
        var tbl = $('table.traiettorie_s3');

        tbl.closest('.block-chart').insertAfter('.data-notes:visible');

        tbl.not('.dataTable').filter(':visible').dataTable({
            language: {
                url: (document.documentElement.lang  === 'it') ? '//cdn.datatables.net/plug-ins/1.10.15/i18n/Italian.json' : null
            },
            scrollY: '400px',
            scrollCollapse: true,
            order: [[0, 'asc']],
            orderMulti: false,
            paging: false,
            info: false,
            columns: [
                null,
                null,
                null,
                {searchable: false},
                {searchable: false},
                {searchable: false}
            ]
        });
    }

    $('#tematizzazione').on('shown.bs.tab', 'a', function() {
        initDataTable();
    });

    initDataTable();
});
