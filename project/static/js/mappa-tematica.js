if (document.documentElement.lang  === 'it') {
    d3.formatDefaultLocale({
        decimal: ',',
        thousands: '.',
        grouping: [3],
        currency: ['€', '']
    });
}

var intword = function(number) {
    number = parseInt(number);
    if (number < 1000000) {
        return d3.format(',d')(number);
    } else if (number < 1000000000) {
        return d3.format(',.1f')(number / 1000000) + ' ' + ((document.documentElement.lang  === 'it') ? 'mln' : 'mln');
    } else {
        return d3.format(',.1f')(number / 1000000000) + ' ' + ((document.documentElement.lang  === 'it') ? 'mld' : 'bln');
    }
};

var renderMapFromTables = function(map_layer, map_column) {
    var table = d3.select('#' + map_layer.split('_')[0]);

    var columns = table.selectAll('thead th').datum(function() {
        return d3.select(this).text();
    }).data();
    columns.shift();

    var data = {};
    table.selectAll('tbody tr').each(function() {
        var row = d3.select(this).selectAll('td').datum(function() {
            var td = d3.select(this);
            var a = td.select('a');
            return a.empty() ? td.text() : {text: a.text(), href: a.attr('href')};
        }).data();

        data[row.shift()] = row;
    });

    var map_container = d3.select('#map-container');

    var colors = map_container.attr('data-colors');
    if (colors) {
        colors = colors.split(',');
    } else {
        // colors = ['#EAE7DF', '#BEE5D3', '#A1CFB9', '#83BCA1', '#66A789', '#509474', '#206243'];
        colors = ['#E5E9EC', '#BCDCF6', '#81B6F7', '#3E83FF', '#326ED8', '#2659B0', '#1C4588', '#123061'];
    }

    const nProj_col = parseInt(map_container.attr('data-nprojcol') || '6');

    OCMap.init({
        containerId: 'map-container',
        width: map_container.attr('data-width') || 555,
        height: map_container.attr('data-height') || 720,
        limitsPath: map_container.attr('data-limitspath'),
        disableDrag: !!map_container.attr('data-disabledrag'),
        layer: map_layer,
        getFeatures: function(limits, layer) {
            return [
                topojson.feature(limits, limits.objects[layer]),
                topojson.feature(limits, limits.objects[layer])
            ];
        },
        minZoomScale: map_container.attr('data-minzoomscale'),
        indicatorData: data,
        populateIndicatorById: function(indicator) {
            var indicatorById = {};
            for (k in indicator) {
                if (indicator.hasOwnProperty(k)) {
                    if ((indicator[k][nProj_col] > 0) || (indicator[k][nProj_col] === undefined)) {
                        indicatorById[k] = indicator[k][map_column];
                    }
                }
            }
            return indicatorById;
        },
        colorize: function(d, layer, color, indicatorById) {
            return color(indicatorById[d.properties['cod']] - 1) || colors[0];
        },
        nColorClasses: colors.length - 2,
        classificationMethod: map_container.attr('data-classificationmethod'),
        colorRange: colors.slice(1),
        activePathsFilter: function(d) {
            return d.properties['cod'] !== null;
        },
        tooltip: function(limits, d, layer, indicatorById) {
            var formatter = d3.format(',d');
            var values = data[d.properties['cod']];
            var str = columns[0] + ': <strong>' + (values[0].text || values[0]) + '</strong>';
            str += '<ul>';
            for (var i = 1; i < columns.length; i++) {
                str += '<li>' + columns[i] + ': <strong>' + (i === nProj_col ? '' : '€ ') + formatter(values[i]) + '</strong></li>';
            }
            str += '</ul>';
            return str;
        },
        legend: function(color, svg) {
            map_container.selectAll('ul.map-legend').remove();
            var legend = map_container.append('ul').classed('map-legend', true);
            legend.append('li').html('<span style="background-color: ' + colors[0] + '"></span>' + map_container.attr('data-nodatatext'));
            for (var i = 0; i < color.domain().length; i++) {
                legend.append('li').html('<span style="background-color: ' + color.range()[i] + '"></span>' + intword(i > 0 ? color.domain()[i - 1] + 1 : 0) + ' - ' + intword(color.domain()[i]));
            }
        },
        areaClassName: function(d) {
            var classNames = ['area'];
            if (d.properties['partizioni']) {
                classNames = classNames.concat(d.properties['partizioni']);
            }
            return classNames.join(' ').toLowerCase();
        },
        areaClickHandler: function(d) {
            var href = data[d.properties['cod']][0].href;
            if (href) {
                location.href = href;
            }
        }
    });
};
