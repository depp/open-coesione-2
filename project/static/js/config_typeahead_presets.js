var config_typeahead_presets = {
    default: {
        maxItem: false,
        highlight: false,
        mustSelectItem: true,
        cancelButton: false,
        filter: false,
        source: {
            territori: {
                minLength: 2,
                display: 'denominazione',
                href: function(item) {
                    return this.generateHref(this.node.data('territori-href'), item);
                },
                dynamic: true,
                ajax: function(query) {
                    return {
                        url: this.node.data('territori-sourceurl'),
                        path: 'territori',
                        data: {
                            q: query
                        }
                    }
                }
            }
        },
        callback: {
            onNavigateBefore: function (node, query, event) {
                if (~[38,40].indexOf(event.keyCode)) {
                    event.preventInputChange = true;
                }
            },
            onHideLayout: function (node, query) {
                node.val('');
            }
        }
    },
    typeahead_facet: {
        filter: true,
        highlight: true,
        maxItem: 20,
        minLength: 2,
        source: {
            facet: {
                name: 'name',
                display: 'name',
                href: '?{{query}}',
                data: function() {
                    return this.node.data('facet-items');
                }
            }
        },
    },
    typeahead_territori: {
    },
    typeahead_all: {
        minLength: 0,
        searchOnFocus: true,
        group: {
            template: function (item) {
                return this.node.data(item.group + '-groupname') || item.group;
            }
        },
        groupOrder: ['progetti', 'soggetti', 'contenuti', 'opendata', 'territori'],
        source: {
            progetti: {
                data: function() {
                    return [this.node.data('progetti-texts').split('|')[0]];
                },
                href: function(item) {
                    return this.generateHref(this.node.data('progetti-href'), item);
                }
            },
            soggetti: {
                data: function() {
                    return [this.node.data('soggetti-texts').split('|')[0]];
                },
                href: function(item) {
                    return this.generateHref(this.node.data('soggetti-href'), item);
                }
            },
            contenuti: {
                data: function() {
                    return [this.node.data('contenuti-texts').split('|')[0]];
                },
                href: function(item) {
                    return this.generateHref(this.node.data('contenuti-href'), item);
                }
            },
            opendata: {
                data: function() {
                    return [this.node.data('opendata-texts').split('|')[0]];
                },
                href: function(item) {
                    return this.generateHref(this.node.data('opendata-href'), item);
                }
            }
        },
        callback: {
            onLayoutBuiltBefore: function (node, query, result, resultHtmlList) {
                var scope = this;

                $.each(['progetti', 'soggetti', 'contenuti', 'opendata'], function (index, item) {
                    resultHtmlList.find('li[data-search-group="' + item + '"]').remove();

                    if (query) {
                        var href = scope.generateHref(node.data(item + '-href'), {query: query});
                        var text = node.data(item + '-texts').split('|')[1].replace('{{query}}', '"' + query + '"');

                        resultHtmlList.find('li[data-group="' + item + '"]').find('a').attr('href', href).find('span').text(text);
                    }
                });

                return resultHtmlList;
            }
        }
    }
};
