FROM python:3.7-slim

# update, upgrade and install useful tools and aliases
RUN apt-get -y update \
    && apt-get install -qy --no-install-recommends apt-utils \
    && apt-get -qy upgrade \
    && apt-get install -qy --no-install-recommends git-core tmux vim less locales\
    && apt-get install -qy --no-install-recommends gettext gcc python3-dev \
    && rm -rf /var/lib/apt/lists/*
RUN echo 'alias ll="ls -l"' >> ~/.bashrc && echo 'alias la="ls -la"' >> ~/.bashrc

# add it locale
COPY locales.txt /etc/locale.gen
RUN locale-gen

# initialize app
RUN mkdir -p /app
WORKDIR /app

# upgrade pip
RUN pip3 install --upgrade pip

# install python requirements
COPY requirements.txt /app
RUN pip3 install -r requirements.txt

# remove gcc and build dependencies to keep image small
RUN apt-get purge -y --auto-remove gcc python3-dev

# aws cli (credentials passed by env vars)
RUN pip3 install awscli==1.14.32

# copy app
COPY . /app/

RUN mkdir -p /app/public/media && rm /app/locales.txt
