#!/usr/bin/env bash

# script to manage environment variables for docker compose (dc)
# variables are containd in config/.dcenv, which should be kept secret

function dcenv_set {
    # set env variables
    eval $(cat config/.dcenv)
}
function dcenv_unset {
    # unset env variables
    eval $(cat config/.dcenv | awk -F"=" '{print $1}' | sed s/export/unset/)
}

function dcenv_show {
    # show env variables
    cat config/.dcenv | \
    awk -F"=" '{print $1}' | \
    sed 's/export[ \t]*\(.*\)/echo \1=\$\1/g' | \
    tr "" "\n" | \
    /bin/bash
}
