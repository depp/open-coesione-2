.. _rancher_deploy:

Advanced deploy (rancher-compose)
---------------------------------

.. note:: this is still experimental and rancher has some stability issues
    docker-compose over docker-machine appears to be more
    straightforward for the moment

The web service image must be built and uploaded to the gitlab registry:

.. code-block:: bash

    docker login registry.gitlab.depp.it
    docker push registry.gitlab.depp.it/depp/open-coesione-2:latest

.. note:: It seems that the gitlab repository myst have public access in order for
          rancher to be able to instantiate the container.

.. todo:: Verify this latter condition.

.. note:: awscli access must be configured with RW permissions on S3,
          in order for the nginx image to be built

.. todo:: Move the nginx image to the gitlab registry.

To deploy in the right rancher **environment**, the correct
API KEYs must be set in the rancher UI and then used in the
``rancher-compose`` invocation.

``rancher-compose`` (rc) uses the ``docker-compose.yml`` definitions
to deploy the stack to the specified environment of the
rancher-managed infrastructure.

.. code-block:: bash

    rc --url http://rancher.openpolis.it \
        --access-key $ACCESS_KEY \
        --secret-key $SECRET_KEY \
        up -d

The stack needs to be created manually, before th CI/CD integration
kicks in, so that the environment can be used.


