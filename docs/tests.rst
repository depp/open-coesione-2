.. _testing:

Testing
=======
To start tests regarding the service itself:

.. code-block:: bash

    python project/manage.py test

To start tests regarding the models (popolo instance):

.. code-block:: bash

    python project/manage.py test opencoesione


To extract a ``coverage`` report:

.. code-block:: bash

    coverage run --source=./  manage.py test opencoesione
    coverage report -m

.. note:: Testing makes no real sense within this project, at the moment.

