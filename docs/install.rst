.. _install:

Install
=========

This is how to get a new laptop run this project.

Development is performed on local workstations, without requiring
Docker.

Environment variables are read from from the ``.env`` file.
This file is not present in the repository,
and it should be generated starting from ``config/samples/.env``.
Variables in the linux environent at execution time override those
read from ``.env``.

A running postgres database is required somewhere,
as well as a running redis-server (to store sessions and cache).
Connections to these servers should be defined in the ``.env`` file, as
``DATABASE_URL`` and ``REDIS_URL`` values.

To start developing, clone this repository, then:

.. code-block:: bash

    # generate (and modify) the .env file
    cp config/samples/.env .env

    # create and activate a virtualenv
    python3 -m venv venv
    source venv/bin/activate

    # install requirements
    pip install -r requirements.txt

    # install other useful packages (docs-writing, ipython, jupyter ...)
    pip install sphinx sphinx_rtd_theme

    # create database
    createdb -Upostgres open_coesione_2

    python manage.py migrate
    python manage.py createsuperuser
    python manage.py compilemessages -l it

    # now the server can be run
    python manage.py runserver


.. _tmuxinator:

Restore development session
---------------------------

``tmuxinator`` can be used to restore a development session.
The ``.tmuxinator.yml`` file contains all information needed to start
the session.


