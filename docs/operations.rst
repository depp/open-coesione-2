.. _operations:

Operations
==========

Management tasks and other operations can be performed within the shell
of deployed web services, executing ``python manage.py ...`` within the
container, using ``rancher``:

.. code-block:: bash

    rancher env ls # list of all enviroments

    rancher config # define standard env

    rancher ps # processes in given environment
    rancher exec -it oc2/web /bin/bash # shell into the service


Operations are here described only with the ``python manage.py``, so that
they're useable in the development environment as well.


Setup
-----
Once the stack has been deployed, then the machine needs to be setup.

.. code-block:: bash

    python manage.py migrate
    python manage.py createsuperuser --username admin --email guglielmo@depp.it
    python manage.py collectstatic --noinput
    python manage.py compilemessages -l en
    python manage.py loaddata resources/fixtures/*.json -v3

Media sync
^^^^^^^^^^

Media files are stored on S3. In the setup phase, they need
to be synced in the ``public/media`` path:

.. code-block:: bash

    aws s3 sync s3://open_coesione/media/ public/media/


Data import
^^^^^^^^^^^

To launch the overall import, encompassing all other imports:

.. code-block:: bash

    python manage.py import_all -v2 \
      "https://www.dropbox.com/sh/x8oj0b62qemmzer/AAAO6fz2lCek7FTMINgh9PVka?dl=1"

Data need to be re-indexed:

.. code-block:: bash

    python manage.py rebuild_index -v2 --noinput --batch-size=500


To launch a single import:

.. code-block:: bash

    python manage.py csvimport \
      --csv-path=resources/data/2017-10-31 \
      --import-type=progetti -v2


Web application modifications
-----------------------------

Whenever the web application source code changes, the docker image needs to
be rebuilt, then the stack re-deployed on the remote server.

Using ``docker-machine``, this amounts to:

.. code-block:: bash

    eval $(docker-machine env oc2-staging)
    docker build --compress -t registry.gitlab.depp.it/depp/open-coesione-2:latest .
    dc up -d web

.. note:: This will *disrupt* all management tasks that are running
    (import, re-indexing, ...).

.. todo:: move bash and python shell_plus histories' files to locations within
    shared volumes (/app/data), so that they won't be removed at each upgrade.



Solr schema modification
------------------------

If a solr field is changed, added or removed, then the schema needs
to be changed, and eventually the data need to be re-indexed in the running
Solr index.

In development a different version is used (Solr 3), while the latest release
(Solr 7) is used in production.

Development
^^^^^^^^^^^

Verify that the symbolic link in ``project/templates/search_configuration/schema.xml``
points to the correct version of the schema template (``schema3.xml``).

Launch the ``build_solr_schema`` management task:

.. code-block:: bash

    python manage rebuild_solr_schema > config/solr/schema.xml


Production
^^^^^^^^^^

Verify that the symbolic link in ``templates/search_configuration`` points to ``solr7.xml``, then build the schema
and put it under the ``compose/solr/config`` directory.

.. code-block:: bash

    python manage rebuild_solr_schema > compose/solr/config/schema.xml

Copy the schema in the correct position within the running container:

.. code-block:: bash

    docker cp compose/solr/schema.xml\
        open-coesione-2_solr_1:/opt/solr/server/solr/mycores/oc2/conf/managed-schema

Restart solr:

.. code-block:: bash

    dc restart solr

.. note:: docker-machine will take care of copying the file on the
    remote server as needed, provided that the client has been correctly
    configured to work with the remote server.


Territories merge
-----------------

Given two territories that have been merged into a new one, the following snippet of code
allows to create the new entity, out of the old ones.

The populations values are the sum of old entities populations. The boundaries are built by the union
of the two entities boundaries.

This code should be run int the ``shell_plus`` environment.

.. code-block:: bash

    from django.contrib.gis import geos

    t_olds = Territorio.objects.filter(
        denominazione__in=(
            'Montoro Inferiore',
            'Montoro Superiore',
        )
    )

    t_new = Territorio.objects.create(
        denominazione='Montoro',
        cod_reg=t_olds[0].cod_reg,
        cod_prov=t_olds[0].cod_prov,
        cod_com=NEW_COD_COM,
        territorio='C',
        popolazione_totale=sum(x.popolazione_totale for x in t_olds),
        popolazione_femminile=sum(x.popolazione_femminile for x in t_olds),
        popolazione_maschile=sum(x.popolazione_maschile for x in t_olds),
        geom=geos.MultiPolygon(t_olds.unionagg())
    )

    # update projects localizzazione, from old to new
    for t_old in t_olds:
        t_old.localizzazione_set.all().update(territorio=t_new)
    
    # verify that the number is not null
    t_new.localizzazione_set.count()

    t_olds.delete()


Old entities are removed after the merge operation.
