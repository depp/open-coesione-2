.. _deploy:

Deploy
========

Intro
-----
The application stack is vertically self-contained on a single host.
It is assumed that no need to clusterize the application will arise.

Database, Redis and Solr are seen by the web app as remote services,
and they could ideally be configured to run on different hosts,
provided that network is set all right.

The stack is defined in ``docker-compose.yml`` file.
It uses:

  - a django app (web)
  - a frontend web server (nginx)
  - a storage RDBMS (postgres)
  - a cache system, at least for sessions (redis)


.. _local_deploy:

Local docker-compose deploy
---------------------------
``docker-compose`` can be used to test the stack locally.

Environment variables need to be defined in ``config/.dcenv``,
before working with docker compose.

.. code-block:: bash

    eval $(cat config/.dcenv)

You can use the shortcut ``dct``, if the ``config/dcenv_functions``
file has been sourced.

The web image must be built:

.. code-block:: bash

    docker build -t registry.gitlab.depp.it/depp/open-coesione-2:latest .

To start all relevant containers (linked container will be started as well):

.. code-block:: bash

    dc up -d web nginx

    dc run --rm tasks python manage.py [TASK]
    ...

dc stands for ``docker-compose`` here, a proper installation should
automatically provide you the shortcut.


To restart all services, after a change in ``docker-compose.yml``:

.. code-block:: bash

    dc up -d

**IMPORTANT**: Images need to be rebuilt whenever there are changes
in the source code.

.. code-block:: bash

    dc up --build

To enter a shell in the container of the web service:

.. code-block:: bash

    dc exec web /bin/bash

This can be done also to enter the other containers, but it shouldn't
be necessary.

To stop all services and remove all containers:

.. code-block:: bash

    dc down

Postgresql data are **persisted** under a docker ``volume``, and
**won't be lost** after a shut down of all services.

Note: On OSX the volumes are under the inner hosting machine.
Volumes can be inspected with the ``docker volume`` command.

See [Docker Compose Documentation](https://docs.docker.com/compose/),
to see other commands.

The source of inspiration for *dockerizing* this django app was:
https://www.capside.com/labs/deploying-full-django-stack-with-docker-compose/

.. _remote_deploy:

Remote deploy with docker-machine
---------------------------------

The stack can be deployed on a remotely created machine,
using ``docker-machine`` and ``docker-compose``.

The web application docker image must be first pushed to the gitlab registry:

.. code-block:: bash

    docker login registry.gitlab.depp.it
    docker push registry.gitlab.depp.it/depp/open-coesione-2:latest

The remote machine must then be created and provisioned:

.. code-block:: bash

    # set environment variable
    export TOKEN=[DIGITAL_OCEAN_TOKEN]

    # generazione di una docker-machine su digital ocean
    docker-machine create --driver digitalocean \
      --digitalocean-access-token $TOKEN \
      --digitalocean-image ubuntu-16-04-x64 \
      --digitalocean-region fra1 \
      --digitalocean-size 16gb \
      --digitalocean-ssh-key-fingerprint 99:c6:71:66:39:df:bc:15:ed:2e:7c:61:96:28:ca:ec \
    oc2-staging

A connection to the remote machine must be made, so that
``docker-machine`` will operate on that server, instead of locally:

.. code-block:: bash

    docker env oc2-staging
    eval $(docker env oc2-staging)

Finally, the stack can be created, after having set all environment variables
required by ``docker-compose``.

.. code-block:: bash

    export POSTGRES_DB=[DB_NAME]
    export POSTGRES_USER=[DB_USER]
    export POSTGRES_PASS=[DB_PASSWORD]
    export DATA_AGGIORNAMENTO=[YYYMMDD]
    export ALLOWED_HOSTS=staging.opencoesione.it,www.opencoesione.it
    export AWS_ACCESS_KEY_ID=[AWS_ACCESS]
    export AWS_SECRET_ACCESS_KEY=[AWS_SECRET]
    dc up -d


CI/CD Integration with gitlab
------------------------------
To setup the continuous integration using ``gitlab``,
``docker-compose`` and ``docker-machine``, make sure you
have shared runner available on your gitlab environment.

Then use https://github.com/XIThing/generate-docker-client-certs/
to generate the client certificates, needed by the runner to connect
to the docker-machine pointed server:

.. code-block:: bash

    ./generate-client-certs.sh \
        ~/.docker/machine/certs/ca.pem \
        ~/.docker/machine/certs/ca-key.pem

This will produce the following files, in the directory where the
command has been launched:

- ``client-cert.pem``
- ``client-key.pem``

Copy the content of ``~/.docker/machine/certs/ca.pem`` to a ``CA``
secret variable in the gitlab project.

Copy the content of ``client-cert.pem`` to a secret variable ``CLIENT_CERT``.

Copy the content of ``client-key.pem`` to a secret variable ``CLIENT_KEY``.

You can now add the following section to your ``.gitlab-ci.yml`` file:

.. code-block:: yaml

    deploy on staging:
      stage: deploy
      script:
        - mkdir $DOCKER_CERT_PATH
        - echo "$CA" > $DOCKER_CERT_PATH/ca.pem
        - echo "$CLIENT_CERT" > $DOCKER_CERT_PATH/cert.pem
        - echo "$CLIENT_KEY" > $DOCKER_CERT_PATH/key.pem
        - docker-compose build
        - docker-compose down
        - docker-compose up -d --force-recreate
        - rm -rf $DOCKER_CERT_PATH
      only:
        - master
      variables:
        DOCKER_TLS_VERIFY: "1"
        DOCKER_HOST: "tcp://staging.opencoesione.it:2376"
        DOCKER_CERT_PATH: "certs"
      tags:
        - docker

.. note:: be sure that the following definitions are at the top of
    ``.gitlab-ci.yml``:

    .. code-block:: yaml

        image: docker:latest

        services:
          - docker:dind

.. note:: The inspiration for docker-machine-based ci on gitlab
   was https://medium.com/@Empanado/simple-continuous-deployment-with-docker-compose-docker-machine-and-gitlab-ci-9047765322e1

When a push to the gitlab.depp.it repository is performed:

- the ``web`` service image is built from web/Dockerfile,
- the image is pushed to gitlab.depp.it's own docker registry
- a test is performed on the runner
- the stack is updated on the remote server


solr deployment
---------------

Solr is built using the `Dockerfile` in `compose/solr/`.
The image is built, then configuration files are copied in the right spot and
a core is started automatically on container launch.

solrconfig.xml modification
---------------------------

The ``solrconfig.xml`` file needs to be changed (if not already changed),
since it contains a modified ``q.op`` default value (AND instead of OR).

The modification is the following:

.. code-block:: xml

  ...

  <requestHandler name="/select" class="solr.SearchHandler">
    <!-- default values for query parameters can be specified, these
         will be overridden by parameters in the request
      -->
    <lst name="defaults">
      <str name="echoParams">explicit</str>
      <int name="rows">10</int>
      <str name="q.op">AND</str>
      ...
    </lst>
    ...
  </requestHandler>

  ...

